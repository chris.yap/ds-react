declare module 'shaper-react' {
  export function Breakpoints<T>(component: T): T;
  export function WhichBrowser<T>(component: T): T;

  interface BaseShaperProps {
    id?: string;
    className?: string;
  }

  interface ContainerProps extends BaseShaperProps {
    fluid?: boolean;
    display?: string;
    alignItems?: string;
    height?: string;
    px?: number;
    mb?: number;
    pt?: number;
  }
  export function Container(
    props: import('react').PropsWithChildren<ContainerProps>
  ): import('react').ReactElement;

  interface ColumnsProps extends BaseShaperProps {
    desktop?: boolean;
    mobile?: boolean;
    multiline?: boolean;
    gapless?: boolean;
  }
  export function Columns(
    props: import('react').PropsWithChildren<ColumnsProps>
  ): import('react').ReactElement;

  interface ColumnProps extends BaseShaperProps {
    tablet?: number;
    desktop?: number;
    mobile?: number;
    widescreen?: number;
    fullhd?: number;
    tabletOffset?: number;
    desktopOffset?: number;
    widescreenOffset?: number;
    fullhdOffset?: number;
  }
  export function Column(
    props: import('react').PropsWithChildren<ColumnProps>
  ): import('react').ReactElement;

  interface DividerProps extends BaseShaperProps {
    my?: number;
    mb?: number;
  }
  export function Divider(
    props: import('react').PropsWithChildren<DividerProps>
  ): import('react').ReactElement;

  interface HeroProps extends BaseShaperProps {
    header?: any;
    primary?: boolean;
    secondary?: boolean;
    success?: boolean;
    info?: boolean;
    warning?: boolean;
    danger?: boolean;
    light?: boolean;
    dark?: boolean;
    gradient?: boolean;
    fullHeight?: boolean;
    medium?: boolean;
    large?: boolean;
    theme?: object;
    backgroundImage?: string;
    backgroundSize?: string;
    p?: number;
    bg?: string;
    py?: number;
    pt?: number;
    mb?: number;
    height?: string;
  }
  export function Hero(
    props: import('react').PropsWithChildren<HeroProps>
  ): import('react').ReactElement;
  export interface BreakpointsProps {
    activeBreakpoints?: {
      below: {
        lg: boolean;
        md: boolean;
        sm: boolean;
        xl: boolean;
        xs: boolean;
        xxl: boolean;
      };
      above: {
        lg: boolean;
        md: boolean;
        sm: boolean;
        xl: boolean;
        xs: boolean;
        xxl: boolean;
      };
    };
  }

  interface ButtonProps extends BaseShaperProps {
    success?: boolean;
    danger?: boolean;
    onClick?: () => void;
    type?: string;
    disabled?: boolean;
    loading?: boolean;
    primary?: boolean;
    secondary?: boolean;
    inverted?: boolean;
    medium?: boolean;
    block?: boolean;
    mt?: number;
    ml?: number;
    mr?: number;
    round?: boolean;
    square?: boolean;
    flat?: boolean;
  }
  export function Button(
    props: import('react').PropsWithChildren<ButtonProps>
  ): import('react').ReactElement;

  interface ModalProps extends BaseShaperProps {
    isOpened?: boolean;
    closeIcon?: boolean;
    fullscreen?: boolean;
    bodyClassName?: string;
    header?: import('react').ReactElement | string;
    footer?: import('react').ReactElement | string;
  }
  export function Modal(
    props: import('react').PropsWithChildren<ModalProps>
  ): import('react').ReactElement;

  interface CollapseProps {
    isOpened?: boolean;
  }
  export function Collapse(
    props: import('react').PropsWithChildren<CollapseProps>
  ): import('react').ReactElement;

  interface AlertProps extends BaseShaperProps {
    isOpened?: boolean;
    danger?: boolean;
    info?: boolean;
    warning?: boolean;
    inverted?: boolean;
    moreInfo?: import('react').ReactElement | string | null;
    mx?: number;
  }
  export function Alert(
    props: import('react').PropsWithChildren<AlertProps>
  ): import('react').ReactElement;

  interface TextfieldProps extends BaseShaperProps {
    required?: boolean;
    mask?: string;
    label?: string;
    value?: string;
    onChange: (
      event: import('react').React.ChangeEvent<HTMLInputElement>
    ) => void;
    error?: boolean;
    prependIcon?: string;
    type?: string;
    maxLength?: number;
    placeholder?: string;
    errorMessage?: string | import('react').ReactElement[];
    autoComplete?: string;
  }
  export function Textfield(
    props: import('react').PropsWithChildren<TextfieldProps>
  ): import('react').ReactElement;

  interface BoxProps extends BaseShaperProps {
    bg?: string;
    display?: string;
    justifyContent?: string;
    boxShadow?: string;
    height?: string;
    minHeight?: string;
    alignItems?: string;
    p?: number;
    py?: number;
    px?: number;
    pb?: number;
    mx?: string;
    mb?: number;
    borderLeft?: string;
    color?: string;
    onClick?: () => void;
    position?: string;
    top?: string;
    width?: string;
    backgroundColor?: string;
    backgroundRepeat?: string;
    backgroundPosition?: string;
    backgroundImage?: string;
    backgroundSize?: string;
  }
  export function Box(
    props: import('react').PropsWithChildren<BoxProps>
  ): import('react').ReactElement;

  interface FooterProps extends BaseShaperProps {
    fixed?: boolean;
    p?: number;
    zIndex?: number;
    borderTop?: string;
    textAlign?: string;
    bg?: string;
  }
  export function Footer(
    props: import('react').PropsWithChildren<FooterProps>
  ): import('react').ReactElement;

  interface IconProps extends BaseShaperProps {
    small?: boolean;
    medium?: boolean;
    danger?: boolean;
    white?: boolean;
    success?: boolean;
    onClick?: () => void;
  }
  export function Icon(
    props: import('react').PropsWithChildren<IconProps>
  ): import('react').ReactElement;

  interface TitleProps extends BaseShaperProps {
    size?: number;
  }
  export function Title(
    props: import('react').PropsWithChildren<TitleProps>
  ): import('react').ReactElement;

  interface CarouselProps extends BaseShaperProps {
    arrows?: boolean;
    dots?: boolean;
    infinite?: boolean;
  }
  export function Carousel(
    props: import('react').PropsWithChildren<CarouselProps>
  ): import('react').ReactElement;

  interface LevelProps extends BaseShaperProps {
    mobile?: boolean;
  }
  export function Level(
    props: import('react').PropsWithChildren<LevelProps>
  ): import('react').ReactElement;

  interface LevelItemProps extends BaseShaperProps {
    left?: boolean;
    right?: boolean;
  }
  export function LevelItem(
    props: import('react').PropsWithChildren<LevelItemProps>
  ): import('react').ReactElement;

  interface TabProps extends BaseShaperProps {
    label?: string;
    tabClick?: () => void;
    button?: boolean;
  }
  export function Tab(
    props: import('react').PropsWithChildren<TabProps>
  ): import('react').ReactElement;

  interface TabsProps extends BaseShaperProps {
    centered?: boolean;
    rounded?: boolean;
    toggle?: boolean;
  }
  export function Tabs(
    props: import('react').PropsWithChildren<TabsProps>
  ): import('react').ReactElement;

  interface CardProps extends BaseShaperProps {
    onClick?: () => void;
  }
  export function Card(
    props: import('react').PropsWithChildren<CardProps>
  ): import('react').ReactElement;

  interface CardHeaderProps extends BaseShaperProps {
    onClick?: null | (() => void);
  }
  export function CardHeader(
    props: import('react').PropsWithChildren<CardHeaderProps>
  ): import('react').ReactElement;

  interface TableHeader extends BaseShaperProps {
    name: string | import('react').ReactElement;
    value?: string;
    align?: string;
  }
  interface TableProps<T> extends BaseShaperProps {
    striped?: boolean;
    stickyHeaderCount?: boolean;
    headers: TableHeader[];
    data: Array<T>;
    tableRow: (row: any, index?: number) => import('react').ReactElement;
    defaultSortCol?: string;
    hasStripes?: boolean;
    stickyHeader?: boolean;
    mb?: number;
  }
  export function Table<T>(
    props: import('react').PropsWithChildren<TableProps<T>>
  ): import('react').ReactElement;

  interface TableRowProps extends BaseShaperProps {}
  export function TableRow(
    props: import('react').PropsWithChildren<TableRowProps>
  ): import('react').ReactElement;

  interface TableCellProps extends BaseShaperProps {
    right?: boolean;
    fontWeight?: string;
  }
  export function TableCell(
    props: import('react').PropsWithChildren<TableCellProps>
  ): import('react').ReactElement;

  interface CardContentProps extends BaseShaperProps {}
  export function CardContent(
    props: import('react').PropsWithChildren<CardContentProps>
  ): import('react').ReactElement;

  export interface SelectOption<T> {
    label: string | import('react').ReactElement;
    value: T;
    name?: string;
    open?: boolean;
  }
  interface SelectProps<T> extends BaseShaperProps {
    options?: SelectOption<T>[];
    defaultValue?: SelectOption<T>;
    onChange?: (item: SelectOption<T>) => void;
    isSearchable?: boolean;
    mx?: number;
    mb?: number;
  }
  export function Select(
    props: import('react').PropsWithChildren<SelectProps>
  ): import('react').ReactElement;

  interface PaginationProps extends BaseShaperProps {
    current?: number;
    total?: number;
    onChange?: (index: number) => void;
  }
  export function Pagination(
    props: import('react').PropsWithChildren<PaginationProps>
  ): import('react').ReactElement;

  interface TagProps extends BaseShaperProps {
    small?: boolean;
    mt?: number;
    ml?: number;
    warning?: boolean;
    success?: boolean;
    danger?: boolean;
  }
  export function Tag(
    props: import('react').PropsWithChildren<TagProps>
  ): import('react').ReactElement;

  interface SectionProps extends BaseShaperProps {
    py?: number;
    px?: number;
    pt?: number;
  }
  export function Section(
    props: import('react').PropsWithChildren<SectionProps>
  ): import('react').ReactElement;

  interface LabelProps extends BaseShaperProps {}
  export function Label(
    props: import('react').PropsWithChildren<LabelProps>
  ): import('react').ReactElement;

  interface ListProps extends BaseShaperProps {
    bordered?: boolean;
  }
  export function List(
    props: import('react').PropsWithChildren<ListProps>
  ): import('react').ReactElement;

  interface ListItemProps extends BaseShaperProps {
    onClick: () => void;
  }
  export function ListItem(
    props: import('react').PropsWithChildren<ListItemProps>
  ): import('react').ReactElement;

  interface SubtitleProps extends BaseShaperProps {
    size?: number;
  }
  export function Subtitle(
    props: import('react').PropsWithChildren<SubtitleProps>
  ): import('react').ReactElement;

  interface ToolbarProps extends BaseShaperProps {
    level?: string;
    fixed?: boolean;
  }
  export function Toolbar(
    props: import('react').PropsWithChildren<ToolbarProps>
  ): import('react').ReactElement;

  interface ToolbarNavProps extends BaseShaperProps {
    left?: boolean;
  }
  export function ToolbarNav(
    props: import('react').PropsWithChildren<ToolbarNavProps>
  ): import('react').ReactElement;

  interface ToolbarNavItemProps extends BaseShaperProps {
    to?: string;
  }
  export function ToolbarNavItem(
    props: import('react').PropsWithChildren<ToolbarNavItemProps>
  ): import('react').ReactElement;

  interface BreadcrumbItemProps extends BaseShaperProps {
    href: string;
    active: boolean;
  }
  export function BreadcrumbItem(
    props: import('react').PropsWithChildren<BreadcrumbItemProps>
  ): import('react').ReactElement;

  interface BreadcrumbProps extends BaseShaperProps {
    alignSelf?: string;
  }
  export function Breadcrumb(
    props: import('react').PropsWithChildren<BreadcrumbProps>
  ): import('react').ReactElement;
}
