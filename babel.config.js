module.exports = function(api) {
  api.cache(true);

  const presets = ['@babel/preset-env', '@babel/preset-react'];
  const plugins = ['@babel/plugin-proposal-class-properties'];
  const sourceMaps = true;

  return {
    minified: true,
    presets,
    plugins,
    sourceMaps
  };
};
