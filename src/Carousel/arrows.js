'use strict';

import React from 'react';
import classnames from 'classnames';
import { canGoNext } from './utils/innerSliderUtils';
import Button from '../Button';
import Icon from '../Icon/';

export class PrevArrow extends React.PureComponent {
  clickHandler(options, e) {
    if (e) {
      e.preventDefault();
    }
    this.props.clickHandler(options, e);
  }
  render() {
    let prevClasses = {
      // "nab-button is-square is-small": true,
      'nab-carousel-prev ml-0 mr-3': true
    };
    let prevHandler = this.clickHandler.bind(this, { message: 'previous' });
    let prevDisabled;

    if (
      !this.props.infinite &&
      (this.props.currentSlide === 0 ||
        this.props.slideCount <= this.props.slidesToShow)
    ) {
      prevClasses['is-disabled'] = true;
      prevHandler = null;
      prevDisabled = true;
    }

    let prevArrowProps = {
      key: '0',
      'data-role': 'none',
      className: classnames(prevClasses),
      // style: { display: "block" },
      onClick: prevHandler,
      disabled: prevDisabled
    };
    let customProps = {
      currentSlide: this.props.currentSlide,
      slideCount: this.props.slideCount
    };
    let prevArrow;

    if (this.props.prevArrow) {
      prevArrow = React.cloneElement(this.props.prevArrow, {
        ...prevArrowProps,
        ...customProps
      });
    } else {
      prevArrow = (
        <Button square key="0" {...prevArrowProps}>
          <Icon primary>arrow-left</Icon>
        </Button>
      );
    }

    return prevArrow;
  }
}

export class NextArrow extends React.PureComponent {
  clickHandler(options, e) {
    if (e) {
      e.preventDefault();
    }
    this.props.clickHandler(options, e);
  }
  render() {
    let nextClasses = {
      // "nab-button nab-arrow": true,
      'nab-carousel-next ml-3 mr-0': true
    };
    let nextHandler = this.clickHandler.bind(this, { message: 'next' });
    let nextDisabled;

    if (!canGoNext(this.props)) {
      nextClasses['is-disabled'] = true;
      nextHandler = null;
      nextDisabled = true;
    }

    let nextArrowProps = {
      key: '1',
      'data-role': 'none',
      className: classnames(nextClasses),
      // style: { display: "block" },
      onClick: nextHandler
    };
    let customProps = {
      currentSlide: this.props.currentSlide,
      slideCount: this.props.slideCount
    };
    let nextArrow;

    if (this.props.nextArrow) {
      nextArrow = React.cloneElement(this.props.nextArrow, {
        ...nextArrowProps,
        ...customProps
      });
    } else {
      nextArrow = (
        <Button square key="1" disabled={nextDisabled} {...nextArrowProps}>
          <Icon primary>arrow-right</Icon>
        </Button>
      );
    }

    return nextArrow;
  }
}
