import React from 'react';
import Slider from './';

const Carousel = ({ innerRef, ...props }) => {
  return <Slider ref={innerRef} {...props} />;
};

export default Carousel;
