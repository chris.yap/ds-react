import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import { Column, Columns, Icon, ResponsiveTable } from './index';

class Table extends React.Component {
	state = Table.getInitialStateFromProps(this.props);

	static getInitialStateFromProps(props) {
		if (props.defaultSortColIndex !== undefined) {
			const header = props.headers[props.defaultSortColIndex];
			if (header && header.sort) {
				const sortDirection = props.defaultSortDirection ? props.defaultSortDirection : 1;
				return {
					rows: header.sort(props.rows, sortDirection === 1),
					sortColIndex: props.defaultSortColIndex,
					sortDirection: sortDirection,
				};
			}
		}
		return {
			rows: props.rows,
			sortColIndex: -1,
			sortDirection: 1, // 1 -> ascending, -1 -> descending
		};
	}

	componentDidUpdate(prevProps) {
		if (this.props !== prevProps) {
			state = Table.getInitialStateFromProps(this.props);
		}
	}

	doSort = (sortFn, colIndex) => {
		if (sortFn) {
			this.setState(prevState => {
				const direction = prevState.sortColIndex === colIndex ? prevState.sortDirection * -1 : 1;
				return {
					rows: sortFn(this.state.rows, direction === 1),
					sortColIndex: colIndex,
					sortDirection: direction,
				};
			});
		}
	};

	doSortWrapper = (sortFn, colIndex) => {
		if (this.props.onSortFn) {
			this.props.onSortFn(sortFn, colIndex);
		} else {
			this.doSort(sortFn, colIndex);
		}
	};

	render() {
		const {
			striped,
			hoverable,
			narrow,
			bordered,
			caption,
			headers,
			footers,
			rowRenderer,
			id,
			className,
		} = this.props;
		const { rows, sortColIndex, sortDirection } = this.state;

		return (
			<ResponsiveTable
				id={id}
				striped={striped}
				hoverable={hoverable}
				narrow={narrow}
				bordered={bordered}
				className={className}
			>
				{caption && <caption>{caption}</caption>}
				{headers && (
					<thead>
						<tr>
							{headers.map((header, index) => (
								<th
									key={index}
									className={cn(
										header.align ? 'has-text-' + header.align : '',
										header.className,
										header.sort ? 'has-pointer' : ''
									)}
									onClick={() => (header.sort ? this.doSortWrapper(header.sort, index) : null)}
									role={header.sort ? 'button' : ''}
								>
									{!header.sort ? (
										<Columns gapless mobile>
											{header.align === 'right' && <Column />}
											<Column narrow>
												<Icon
													small
													className={cn(
														index === sortColIndex
															? 'has-text-primary'
															: 'has-text-black text--lighten-4'
													)}
												>
													{index === sortColIndex && sortDirection === 1
														? 'sort-asc'
														: index === sortColIndex && sortDirection !== 1
														? 'sort-desc'
														: 'sort'}
												</Icon>
											</Column>
											<Column narrow>{header.value}</Column>
										</Columns>
									) : (
										header.value
									)}
								</th>
							))}
						</tr>
					</thead>
				)}
				{rows && <tbody>{rows.map((row, index) => rowRenderer(row, index))}</tbody>}
				{footers && (
					<tfoot>
						<tr>
							{footers.map((footer, index) => (
								<th key={index} className={footer.className}>
									{footer.value}
								</th>
							))}
						</tr>
					</tfoot>
				)}
			</ResponsiveTable>
		);
	}
}

Table.propTypes = {
	caption: PropTypes.string,
	headers: PropTypes.arrayOf(
		PropTypes.shape({
			value: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
			className: PropTypes.string,
			sort: PropTypes.func,
		})
	).isRequired,
	defaultSortColIndex: PropTypes.number,
	defaultSortDirection: PropTypes.number,
	rows: PropTypes.array.isRequired,
	rowRenderer: PropTypes.func.isRequired,
	footers: PropTypes.arrayOf(
		PropTypes.shape({
			value: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
			className: PropTypes.string,
		})
	),
	id: PropTypes.string,
	onSortFn: PropTypes.func,
};

export default Table;
