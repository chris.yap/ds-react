import React from 'react';
import cn from 'classnames';

const LevelItem = ({ left, right, narrow, className, ...props }) => {
  return (
    <div
      className={cn(
        left ? 'nab-level-left' : right ? 'nab-level-right' : 'nab-level-item',
        narrow ? 'is-narrow' : '',
        className
      )}
      {...props}
    />
  );
};

export default LevelItem;
