import styled from 'styled-components';
import { composed } from '../Theme/Composed';

const Div = styled.div`
  ${composed}
`;

export default Div;
