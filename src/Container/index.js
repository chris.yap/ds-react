import styled from 'styled-components';
import { composed } from '../Theme/Composed';
import { breakpoints, mediaQueries } from '../Theme/breakpoints';

const gap = 32;
const offset = gap * 2;

const Container = styled.div`
  flex-grow: 1;
  margin: 0 auto;
  position: relative;
  width: auto;

  max-width: ${props =>
    props.widescreen
      ? `calc(${breakpoints[3]} - ${offset}px)`
      : props.fullhd
      ? `calc(${breakpoints[4]} - ${offset}px)`
      : props.fluid && 'none'};
  padding-left: ${props => props.fluid && gap + 'px'};
  padding-right: ${props => props.fluid && gap + 'px'};
  width: ${props => props.fluid && '100%'};

  ${mediaQueries[2]} {
    max-width: ${props =>
      !props.fluid &&
      !props.widescreen &&
      !props.fullhd &&
      `calc(${breakpoints[2]} - ${offset}px)`};
  }
  ${mediaQueries[3]} {
    max-width: ${props =>
      !props.fluid &&
      !props.widescreen &&
      !props.fullhd &&
      `calc(${breakpoints[3]} - ${offset}px)`};
  }
  ${mediaQueries[4]} {
    max-width: ${props =>
      !props.fluid &&
      !props.widescreen &&
      !props.fullhd &&
      `calc(${breakpoints[4]} - ${offset}px)`};
  }

  ${composed}
`;

// const Container = ({ fluid, widescreen, fullhd, className, ...props }) => {
//   return (
//     <div
//       className={cn(
//         'nab-container',
//         fluid ? 'is-fluid' : '',
//         widescreen ? 'is-widescreen' : '',
//         fullhd ? 'is-fullhd' : '',
//         className
//       )}
//       {...props}
//     />
//   );
// };

export default Container;
