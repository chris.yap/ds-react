import React from 'react';
import Collapse from '../Collapse';
import styled from 'styled-components';
import { rgba } from 'polished';
import theme from '../Theme/';
import { flexbox, position, space } from 'styled-system';

const DropdownWrapper = styled.div`
  display: inline-flex;
  position: relative;
  vertical-align: top;
  ${flexbox};
  ${position};
  ${space};
`;

const DropdownTrigger = styled.div``;

const DropdownMenu = styled.div`
  /* display: ${props => (props.isOpened ? 'block' : 'none')}; */
  left: ${props => !props.right && '0'};
  right: ${props => props.right && '0'};
  min-width: ${props => (props.width ? props.width + 'px' : '12rem')};
  width:${props => (props.fullWidth ? '100%' : '')};
  padding-top: ${props => (props.up ? 'initial' : '4px')};
  padding-bottom: ${props => props.up && '4px'};
  position: absolute;
  top: ${props => (props.up ? 'auto' : '100%')};
  bottom: ${props => props.up && '100%'};
  z-index: 20;
`;

const DropdownContent = styled.div`
  transition: 0.3s;
  background-color: white;
  border-radius: ${props => props.theme.radius};
  box-shadow: ${props =>
    props.isOpened &&
    `0 0.5em 1em -0.125em ${rgba(
      props.theme.colors.black,
      0.1
    )}, 0 0px 0 1px ${rgba(props.theme.colors.black, 0.1)}`};
  /* padding-bottom: ${props => props.isOpened && '0.5rem'};
  padding-top: ${props => props.isOpened && '0.5rem'}; */
`;

const Dropdown = ({ activator, ...props }) => {
  const [isOpened, updateIsOpened] = React.useState(false);
  const node = React.useRef();

  let toggleDropdown = e => {
    e.stopPropagation();
    e.preventDefault();
    updateIsOpened(!isOpened);
  };

  let handleClick = e => {
    if (node.current.contains(e.target)) {
      // inside click
      // console.log('inside');
      return;
    }
    // outside click
    // ... do whatever on click outside here ...
    // console.log('outside');
    updateIsOpened(false);
  };

  React.useEffect(() => {
    // add when mounted
    document.addEventListener('mousedown', handleClick);
    // return function to be called when unmounted
    return () => {
      document.removeEventListener('mousedown', handleClick);
    };
  });

  return (
    <DropdownWrapper
      ref={node}
      {...props}
      onMouseEnter={() => props.hover && updateIsOpened(true)}
      onMouseLeave={() => props.hover && updateIsOpened(false)}>
      <DropdownTrigger onClick={!props.disabled && toggleDropdown}>
        {activator}
      </DropdownTrigger>
      <DropdownMenu
        right={props.right}
        width={props.width}
        fullWidth={props.fullWidth}
        up={props.up}
        isOpened={isOpened}
        hover={props.hover}
        role="menu">
        <DropdownContent isOpened={isOpened} onClick={toggleDropdown}>
          <Collapse isOpened={isOpened} duration={100}>
            {props.children}
          </Collapse>
        </DropdownContent>
      </DropdownMenu>
    </DropdownWrapper>
  );
};

export default Dropdown;

DropdownContent.defaultProps = {
  theme
};
