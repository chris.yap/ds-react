import React from 'react'
import cn from 'classnames'

const Title = ({ dark, size, className, ...props }) => {
  return (
    <h1
      className={cn(
        'nab-title',
        size ? 'is-' + size : '',
        dark ? 'is-dark' : '',
        className
      )}
      { ...props }
    />
  )
}

export default Title