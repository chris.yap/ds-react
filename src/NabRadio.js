import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import nextId from 'react-id-generator'

export default class Radio extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { name, selectedValue, onChange } = this.context.radioGroup
    const optional = {}
    const {
      button,
      className,
      label,
      id,
      primary,
      secondary,
      spreadEvenly,
      success,
      info,
      warning,
      danger,
      value,
      small,
      large,
      ...props
    } = this.props
    if (selectedValue !== undefined) {
      optional.checked = value === selectedValue
    }
    if (typeof onChange === 'function') {
      optional.onChange = onChange.bind(null, value)
    }
    const htmlId = nextId()

    return (
      <div
        className={cn('nab-radio', className)}
        style={spreadEvenly ? { flexBasis: 'auto' } : null}
      >
        <input
          {...props}
          role='radio'
          id={id || htmlId}
          aria-checked={optional.checked}
          type='radio'
          name={name}
          {...optional}
        />
        <label
          className={cn(
            button ? 'nab-button' : '',
            optional.checked ? 'is-active' : '',
            primary ? 'is-primary' : '',
            secondary ? 'is-secondary' : '',
            success ? 'is-success' : '',
            info ? 'is-info' : '',
            warning ? 'is-warning' : '',
            danger ? 'is-danger' : '',
            small && 'has-text-size-2',
            large && 'has-text-size2'
          )}
          htmlFor={id || htmlId}
        >
          {label}
        </label>
      </div>
    )
  }
}

Radio.contextTypes = {
  radioGroup: PropTypes.object
}
