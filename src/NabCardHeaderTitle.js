import React from 'react'
import cn from 'classnames'

const CardHeaderTitle = ({ className, ...props}) => {
  return (
    <div 
      className={cn(
        'nab-card-header-title',
        className
      )}
      {...props}
    />
  )
}

export default CardHeaderTitle