import React from 'react'
import cn from 'classnames'

const CardHeaderIcon = ({ className, ...props}) => {
  return (
    <div 
      className={cn(
        'nab-card-header-icon',
        className
      )}
      {...props}
    />
  )
}

export default CardHeaderIcon