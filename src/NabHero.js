import React from 'react';
import theme from './Theme';
import styled from 'styled-components';
import { composed } from './Theme/Composed';
import { mediaQueries } from './Theme/breakpoints';
import { shade, tint, linearGradient } from 'polished';
import { background } from 'styled-system';

const style = {
  padding: {
    small: '1.5rem',
    default: '3rem',
    medium: '9rem',
    large: '18rem'
  }
};

const getGradient = colors => {
  if (colors) {
    const step1 = `${colors[1]} 0%`;
    const step2 = `${colors[2]} 100%`;
    let style = {
      ...linearGradient({
        colorStops: [step1, step2],
        toDirection: 'to bottom right',
        fallback: colors[0]
      })
    };
    return style;
  }
  return null;
};

const HeroWrapper = styled.section`
  align-items: stretch;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  position: relative;
  overflow: hidden;
  padding: ${props =>
    props.small
      ? `${style.padding.small} 1.5rem`
      : `${style.padding.default} 1.5rem`};
  ${mediaQueries[1]} {
    padding: ${props => props.medium && `${style.padding.medium} 1.5rem`};
    padding: ${props => props.large && `${style.padding.large} 1.5rem`};
  }
  min-height: ${props => props.fullHeight && '100vh'};
  ${composed};
  ${background};
`;

const HeroHeader = styled.div`
  flex-grow: 0;
  flex-shrink: 0;
  z-index: 1;
`;
const HeroBody = styled.div`
  flex-grow: 1;
  flex-shrink: 0;
  display: ${props => props.fullHeight && 'flex'};
  align-items: ${props => props.fullHeight && 'center'};
  z-index: 1;
  * {
    color: ${props =>
      (props.primary ||
        props.secondary ||
        props.success ||
        props.info ||
        props.warning ||
        props.danger ||
        props.dark) &&
      'white'};
  }
  > * {
    flex-grow: 1;
    flex-shrink: 1;
  }
`;
const HeroFooter = styled.div`
  flex-grow: 0;
  flex-shrink: 0;
  z-index: 1;
`;

const HeroBG = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 0;
  mix-blend-mode: hue;
  background-color: ${props =>
    props.primary && !props.gradient
      ? props.theme.colors.primary
      : props.secondary && !props.gradient
      ? props.theme.colors.secondary
      : props.success && !props.gradient
      ? props.theme.colors.success
      : props.info && !props.gradient
      ? props.theme.colors.info
      : props.warning && !props.gradient
      ? props.theme.colors.warning
      : props.danger && !props.gradient
      ? props.theme.colors.danger
      : props.light && !props.gradient
      ? props.theme.colors.blacks[0]
      : props.dark && !props.gradient
      ? props.theme.colors.blacks[4]
      : null};
  ${props =>
    props.gradient &&
    getGradient(
      props.primary
        ? [
            `${props.theme.colors.primary}`,
            `${shade(0.5, props.theme.colors.primary)}`,
            `${tint(0.25, props.theme.colors.primary)}`
          ]
        : props.secondary
        ? [
            `${props.theme.colors.secondary}`,
            `${shade(0.5, props.theme.colors.secondary)}`,
            `${tint(0.25, props.theme.colors.secondary)}`
          ]
        : props.success
        ? [
            `${props.theme.colors.success}`,
            `${shade(0.5, props.theme.colors.success)}`,
            `${tint(0.25, props.theme.colors.success)}`
          ]
        : props.info
        ? [
            `${props.theme.colors.info}`,
            `${shade(0.5, props.theme.colors.info)}`,
            `${tint(0.25, props.theme.colors.info)}`
          ]
        : props.warning
        ? [
            `${props.theme.colors.warning}`,
            `${shade(0.5, props.theme.colors.warning)}`,
            `${tint(0.25, props.theme.colors.warning)}`
          ]
        : props.danger
        ? [
            `${props.theme.colors.danger}`,
            `${shade(0.5, props.theme.colors.danger)}`,
            `${tint(0.25, props.theme.colors.danger)}`
          ]
        : props.light
        ? [
            `${props.theme.colors.blacks[0]}`,
            `${props.theme.colors.blacks[1]}`,
            `${tint(1, props.theme.colors.blacks[0])}`
          ]
        : props.dark
        ? [
            `${props.theme.colors.blacks[4]}`,
            `${shade(0.5, props.theme.colors.blacks[4])}`,
            `${tint(0.25, props.theme.colors.blacks[4])}`
          ]
        : null
    )}
  ${background};
`;

const Hero = ({
  header,
  footer,
  children,
  primary,
  secondary,
  success,
  info,
  warning,
  danger,
  light,
  dark,
  gradient,
  fullHeight,
  medium,
  large,
  theme,
  ...props
}) => {
  return (
    <HeroWrapper
      primary={primary}
      secondary={secondary}
      success={success}
      info={info}
      warning={warning}
      danger={danger}
      dark={dark}
      light={light}
      fullHeight={fullHeight}
      theme={theme}
      medium={medium}
      large={large}
      {...props}>
      {header && <HeroHeader>{header}</HeroHeader>}
      <HeroBody
        primary={primary}
        secondary={secondary}
        success={success}
        info={info}
        warning={warning}
        danger={danger}
        dark={dark}
        light={light}
        fullHeight={fullHeight}
        medium={medium}
        large={large}>
        {children}
      </HeroBody>
      {footer && <HeroFooter>{footer}</HeroFooter>}
      <HeroBG
        primary={primary}
        secondary={secondary}
        success={success}
        info={info}
        warning={warning}
        danger={danger}
        dark={dark}
        light={light}
        theme={theme}
        gradient={gradient}
      />
    </HeroWrapper>
  );
};

export default Hero;

HeroBG.defaultProps = {
  theme
};
