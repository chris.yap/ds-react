import React from 'react';
import Select from 'react-select';
import AsyncSelect from 'react-select/async';
import nextId from 'react-id-generator';
import Field from '../Forms/Field';
import Icon from '../Icon/';

const theme = theme => ({
  ...theme,
  colors: {
    ...theme.colors,
    primary: '#b2bbc0'
  }
});

const IconOption = option => (
  <div className="is-flex is-align-items-center">
    <Icon className="mr-1">{option.icon}</Icon>
    {option.label}
  </div>
);

const SelectDropdown = ({
  async,
  className,
  error,
  errorMessage,
  height,
  id,
  innerRef,
  label,
  onInputChange,
  placeholder,
  required,
  ...props
}) => {
  const customStyles = {
    control: (styles, { isDisabled, isFocused }) => ({
      ...styles,
      borderColor: isDisabled
        ? 'transparent'
        : isFocused
        ? '#909ea4'
        : '#d4d9dc',
      boxShadow: isDisabled
        ? 'none'
        : isFocused
        ? 'inset 0 1px 2px transparent, 0 0 0.25rem 0.125em #d4d9dc'
        : 'none',
      height: height ? height + 'px' : null
    }),

    option: (styles, state) => ({
      ...styles,
      '&:active': {
        backgroundColor: '#f6f7f8',
        color: 'black'
      },
      transition: '.2s ease',
      cursor: state.isSelected ? 'not-allowed' : 'pointer',
      backgroundColor: state.isSelected
        ? '#4c626c'
        : state.isFocused
        ? '#d4d9dc'
        : null,
      color: state.isSelected ? 'white' : 'black'
    })
  };

  const htmlId = nextId();

  return (
    <React.Fragment>
      <Field error={error} {...props}>
        {/* <div className={`nab-field ${error ? 'has-error' : ''} ${className || ''}`}> */}
        {label && (
          <label className="nab-label" htmlFor={id ? id : htmlId}>
            {label}
            {required && <sup>*</sup>}
          </label>
        )}
        {!async ? (
          <Select
            className={`nab-select
              ${
                !props.components
                  ? ''
                  : !props.isMulti
                  ? 'has-custom-components'
                  : ''
              }
            `}
            inputId={id ? id : htmlId}
            classNamePrefix="nab-select"
            getOptionLabel={props.hasIcons ? IconOption : props.getOptionLabel}
            ref={innerRef}
            styles={customStyles}
            theme={theme}
            onInputChange={onInputChange}
            placeholder={placeholder}
            {...props}
          />
        ) : (
          <AsyncSelect
            className={`nab-select
              ${
                !props.components
                  ? ''
                  : !props.isMulti
                  ? 'has-custom-components'
                  : ''
              }
            `}
            inputId={id ? id : htmlId}
            classNamePrefix="nab-select"
            getOptionLabel={props.hasIcons ? IconOption : props.getOptionLabel}
            ref={innerRef}
            styles={customStyles}
            theme={theme}
            onInputChange={onInputChange}
            placeholder={placeholder}
            {...props}
          />
        )}
        {error && errorMessage && <p className="nab-help">{errorMessage}</p>}
      </Field>
    </React.Fragment>
  );
};

export default SelectDropdown;
