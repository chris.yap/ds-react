import React from 'react'
import cn from 'classnames'

const Subtitle = ({ size, className, ...props }) => {
  return (
    <h3 
      className={cn(
        'nab-subtitle',
        size ? 'is-' + size : '',
        className
      )}
      {...props}
    />
  )
}

export default Subtitle