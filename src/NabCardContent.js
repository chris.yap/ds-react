import React from 'react'
import cn from 'classnames'

const CardContent = ({ className, ...props }) => {
  return (
    <div 
      className={cn(
        'nab-card-content',
        className
      )}
      { ...props }
    />
  )
}

export default CardContent