import React from 'react';
import PropTypes from 'prop-types';
import { breakpoints } from '../constants/config';

function above(breakpoint) {
  const query = breakpoints[breakpoint];
  return window.matchMedia(`screen and (min-width: ${query[1]})`).matches;
}

function below(breakpoint) {
  const query = breakpoints[breakpoint];
  return window.matchMedia(`screen and (max-width: ${query[0]})`).matches;
}

function when(breakpoint) {
  const query = breakpoints[breakpoint];
  return window.matchMedia(
    `screen and (min-width: ${query[0]}) and (max-width: ${query[1]})`
  ).matches;
}

// function except(breakpoint) {
//   const query = breakpoints[breakpoint];
//   return window.matchMedia(
//     `screen and (min-width: ${query[0]}) and (max-width: ${query[1]})`
//   ).matches;
// }

const Breakpoints = ComposedComponent => {
  class Wrapper extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        above: {},
        below: {},
        when: {}
        // except: {}
      };
      this._processBreakpoints = this._processBreakpoints.bind(this);
    }

    componentDidMount() {
      this._processBreakpoints();
      window.addEventListener('resize', this._processBreakpoints);
    }

    componentWillUnmount() {
      window.removeEventListener('resize', this._processBreakpoints);
    }

    _processBreakpoints() {
      const state = this.state;

      Object.keys(breakpoints).forEach(breakpoint => {
        state.above[breakpoint] = above(breakpoint);
        state.below[breakpoint] = below(breakpoint);
        state.when[breakpoint] = when(breakpoint);
        // state.except[breakpoint] = except(breakpoint);
      });

      this.setState(state);
    }

    render() {
      return (
        <ComposedComponent activeBreakpoints={this.state} {...this.props} />
      );
    }
  }

  Wrapper.propTypes = {
    activeBreakpoints: PropTypes.any
  };

  return Wrapper;
};

export default Breakpoints;
