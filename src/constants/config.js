const breakpoints = {
  // xs: ['0', '320px'],
  // sm: ['320px', '544px'],
  // md: ['544px', '768px'],
  // lg: ['768px', '992px'],
  // xl: ['992px', '1200px'],
  xs: ['0px', '480px'],
  sm: ['481px', '768px'],
  md: ['769px', '1024px'],
  lg: ['1025px', '1216px'],
  xl: ['1217px', '1408px'],
  xxl: ['1409px', '4000px']
};

export { breakpoints };
