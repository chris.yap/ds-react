import React from 'react';
import cn from 'classnames';

const Label = ({ className, ...props }) => {
	return <label htmlFor={props.for} className={cn(className)} {...props} />;
};

export default Label;
