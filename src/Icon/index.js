import React from 'react';
import cn from 'classnames';
import IconPaths from './icons/shaper-icons';
import styled from 'styled-components';

// const IconWrapper = styled.svg`
//   width: 24px;
//   height: 24px;
// `;

// const Icon = ({ children, ...props }) => {
//   return <IconWrapper>123</IconWrapper>;
// };
const Icon = ({
  small,
  medium,
  large,
  left,
  right,
  white,
  primary,
  secondary,
  success,
  info,
  warning,
  danger,
  material,
  number,
  className,
  ...props
}) => {
  return (
    <span
      className={cn(
        'nab-icon',
        small ? 'is-small' : '',
        medium ? 'is-medium' : '',
        large ? 'is-large' : '',
        left ? 'is-left' : '',
        right ? 'is-right' : '',
        className
      )}
      {...props}>
      {material && (
        <i
          className={cn(
            'material-icons',
            white ? 'has-text-white' : '',
            primary ? 'has-text-primary' : '',
            secondary ? 'has-text-secondary' : '',
            success ? 'has-text-success' : '',
            info ? 'has-text-info' : '',
            warning ? 'has-text-warning' : '',
            danger ? 'has-text-danger' : ''
          )}>
          {props.children}
          {number && <span>{number}</span>}
        </i>
      )}
      {!material && (
        <i
          className={cn(
            'icon-' + props.children,
            white ? 'has-text-white' : '',
            primary ? 'has-text-primary' : '',
            secondary ? 'has-text-secondary' : '',
            success ? 'has-text-success' : '',
            info ? 'has-text-info' : '',
            warning ? 'has-text-warning' : '',
            danger ? 'has-text-danger' : ''
          )}>
          {number && <span>{number}</span>}
        </i>
      )}
    </span>
  );
};

export default Icon;
