import React from 'react';
import cn from 'classnames';

const Tile = ({
  ancestor,
  parent,
  child,
  vertical,
  size,
  className,
  tileBreak,
  ...props
}) => {
  return (
    <div
      className={cn(
        `nab-tile`,
        ancestor ? 'is-ancestor' : '',
        parent ? 'is-parent' : '',
        child ? 'is-child' : '',
        vertical ? 'is-vertical' : '',
        size ? 'is-' + size : '',
        tileBreak ? 'is-break-' + tileBreak : '',
        className
      )}
      {...props}
    />
  );
};

export default Tile;
