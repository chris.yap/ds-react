import React from 'react'
import cn from 'classnames'

const Column = ({ size, mobile, tablet, desktop, widescreen, fullhd, sizeOffset, mobileOffset, tabletOffset, desktopOffset, widescreenOffset, fullhdOffset, narrow, className, ...props}) => {
  return (
    <div 
      className={cn(
        'nab-column',
        size ? 'is-' + size : '',
        mobile ? 'is-' + mobile + '-mobile' : '',
        tablet ? 'is-' + tablet + '-tablet' : '',
        desktop ? 'is-' + desktop + '-desktop' : '',
        widescreen ? 'is-' + widescreen + '-widescreen' : '',
        fullhd ? 'is-' + fullhd + '-fullhd' : '',
        sizeOffset ? 'is-offset-' + sizeOffset : '',
        mobileOffset ? 'is-offset-' + mobileOffset + '-mobile' : '',
        tabletOffset ? 'is-offset-' + tabletOffset + '-tablet' : '',
        desktopOffset ? 'is-offset-' + desktopOffset + '-desktop' : '',
        widescreenOffset ? 'is-offset-' + widescreenOffset + '-widescreen' : '',
        fullhdOffset ? 'is-offset-' + fullhdOffset + '-fullhd' : '',
        narrow ? 'is-narrow' : '',
        className
      )}
      {...props}
    />
  )
}

export default Column