// const space = [0, 2, 4, 8, 16, 32, 64, 128, 256, 512];
const space = [];
const spacer = 16;

for (let i = 0; i < 6; i++) {
  space.push(spacer * (i * 0.25));
}
space.push(32, 64, 128, 256, 512);

export { space };
