import React from 'react'
import cn from 'classnames'
import PropTypes from 'prop-types'

export default class RadioGroup extends React.Component {
  getChildContext() {
    const { name, selectedValue, onChange } = this.props
    return {
      radioGroup: {
        name,
        selectedValue,
        onChange
      }
    }
  }

  render() {
    const {
      buttons,
      children,
      className,
      fullwidth,
      fullWidth,
      label,
      name,
      onChange,
      rounded,
      selectedValue,
      required,
      small,
      ...rest
    } = this.props
    return (
      <div
        role='radiogroup'
        className={cn(
          'nab-field',
          'nab-radio-group',
          buttons ? 'nab-buttons is-grouped' : '',
          fullWidth || fullwidth ? 'is-fullwidth' : '',
          rounded ? 'is-rounded' : '',
          label ? 'is-block' : '',
          className
        )}
        {...rest}
      >
        {label && (
          <React.Fragment>
            <label className={cn('is-block nab-label', !buttons ? 'mb-0' : '')}>
              {label}
              {required && <sup>*</sup>}
            </label>
          </React.Fragment>
        )}
        <div className={cn(fullWidth || fullwidth ? 'is-flex' : '')}>
          {children}
        </div>
      </div>
    )
  }
}

RadioGroup.propTypes = {
  name: PropTypes.string,
  selectedValue: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.bool,
    PropTypes.object
  ]),
  onChange: PropTypes.func,
  children: PropTypes.node.isRequired
  // Component: PropTypes.oneOfType([PropTypes.string, PropTypes.func, PropTypes.object]),
}

RadioGroup.childContextTypes = {
  radioGroup: PropTypes.object
}
