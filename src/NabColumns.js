import React from 'react'
import cn from 'classnames'

const Columns = ({ mobile, desktop, multiline, gapless, centered, className, ...props }) => {
  return (
    <div 
      className={cn(
        'nab-columns',
        mobile ? 'is-mobile' : '',
        desktop ? 'is-desktop' : '',
        multiline ? 'is-multiline' : '',
        gapless ? 'is-gapless' : '',
        centered ? 'is-centered' : '',
        className
      )}
      { ...props }
    />
  )
}

export default Columns
