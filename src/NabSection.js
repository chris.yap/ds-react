import styled from 'styled-components';
import { composed } from './Theme/Composed';

const paddings = {
  default: '3rem 1.5rem',
  medium: '9rem 1.5rem',
  large: '18rem 1.5rem'
};

const Section = styled.section`
  padding: ${props =>
    props.medium
      ? paddings.medium
      : props.large
      ? paddings.large
      : paddings.default};
  ${composed};
`;

export default Section;
