import styled from 'styled-components';
import { space } from 'styled-system';

const Divider = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${props => props.vertical && '1px'};
  height: ${props => props.vertical && '100%'};
  min-height: 1px;
  position: relative;
  &:before,
  &:after {
    content: '';
    flex: 1;
    height: 1px;
    background-color: ${props =>
      props.dark
        ? 'rgba(255,255,255, .2)'
        : props.dotted
        ? 'transparent'
        : 'rgba(0,0,0, .12)'};
    background-image: ${props =>
      props.dotted &&
      'url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAACCAYAAACddGYaAAAAGUlEQVQYV2NkYGCQZ2BgeMjAwMDACCJgAAASvQEDOTli/wAAAABJRU5ErkJggg==)'};
  }
  &:before {
    margin-right: 0;
    height: ${props => props.vertical && '100%'};
    min-height: ${props => props.vertical && '44px'};
  }
  &:after {
    margin-left: 0;
    content: ${props => props.vertical && 'none'};
  }
  ${space}
`;

export default Divider;
