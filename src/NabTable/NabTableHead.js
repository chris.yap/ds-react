import React from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';
import Breakpoints from '../Breakpoints/';
import Column from '../NabColumn';
import Columns from '../NabColumns';
import Icon from '../Icon/';

class TableHead extends React.Component {
  sortTable(field, sortType) {
    this.props.sortTableStateBy(field, sortType);
  }
  componentDidMount = () => {
    if (this.props.defaultSortCol) {
      this.props.sortTableStateBy(
        this.props.defaultSortCol,
        null,
        this.props.desc ? 'desc' : true
      );
    }
  };
  componentDidUpdate = prevProps => {
    if (this.props.data !== prevProps.data && this.props.defaultSortCol) {
      this.props.sortTableStateBy(
        this.props.defaultSortCol,
        this.props.sort,
        true
      );
    }
  };
  render() {
    const {
      activeBreakpoints,
      headers,
      ascending,
      desktopOffset,
      mobileOffset,
      ...props
    } = this.props;
    const headStyle = {
      top:
        (activeBreakpoints.when.lg || activeBreakpoints.above.lg) &&
        desktopOffset
          ? desktopOffset + 'px'
          : 0
    };
    return (
      <thead className={props.className}>
        <tr>
          {headers.map((header, colIndex) => (
            <th
              key={colIndex}
              className={cn(
                header.align ? 'has-text-' + header.align : '',
                header.className,
                header.sort ? 'has-pointer' : ''
              )}
              role={header.sort ? 'button' : ''}
              onClick={
                header.sort
                  ? this.sortTable.bind(this, header.value, header.sort)
                  : null
              }
              style={headStyle}>
              {header.sort ? (
                <Columns
                  gapless
                  mobile
                  className={`${
                    header.align === 'center' || header.align === 'centered'
                      ? 'is-justify-content-center'
                      : ''
                  }`}>
                  {header.align === 'right' && <Column />}
                  <Column narrow>
                    <Icon
                      small
                      className={cn(
                        this.props.selectedCol === header.value
                          ? 'has-text-primary'
                          : 'has-text-black text--lighten-4'
                      )}>
                      {this.props.selectedCol === header.value && ascending
                        ? 'sort-asc'
                        : this.props.selectedCol === header.value && !ascending
                        ? 'sort-desc'
                        : 'sort'}
                      {/* ? 'sort-desc'
											: 'sort'} */}
                    </Icon>
                  </Column>
                  <Column narrow>{header.name}</Column>
                </Columns>
              ) : (
                header.name
              )}
            </th>
          ))}
        </tr>
      </thead>
    );
  }
}

export default Breakpoints(TableHead);

TableHead.proptypes = {
  desktopOffset: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  mobileOffset: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
};
