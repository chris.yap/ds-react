import React from 'react';
import cn from 'classnames';
import { DetectOverflow } from '../NabOverflowDetection';
import PropTypes from 'prop-types';
import TableHead from './NabTableHead';
import TableBody from './NabTableBody';

export default class Table extends React.Component {
  constructor(props) {
    super(props);
    this.handleOverflowChange = this.handleOverflowChange.bind(this);
  }
  state = {
    field: null,
    ascending: true,
    overflow: false,
    sortedData: []
  };

  isNumeric = num => {
    return !isNaN(num);
  };

  formatNumber = figure => {
    let num = figure ? figure : figure === 0 ? 0 : -Math.pow(10, 1000);
    if (!this.isNumeric(num)) {
      if (!this.isNumeric(Number(num.replace(/[^0-9.-]+/g, '')))) {
        return -Math.pow(10, 1000);
      }
      return Number(num.replace(/[^0-9.-]+/g, ''));
    } else {
      return Number(num);
    }
  };

  sortTableStateBy = (field, sortType, dir) => {
    let tempData = this.props.data;
    if (sortType && sortType !== true) sortType = sortType.toLowerCase();
    const newDir =
      dir && dir !== 'desc'
        ? true
        : dir && dir === 'desc'
        ? false
        : this.state.field !== field
        ? true
        : this.state.ascending
        ? false
        : true;

    this.setState({
      field: field,
      ascending: newDir
    });

    setTimeout(() => {
      if (newDir) {
        if (sortType === 'number') {
          tempData.sort(
            (a, b) => this.formatNumber(a[field]) - this.formatNumber(b[field])
          );
        } else {
          tempData.sort((a, b) => {
            return a[field] < b[field] ? -1 : b[field] < a[field] ? 1 : 0;
          });
        }
      } else {
        if (sortType === 'number') {
          tempData.sort(
            (a, b) => this.formatNumber(b[field]) - this.formatNumber(a[field])
          );
        } else {
          tempData.sort((a, b) => {
            return b[field] < a[field] ? -1 : a[field] < b[field] ? 1 : 0;
          });
        }
      }
      this.setState({
        sortedData: tempData
      });
    }, 100);
  };

  handleOverflowChange = isOverflowed => {
    this.setState({
      overflow: isOverflowed ? true : false
    });
  };

  render() {
    const {
      desc,
      bordered,
      className,
      clickFunc,
      data,
      defaultSortCol,
      fullwidth,
      headers,
      id,
      hoverable,
      desktopOffset,
      mobileOffset,
      narrow,
      noScroll,
      striped,
      stickyHeader,
      tableRow,
      ...props
    } = this.props;
    return (
      <React.Fragment>
        <DetectOverflow
          onOverflowChange={this.handleOverflowChange}
          className={cn(
            'nab-table-wrapper',
            noScroll
              ? 'is-overflow-visible'
              : !this.state.overflow
              ? 'is-overflow-visible'
              : 'is-overflow-auto',
            className
          )}
          style={
            {
              // overflow: this.state.overflow === false ? 'visible' : 'auto'
              // maxHeight: this.state.overflow ? 'calc(100vh - 70px)' : 'none'
            }
          }
          {...props}>
          <table
            id={id}
            className={cn(
              'nab-table',
              bordered ? 'is-bordered' : '',
              fullwidth ? 'is-fullwidth' : '',
              hoverable ? 'is-hoverable' : '',
              stickyHeader ? 'has-sticky-header' : '',
              narrow ? 'is-narrow' : '',
              striped ? 'is-striped' : ''
            )}>
            {headers && (
              <TableHead
                overflow={this.state.overflow}
                headers={headers}
                data={data}
                selectedCol={this.state.field}
                selectedColDir={this.state.ascending}
                sortTableStateBy={this.sortTableStateBy}
                defaultSortCol={defaultSortCol}
                ascending={this.state.ascending}
                desc={desc}
              />
            )}
            <TableBody
              data={
                this.state.sortedData && this.state.sortedData.length > 0
                  ? this.state.sortedData
                  : data
              }
              TableRow={tableRow}
              overflow={this.state.overflow}
              clickFunc={clickFunc}
            />
          </table>
        </DetectOverflow>
      </React.Fragment>
    );
  }
}

Table.propTypes = {
  bordered: PropTypes.bool,
  className: PropTypes.string,
  data: PropTypes.array.isRequired,
  defaultSortCol: PropTypes.string,
  fullwidth: PropTypes.bool,
  headers: PropTypes.array,
  hoverable: PropTypes.bool,
  narrow: PropTypes.bool,
  striped: PropTypes.bool,
  tableRow: PropTypes.any,
  stickyHeader: PropTypes.bool
};

Table.defaultProps = {
  data: []
};
