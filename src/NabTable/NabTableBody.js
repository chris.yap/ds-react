import React from 'react';
// import PropTypes from 'prop-types';

export default class TableBody extends React.Component {
	render() {
		// Create player cards from sorted, dynamic JSON collection
		var Rows = [];
		const { data, TableRow, ...props } = this.props;

		data.forEach((row, index) => {
			Rows.push(<TableRow key={index} row={row} {...props} />);
		});

		return <tbody>{Rows}</tbody>;
	}
}

// TableBody.propTypes = {
// 	data: PropTypes.array.isRequired,
// 	TableRow: PropTypes.node.isRequired,
// };
