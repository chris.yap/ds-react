import React from 'react'
import cn from 'classnames'

const Content = ({ small, medium, large, className, ...props }) => (
  <div 
    className={cn(
      'nab-content',
      small ? 'is-small' : '',
      medium ? 'is-medium' : '',
      large ? 'is-large' : '',
      className
    )}
    { ...props }
  />
)

export default Content