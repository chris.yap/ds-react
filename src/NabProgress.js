import styled, { keyframes } from 'styled-components';
import { composed } from './Theme/Composed';
import { colors } from './Theme/colors';

const Styling = {
  bg: 'rgba(0, 0, 0, 0.12)',
  bar: colors.blacks[4],
  small: '0.875em',
  medium: '1.25em',
  large: '1.5em'
};

const moveIndeterminate = keyframes`
  0% {
    background-position-x: 250%
  }
  100% {
    background-position-x: -250%
  }
`;

const Progress = styled.progress`
  &:not(:last-child) {
    margin-bottom: 1rem;
  }
  appearance: none;
  border: none;
  border-radius: ${props => (!props.square ? '290486px' : '0px')};
  display: block;
  height: ${props =>
    props.height
      ? props.height + 'px'
      : props.small
      ? Styling.small
      : props.medium
      ? Styling.medium
      : props.large
      ? Styling.large
      : '1em'};
  overflow: hidden;
  padding: 0;
  width: 100%;
  transform: ${props => props.right && 'scaleX(-1)'};
  transition: 500ms ease-in-out;
  &::-webkit-progress-bar {
    background-color: ${props =>
      !props.transparent ? Styling.bg : 'transparent'};
    box-shadow: ${props =>
      !props.transparent && 'inset 0 3px 0 rgba(0, 0, 0, 0.05)'};
  }
  &::-webkit-progress-value {
    background-color: ${props =>
      props.primary
        ? props.theme.colors.primary
        : props.secondary
        ? props.theme.colors.secondary
        : props.info
        ? props.theme.colors.info
        : props.success
        ? props.theme.colors.success
        : props.warning
        ? props.theme.colors.warning
        : props.danger
        ? props.theme.colors.danger
        : Styling.bar};
    transition: 500ms ease-in-out;
  }
  &::-moz-progress-bar {
    background-color: ${Styling.bg};
  }
  &::-ms-fill {
    background-color: ${Styling.bg};
    border: none;
  }
  &:indeterminate {
    animation-duration: 2s;
    animation-iteration-count: infinite;
    animation-name: ${moveIndeterminate};
    animation-timing-function: linear;
    background-color: transparent;
    background-image: linear-gradient(
      to right,
      transparent 0%,
      ${props =>
          props.primary
            ? props.theme.colors.primary
            : props.secondary
            ? props.theme.colors.secondary
            : props.info
            ? props.theme.colors.info
            : props.success
            ? props.theme.colors.success
            : props.warning
            ? props.theme.colors.warning
            : props.danger
            ? props.theme.colors.danger
            : Styling.bar}
        10%,
      ${props =>
          props.primary
            ? props.theme.colors.primary
            : props.secondary
            ? props.theme.colors.secondary
            : props.info
            ? props.theme.colors.info
            : props.success
            ? props.theme.colors.success
            : props.warning
            ? props.theme.colors.warning
            : props.danger
            ? props.theme.colors.danger
            : Styling.bar}
        20%,
      transparent 30%
    );
    background-position: top left;
    background-repeat: no-repeat;
    background-size: 150% 150%;
  }
  ${composed}
`;

export default Progress;

Progress.defaultProps = {
  theme: {
    colors
  },
  max: 100
};
