import React from 'react';
import Icon from './Icon/';
import List from './List/List';
import ListItem from './List/ListItem';
import Tabs from './Tabs/Tabs';
import Tab from './Tabs/Tab';

const FloatingNav = ({ ...props }) => {
  return (
    <div className="nab-floating-widget">
      <Tabs
        justify
        stacked
        closable
        mandatory={false}
        className="has-bg-blue-grey lighten-5 mb-0">
        {TabsItems.map((tab, t) => {
          return (
            <Tab key={t} label={tab.label} icon={tab.icon}>
              {tab.items && (
                <List>
                  {tab.items.map((ttab, tt) => {
                    return (
                      <ListItem
                        href={ttab.to}
                        icon={ttab.icon}
                        className={ttab.className}>
                        {ttab.label}
                      </ListItem>
                    );
                  })}
                  {t === 0 && (
                    <ListItem className="has-bg-blue-grey">
                      <span className="has-text-white is-flex">
                        <Icon small className="mr-2">
                          settings
                        </Icon>
                        <span>Manage quick links</span>
                      </span>
                    </ListItem>
                  )}
                  {t === 3 && (
                    <ListItem
                      className="has-bg-blue-grey"
                      onClick={props.logout}>
                      <span className="has-text-white is-flex">
                        <Icon small className="mr-2">
                          logout
                        </Icon>
                        <span>Log out</span>
                      </span>
                    </ListItem>
                  )}
                </List>
              )}
            </Tab>
          );
        })}
      </Tabs>
    </div>
  );
};

export default FloatingNav;

const TabsItems = [
  {
    icon: 'quicklinks',
    label: 'Quick links',
    items: [
      { label: 'Cash transfer', to: '' },
      { label: "Andre's portfolio", to: '' },
      { label: 'My rainy day play watchlist', to: '' },
      { label: 'BHP company page', to: '' },
      { label: 'MY historical transactions', to: '' }
    ]
  },
  {
    icon: 'watchlist',
    label: 'Watchlists'
  },
  {
    icon: 'bell-outline',
    label: 'Alerts'
  },
  {
    icon: 'settings',
    label: 'Settings',
    items: [
      { label: 'Account', to: '' },
      { label: 'Communication preferences', to: '' },
      { label: 'Alerts & notifications', to: '' }
    ]
  }
];
