import React from 'react';
import styled, {
  ThemeProvider as StyledThemeProvider
} from 'styled-components';
import theme from '../Theme';

export const Base = styled.div`
  font-family: ${props => props.theme.font};
  line-height: ${props => props.theme.lineHeights.standard};
  font-weight: ${props => props.theme.fontWeights.normal};
  * {
    box-sizing: border-box;
  }
`;

const defaultTheme = theme;

const App = ({ newTheme, ...props }) => {
  // const breakpoints = customBreakpoints || nextTheme.breakpoints;
  const breakpoints = {
    ...defaultTheme.breakpoints,
    ...((newTheme && newTheme.breakpoints) || {})
  };
  const colors = {
    ...defaultTheme.colors,
    ...((newTheme && newTheme.colors) || {})
  };
  const space = {
    ...defaultTheme.space,
    ...((newTheme && newTheme.space) || {})
  };
  const font = defaultTheme.font || (newTheme && newTheme.font) || {};
  const fontSizes = {
    ...defaultTheme.fontSizes,
    ...((newTheme && newTheme.fontSizes) || {})
  };
  const fontWeights = {
    ...defaultTheme.fontWeights,
    ...((newTheme && newTheme.fontWeights) || {})
  };
  const lineHeights = {
    ...defaultTheme.lineHeights,
    ...((newTheme && newTheme.lineHeights) || {})
  };
  const letterSpacings = {
    ...defaultTheme.letterSpacings,
    ...((newTheme && newTheme.letterSpacings) || {})
  };
  const textStyles = {
    ...defaultTheme.textStyles,
    ...((newTheme && newTheme.textStyles) || {})
  };
  const colorStyles = {
    ...defaultTheme.colorStyles,
    ...((newTheme && newTheme.colorStyles) || {})
  };
  const radii = {
    ...defaultTheme.radii,
    ...((newTheme && newTheme.radii) || {})
  };
  const shadows = {
    ...defaultTheme.shadows,
    ...((newTheme && newTheme.shadows) || {})
  };
  const duration = {
    ...defaultTheme.duration,
    ...((newTheme && newTheme.duration) || {})
  };
  const transitionDelays = {
    ...defaultTheme.transitionDelays,
    ...((newTheme && newTheme.transitionDelays) || {})
  };
  const themeNow = {
    ...defaultTheme,
    breakpoints,
    colors,
    space,
    font,
    fontSizes,
    fontWeights,
    lineHeights,
    letterSpacings,
    textStyles,
    colorStyles,
    radii,
    shadows,
    duration,
    transitionDelays
  };
  // console.log(`${Base.theme}`);
  return (
    <StyledThemeProvider theme={themeNow} newTheme={newTheme}>
      <Base {...props} />
    </StyledThemeProvider>
  );
};

export default App;

Base.defaultProps = {
  theme
};
