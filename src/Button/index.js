import React from 'react';
import styled from 'styled-components';
import { composed } from '../Theme/Composed';
import { rgba } from 'polished';
import theme from '../Theme';

const e = React.createElement;
const ButtonWrapper = styled(({ as, children, ...props }) =>
  e(as, props, children)
)`
  appearance: none;
  align-items: center;
  border-radius: ${props =>
    props.round ? props.theme.radiusRounded : props.theme.radii[2] + 'px'};
  box-shadow: ${props => (props.flat ? 'none' : '0 0 0 0 black')};
  display: inline-flex;
  font-size: ${props =>
    props.round || props.square
      ? 'inherit'
      : props.small
      ? '.875em'
      : props.medium
      ? '1.25em'
      : props.large
      ? '1.5em'
      : '1rem'};
  height: ${props =>
    props.mega
      ? 'auto'
      : props.small
      ? '1.75rem'
      : props.medium
      ? '2.75rem'
      : props.large
      ? '3.5rem'
      : '2.375rem'};
  width: ${props =>
    props.block || props.fullwidth || props.fullWidth
      ? '100%'
      : props.small && (props.round || props.square)
      ? '1.75rem'
      : props.medium && (props.round || props.square)
      ? '2.75rem'
      : props.large && (props.round || props.square)
      ? '3.5rem'
      : (props.round || props.square) && '2.375rem'};
  justify-content: flex-start;
  line-height: 1.5;
  position: ${props => props.loading && 'relative'};
  vertical-align: top;
  background-color: ${props =>
    props.active
      ? props.primary
        ? props.theme.colors.primary
        : props.secondary
        ? props.theme.colors.secondary
        : props.success
        ? props.theme.colors.success
        : props.info
        ? props.theme.colors.info
        : props.warning
        ? props.theme.colors.warning
        : props.danger
        ? props.theme.colors.danger
        : props.theme.colors.secondary
      : props.inverted
      ? props.primary
        ? rgba(props.theme.colors.primaries[0], 0.9)
        : props.secondary
        ? rgba(props.theme.colors.secondaries[0], 0.9)
        : props.success
        ? rgba(props.theme.colors.successes[0], 0.9)
        : props.info
        ? rgba(props.theme.colors.infos[0], 0.9)
        : props.warning
        ? rgba(props.theme.colors.warnings[0], 0.9)
        : props.danger
        ? rgba(props.theme.colors.dangers[0], 0.9)
        : rgba(props.theme.colors.blacks[0], 0.9)
      : props.flat
      ? 'transparent'
      : props.primary
      ? props.theme.colors.primary
      : props.secondary
      ? props.theme.colors.secondary
      : props.success
      ? props.theme.colors.success
      : props.info
      ? props.theme.colors.info
      : props.warning
      ? props.theme.colors.warning
      : props.danger
      ? props.theme.colors.danger
      : '#fff'};
  border: ${props =>
    props.flat || props.inverted
      ? 'none'
      : props.white ||
        props.primary ||
        props.secondary ||
        props.success ||
        props.info ||
        props.warning ||
        props.danger
      ? '1px solid transparent'
      : `1px solid ${rgba(76, 98, 108, 0.25)}`};
  font-weight: 600;
  color: ${props =>
    props.loading
      ? 'transparent'
      : props.active
      ? 'white'
      : props.flat || props.inverted
      ? props.white
        ? 'white'
        : props.primary
        ? props.theme.colors.primary
        : props.success
        ? props.theme.colors.success
        : props.info
        ? props.theme.colors.info
        : props.warning
        ? props.theme.colors.warning
        : props.danger
        ? props.theme.colors.danger
        : props.theme.colors.secondary
      : props.white
      ? props.theme.colors.text
      : props.primary ||
        props.secondary ||
        props.success ||
        props.info ||
        props.warning ||
        props.danger
      ? 'white'
      : props.theme.colors.secondary};
  cursor: ${props => (props.noAction ? 'not-allowed' : 'pointer')};
  pointer-events: ${props => props.noAction && 'none'};
  justify-content: center;
  padding: ${props =>
    props.mega
      ? '.75rem 1.5rem .75rem 1rem'
      : props.round || props.square
      ? '0'
      : '0 0.75em'};
  margin: ${props =>
    props.block || props.fullwidth || props.fullWidth ? '0' : '0 0.25rem'};
  text-align: ${props => (props.mega ? 'left' : 'center')};
  text-transform: ${props => (props.round || props.square) && 'uppercase'};
  white-space: nowrap;
  outline: none;
  transition: ${props => props.theme.transitionDelays.large};

  &:first-child {
    margin-left: 0;
    ${composed};
  }
  &:last-child {
    margin-right: 0;
    ${composed};
  }
  &:hover {
    background-color: ${props =>
      props.flat
        ? props.white
          ? rgba('white', 0.1)
          : props.primary
          ? rgba(props.theme.colors.primary, 0.1)
          : props.success
          ? rgba(props.theme.colors.success, 0.1)
          : props.info
          ? rgba(props.theme.colors.info, 0.1)
          : props.warning
          ? rgba(props.theme.colors.warning, 0.1)
          : props.danger
          ? rgba(props.theme.colors.danger, 0.1)
          : rgba(props.theme.colors.secondary, 0.1)
        : props.inverted
        ? props.white
          ? props.theme.colors.blacks[1]
          : props.primary
          ? props.theme.colors.primaries[1]
          : props.success
          ? props.theme.colors.successes[1]
          : props.info
          ? props.theme.colors.infos[1]
          : props.warning
          ? props.theme.colors.warnings[1]
          : props.danger
          ? props.theme.colors.dangers[1]
          : props.theme.colors.secondaries[1]
        : props.white
        ? props.theme.colors.blacks[0]
        : props.primary
        ? props.theme.colors.primaries[7]
        : props.secondary
        ? props.theme.colors.secondaries[7]
        : props.success
        ? props.theme.colors.successes[7]
        : props.info
        ? props.theme.colors.infos[7]
        : props.warning
        ? props.theme.colors.warnings[7]
        : props.danger
        ? props.theme.colors.dangers[7]
        : 'white'};
    border-color: ${props =>
      props.white ||
      props.primary ||
      props.secondary ||
      props.success ||
      props.info ||
      props.warning ||
      props.danger
        ? 'transparent'
        : props.theme.colors.blacks[1]};
    color: ${props =>
      props.loading
        ? 'transparent'
        : props.flat || props.inverted
        ? props.white
          ? 'white'
          : props.primary
          ? props.theme.colors.primary
          : props.secondary
          ? props.theme.colors.secondary
          : props.success
          ? props.theme.colors.success
          : props.info
          ? props.theme.colors.info
          : props.warning
          ? props.theme.colors.warning
          : props.danger
          ? props.theme.colors.danger
          : props.theme.colors.primary
        : props.primary ||
          props.secondary ||
          props.success ||
          props.info ||
          props.warning ||
          props.danger
        ? 'white'
        : props.white
        ? props.theme.colors.text
        : props.theme.colors.primary};
  }
  &:focus {
    &:not(:active) {
      box-shadow: ${props =>
        props.primary
          ? `0 0 0 0.125em ${rgba(props.theme.colors.primary, 0.2)}`
          : props.success
          ? `0 0 0 0.125em ${rgba(props.theme.colors.success, 0.2)}`
          : props.info
          ? `0 0 0 0.125em ${rgba(props.theme.colors.info, 0.2)}`
          : props.warning
          ? `0 0 0 0.125em ${rgba(props.theme.colors.warning, 0.2)}`
          : props.danger
          ? `0 0 0 0.125em ${rgba(props.theme.colors.danger, 0.2)}`
          : `0 0 0 0.125em ${rgba(props.theme.colors.secondary, 0.2)}`};
    }
    border-color: ${props =>
      props.white ||
      props.primary ||
      props.secondary ||
      props.success ||
      props.info ||
      props.warning ||
      props.danger
        ? 'white'
        : props.theme.colors.info};
    color: ${props =>
      props.loading
        ? 'transparent'
        : props.active || props.white
        ? 'white'
        : props.flat || props.inverted
        ? props.primary
          ? props.theme.colors.primary
          : props.secondary
          ? props.theme.colors.secondary
          : props.success
          ? props.theme.colors.success
          : props.info
          ? props.theme.colors.info
          : props.warning
          ? props.theme.colors.warning
          : props.danger
          ? props.theme.colors.danger
          : props.theme.colors.text
        : props.primary ||
          props.secondary ||
          props.success ||
          props.info ||
          props.warning ||
          props.danger
        ? 'white'
        : props.theme.colors.text};
  }
  &:active {
    border-color: ${props =>
      props.white
        ? props.theme.colors.blacks[0]
        : props.primary
        ? props.theme.colors.primaries[6]
        : props.secondary
        ? props.theme.colors.secondaries[6]
        : props.success
        ? props.theme.colors.successes[6]
        : props.info
        ? props.theme.colors.infos[6]
        : props.warning
        ? props.theme.colors.warnings[6]
        : props.danger
        ? props.theme.colors.dangers[6]
        : props.theme.colors.text};
    color: ${props =>
      props.loading
        ? 'transparent'
        : props.active || props.white
        ? 'white'
        : props.flat || props.inverted
        ? props.primary
          ? props.theme.colors.primary
          : props.secondary
          ? props.theme.colors.secondary
          : props.success
          ? props.theme.colors.success
          : props.info
          ? props.theme.colors.info
          : props.warning
          ? props.theme.colors.warning
          : props.danger
          ? props.theme.colors.danger
          : props.theme.colors.text
        : props.primary ||
          props.secondary ||
          props.success ||
          props.info ||
          props.warning ||
          props.danger
        ? 'white'
        : props.theme.colors.text};
  }
  * {
    margin: ${props => props.mega && '0'};
    line-height: ${props => props.mega && '1.1'};
  }
  &[disabled] {
    opacity: 0.5;
    pointer-events: none;
    cursor: not-allowed;
  }
  ${composed};
  ${props => !props.loading} {
    &::before {
      content: '';
      position: absolute;
      animation-name: spinAround;
      animation-duration: 500ms;
      animation-iteration-count: infinite;
      animation-direction: normal;
      animation-timing-function: linear;
      animation-fill-mode: forwards;
      animation-delay: 0s;
      height: 1em;
      width: 1em;
      border-radius: ${props => props.theme.radiusRounded};
      border: 2px solid;
      border-color: ${props =>
        !props.loading
          ? 'transparent'
          : props.primary ||
            props.secondary ||
            props.success ||
            props.info ||
            props.warning ||
            props.danger
          ? 'white white transparent'
          : props.theme.colors.blacks[2] +
            ' ' +
            props.theme.colors.blacks[2] +
            ' transparent'};
    }
  }

  @keyframes spinAround {
    from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(360deg);
    }
  }

  > .nab-icon {
    &:first-child {
      margin-left: ${props =>
        props.round || props.square ? '0' : `-${theme.space[1]}px`};
      margin-right: ${props =>
        props.round || props.square
          ? '0'
          : props.small
          ? `${theme.space[0]}px`
          : props.large || props.medium
          ? `${theme.space[2]}px`
          : `${theme.space[1]}px`};
    }
    &:last-child {
      margin-left: ${props =>
        props.round || props.square
          ? '0'
          : props.small
          ? `${theme.space[0]}px`
          : props.large || props.medium
          ? `${theme.space[2]}px`
          : `${theme.space[1]}px`};
      margin-right: ${props =>
        props.round || props.square ? '0' : `-${theme.space[1]}px`};
    }
    width: ${props =>
      (props.square || props.round) &&
      (props.small
        ? '1.25em'
        : props.medium
        ? '1.5em'
        : props.large && '1.75em')};
    font-size: ${props =>
      props.small
        ? '1.25em'
        : props.medium
        ? '1.5em'
        : props.large
        ? '2em'
        : '1.5em'};
  }
`;

const Button = ({ href, id, loading, onClick, type, ...props }) => {
  return (
    <ButtonWrapper
      as={href && 'a'}
      href={href}
      id={id}
      loading={loading ? 1 : 0}
      onClick={e => onClick && onClick(e)}
      type={type || 'button'}
      {...props}
    />
  );
};

export default Button;

ButtonWrapper.defaultProps = {
  as: 'button',
  theme
};
