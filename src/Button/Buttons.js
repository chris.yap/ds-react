import React from 'react';
import cn from 'classnames';

const Buttons = ({ rounded, fullwidth, fullWidth, className, ...props }) => {
  return (
    <div
      className={cn(
        `nab-buttons is-grouped`,
        rounded ? 'is-rounded' : '',
        fullWidth || fullwidth ? 'is-fullwidth' : '',
        className
      )}
      {...props}
    />
  );
};

export default Buttons;
