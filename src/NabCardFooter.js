import React from 'react'
import cn from 'classnames'

const CardFooter = ({ className, ...props }) => {
  return (
    <div 
      className={cn(
        'nab-card-footer',
        className
      )}
      { ...props }
    />
  )
}

export default CardFooter