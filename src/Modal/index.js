import React from 'react';
import styled, { createGlobalStyle } from 'styled-components';
import WhichBrowser from '../NabWhichBrowser';
import { radialGradient, rgba } from 'polished';
import { composed } from '../Theme/Composed';
import theme from '../Theme/';
import { mediaQueries } from '../Theme/breakpoints';
import { Transition } from 'react-transition-group';

import Alert from '../Alert';
import Button from '../Button';
import Divider from '../Divider';
import Icon from '../Icon/';
import Title from '../NabTitle';

const cardVar = {
  contentMarginXS: '20px',
  contentSpacingXS: '160px',
  contentSpacingSM: '40px',
  spacing: '40px'
};

const BodyStyle = createGlobalStyle`
  body {
    overflow: hidden;
    position: ${props =>
      props.activeBrowser && props.activeBrowser.isIos ? 'fixed' : ''};
    min-width: 100vw;
    max-width: 100vw;
  }
`;

const ModalCardHeader = styled.div`
  align-items: center;
  display: flex;
  flex-shrink: 0;
  justify-content: flex-start;
  padding: 1rem 1.5rem;
  position: relative;
`;

const ModalCardFooter = styled.div`
  background-color: ${props => rgba(props.theme.colors.bluegreys[1], 0.1)};
  position: relative;
  align-items: center;
  display: flex;
  flex-shrink: 0;
  justify-content: flex-start;
  padding: 1rem 1.5rem;
  > button {
    margin-top: 0;
    margin-bottom: 0;
    &:first-child {
      margin-left: 0;
    }
    &:last-child {
      margin-right: 0;
    }
  }
`;

const ModalCardBody = styled.div`
  /*-webkit-overflow-scrolling: touch;*/
  flex-grow: 1;
  flex-shrink: 1;
  overflow: auto;
  overflow-y: ${props => props.fullscreen && 'scroll'};
  padding: 1.5rem;
`;

const ModalCard = styled.div`
  margin: ${props => (props.fullscreen ? '0' : `0 ${cardVar.contentMarginXS}`)};
  overflow: auto;
  position: relative;
  width: ${props => (props.modalWidth ? props.modalWidth + '%' : '100%')};
  display: flex;
  flex-direction: column;
  border-radius: ${props => props.theme.radii[2]}px;
  overflow: hidden;
  background-color: white;
  max-height: ${props => !props.fullscreen && '70vh'};
  height: ${props => props.fullscreen && '100%'};
  ${mediaQueries[1]} {
    /* margin:  && '0'}; */
    width: ${props => (props.modalWidth ? props.modalWidth + '%' : '640px')};
  }
  ${composed}
`;

const ModalWrapper = styled.div`
  left: 0;
  position: absolute;
  top: 0;
  height: 100%;
  width: 100%;
  align-items: center;
  display: flex;
  justify-content: center;
  /* overflow: hidden;*/
  position: fixed;
  z-index: 199;
  > ${ModalCard} {
    position: ${props => props.fullscreen && 'absolute'};
    top: ${props => props.fullscreen && '0'};
    left: ${props => props.fullscreen && '0'};
    width: ${props => props.fullscreen && '100%'};
    border-radius: ${props => props.fullscreen && '0'};
  }
  > button {
    position: fixed;
    top: 1rem;
    right: 1rem;
  }
  transition: 0.5s;
  opacity: ${({ state }) => (state === 'entered' ? 1 : 0)};
  backdrop-filter: blur(10px);
`;

const ModalOverlay = styled.div`
  left: 0;
  position: absolute;
  top: 0;
  height: 100vh;
  width: 100vw;
  /* background-color: ${props => rgba(props.theme.colors.bluegrey, 0.86)}; */
  ${radialGradient({
    colorStops: [
      `${theme.colors.bluegrey} 20%`,
      rgba(`${theme.colors.bluegrey}`, 0.7)
    ],
    extent: 'circle at center'
    // position: 'center',
    // shape: 'circle'
  })}
  background-color: transparent;
`;

const Modal = ({
  activeBrowser,
  alert,
  bodyClassName,
  className,
  closeIcon,
  closeButton,
  enableBodyScroll,
  onClose,
  onClosed,
  onExited,
  footer,
  fullscreen,
  header,
  isOpened,
  modalWidth,
  ...props
}) => {
  return (
    <React.Fragment>
      <Transition
        in={isOpened}
        timeout={{
          appear: 300,
          enter: 0,
          exit: 300
        }}
        unmountOnExit
        mountOnEnter
        onExited={onClosed}>
        {state => (
          <ModalWrapper fullscreen={fullscreen} state={state}>
            <ModalOverlay isOpened={isOpened} />

            <ModalCard
              modalWidth={modalWidth}
              fullscreen={fullscreen}
              role="dialog"
              className={className}
              {...props}>
              {header && typeof header === 'string' && (
                <React.Fragment>
                  <ModalCardHeader>
                    <Title size="5" className="mb-0">
                      {header}
                    </Title>
                  </ModalCardHeader>
                  <Divider />
                </React.Fragment>
              )}
              {header && typeof header !== 'string' && (
                <React.Fragment>
                  <ModalCardHeader>{header}</ModalCardHeader>
                  <Divider />
                </React.Fragment>
              )}

              {alert ? (
                <Alert
                  info={alert === 'info'}
                  warning={alert === 'warning'}
                  danger={alert === 'danger'}
                  success={alert === 'success'}
                  inverted
                  isOpened={true}>
                  {props.children}
                </Alert>
              ) : (
                props.children && (
                  <ModalCardBody
                    className={bodyClassName}
                    activeBrowser={activeBrowser}
                    fullscreen={fullscreen}>
                    {props.children}
                  </ModalCardBody>
                )
              )}
              {footer && (
                <React.Fragment>
                  <Divider />
                  <ModalCardFooter>{footer}</ModalCardFooter>
                </React.Fragment>
              )}
            </ModalCard>

            {(closeIcon || closeButton) && !fullscreen && (
              <Button
                flat
                round
                medium
                white
                // className="nab-modal-close"
                aria-label="close"
                onClick={onClose}>
                <Icon medium>close</Icon>
              </Button>
            )}
          </ModalWrapper>
        )}
      </Transition>
      {!enableBodyScroll && isOpened === true && (
        <BodyStyle activeBrowser={activeBrowser} />
      )}
    </React.Fragment>
  );
};

export default WhichBrowser(Modal);

ModalCardFooter.defaultProps = {
  theme
};
ModalCard.defaultProps = {
  theme
};
ModalOverlay.defaultProps = {
  theme
};
ModalWrapper.defaultProps = {
  theme
};
