import React from 'react';
import cn from 'classnames';
import Breakpoints from './Breakpoints/';

const Card = ({
  activeBreakpoints,
  outline,
  dark,
  flat,
  hasLink,
  href,
  className,
  modalWidth,
  ...props
}) => {
  let Tag = href ? 'a' : 'div';
  return (
    <Tag
      className={cn(
        'nab-card',
        (href || hasLink) && !activeBreakpoints.below.sm ? 'has-link' : '',
        flat ? 'is-flat' : '',
        dark ? 'is-dark' : '',
        outline ? 'is-outline' : '',
        className
      )}
      style={{ width: modalWidth + '%' }}
      href={href}
      {...props}
    />
  );
};

export default Breakpoints(Card);
