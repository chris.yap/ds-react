import styled from 'styled-components';

const Row = styled.div`
  display: table-row;
`;

export default Row;
