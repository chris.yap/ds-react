import React from 'react';
import styled from 'styled-components';

const BodyWrap = styled.div`
  display: table-row-group;
`;

const Body = ({ data, TableRow, ...props }) => {
  var Rows = [];
  data.forEach((row, index) => {
    Rows.push(<TableRow key={index} row={row} {...props} />);
  });
  return <BodyWrap>{Rows}</BodyWrap>;
};

export default { Body, BodyWrap };
