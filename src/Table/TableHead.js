import React from 'react';
import styled from 'styled-components';
import { rgba, tint } from 'polished';
import Row from './TableRow';
import { CellWrapper, ChildWrapper } from './Cell';
import Icon from '../Icon';
import Div from '../Div';
import { composed } from '../Theme/Composed';
import theme from '../Theme';

const TableCell = styled(CellWrapper)`
  border-width: 0 0 2px;
  border-top-color: transparent;
  border-bottom-color: ${props => rgba(props.theme.colors.secondaries[2], 1)};
  font-weight: 700;
  vertical-align: top;
  line-height: 1.1;
  padding-top: calc(0.5em + 2px);
  padding-left: ${props => props.sort && '.3em'};
  ${ChildWrapper} {
    padding-left: 0;
  }
`;

const HeadWrapper = styled.div`
  display: table-header-group;
  ${composed};

  ${TableCell} {
    top: ${props => (props.top ? props.top : 0)};
    background-color: ${props =>
      props.success
        ? tint(0.9, props.theme.colors.successes[1])
        : props.info
        ? tint(0.9, props.theme.colors.infos[1])
        : props.warning
        ? tint(0.9, props.theme.colors.warnings[1])
        : props.danger
        ? tint(0.9, props.theme.colors.dangers[1])
        : 'white'};
    color: ${props =>
      props.success
        ? props.theme.colors.successes[6]
        : props.info
        ? props.theme.colors.infos[6]
        : props.warning
        ? props.theme.colors.warnings[6]
        : props.danger && props.theme.colors.dangers[6]};
    border-bottom-color: ${props =>
      props.success
        ? props.theme.colors.success
        : props.info
        ? props.theme.colors.info
        : props.warning
        ? props.theme.colors.warning
        : props.danger && props.theme.colors.danger};
  }
`;

const Head = ({ ascending, selectedField: field, headers, ...props }) => {
  return (
    <HeadWrapper {...props}>
      <Row>
        {headers.map((header, index) => {
          const contentProps = {
            role: header.sort && 'button'
          };
          return (
            <TableCell
              key={index}
              sort={header.sort}
              right={header.align === 'right'}
              className={`${header.className || ''} ${header.sort &&
                'has-pointer'}`}
              onClick={
                header.sort ? () => props.sort(header.value, header.sort) : null
              }
              contentProps={contentProps}
              style={{ width: header.width ? header.width : 'auto' }}>
              <Div
                display="flex"
                justifyContent={
                  header.align === 'right'
                    ? 'flex-end'
                    : header.align === 'centered'
                    ? 'center'
                    : 'flex-start'
                }>
                {header.sort && (
                  <Div minHeight="20px">
                    <Icon
                      small
                      className={`${
                        field === header.value
                          ? 'has-text-primary'
                          : 'has-text-black text--lighten-4'
                      }`}>
                      {field === header.value && ascending
                        ? 'sort-asc'
                        : field === header.value && !ascending
                        ? 'sort-desc'
                        : 'sort'}
                    </Icon>
                  </Div>
                )}
                <Div>{header.name}</Div>
              </Div>
            </TableCell>
          );
        })}
      </Row>
    </HeadWrapper>
  );
};

export default { Head, HeadWrapper };

Head.defaultProps = {
  theme
};

TableCell.defaultProps = {
  theme
};
