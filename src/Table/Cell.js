import React from 'react';
import styled from 'styled-components';
import { rgba } from 'polished';
import { composed } from '../Theme/Composed';
import theme from '../Theme';

const CellWrapper = styled.span`
  display: table-cell;
  box-sizing: border-box;
  background-color: white;
  border-color: ${rgba('black', 0.1)};
  border-style: solid;
  border-width: 0;
  padding: 0.5em 0.75em;
  vertical-align: middle;
  white-space: ${props =>
    props.hasNoWrap || props.hasEllipsis ? 'nowrap' : 'normal'};
  overflow: hidden;
  text-overflow: ${props => props.hasEllipsis && 'ellipsis'};
  overflow: ${props => props.hasEllipsis && 'hidden'};
  text-align: ${props => (props.right ? 'right' : props.centered && 'center')};
  ${composed};
`;

const ChildWrapper = styled.span`
  ${composed};
`;

const Cell = ({ children, style, contentProps, ...props }) => {
  return (
    <CellWrapper {...props} style={style}>
      <ChildWrapper {...contentProps} {...props}>
        {children}
      </ChildWrapper>
    </CellWrapper>
  );
};

export { Cell, CellWrapper, ChildWrapper };

Cell.defaultProps = {
  // theme
};
