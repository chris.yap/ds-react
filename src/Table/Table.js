import React from 'react';
import styled from 'styled-components';
import { tint } from 'polished';
import theme from '../Theme';
import PropTypes from 'prop-types';
import TableHead from './TableHead';
import TableFoot from './TableFoot';
import TableBody from './TableBody';
import TableRow from './TableRow';
import { CellWrapper } from './Cell';
import { DetectOverflow } from '../NabOverflowDetection';
import { composed } from '../Theme/Composed';

const GetHeaderNthChild = props => {
  return `
    &:nth-child(${props.headerCount}) {
      ${CellWrapper} {
        position: sticky;
        z-index: 2;
        &:nth-child(-n+${props.columnCount}) {
          position: sticky;
          border-right-width: ${props.columnCount && props.overflow && '1px'};
          border-bottom-width: 2px;
          top: 0;
          left: 0;
          z-index: 3;
        }
      }
    }
  `;
};

const GetFooterNthChild = props => {
  return `
    &:nth-child(-n+${props.columnCount}) {
      position: sticky;
      border-right-width: ${props.columnCount && props.overflow && '1px'};
      border-top-width: 2px;
      bottom: 0;
      left: 0;
      z-index: 3;
    }
  `;
};

const GetColumnNthChild = props => {
  return `
    &:nth-child(-n+${props.columnCount}) {
      position: sticky;
      border-right-width: ${props.columnCount && props.overflow && '1px'};
      left: 0;
      z-index: 2;
    }
  `;
};

const TableWrap = styled.div`
  display: table;
  border-collapse: separate;
  background-color: white;
  color: ${props => props.theme.colors.text};
  width: 100%;
`;

const TableWrapper = styled(DetectOverflow)`
  margin-bottom: 1rem;
  -webkit-overflow-scrolling: touch;
  background-image: linear-gradient(90deg, #fff, #fff 50%, #fff),
    linear-gradient(90deg, #fff, #fff 50%, #fff),
    linear-gradient(90deg, rgba(0, 0, 0, 0.1), transparent),
    linear-gradient(270deg, rgba(0, 0, 0, 0.1), transparent);
  background-position: 0 50%, 100% 50%;
  background-repeat: no-repeat;
  background-color: #fff;
  background-size: 40px 100%, 40px 100%, 15px 100%, 15px 100%;
  background-attachment: local, local, scroll, scroll;
  border: ${props =>
    (props.bordered || props.hasBorders) &&
    `1px solid ${props.theme.colors.secondaries[1]}`};

  &::-webkit-scrollbar {
    height: 4px;
    width: 4px;
  }

  &::-webkit-scrollbar-track {
    background-color: ${props => props.theme.colors.secondaries[1]};
  }

  &::-webkit-scrollbar-thumb {
    background-color: ${props => props.theme.colors.secondary};
    border: ${props => `1px solid ${props.theme.colors.secondary}`};
  }

  &::-webkit-scrollbar-corner {
    background-color: transparent;
  }

  ${composed};

  ${TableHead.HeadWrapper} {
    ${TableRow} {
      ${CellWrapper} {
        border-right-width: ${props =>
          (props.bordered || props.hasBorders) && '1px'};
      }
      ${props =>
        GetHeaderNthChild({
          theme: props.theme,
          headerCount: props.headerCount,
          columnCount: props.columnCount,
          overflow: props.overflow,
          ...props.headerProps
        })};
    }
  }
  ${TableFoot.FootWrapper} {
    ${TableRow} {
      ${CellWrapper} {
        position: ${props => props.footerCount && 'sticky'};
        bottom: ${props => props.footerCount && 0};
        ${props =>
          GetFooterNthChild({
            theme: props.theme,
            footerCount: props.footerCount,
            columnCount: props.columnCount,
            overflow: props.overflow,
            ...props.headerProps
          })};
      }
    }
  }

  ${TableBody.BodyWrap} {
    ${CellWrapper} {
      ${props =>
        GetColumnNthChild({
          columnCount: props.columnCount,
          overflow: props.overflow
        })};
      border-width: ${props =>
        (props.bordered || props.hasBorders) && '0 1px 1px 0'};
      &:last-child {
        border-right-width: 0;
      }
    }
    ${TableRow} {
      &:last-child {
        ${CellWrapper} {
          border-bottom-width: ${props =>
            (props.bordered || props.hasBorders) && '0'};
        }
      }
      &:nth-child(odd) {
        ${CellWrapper} {
          ${props => GetColumnNthChild({ columnCount: props.columnCount })};
          background-color: ${props =>
            (props.striped || props.hasStripes) &&
            tint(0.7, props.theme.colors.secondaries[1])};
        }
      }
      &:hover {
        &:nth-child(even) {
          ${CellWrapper} {
            background-color: ${props =>
              (props.hoverable || props.isHoverable) &&
              tint(0.9, props.theme.colors.secondaries[1])};
          }
        }
        &:nth-child(odd) {
          ${CellWrapper} {
            background-color: ${props =>
              (props.striped || props.hasStripes) &&
              (props.hoverable || props.isHoverable)
                ? tint(0.6, props.theme.colors.secondaries[1])
                : (props.hoverable || props.isHoverable) &&
                  tint(0.9, props.theme.colors.secondaries[1])};
          }
        }
      }
    }
  }

  ${CellWrapper} {
    padding-top: ${props =>
      (props.narrow || props.isNarrow) && 'calc(.25em + 2px)'};
    padding-bottom: ${props => (props.narrow || props.isNarrow) && '.25em'};
  }
`;

export default class Table extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      field: null,
      ascending: true,
      overflow: false,
      sortedData: []
    };

    this.handleOverflowChange = this.handleOverflowChange.bind(this);
  }

  componentDidMount = () => {
    if (!this.props.externalSort && this.props.defaultSortCol) {
      this.sortTableStateBy(
        this.props.defaultSortCol,
        null,
        this.props.desc ? 'desc' : null
      );
    } else {
      this.setState({
        field: this.props.defaultSortCol && this.props.defaultSortCol,
        ascending: this.props.desc ? false : true,
        sortedData: this.props.data
      });
    }
  };

  componentDidUpdate = prevProps => {
    if (this.props.data !== prevProps.data) {
      if (!this.props.externalSort && this.props.defaultSortCol) {
        this.sortTableStateBy(this.props.defaultSortCol);
      } else {
        this.setState({
          sortedData: this.props.data
        });
      }
    }
  };

  isNumeric = num => {
    return !isNaN(num);
  };

  formatNumber = figure => {
    let num = figure ? figure : figure === 0 ? 0 : -Math.pow(10, 1000);
    if (!this.isNumeric(num)) {
      if (!this.isNumeric(Number(num.replace(/[^0-9.-]+/g, '')))) {
        return -Math.pow(10, 1000);
      }
      return Number(num.replace(/[^0-9.-]+/g, ''));
    } else {
      return Number(num);
    }
  };

  sortTableStateBy = (field, sortType, dir) => {
    let tempData = this.props.data;
    if (sortType && sortType !== true) sortType = sortType.toLowerCase();
    const newDir =
      dir && dir !== 'desc'
        ? true
        : dir && dir === 'desc'
        ? false
        : this.state.field !== field
        ? true
        : this.state.ascending
        ? false
        : true;

    this.setState({
      field: field,
      ascending: newDir
    });

    if (!this.props.externalSort) {
      setTimeout(() => {
        if (newDir) {
          if (sortType === 'number') {
            tempData.sort(
              (a, b) =>
                this.formatNumber(a[field]) - this.formatNumber(b[field])
            );
          } else {
            tempData.sort((a, b) => {
              return a[field] < b[field] ? -1 : b[field] < a[field] ? 1 : 0;
            });
          }
        } else {
          if (sortType === 'number') {
            tempData.sort(
              (a, b) =>
                this.formatNumber(b[field]) - this.formatNumber(a[field])
            );
          } else {
            tempData.sort((a, b) => {
              return b[field] < a[field] ? -1 : a[field] < b[field] ? 1 : 0;
            });
          }
        }
        this.setState({
          sortedData: tempData
        });
      }, 100);
    } else {
      this.props.externalSort(field, newDir);
    }
  };

  handleOverflowChange = isOverflowed => {
    this.setState({
      overflow: isOverflowed ? true : false
    });
  };

  render() {
    const {
      className,
      clickFunc,
      data,
      defaultSortCol,
      footers,
      hasNoScroll,
      fullwidth,
      headers,
      id,
      sortedCol,
      tableRow,
      desc,
      ...props
    } = this.props;

    let stickyColumn = Math.min(
      props.stickyColumn
        ? props.stickyColumn
        : props.stickyColumnCount && props.stickyColumnCount
    );
    let stickyHeader = Math.min(
      props.stickyHeader
        ? props.stickyHeader
        : props.stickyHeaderCount && props.stickyHeaderCount
    );
    let stickyFooter = Math.min(
      props.stickyFooter
        ? props.stickyFooter
        : props.stickyFooterCount && props.stickyFooterCount
    );

    const { ascending, field, overflow, sortedData } = this.state;

    return (
      <React.Fragment>
        <TableWrapper
          headerCount={stickyHeader}
          columnCount={stickyColumn}
          footerCount={stickyFooter}
          onOverflowChange={this.handleOverflowChange}
          overflow={overflow && !hasNoScroll}
          style={{ maxHeight: !hasNoScroll && '80vh', zIndex: 0 }}
          {...props}>
          <TableWrap id={id}>
            {headers && (
              <TableHead.Head
                ascending={ascending}
                headers={headers}
                selectedField={field}
                sort={this.sortTableStateBy}
                {...props.headerProps}
              />
            )}

            <TableBody.Body
              data={sortedData ? sortedData : data}
              TableRow={tableRow}
              overflow={overflow}
              clickFunc={clickFunc}
            />

            {footers && (
              <TableFoot.Foot
                ascending={ascending}
                footers={footers}
                selectedField={field}
                sort={this.sortTableStateBy}
                {...props.footerProps}
              />
            )}
          </TableWrap>
        </TableWrapper>
      </React.Fragment>
    );
  }
}

Table.propTypes = {
  bordered: PropTypes.bool,
  hasBorders: PropTypes.bool,
  className: PropTypes.string,
  data: PropTypes.array.isRequired,
  defaultSortCol: PropTypes.string,
  hasNoScroll: PropTypes.bool,
  fullwidth: PropTypes.bool,
  headers: PropTypes.array,
  hoverable: PropTypes.bool,
  isHoverable: PropTypes.bool,
  narrow: PropTypes.bool,
  isNarrow: PropTypes.bool,
  overflow: PropTypes.bool,
  striped: PropTypes.bool,
  hasStrips: PropTypes.bool,
  tableRow: PropTypes.any,
  stickyHeaderCount: PropTypes.oneOfType([PropTypes.bool, PropTypes.number]),
  stickyColumnCount: PropTypes.oneOfType([PropTypes.bool, PropTypes.number])
};

Table.defaultProps = {
  data: [],
  theme,
  stickyHeaderCount: false,
  stickyColumnCount: false
};

TableWrap.defaultProps = {
  theme
};
