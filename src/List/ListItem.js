import React from 'react';
import cn from 'classnames';
import styled from 'styled-components';
import { rgba } from 'polished';
import { Link } from 'react-router-dom';
import theme from '../Theme';
import { composed } from '../Theme/Composed';

const ListItemWrapper = styled.li`
  > a,
  > div {
    display: flex;
    position: relative;
    transition: $speed;
    text-decoration: none;
    flex-grow: 1;
    justify-content: space-between;
    align-items: center;
    padding: 0.75em 1em;
    opacity: ${props => props.disabled && '0.5'};
    pointer-events: ${props => props.disabled && 'none'};
    cursor: ${props => props.disabled && 'not-allowed'};

    ${composed};
  }
  &:first-child {
    border-top-left-radius: ${props => props.theme.radius};
    border-top-right-radius: ${props => props.theme.radius};
  }
  &:last-child {
    border-bottom-left-radius: ${props => props.theme.radius};
    border-bottom-right-radius: ${props => props.theme.radius};
  }
  &:not(:last-child) {
    > a,
    > div {
      border-bottom: ${props =>
        `1px solid ${rgba(props.theme.colors.secondary, 0.1)}`};
    }
  }
  &.is-active {
    background-color: $list-item-active-background-color;
    color: $list-item-active-color;
  }
  > a {
    cursor: pointer;
    color: ${props =>
      props.dark ? props.theme.colors.white : props.theme.colors.black};
    background-color: ${props =>
      props.dark ? 'transparent' : props.theme.colors.white};
    transition: 0.3s;
    ${composed};
    &:hover {
      background-color: ${props =>
        props.dark
          ? 'rgba(255,255,255,0.1)'
          : props.theme.colors.secondaries[0]};
    }
  }
`;

const ListItem = ({
  disabled,
  href,
  style,
  isDisabled,
  className,
  onClick,
  linkType,
  target,
  dark,
  ...props
}) => {
  return (
    <ListItemWrapper
      disabled={!!(isDisabled || disabled)}
      dark={dark}
      {...props}
    >
      {href && linkType === 'external' ? (
        <a href={href} className={className} style={style} target={target}>
          {props.children}
        </a>
      ) : href && linkType !== 'external' ? (
        <Link to={href} onClick={onClick} className={className} style={style}>
          {props.children}
        </Link>
      ) : (
        <div className={className} style={style} onClick={onClick}>
          {props.children}
        </div>
      )}
    </ListItemWrapper>
  );
};

export default ListItem;

ListItemWrapper.defaultProps = { theme };
