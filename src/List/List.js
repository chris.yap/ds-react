import React from 'react';
import styled from 'styled-components';
import { rgba } from 'polished';
import theme from '../Theme';
import { composed } from '../Theme/Composed';

const List = styled.ul`
  &:not(:last-child) {
    margin-bottom: 1rem;
    ${composed};
  }
  background-color: ${props => props.theme.colors.white};
  border-radius: ${props => props.theme.radius};
  ${composed};
`;

export default List;

List.defaultProps = { theme };
