import React from 'react'
import cn from 'classnames'

const CardHeader = ({ className, ...props}) => {
  return (
    <div 
      className={cn(
        'nab-card-header',
        className
      )}
      {...props}
    />
  )
}

export default CardHeader