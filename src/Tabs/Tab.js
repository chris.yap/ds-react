import styled from 'styled-components';
import { composed } from '../Theme/Composed';

const Tab = styled.div`
  transition: 300ms;
  padding-top: 1rem;
  opacity: ${({ state }) =>
    state === 'entering'
      ? 0
      : state === 'entered'
      ? 1
      : state === 'exiting'
      ? 0
      : state === 'exited' && 0};
  ${composed}
`;

export default Tab;
