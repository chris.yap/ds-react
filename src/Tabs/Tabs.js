import React from 'react';
import styled from 'styled-components';
import { rgba } from 'polished';
import Icon from '../Icon';
import PropTypes from 'prop-types';
import { Transition } from 'react-transition-group';
import theme from '../Theme';
import { composed } from '../Theme/Composed';

const TabStyle = {
  borderBottomColor: 'rgba(0,0,0,.12)',
  borderBottomWidth: '1px',
  tabsLinkPadding: '0.5em 1em',
  radius: '.25rem'
};

const TabsParent = styled.div`
  ${composed}
`;

const TabsWrapper = styled.div`
  -webkit-overflow-scrolling: touch;
  user-select: none;
  align-items: stretch;
  display: flex;
  position: relative;
  font-size: ${props =>
    props.small
      ? '0.875rem '
      : props.medium
      ? '1.25rem'
      : props.large
      ? '1.5rem'
      : '1rem'};
  justify-content: space-between;
  overflow: hidden;
  overflow-x: auto;
  white-space: nowrap;
  margin-bottom: 0;
  background-color: ${props =>
    props.boxed && props.theme.colors.secondaries[0]};

  .nab-icon {
    &:first-child {
      margin-right: 0.25em;
    }
    &:last-child {
      margin-left: 0.25em;
    }
  }
`;

const List = styled.ul`
  align-items: center;
  border-bottom-color: ${TabStyle.borderBottomColor};
  border-bottom-style: solid;
  border-bottom-width: ${props =>
    props.toggle ? '0' : TabStyle.borderBottomWidth};
  display: flex;
  flex-grow: 1;
  flex-shrink: 0;

  padding-left: ${props => (props.centered || props.right) && '0.75em'};
  padding-right: ${props => props.left || (props.centered && '0.75em')};

  flex: ${props => props.centered && '1'};
  justify-content: ${props =>
    props.centered ? 'center' : props.right ? 'flex-end' : 'flex-start'};
`;

const ListItem = styled.li`
  display: block;
  flex-grow: ${props => props.fullwidth && 1};
  flex-shrink: ${props => props.fullwidth && 0};

  a {
    display: flex;
    justify-content: center;
    align-items: center;
    color: ${props => props.theme.colors.text};
    margin-bottom: ${props =>
      props.toggle ? '0' : `-${TabStyle.borderBottomWidth}`};
    padding: ${TabStyle.tabsLinkPadding};
    padding-left: ${props => props.rounded && '1.25em'};
    padding-right: ${props => props.rounded && '1.25em'};
    &[disabled] {
      color: ${props => rgba(props.theme.colors.text, 0.4)};
    }

    transition: 300ms;
    background-color: ${props =>
      props.boxed && props.active
        ? 'white'
        : props.toggle && props.active && !props.rounded
        ? props.theme.colors.secondary
        : props.toggle && rgba(props.theme.colors.secondary, 0.05)};
    border-top-color: ${props =>
      props.boxed && props.active ? props.theme.colors.primary : 'transparent'};
    border-top-style: solid;
    border-top-width: ${props => props.boxed && '3px'};
    border-bottom-color: ${props =>
      props.boxed
        ? 'transparent'
        : props.active
        ? props.theme.colors.primary
        : 'transparent'};
    border-bottom-style: solid;
    border-bottom-width: ${props => (props.toggle ? '0' : '3px')};
    color: ${props =>
      props.toggle && props.active
        ? props.theme.colors.white
        : props.active
        ? props.theme.colors.primary
        : props.toggle && props.theme.colors.secondary};
    &:hover {
      background-color: ${props =>
        props.boxed && !props.active
          ? rgba('black', 0.1)
          : props.rounded
          ? rgba(props.theme.colors.secondary, 0.05)
          : props.toggle &&
            !props.active &&
            rgba(props.theme.colors.secondary, 0.1)};
      color: ${props =>
        props.active && props.toggle
          ? props.theme.colors.white
          : props.active && props.theme.colors.primary};

      &:before {
        background-color: ${props =>
          props.rounded &&
          !props.active &&
          rgba(props.theme.colors.secondary, 0.1)};
      }
    }

    border-width: ${props => props.toggle && '0'};
    font-weight: ${props => props.toggle && props.theme.fontWeights.semibold};
    box-shadow: ${props =>
      props.toggle && `inset 0 2px 0 ${rgba('black', 0.1)}`};
    margin-bottom: ${props => props.toggle && '0'};
    position: ${props => props.toggle && 'relative'};
    

    &:before,
    &:after {
      ${props => !props.rounded} {
        content: '';
      }
      position: absolute;
      border-top-left-radius: ${props => props.rounded && '290486px'};
      border-top-right-radius: ${props => props.rounded && '290486px'};
      border-bottom-left-radius: ${props => props.rounded && '290486px'};
      border-bottom-right-radius: ${props => props.rounded && '290486px'};
      top: 0px;
      left: 0px;
      width: 100%;
      height: 100%;
    }

    &:before {
      background-color: ${props =>
        props.rounded && props.active && props.theme.colors.secondary};
    }

    span {
      z-index: ${props => props.rounded && 1};
    }

    /* ${props => !props.rounded} {
      &:before,
      &:after {
        content: '';
      }
    } */
  
  }

  &:first-child a {
    border-radius: ${props =>
      props.toggle && props.rounded
        ? '290486px 0 0 290486px'
        : props.toggle && `${TabStyle.radius} 0 0 ${TabStyle.radius}`};
  }
  &:last-child a {
    border-radius: ${props =>
      props.toggle && props.rounded
        ? '0 290486px 290486px 0'
        : props.toggle && `0 ${TabStyle.radius} ${TabStyle.radius} 0`};
  }
`;

const AdditionalContent = styled.div`
  position: absolute;
  display: flex;
  top: 0;
  height: 100%;
  align-items: center;
  right: ${props => props.left && 0};
  left: ${props => props.right && 0};
`;

export default class Tabs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTab: 0,
      mandatory: true,
      closable: false
    };
    if (this.props.mandatory === false) {
      this.state.mandatory = false;
      this.state.selectedTab = null;
    }
    if (this.props.selected) this.state.selectedTab = this.props.selected;
    if (this.props.closable) this.state.closable = true;
  }
  componentDidMount() {
    if (this.props.selectedTab) {
      this.setState({
        selectedTab: this.props.selectedTab
      });
    }
  }
  componentDidUpdate(prevProps) {
    if (this.props.selectedTab !== prevProps.selectedTab) {
      let newIndex = parseInt(this.props.selectedTab, 10);
      this.setState({ selectedTab: newIndex });
    }
  }
  selectTab = index => {
    let newIndex = parseInt(index, 10);
    if (!this.state.closable) {
      this.setState({ selectedTab: newIndex });
    } else if (this.state.selectedTab === newIndex) {
      this.setState({ selectedTab: null });
    } else {
      this.setState({ selectedTab: newIndex });
    }
  };

  render() {
    const {
      additionalContent,
      left,
      right,
      stacked,
      className,
      contentClassName,
      ...props
    } = this.props;
    const children = props.children || [];
    return (
      <TabsParent {...props}>
        <TabsWrapper className={className || ''}>
          {/* <div
          className={cn(
            closable ? 'is-closable' : '',
            rounded ? 'is-toggle-rounded' : '',
            stacked ? 'is-stacked' : '',
            toggle ? 'is-toggle' : '',
            className
          )}
          style={{ position: 'relative' }}> */}
          <List left={left} right={right} {...props}>
            {children.map((child, index) => {
              const {
                className,
                icon,
                id,
                success,
                danger,
                tabClick,
                ...childProps
              } = child.props;
              let Classes = className && className;
              let disabled = childProps.disabled ? true : false;
              return (
                <ListItem
                  boxed={props.boxed}
                  toggle={props.toggle}
                  rounded={props.rounded}
                  active={index == this.state.selectedTab}
                  className={Classes}
                  key={index}
                  onClick={() => (
                    !disabled ? this.selectTab(index) : false,
                    tabClick && index !== this.state.selectedTab
                      ? tabClick()
                      : null
                  )}
                  {...props}>
                  <a id={id} disabled={disabled}>
                    {icon && (
                      <Icon small={childProps.iconSmall === true}>{icon}</Icon>
                    )
                    // <span className="nab-icon is-small">
                    //   <i className={'icon-' + child.props.icon} aria-hidden="true" />
                    // </span>
                    }
                    <span>{childProps.label}</span>
                  </a>
                </ListItem>
              );
            })}
          </List>
          {additionalContent && (
            <AdditionalContent left={left} right={right}>
              {additionalContent}
            </AdditionalContent>
          )}
        </TabsWrapper>
        {children.map((child, i) => (
          <Transition key={i} in={this.state.selectedTab === i} timeout={500}>
            {state =>
              this.state.selectedTab === i &&
              React.cloneElement(child, { state })
            }
          </Transition>
        ))}
      </TabsParent>
    );
  }
}

Tabs.propTypes = {
  additionalContent: PropTypes.any,
  stacked: PropTypes.bool,
  left: PropTypes.bool,
  centered: PropTypes.bool,
  right: PropTypes.bool,
  fullwidth: PropTypes.bool,
  fullWidth: PropTypes.bool,
  small: PropTypes.bool,
  medium: PropTypes.bool,
  large: PropTypes.bool,
  boxed: PropTypes.bool,
  toggle: PropTypes.bool,
  closable: PropTypes.bool,
  rounded: PropTypes.bool,
  selectedTab: PropTypes.number
};

Tabs.defaultProps = {
  left: true,
  theme,
  selectedTab: 0
};
