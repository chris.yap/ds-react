import React from 'react';
import styled from 'styled-components';
import { position } from 'styled-system';
import { radialGradient, rgba } from 'polished';
import { Transition } from 'react-transition-group';
import theme from '../Theme';

const OverlayWrapper = styled.div`
  position: ${props => (props.absolute ? 'absolute' : 'fixed')};
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 200;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  color: ${props => (props.dark || props.isDark ? 'white' : 'black')};
  ${props =>
    !props.transparent &&
    !props.isTransparent &&
    (props.dark || props.isDark
      ? radialGradient({
          colorStops: [
            `${theme.colors.bluegrey} 20%`,
            rgba(`${theme.colors.bluegrey}`, 0.7)
          ],
          extent: 'circle at center'
        })
      : radialGradient({
          colorStops: ['#fff 20%', rgba('#fff', 0.7)],
          extent: 'circle at center'
        }))}
  background-color: transparent;
  transition: 0.3s;
  opacity: ${({ state }) => (state === 'entered' ? 1 : 0)};
  backdrop-filter: ${props => !props.isClear && 'blur(10px)'};
  ${position}
`;

const Overlay = ({ isOpened, spinner, ...props }) => {
  return (
    <Transition
      in={isOpened}
      timeout={{
        appear: 300,
        enter: 0,
        exit: 300
      }}
      unmountOnExit
      mountOnEnter>
      {state => (
        <OverlayWrapper state={state} {...props}>
          {!!spinner === spinner && spinner === true ? (
            <svg
              width="8rem"
              height="8rem"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 100 100"
              preserveAspectRatio="xMidYMid"
              className="lds-ripple mb-4"
              style={{ background: 'none' }}>
              <circle
                cx="50"
                cy="50"
                r="25.9414"
                fill="none"
                stroke={`${props =>
                  props.dark || props.isDark
                    ? 'white'
                    : theme.colors.bluegrey}`}
                strokeWidth="1">
                <animate
                  attributeName="r"
                  calcMode="spline"
                  values="0;40"
                  keyTimes="0;1"
                  dur="1.2"
                  keySplines="0 0.2 0.8 1"
                  begin="-0.5s"
                  repeatCount="indefinite"></animate>
                <animate
                  attributeName="opacity"
                  calcMode="spline"
                  values=".3;0"
                  keyTimes="0;1"
                  dur="1.2"
                  keySplines="0.2 0 0.8 1"
                  begin="-0.5s"
                  repeatCount="indefinite"></animate>
              </circle>
              <circle
                cx="50"
                cy="50"
                r="40"
                fill="none"
                stroke={`${props =>
                  props.dark || props.isDark
                    ? 'white'
                    : theme.colors.bluegrey}`}
                strokeWidth="3">
                <animate
                  attributeName="r"
                  calcMode="spline"
                  values="0;40"
                  keyTimes="0;1"
                  dur="1.2"
                  keySplines="0 0.2 0.8 1"
                  begin="0s"
                  repeatCount="indefinite"></animate>
                <animate
                  attributeName="opacity"
                  calcMode="spline"
                  values=".5;0"
                  keyTimes="0;1"
                  dur="1.2"
                  keySplines="0.2 0 0.8 1"
                  begin="0s"
                  repeatCount="indefinite"></animate>
              </circle>
            </svg>
          ) : (
            spinner && spinner
          )}
          {props.children}
        </OverlayWrapper>
      )}
    </Transition>
  );
};

export default Overlay;

OverlayWrapper.defaultProps = {
  theme
};
