import React from 'react';
import PropTypes from 'prop-types';
import Icon from './Icon/';

class Pagination extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      steps: 3
    };
    this.prevPage = this.prevPage.bind(this);
    this.nextPage = this.nextPage.bind(this);
    this.gotoPage = this.gotoPage.bind(this);
  }
  componentDidUpdate = prevProps => {
    if (prevProps !== this.props) {
      // console.log('updated');
    }
  };
  createSteps = (current, total) => {
    let def = current;
    let Steps = [];
    if (def <= 4) {
      // console.log('s1');
      def = 3;
    } else if (def >= 3) {
      // console.log('s2');
      def = current;
      if (def >= total - 2) {
        // console.log('s3');
        def = total - 2;
      }
    }
    for (let i = def - 1; i < def + (this.state.steps - 1); i++) {
      let step = (
        <li key={i}>
          <a
            className={`nab-pagination-link ${
              i === current ? 'is-current' : ''
            } ${this.props.flat ? 'is-flat' : ''} ${
              this.props.inverted ? 'is-inverted' : ''
            }`}
            aria-label={`Goto page ${i}`}
            onClick={() => this.gotoPage(i, total)}>
            {i}
          </a>
        </li>
      );
      if (i < total) {
        Steps.push(step);
      }
    }
    return Steps;
  };
  prevPage = () => {
    let newPage = this.props.current;
    if (this.props.current > 1) {
      newPage -= 1;
    }
    this.props.onChange(newPage, this.props.total);
  };
  nextPage = () => {
    let newPage = this.props.current;
    if (this.props.current < this.props.total) {
      newPage += 1;
    }
    this.props.onChange(newPage, this.props.total);
  };
  gotoPage = (newPage, total) => {
    this.props.onChange(newPage, total);
  };
  render() {
    const {
      small,
      medium,
      large,
      flat,
      inverted,
      current,
      total,
      onChange,
      className,
      ...props
    } = this.props;

    return (
      <nav
        role="navigation"
        aria-label="pagination"
        className={`nab-pagination ${small ? 'is-small' : ''} ${
          medium ? 'is-medium' : ''
        } ${large ? 'is-large' : ''} ${className}`}>
        <a
          className={`nab-pagination-previous
					${flat ? 'is-flat' : ''} 
					${inverted ? 'is-inverted' : ''}`}
          onClick={this.prevPage}
          disabled={current === 1 ? true : false}>
          <Icon>arrow-left</Icon>
        </a>
        <a
          className={`nab-pagination-next 
					${flat ? 'is-flat' : ''} 
					${inverted ? 'is-inverted' : ''}`}
          onClick={this.nextPage}
          disabled={current === total ? true : false}>
          <Icon>arrow-right</Icon>
        </a>
        <ul className="nab-pagination-list">
          {total !== 1 && (
            <li>
              <a
                className={`nab-pagination-link 
							${current === 1 ? 'is-current' : ''}
							${flat ? 'is-flat' : ''} 
							${inverted ? 'is-inverted' : ''}`}
                aria-label="Goto page 1"
                onClick={() => this.gotoPage(1, total)}>
                1
              </a>
            </li>
          )}
          {total > 5 && current > 3 && (
            <li>
              <span className="nab-pagination-ellipsis">&hellip;</span>
            </li>
          )}

          {this.createSteps(current, total)}

          {total > 5 && current + 2 < total && (
            <li>
              <span className="nab-pagination-ellipsis">&hellip;</span>
            </li>
          )}
          <li>
            <a
              className={`nab-pagination-link ${
                current === total ? 'is-current' : ''
              }
							${flat ? 'is-flat' : ''} 
							${inverted ? 'is-inverted' : ''}`}
              aria-label={`Goto page ${total}`}
              onClick={() => this.gotoPage(total, total)}>
              {total}
            </a>
          </li>
        </ul>
      </nav>
    );
  }
}

export default Pagination;

Pagination.propTypes = {
  small: PropTypes.bool,
  medium: PropTypes.bool,
  large: PropTypes.bool,
  total: PropTypes.number,
  current: PropTypes.number
};

Pagination.defaultProps = {
  current: 1
};
