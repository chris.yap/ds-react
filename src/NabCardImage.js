import React from "react";
import PropTypes from "prop-types";
import cn from "classnames";

export default class CardImage extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const {
      aspectRatio,
      height,
      image,
      style,
      className,
      ...props
    } = this.props;
    let imageStyle;
    if (height) {
      imageStyle = {
        backgroundImage: image && `url(${image})`,
        height: `${height}px`,
        paddingTop: 0
      };
    } else if (aspectRatio) {
      imageStyle = {
        backgroundImage: image && `url(${image})`,
        height: `auto`,
        paddingTop: (1 / aspectRatio) * 100 + "%"
      };
    } else {
      imageStyle = {
        backgroundImage: image && `url(${image})`,
        height: `auto`,
        paddingTop: (1 / aspectRatio) * 100 + "%"
      };
    }
    return (
      <div
        className={cn("nab-card-image pos-r", className)}
        style={Object.assign(imageStyle, style)}
        {...props}
      >
        {props.children && (
          <div
            className="pos-a is-flex is-align-items-center"
            style={{ top: 0, left: 0, right: 0, bottom: 0 }}
          >
            {props.children}
          </div>
        )}
      </div>
    );
  }
}

CardImage.propTypes = {
  aspectRatio: PropTypes.string
};

CardImage.defaultProps = {
  aspectRatio: "2.75"
};
