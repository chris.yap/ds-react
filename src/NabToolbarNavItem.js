import React from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter, NavLink } from 'react-router-dom';
import cn from 'classnames';

const ToolbarNavItem = ({
  primary,
  secondary,
  isActive,
  stacked,
  to,
  href,
  className,
  onClick,
  ...props
}) => {
  // const Tag = to ? 'NavLink' : 'a';
  return (
    <React.Fragment>
      {onClick ? (
        <a
          onClick={onClick}
          href={href}
          className={cn(
            'nab-toolbar-item',
            primary ? 'is-primary' : '',
            secondary ? 'is-secondary' : '',
            stacked ? 'is-stacked' : '',
            className
          )}
          {...props}
        />
      ) : (
        <NavLink
          to={to}
          exact={true}
          activeClassName="is-active"
          className={cn(
            'nab-toolbar-item',
            primary ? 'is-primary' : '',
            secondary ? 'is-secondary' : '',
            stacked ? 'is-stacked' : '',
            className
          )}
          {...props}
        />
      )}
    </React.Fragment>
  );
};

export default ToolbarNavItem;

ToolbarNavItem.propTypes = {
  primary: PropTypes.bool,
  secondary: PropTypes.bool,
  stacked: PropTypes.bool,
  to: PropTypes.string,
  href: PropTypes.string,
  className: PropTypes.node,
  onClick: PropTypes.func
};

ToolbarNavItem.defaultProps = {
  to: '#',
  href: null
};
