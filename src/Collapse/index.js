import React from 'react';
import styled from 'styled-components';
import { Transition } from 'react-transition-group';
import { space } from 'styled-system';

const Container = styled.div`
  overflow: ${p => (p.state === 'entered' ? 'visible' : 'hidden')};
  transition: ${props => props.duration + 'ms ease-in-out'};
  height: ${p => (p.state === 'entered' ? 'auto' : '0')};
  ${space}
`;

const Wrapper = styled.div`
  display: flex;
`;

const InnerWrapper = styled.div`
  width: 100%;
`;

class Collapse extends React.Component {
  wrapper = null;
  widthZeroTimer = null;

  componentWillUnmount() {
    if (this.widthZeroTimer) {
      clearTimeout(this.widthZeroTimer);
    }
  }

  getWrapperHeight = () => {
    return this.wrapper ? this.wrapper.clientHeight : 0;
  };

  handleEnter = node => {
    const wrapperHeight = this.getWrapperHeight();
    node.style.height = `${wrapperHeight}px`;
  };

  handleEntering = node => {
    const wrapperHeight = this.getWrapperHeight();
    node.style.height = `${wrapperHeight}px`;
  };

  handleEntered = node => {
    node.style.height = `auto`;
  };

  handleExit = node => {
    const wrapperHeight = this.getWrapperHeight();
    node.style.height = `${wrapperHeight}px`;
  };

  handleExiting = node => {
    // By putting this inside a timeout block
    // we ensure the height we set in handleExit will be flushed to the dom by React.
    // If we don't then React will actually batch the style updates from
    // handleExit and this one together, resulting in a height change from "auto" to "0px"
    // which will break the CSS transition.
    this.widthZeroTimer = setTimeout(() => {
      node.style.height = '0px';
    }, 0);
  };

  render() {
    const { children, duration, isOpened, ...props } = this.props;

    return (
      <Transition
        onEnter={this.handleEnter}
        onEntering={this.handleEntering}
        onEntered={this.handleEntered}
        onExit={this.handleExit}
        onExiting={this.handleExiting}
        onExited={this.handleExited}
        in={isOpened}
        timeout={duration}>
        {(state, childProps) => {
          return (
            <Container
              state={state}
              style={{ minHeight: '0px' }}
              duration={duration}
              {...props}
              {...childProps}>
              <div
                ref={node => {
                  this.wrapper = node;
                }}>
                <Wrapper>
                  <InnerWrapper>{children}</InnerWrapper>
                </Wrapper>
              </div>
            </Container>
          );
        }}
      </Transition>
    );
  }
}

export default Collapse;

Collapse.defaultProps = {
  duration: 300
};
