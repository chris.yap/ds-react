import styled from 'styled-components';
import { composed } from './Theme/Composed';

const Footer = styled.footer`
  background-color: black;
  padding: 3rem 1.5rem 4rem;
  position: ${props => props.fixed && 'fixed'};
  bottom: ${props => props.fixed && '0'};
  width: ${props => props.fixed && '100%'};
  * {
    color: ${props => props.dark && 'white'};
  }
  ${composed};
`;

export default Footer;
