import React from 'react';
import PropTypes from 'prop-types';

// Opera 8.0+
const isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

// Firefox 1.0+
const isFirefox = typeof InstallTrigger !== 'undefined';

// Safari 3.0+ "[object HTMLElementConstructor]"
const isSafari =
  /constructor/i.test(window.HTMLElement) ||
  (p => {
    return p.toString() === '[object SafariRemoteNotification]';
  })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));

// Internet Explorer 6-11
const isIE = /*@cc_on!@*/ false || !!document.documentMode;

// Edge 20+
const isEdge = !isIE && !!window.StyleMedia;

// Chrome 1 - 71
const isChrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);

// Blink engine detection
// const isBlink = (isChrome || isOpera) && !!window.CSS;

// Device detection
const userAgent = navigator.userAgent || navigator.vendor || window.opera;
const isAndroid = /android/i.test(userAgent);
const isIos = /iPad|iPhone|iPod/.test(userAgent) && !window.MSStream;
const isWindows = navigator.appVersion.indexOf('Win') != -1;
const isMac = navigator.appVersion.indexOf('Mac') != -1;

const getDisplayName = ComposedComponent => {
  return ComposedComponent.displayName || ComposedComponent.name || 'Component';
};

const WhichBrowser = ComposedComponent => {
  class Wrapper extends React.Component {
    constructor(props) {
      super(props);
    }

    componentDidMount = () => {
      this.findOut();
    };

    findOut = () => {
      this.setState({
        isAndroid,
        isIos,
        isWindows,
        isMac,
        isOpera,
        isFirefox,
        isSafari,
        isIE,
        isEdge,
        isChrome
        // isBlink
      });
    };
    render() {
      return <ComposedComponent activeBrowser={this.state} {...this.props} />;
    }
  }
  Wrapper.propTypes = {
    activeBrowser: PropTypes.any
  };
  Wrapper.displayName = `${getDisplayName(ComposedComponent)}`;
  return Wrapper;
};

export default WhichBrowser;
