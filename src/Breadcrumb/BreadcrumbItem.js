import React from 'react';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';
import theme from '../Theme';
import { composed } from '../Theme/Composed';

const BreadcrumbItemWrapper = styled.li`
  align-items: center;
  display: flex;
  &:first-child a {
    padding-left: 0;
  }
  &:before {
    content: '';
  }
  a {
    align-items: center;
    font-size: 0.95em;
    display: flex;
    justify-content: center;
    padding: 0 0.75em;
    font-weight: ${props => !props.active && '600'};
    color: ${props => props.active && props.theme.colors.blacks[3]};
    cursor: ${props => props.active && 'default'};
    pointer-events: ${props => props.active && 'none'};
    text-decoration: ${props => props.active && 'none'};
    transition: ${props => props.theme.transitionDelays.medium}
    &:hover {
      color: ${props => props.theme.colors.primary};
    }
  }
  & + li::before {
    color: ${props => props.theme.colors.blacks[1]};
    font-size: .75rem;
    line-height: 1;
    /* content: '>'; */
    content: ${props => props.divider && `'${props.divider}'`};
  }
  ${composed};
`;

const BreadcrumbItem = ({ href, icon, divider, ...props }) => {
  let link;
  if (icon) {
    link = (
      <NavLink to={href}>
        <span className="nab-icon is-small">
          <i className={`icon-` + icon} />
        </span>
        <span>{props.children}</span>
      </NavLink>
    );
  } else {
    link = <NavLink to={href}>{props.children}</NavLink>;
  }
  return (
    <BreadcrumbItemWrapper divider={divider} {...props}>
      {link}
    </BreadcrumbItemWrapper>
  );
};

export default BreadcrumbItem;

BreadcrumbItem.defaultProps = {
  divider: '>',
  theme
};
