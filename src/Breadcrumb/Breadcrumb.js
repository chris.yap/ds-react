import React from 'react';
import styled from 'styled-components';
import { composed } from '../Theme/Composed';
import { BrowserRouter, NavLink } from 'react-router-dom';

const BreadcrumbWrapper = styled.nav`
  user-select: none;
  &:not(:last-child) {
    margin-bottom: 1rem;
  }
  font-size: ${props =>
    props.small
      ? '.875rem'
      : props.medium
      ? '1.25rem'
      : props.large
      ? '1.5rem'
      : '1rem'};
  white-space: nowrap;
  ul,
  ol {
    align-items: flex-start;
    display: flex;
    flex-wrap: wrap;
    justify-content: ${props =>
      props.centered ? 'center' : props.right ? 'flex-end' : 'flex-start'};
  }
  ${composed}
`;

const Breadcrumb = ({ children, ...props }) => {
  return (
    <BreadcrumbWrapper aria-label="breadcrumbs" {...props}>
      <ul>
        {React.Children.map(children, child =>
          React.cloneElement(child, { divider: props.divider })
        )}
      </ul>
    </BreadcrumbWrapper>
  );
};

export default Breadcrumb;
