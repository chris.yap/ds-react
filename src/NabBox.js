import styled from 'styled-components';
import theme from './Theme';
import { composed } from './Theme/Composed';

const Box = styled.div`
  /* &:not(:last-child) {
    margin-bottom: 1rem;
  } */
  background-color: white;
  border-radius: 0.125rem;
  box-shadow: 0 2px 3px rgba(0, 0, 0, 0.1), 0 0 0 1px rgba(0, 0, 0, 0.1);
  color: ${props => props.theme.colors.text};
  display: block;
  padding: 1.25rem;
  ${composed}
`;

export default Box;

Box.defaultProps = {
  theme
};
