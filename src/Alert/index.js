import React from 'react';
import { Transition } from 'react-transition-group';
import { rgba } from 'polished';
import styled from 'styled-components';
import theme from '../Theme';
import { composed } from '../Theme/Composed';
import { mediaQueries } from '../Theme/breakpoints';

import Box from '../NabBox';
import Button from '../Button';
import Div from '../Div';
import Icon from '../Icon';
import Collapse from '../Collapse';

const Blob = styled(Div)`
  background-color: ${props =>
    props.inverted ? 'rgba(255,255,255,.2)' : 'rgba(0,0,0, 0.1)'};
  padding: 0.75em 1em;
  border-radius: 2px;
  border: ${props =>
    props.inverted
      ? '1px solid rgba(0, 0, 0,.08)'
      : '1px solid rgba(255,255,255, 0.2)'};
  white-space: -moz-pre-wrap !important; /* Mozilla, since 1999 */
  white-space: -webkit-pre-wrap; /*Chrome & Safari */
  white-space: -pre-wrap; /* Opera 4-6 */
  white-space: -o-pre-wrap; /* Opera 7 */
  white-space: pre-wrap; /* CSS3 */
  word-wrap: break-word; /* Internet Explorer 5.5+ */
  word-break: break-all;
  white-space: normal;
`;

const AlertWrapper = styled.div`
  &:not(:last-child) {
    margin-bottom: ${props =>
      props.isOpened && (({ state }) => state === 'entered' && '1rem')};
    ${composed}
  }
  background-color: ${props =>
    props.inverted
      ? props.success
        ? rgba(props.theme.colors.success, 0.1)
        : props.info
        ? rgba(props.theme.colors.info, 0.1)
        : props.warning
        ? rgba(props.theme.colors.warning, 0.1)
        : props.danger
        ? rgba(props.theme.colors.danger, 0.1)
        : rgba(props.theme.colors.secondary, 0.1)
      : props.success
      ? props.theme.colors.success
      : props.info
      ? props.theme.colors.info
      : props.warning
      ? props.theme.colors.warning
      : props.danger
      ? props.theme.colors.danger
      : props.theme.colors.secondary};
  border-radius: ${props => props.theme.radii[2]}px;
  padding: 1.25rem 2.5rem 1.25rem 1.5rem;
  color: ${props =>
    !props.inverted
      ? '#fff'
      : props.success
      ? props.theme.colors.success
      : props.info
      ? props.theme.colors.info
      : props.warning
      ? props.theme.colors.warning
      : props.danger
      ? props.theme.colors.danger
      : props.theme.colors.secondary};
  display: flex;
  overflow: hidden;
  transition: 0.3s;
  box-shadow: ${props =>
    props.inverted
      ? props.success
        ? `inset 0 0 5px 1px ${rgba(props.theme.colors.success, 0.1)}`
        : props.info
        ? `inset 0 0 5px 1px ${rgba(props.theme.colors.info, 0.1)}`
        : props.warning
        ? `inset 0 0 5px 1px ${rgba(props.theme.colors.warning, 0.1)}`
        : props.danger
        ? `inset 0 0 5px 1px ${rgba(props.theme.colors.danger, 0.1)}`
        : `inset 0 0 5px 1px ${rgba(props.theme.colors.secondary, 0.1)}`
      : props.success
      ? `0 0 8px 4px ${props.theme.colors.successes[1]}`
      : props.info
      ? `0 0 8px 4px ${props.theme.colors.infos[1]}`
      : props.warning
      ? `0 0 8px 4px ${props.theme.colors.warnings[1]}`
      : props.danger
      ? `0 0 8px 4px ${props.theme.colors.dangers[1]}`
      : `0 0 8px 4px ${props.theme.colors.bluegreys[1]}`};
  ${Box} {
    color: ${props =>
      !props.inverted
        ? '#fff'
        : props.success
        ? props.theme.colors.success
        : props.info
        ? props.theme.colors.info
        : props.warning
        ? props.theme.colors.warning
        : props.danger
        ? props.theme.colors.danger
        : props.theme.colors.secondary};
  }
  a {
    color: ${props =>
      !props.inverted
        ? '#fff'
        : props.success
        ? props.theme.colors.success
        : props.info
        ? props.theme.colors.info
        : props.warning
        ? props.theme.colors.warning
        : props.danger
        ? props.theme.colors.danger
        : props.theme.colors.secondary};
    text-decoration: underline;
    font-weight: inherit;
    &:hover {
      color: ${props =>
        !props.inverted
          ? rgba('#fff', 0.8)
          : props.success
          ? rgba(props.theme.colors.success, 0.8)
          : props.info
          ? rgba(props.theme.colors.info, 0.8)
          : props.warning
          ? rgba(props.theme.colors.warning, 0.8)
          : props.danger
          ? rgba(props.theme.colors.danger, 0.8)
          : rgba(props.theme.colors.secondary, 0.8)};
    }
  }

  position: ${props => (!props.inverted && !props.demo ? 'fixed' : 'relative')};
  top: ${props => !props.inverted && !props.demo && '1rem'};
  right: ${props => !props.inverted && !props.demo && '1rem'};
  width: ${props => !props.inverted && !props.demo && 'calc(100% - 2rem)'};
  z-index: ${props => !props.inverted && !props.demo && 9999};
  ${mediaQueries[1]} {
    width: ${props => !props.inverted && !props.demo && '20rem'};
  }

  display: ${({ state }) => (state === 'exited' ? 'none' : 'flex')};
  opacity: ${({ state }) => (state === 'entered' ? 1 : 0)};

  height: ${props =>
    props.inverted && (({ state }) => state !== 'entered' && 0)};
  padding-top: ${props =>
    props.inverted && (({ state }) => state !== 'entered' && 0)};
  padding-bottom: ${props =>
    props.inverted && (({ state }) => state !== 'entered' && 0)};

  transform: ${props =>
    !props.inverted &&
    !props.demo &&
    (({ state }) =>
      state === 'entered' ? 'translateX(0%)' : 'translateX(110%)')};

  ${composed}
`;

const DeleteButton = styled.button`
  appearance: none;
  position: absolute;
  top: 0.5rem;
  right: 0.5rem;
  background-color: rgba(0, 0, 0, 0.2);
  border: none;
  border-radius: 290486px;
  cursor: pointer;
  display: inline-block;
  height: 16px;
  width: 16px;
  outline: none;
  transition: 0.5s;
  &::before,
  &::after {
    background-color: white;
    content: '';
    display: block;
    left: 50%;
    position: absolute;
    top: 50%;
    transform: translateX(-50%) translateY(-50%) rotate(45deg);
    transform-origin: center center;
  }
  &::before {
    height: 2px;
    width: 50%;
  }
  &::after {
    height: 50%;
    width: 2px;
  }
  &:hover,
  &:focus {
    background-color: rgba(0, 0, 0, 0.5);
  }
  &:active {
    background-color: rgba(0, 0, 0, 0.8);
  }
`;

const Alert = ({ icon, id, isOpened, moreInfo, onClose, ...props }) => {
  const [showMoreInfo, setShowMoreInfo] = React.useState(false);
  let iconContent;
  if (!icon) {
    iconContent = (
      <Icon className="mr-2">
        {props.success
          ? 'checked-circle'
          : props.warning || props.danger
          ? 'warning-triangle'
          : 'info-circle'}
      </Icon>
    );
  } else {
    if (icon !== true) {
      iconContent = <Icon className="mr-2">{icon}</Icon>;
    }
  }
  return (
    <Transition in={isOpened} timeout={500}>
      {state => (
        <AlertWrapper id={id} isOpened={isOpened} state={state} {...props}>
          {iconContent}
          <Box
            p={0}
            mb={0}
            bg="transparent"
            boxShadow="none"
            id={id && `${id}-message`}>
            {props.children}
            {moreInfo && (
              <React.Fragment>
                <Button
                  mx={0}
                  mb={showMoreInfo && 2}
                  flat
                  secondary={
                    props.inverted &&
                    !props.success &&
                    !props.info &&
                    !props.warning &&
                    !props.danger &&
                    true
                  }
                  success={props.success && true}
                  info={props.info && true}
                  warning={props.warning && true}
                  danger={props.danger && true}
                  white={!props.inverted && true}
                  small
                  onClick={() => setShowMoreInfo(showMoreInfo ? false : true)}>
                  <span>{showMoreInfo ? 'Hide' : 'Show'} info</span>
                  <Icon>{showMoreInfo ? 'arrow-up' : 'arrow-down'}</Icon>
                </Button>
                <Collapse isOpened={showMoreInfo}>
                  <Blob inverted={props.inverted}>{moreInfo}</Blob>
                </Collapse>
              </React.Fragment>
            )}
          </Box>
          {onClose && <DeleteButton onClick={onClose} />}
        </AlertWrapper>
      )}
    </Transition>
  );
};

export default Alert;

AlertWrapper.defaultProps = {
  theme
};
