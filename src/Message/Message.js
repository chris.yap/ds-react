import * as React from 'react';
import styled from 'styled-components';
import { composed } from '../Theme/Composed';
import Button from '../Button';
import Theme from '../Theme';
import Collapse from '../Collapse';
import { MessageHeaderWrapper, MessageDelete } from './MessageHeader';
import MessageBody from './MessageBody';

const MessageWrapper = styled.div`
  background-color: ${props =>
    props.danger
      ? props.theme.colors.dangers[0]
      : props.warning
      ? props.theme.colors.warnings[0]
      : props.info
      ? props.theme.colors.infos[0]
      : props.success
      ? props.theme.colors.successes[0]
      : props.primary
      ? props.theme.colors.primaries[0]
      : props.theme.colors.secondaries[0]};
  border-radius: ${props => props.theme.radii[2]}px;
  ${composed};

  ${MessageHeaderWrapper} {
    background-color: ${props =>
      props.danger
        ? props.theme.colors.danger
        : props.warning
        ? props.theme.colors.warning
        : props.info
        ? props.theme.colors.info
        : props.success
        ? props.theme.colors.success
        : props.primary
        ? props.theme.colors.primary
        : props.theme.colors.secondary};
    ${MessageDelete} {
      background-color: ${props =>
        props.danger
          ? props.theme.colors.dangers[7]
          : props.warning
          ? props.theme.colors.warnings[7]
          : props.info
          ? props.theme.colors.infos[7]
          : props.success
          ? props.theme.colors.successes[7]
          : props.primary
          ? props.theme.colors.primaries[7]
          : props.theme.colors.secondaries[7]};
      color: white;
      &:hover {
        background-color: ${props =>
          props.danger
            ? props.theme.colors.dangers[9]
            : props.warning
            ? props.theme.colors.warnings[9]
            : props.info
            ? props.theme.colors.infos[9]
            : props.success
            ? props.theme.colors.successes[9]
            : props.primary
            ? props.theme.colors.primaries[9]
            : props.theme.colors.secondaries[9]};
        color: white;
      }
    }
  }

  ${MessageBody} {
    border-color: ${props =>
      props.danger
        ? props.theme.colors.dangers[3]
        : props.warning
        ? props.theme.colors.warnings[3]
        : props.info
        ? props.theme.colors.infos[3]
        : props.success
        ? props.theme.colors.successes[3]
        : props.primary
        ? props.theme.colors.primaries[3]
        : props.theme.colors.secondaries[3]};
    color: ${props =>
      props.danger
        ? props.theme.colors.dangers[7]
        : props.warning
        ? props.theme.colors.warnings[7]
        : props.info
        ? props.theme.colors.infos[7]
        : props.success
        ? props.theme.colors.successes[7]
        : props.primary
        ? props.theme.colors.primaries[7]
        : props.theme.colors.secondaries[7]};
    a {
      color: ${props =>
        props.danger
          ? props.theme.colors.dangers[7]
          : props.warning
          ? props.theme.colors.warnings[7]
          : props.info
          ? props.theme.colors.infos[7]
          : props.success
          ? props.theme.colors.successes[7]
          : props.primary
          ? props.theme.colors.primaries[7]
          : props.theme.colors.secondaries[7]};
    }
  }
`;

class Message extends React.Component {
  renderChildren = () => {
    const { children } = this.props;
    return React.Children.map(children, child =>
      React.cloneElement(child, {
        onClose: this.props.onClose
      })
    );
  };
  render = () => {
    const { isOpened, ...props } = this.props;
    return (
      <Collapse isOpened={isOpened}>
        <MessageWrapper {...props}>{this.renderChildren()}</MessageWrapper>
      </Collapse>
    );
  };
}

export default Message;

MessageWrapper.defaultProps = {
  Theme
};
