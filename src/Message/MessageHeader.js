import * as React from 'react';
import styled from 'styled-components';
import { composed } from '../Theme/Composed';
import Theme from '../Theme';
import Button from '../Button';
import Div from '../Div';
import Icon from '../Icon';
import MessageBody from './MessageBody';

const MessageHeaderWrapper = styled.div`
  align-items: center;
  border-radius: ${props =>
    `${props.theme.radii[2]}px ${props.theme.radii[2]}px 0 0`};
  color: white;
  display: flex;
  font-weight: ${props => props.theme.fontWeights.bold};
  justify-content: space-between;
  line-height: 1.25;
  padding: 0.75em 1em;
  position: relative;
  ${composed};

  & + ${MessageBody} {
    border-width: 0;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
  }
`;

const MessageDelete = styled(Button)`
  position: relative;
  &:before,
  &:after {
    content: '';
    background-color: #fff;
    display: block;
    left: 50%;
    position: absolute;
    top: 50%;
    transform: translateX(-50%) translateY(-50%) rotate(45deg);
    transform-origin: center center;
  }
  &:before {
    height: 2px;
    width: 50%;
  }
  &:after {
    height: 50%;
    width: 2px;
  }
`;

const MessageHeader = ({ icon, onClose, ...props }) => (
  <MessageHeaderWrapper>
    {icon ? (
      <Div display="flex" alignItems="center">
        <Icon small className="mr-2">
          {icon}
        </Icon>
        {props.children}
      </Div>
    ) : (
      props.children
    )}
    {onClose && (
      <MessageDelete
        round
        inverted
        small
        height="20px"
        width="20px"
        onClick={onClose}
      />
    )}
  </MessageHeaderWrapper>
);

export { MessageHeader, MessageHeaderWrapper, MessageDelete };

MessageHeader.defaultProps = {
  Theme
};
