import styled from 'styled-components';
import { composed } from '../Theme/Composed';
import Theme from '../Theme';

const MessageBody = styled.div`
  border-top-left-radius: ${props => props.theme.radii[2]}px;
  border-top-right-radius: ${props => props.theme.radii[2]}px;
  border-bottom-left-radius: ${props => props.theme.radii[2]}px;
  border-bottom-right-radius: ${props => props.theme.radii[2]}px;
  border-style: solid;
  border-width: 0 0 0 4px;
  padding: 1.25em 1.5em;
  ${composed};
  a {
    text-decoration: underline;
  }
`;

export default MessageBody;

MessageBody.defaultProps = {
  Theme
};
