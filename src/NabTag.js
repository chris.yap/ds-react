import React from 'react';
import styled from 'styled-components';
import { composed } from './Theme/Composed';
import theme from './Theme';

const sizes = {
  small: '20px',
  default: '24px',
  medium: '32px',
  large: '40px'
};

const TagWrap = styled.span`
  box-sizing: border-box;
  align-items: center;
  line-height: 1;
  display: inline-flex;
  font-weight: 400 !important;
  justify-content: center;
  white-space: nowrap;
  letter-spacing: ${props => props.small && '.025em'};
  background-color: ${props =>
    props.white
      ? props.theme.colors.white
      : props.black
      ? props.theme.colors.black
      : props.primary
      ? props.theme.colors.primary
      : props.secondary
      ? props.theme.colors.secondary
      : props.success
      ? props.theme.colors.success
      : props.info
      ? props.theme.colors.info
      : props.warning
      ? props.theme.colors.warning
      : props.danger
      ? props.theme.colors.danger
      : props.theme.colors.bluegreys[1]};
  border-radius: ${props =>
    props.small
      ? `calc(${sizes.small} / 2)`
      : props.medium
      ? `calc(${sizes.medium} / 2)`
      : props.large
      ? `calc(${sizes.large} / 2)`
      : `calc(${sizes.default} / 2)`};
  color: ${props =>
    props.black ||
    props.primary ||
    props.secondary ||
    props.success ||
    props.info ||
    props.warning ||
    props.danger
      ? props.theme.colors.white
      : props.theme.colors.text};
  font-size: ${props =>
    props.small
      ? '0.675rem'
      : props.medium
      ? '1rem'
      : props.large
      ? '1.25rem'
      : '0.875rem'};
  height: ${props =>
    props.small
      ? sizes.small
      : props.medium
      ? sizes.medium
      : props.large
      ? sizes.large
      : sizes.default};
  padding: ${props =>
    props.small
      ? `calc(${sizes.small} / 2)`
      : props.medium
      ? `calc(${sizes.medium} / 2)`
      : props.large
      ? `calc(${sizes.large} / 2)`
      : `calc(${sizes.default} / 2)`};
  ${composed}
`;

const DeleteButton = styled.button`
  margin-left: ${props =>
    props.medium ? '.3rem' : props.large ? '.45rem' : '0.25rem'};
  margin-right: ${props =>
    props.small
      ? '-.35rem'
      : props.medium
      ? '-.5rem'
      : props.large
      ? '-.65rem'
      : '-0.45rem'};
  appearance: none;
  background-color: rgba(0, 0, 0, 0.2);
  border: none;
  border-radius: 290486px;
  cursor: pointer;
  display: inline-block;
  flex-grow: 0;
  flex-shrink: 0;
  font-size: 0;
  height: ${props =>
    props.small ? '14px' : props.medium || props.large ? '20px' : '16px'};
  width: ${props =>
    props.small ? '14px' : props.medium || props.large ? '20px' : '16px'};
  outline: none;
  position: relative;
  vertical-align: top;
  transition: 0.5s;
  &::before,
  &::after {
    background-color: white;
    content: '';
    display: block;
    left: 50%;
    position: absolute;
    top: 50%;
    transform: translateX(-50%) translateY(-50%) rotate(45deg);
    transform-origin: center center;
  }
  &::before {
    height: 2px;
    width: 50%;
  }
  &::after {
    height: 50%;
    width: 2px;
  }
  &:hover,
  &:focus {
    background-color: rgba(0, 0, 0, 0.5);
  }
  &:active {
    background-color: rgba(0, 0, 0, 0.8);
  }
`;

const Tag = ({ children, onClose, small, medium, large, ...props }) => {
  return (
    <TagWrap small={small} medium={medium} large={large} {...props}>
      {children}
      {onClose && (
        <DeleteButton
          small={small}
          medium={medium}
          large={large}
          onClick={onClose}
        />
      )}
    </TagWrap>
  );
};

export default Tag;

Tag.defaultProps = {
  theme
};

TagWrap.defaultProps = {
  theme
};
