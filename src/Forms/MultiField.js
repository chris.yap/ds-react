import React from 'react';
import cn from 'classnames';
import Column from '../NabColumn';
import Columns from '../NabColumns';
import Label from '../NabLabel';
import styled from 'styled-components';
import InputWrapper from './InputWrapper';

const MultiFieldWrapper = styled.div`
  position: relative;
  &:not(:last-child) {
    margin-bottom: 1rem;
  }
  .nab-columns {
    .nab-column:first-child {
      .nab-select__control {
        border-top-right-radius: 0 !important;
        border-bottom-right-radius: 0 !important;
      }
      ${InputWrapper} {
        border-top-right-radius: 0 !important;
        border-bottom-right-radius: 0 !important;
      }
    }
    .nab-column:last-child {
      .nab-select__control {
        margin-left: -1px;
        border-top-left-radius: 0 !important;
        border-bottom-left-radius: 0 !important;
      }
      ${InputWrapper} {
        margin-left: -1px;
        border-top-left-radius: 0 !important;
        border-bottom-left-radius: 0 !important;
      }
    }
  }
`;

const MultiField = ({
  label,
  leftClassName,
  leftSlot,
  required,
  rightClassName,
  rightSlot,
  className,
  ...props
}) => {
  return (
    <MultiFieldWrapper className={className || ''} {...props}>
      {label && (
        <Label className="nab-label">
          {label}
          {required && <sup>*</sup>}
        </Label>
      )}
      <Columns gapless mobile>
        <Column className={leftClassName}>{leftSlot}</Column>
        <Column className={rightClassName}>{rightSlot}</Column>
      </Columns>
    </MultiFieldWrapper>
  );
};
export default MultiField;
