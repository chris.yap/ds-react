import React from 'react';
import styled from 'styled-components';
import { rgba } from 'polished';
import nextId from 'react-id-generator';
import Label from './Label';
import CheckboxRadioWrapper from './CheckboxRadioWrapper';
import theme from '../Theme';

const CheckboxLabel = styled(Label)`
  text-transform: none;
  font-size: inherit;
  position: relative;
  cursor: pointer;
  transition: 0.3s ease;
  margin: ${props =>
    props.buttons ? '0' : props.switch ? '.25rem 0' : '0 1.5rem 0 1.75rem'};
  padding: ${props =>
    props.buttons && props.rounded ? '0 1.25em' : props.buttons && '0 .75em'};
  padding-left: ${props =>
    props.switch && props.right ? '0' : props.switch && '4.25rem'};
  padding-right: ${props => props.switch && '4.25em'};
  width: ${props => props.switch && props.fullwidth && '100%'};
  height: ${props => (props.buttons ? '2.375em' : props.switch && '2em')};
  display: ${props => (props.switch || props.buttons) && 'inline-flex'};
  align-items: ${props => (props.switch || props.buttons) && 'center'};
  color: ${props =>
    props.switch
      ? props.theme.colors.text
      : props.buttons && props.theme.colors.secondary};
  background-color: ${props =>
    props.buttons && rgba(props.theme.colors.secondary, 0.05)};
  box-shadow: ${props => props.buttons && 'inset 0 2px 0 rgba(0, 0, 0, 0.1)'};
  margin-bottom: ${props => props.buttons && '0.5rem'};
  box-sizing: border-box;

  &:before {
    top: ${props => props.switch && 0};
    bottom: ${props => props.switch && 0};
    left: ${props =>
      props.switch && props.right ? 'auto' : props.switch ? '0' : '-1.75rem'};
    right: ${props => props.switch && 0};
    width: ${props => (props.switch ? '3.5rem' : '1.25rem')};
    height: ${props => !props.switch && '1.25rem'};
    border: ${props =>
      props.switch
        ? `1px solid ${rgba('black', 0.2)}`
        : `2px solid ${rgba(props.theme.colors.secondary, 0.5)}`};
    border-radius: ${props => (props.switch ? '1.125em' : '0.25rem')};
    background-color: ${props =>
      props.switch && rgba(props.theme.colors.secondary, 0.05)};
    box-shadow: ${props =>
      props.switch && `inset 0 2px 0 ${rgba('black', 0.1)}`};
  }
  &:after {
    width: ${props => (props.switch ? '1.75em' : '0.75rem')};
    height: ${props => (props.switch ? '1.5em' : '0.35rem')};
    left: ${props =>
      props.switch && props.right
        ? 'auto'
        : props.switch
        ? '.25em'
        : '-1.5rem'};
    right: ${props => props.switch && props.right && '.25em'};
    top: ${props => !props.switch && '0.4rem'};
    border: ${props =>
      props.switch ? `1px solid ${rgba('black', 0.2)}` : '0 solid #fff'};
    border-radius: ${props => props.switch && '2em'};
    transform: ${props =>
      props.switch && props.right
        ? 'translateX(-1.25em)'
        : props.switch
        ? 'translateX(0)'
        : 'rotate(-45deg)'};
    background-color: ${props => props.switch && 'white'};
    box-shadow: ${props => props.switch && `0 2px 0 0 ${rgba('black', 0.1)}`};
  }
  &:before,
  &:after {
    box-sizing: border-box;
    content: '';
    position: absolute;
    transition: 0.3s ease;
    display: ${props => props.buttons && 'none'};
  }
`;

const Input = styled.input.attrs({ type: 'checkbox' })`
  border: 0;
  clip: rect(0 0 0 0);
  clippath: inset(50%);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  white-space: nowrap;
  width: 1px;
  &:focus + ${CheckboxLabel} {
    &:before {
      border-width: 2px;
      border-color: ${props =>
        props.success
          ? rgba(props.theme.colors.success, 0.8)
          : props.info
          ? rgba(props.theme.colors.info, 0.8)
          : props.warning
          ? rgba(props.theme.colors.warning, 0.8)
          : props.danger
          ? rgba(props.theme.colors.danger, 0.8)
          : rgba(props.theme.colors.secondary, 0.8)};
      box-shadow: ${props =>
        props.success
          ? `0 0 6px 2px ${rgba(props.theme.colors.success, 0.5)}`
          : props.info
          ? `0 0 6px 2px ${rgba(props.theme.colors.info, 0.5)}`
          : props.warning
          ? `0 0 6px 2px ${rgba(props.theme.colors.warning, 0.5)}`
          : props.danger
          ? `0 0 6px 2px ${rgba(props.theme.colors.danger, 0.5)}`
          : `0 0 6px 2px ${rgba(props.theme.colors.secondary, 0.5)}`};
    }
  }
  &:checked + ${CheckboxLabel} {
    background: ${props => props.buttons && props.theme.colors.secondary};
    color: ${props => props.buttons && props.theme.colors.white};
    box-shadow: ${props =>
      props.buttons && `inset -1px 0 0 0 ${rgba('black', 0.3)}`};
    &:before {
      border: ${props =>
        !props.switch && `10px solid ${props.theme.colors.secondary}`};
      background-color: ${props =>
        props.switch &&
        (props.success
          ? props.theme.colors.success
          : props.info
          ? props.theme.colors.info
          : props.warning
          ? props.theme.colors.warning
          : props.danger
          ? props.theme.colors.danger
          : props.theme.colors.secondary)};
    }
    &:after {
      transform: ${props =>
        props.switch && props.right
          ? 'translateX(0)'
          : props.switch && 'translateX(1.25em)'};
      border: ${props => !props.switch && '2px solid #fff'};
      border-width: ${props => !props.switch && '0 0 2px 2px'};
      background-color: ${props => props.switch && 'white'};
    }
  }
  &:focus:not(:checked) + ${CheckboxLabel} {
    :after {
      background-color: ${props =>
        props.switch &&
        (props.success
          ? props.theme.colors.success
          : props.info
          ? props.theme.colors.info
          : props.warning
          ? props.theme.colors.warning
          : props.danger
          ? props.theme.colors.danger
          : props.theme.colors.secondary)};
    }
  }
  &[disabled] + ${CheckboxLabel} {
    opacity: 0.5;
    pointer-events: none;
    cursor: not-allowed;
  }
`;

const CheckboxWrapper = styled(CheckboxRadioWrapper)`
  display: ${props => props.stacked && 'flex'};
  min-height: 0;
  padding: 0 0 0.25em;
  &:last-child {
    padding-bottom: ${props => !props.stacked && 0};
  }
  &:first-child ${CheckboxLabel} {
    border-top-left-radius: ${props =>
      props.buttons && props.rounded ? '290486px' : props.buttons && '.25em'};
    border-bottom-left-radius: ${props =>
      props.buttons && props.rounded ? '290486px' : props.buttons && '.25em'};
  }
  &:last-child ${CheckboxLabel} {
    border-top-right-radius: ${props =>
      props.buttons && props.rounded ? '290486px' : props.buttons && '.25em'};
    border-bottom-right-radius: ${props =>
      props.buttons && props.rounded ? '290486px' : props.buttons && '.25em'};
  }
`;

export default class Checkbox extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    if (!(this.props && this.props.checkboxGroup)) {
      throw new Error(
        'The `Checkbox` component must be used as a child of `CheckboxGroup`.'
      );
    }
  }

  render() {
    const {
      checkboxGroup: {
        name,
        buttons,
        rounded,
        stacked,
        isSwitch,
        right,
        fullwidth,
        checkedValues,
        onChange,
        className
      },
      id,
      label,
      ...rest
    } = this.props;
    const optional = {};

    if (checkedValues) {
      optional.checked = checkedValues.indexOf(this.props.value) >= 0;
    }
    if (typeof onChange === 'function') {
      optional.onChange = onChange.bind(null, this.props.value);
    }

    const htmlId = nextId();
    return (
      <CheckboxWrapper
        fullwidth={fullwidth}
        buttons={buttons}
        rounded={rounded}
        stacked={stacked}
        className={rest.className || ''}>
        {/* <div
        className={cn(
          isSwitch ? 'nab-switch' : 'nab-checkbox',
          right ? 'is-right' : '',
          fullwidth && isSwitch ? 'is-fullwidth' : fullwidth ? 'is-flex-1' : '',
          rest.success ? 'is-success' : '',
          rest.info ? 'is-info' : '',
          rest.warning ? 'is-warning' : '',
          rest.danger ? 'is-danger' : '',
          rest.disabled ? 'is-disabled' : '',
          rest.className
        )}> */}
        <Input
          {...rest}
          buttons={buttons}
          rounded={rounded}
          name={name}
          switch={isSwitch}
          right={right}
          id={id ? id : htmlId}
          disabled={rest.disabled}
          {...optional}
        />
        <CheckboxLabel
          buttons={buttons}
          switch={isSwitch}
          rounded={rounded}
          right={right}
          fullwidth={fullwidth}
          className={className || ''}
          htmlFor={id ? id : htmlId}>
          {label}
        </CheckboxLabel>
      </CheckboxWrapper>
    );
  }
}

CheckboxLabel.defaultProps = {
  theme
};

Input.defaultProps = {
  theme
};

Label.defaultProps = {
  theme
};
