import styled from 'styled-components';
import { color } from 'styled-system';
import theme from '../Theme';

const Input = styled.input`
  appearance: none;
  border: none;
  outline: none;
  align-items: center;
  display: inline-flex;
  font-size: inherit;
  height: 2.375em;
  justify-content: flex-start;
  line-height: 1.5;
  padding: 0 calc(0.625em - 1px);
  position: relative;
  background-color: transparent;
  vertical-align: top;
  color: ${props => (props.dark ? 'white' : '#363636')};
  max-width: 100%;
  width: 100%;
  transition: 0.3s ease-out;
  ${color};

  ${props => props.type !== 'date'} {
    &::-webkit-calendar-picker-indicator {
      position: absolute;
      right: 0;
      top: 50%;
      width: 9px;
      height: 8px;
      transform: translateY(-50%);
      padding: 0 0.5em;
      background: transparent;
      color: ${props => props.theme.colors.secondary};
      cursor: pointer;
      appearance: none;
    }

    &::-webkit-inner-spin-button,
    &::-webkit-clear-button {
      display: none;
      appearance: none;
    }
  }
  &::-webkit-inner-spin-button {
    width: 10px;
    height: 10px;
    background: red;
    color: ${props => props.theme.colors.secondary};
    appearance: 'inner-spin-button';
  }
`;

export default Input;

Input.defaultProps = {
  theme
};
