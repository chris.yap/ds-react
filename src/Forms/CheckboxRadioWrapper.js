import styled from 'styled-components';

const CheckboxRadioWrapper = styled.div`
  display: inline-flex;
  align-items: center;
  line-height: 1.25;
  min-height: 2.25rem;
  position: relative;
  width: ${props => props.fullwidth && '100%'};
`;

export default CheckboxRadioWrapper;
