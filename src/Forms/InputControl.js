import styled from 'styled-components';

const InputControl = styled.div`
  font-size: 1rem;
  position: relative;
  text-align: left;
`;

export default InputControl;
