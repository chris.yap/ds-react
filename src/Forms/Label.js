import styled from 'styled-components';
import theme from '../Theme';

const Label = styled.label`
  &:not(:last-child) {
    margin-bottom: 0.25rem;
  }
  color: ${props => (props.whiteLabel ? 'white' : '#4c626c')};
  display: block;
  font-size: ${props => props.theme.fontSizes[6]}px;
  text-transform: uppercase;
  font-weight: ${props => props.theme.fontWeights.regular};
`;

export default Label;

Label.defaultProps = {
  theme
};
