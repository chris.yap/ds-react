import React from 'react'
import nextId from 'react-id-generator'
import Proptypes from 'prop-types'
import InputMask from 'react-input-mask'
import Icon from '../Icon'
import WhichBrowser from '../NabWhichBrowser'

import Field from './Field'
import Label from './Label'
import InputControl from './InputControl'
import InputWrapper from './InputWrapper'
import Input from './Input'
import Textarea from './Textarea'
import InputValidation from './InputValidation'
import Help from './HelpText'

class Textfield extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      type: 'text',
      focus: false,
      badInput: false,
      initialValue: null,
      internalChange: false,
      isClearing: false,
      error: false,
      errorMessage: null
    }
    if (this.props.type) this.state.type = this.props.type
    this.myRef = React.createRef()
  }

  componentDidMount() {
    this.setState({
      errorMessage: this.props.errorMessage,
      error: this.props.error
    })
  }

  componentDidUpdate(nextProps, nextState) {
    if (nextProps.error == true && nextState.error == false) {
      this.setState({
        error: nextProps.error
      })
    }
    if (nextProps.errorMessage !== nextState.errorMessage) {
      this.setState({
        errorMessage: nextProps.errorMessage
      })
    }
  }

  onFocus() {
    this.setState({ focus: !this.props.readonly })
    //only for android devices fix issue with input focus
    if (this.props.activeBrowser.isAndroid) {
      setTimeout(() => {
        const selectedInput = this.myRef || this.props.inputRef
        if (selectedInput) {
          selectedInput.current.scrollIntoView({
            behavior: 'smooth',
            block: 'center',
            inline: 'end'
          })
        }
      }, 500)
    }
  }

  onBlur() {
    this.setState({ focus: false })
  }

  render() {
    const {
      activeBrowser,
      appendBoxedIcon,
      appendIcon,
      autocomplete,
      backgroundColor,
      boxShadow,
      bg,
      border,
      centered,
      className,
      error,
      errorMessage,
      hint,
      id,
      inputRef,
      isDisabled,
      label,
      mask,
      maxLength,
      multiline,
      onChange,
      placeholder,
      prefix,
      prependBoxedIcon,
      prependIcon,
      required,
      rows,
      readonly,
      small,
      medium,
      large,
      success,
      suffix,
      validationIcon,
      underlined,
      value,
      dark,
      whiteLabel,
      ...props
    } = this.props
    let Tag = multiline ? 'textarea' : 'input'
    const { focus, type, autofocus, ...state } = this.state
    const htmlId = nextId()
    return (
      <Field
        focus={focus}
        error={error}
        success={success}
        readonly={readonly}
        className={className || ''}
        {...props}
      >
        {label && (
          <Label htmlFor={id || htmlId} whiteLabel={whiteLabel}>
            {label}
            {required && <sup>*</sup>}
          </Label>
        )}

        <InputControl>
          <InputWrapper
            border={border}
            boxShadow={boxShadow}
            bg={bg}
            backgroundColor={backgroundColor}
            focus={focus}
            prependIcon={prependIcon}
            prependBoxedIcon={prependBoxedIcon}
            appendIcon={appendIcon}
            appendBoxedIcon={appendBoxedIcon}
            prefix={prefix}
            suffix={suffix}
            multiline={multiline}
            success={success}
            error={error}
            dark={dark}
            underlined={underlined}
            small={small}
            medium={medium}
            large={large}
            centered={centered}
            disabled={isDisabled}
          >
            {prependIcon && <Icon small>{prependIcon}</Icon>}

            {prependBoxedIcon && (
              <Icon small className={`${prependBoxedIcon && 'is-boxed'}`}>
                {prependBoxedIcon}
              </Icon>
            )}

            {prefix && <span className='nab-input-prefix mr-1'>{prefix}</span>}

            {!props.children ? (
              mask && !multiline ? (
                <InputMask
                  autoComplete={autocomplete}
                  mask={mask}
                  maskChar=''
                  value={value}
                  onChange={onChange}
                  onFocus={() => this.onFocus()}
                  onBlur={() => this.onBlur()}
                  disabled={isDisabled}
                  readOnly={readonly}
                >
                  {inputProps => (
                    <Tag
                      type={type}
                      id={id || htmlId}
                      placeholder={placeholder}
                      required={required}
                      rows={multiline ? rows : null}
                      maxLength={maxLength}
                      ref={inputRef || this.myRef}
                      {...inputProps}
                      {...props}
                    />
                  )}
                </InputMask>
              ) : multiline ? (
                <Textarea
                  autoComplete={autocomplete}
                  type={type}
                  id={id || htmlId}
                  // className="is-multiline nab-textarea"
                  placeholder={placeholder}
                  required={required}
                  onFocus={() => this.onFocus()}
                  onBlur={() => this.onBlur()}
                  value={value}
                  onChange={onChange}
                  rows={multiline ? rows : null}
                  maxLength={maxLength}
                  disabled={isDisabled}
                  readOnly={readonly}
                  ref={inputRef || this.myRef}
                  {...props}
                />
              ) : (
                <Input
                  autoComplete={autocomplete}
                  type={type}
                  id={id || htmlId}
                  placeholder={placeholder}
                  required={required}
                  onFocus={() => this.onFocus()}
                  onBlur={() => this.onBlur()}
                  value={value}
                  onChange={onChange}
                  rows={multiline ? rows : null}
                  maxLength={maxLength}
                  disabled={isDisabled}
                  readOnly={readonly}
                  ref={inputRef || this.myRef}
                  dark={dark}
                  {...props}
                />
              )
            ) : (
              React.cloneElement(props.children, { id: id || htmlId })
            )}

            {/* Validation icon */}
            {validationIcon !== false ? (
              <InputValidation>
                {error && (
                  <Icon small white>
                    warning-triangle
                  </Icon>
                )}
                {success && (
                  <Icon small success>
                    checked
                  </Icon>
                )}
              </InputValidation>
            ) : (
              ''
            )}

            {appendIcon && (
              <span className='nab-icon is-small'>
                <i className={'icon-' + appendIcon} />
              </span>
            )}

            {appendBoxedIcon && (
              <span className='nab-icon is-boxed is-small'>
                <i className={'icon-' + appendBoxedIcon} />
              </span>
            )}

            {suffix && <span className='nab-input-suffix ml-1'>{suffix}</span>}
          </InputWrapper>

          {error && errorMessage ? (
            <Help error={error} focus={focus} id={id && `${id}-error`}>
              {errorMessage}
            </Help>
          ) : hint ? (
            <Help focus={focus} id={id && `${id}-hint`}>
              {hint}
            </Help>
          ) : (
            ''
          )}
        </InputControl>
      </Field>
    )
  }
}

export default WhichBrowser(Textfield)

Textfield.proptypes = {
  rows: Proptypes.number
}

Textfield.defaultProps = {
  rows: 5
}
