import styled from 'styled-components';
import Label from './Label';
import InputWrapper from './InputWrapper';
import theme from '../Theme';
import { flexbox, layout, space, position } from 'styled-system';

const Field = styled.div`
  position: relative;
  &:not(:last-child) {
    margin-bottom: 1rem;
    ${layout};
    ${space};
  }
  ${layout};
  ${space};
  ${position};
  ${flexbox};

  ${Label} {
    color: ${props => props.error && props.theme.colors.danger};
  }
  ${InputWrapper} {
    border-color: ${props =>
      props.success
        ? props.theme.colors.successes[2]
        : props.error && 'transparent'};
    background-color: ${props => props.error && props.theme.colors.danger};
    * {
      color: ${props => props.error && 'white'};
    }
  }
`;

export default Field;

Field.defaultProps = {
  theme
};
