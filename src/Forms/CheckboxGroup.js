import React from 'react';
import styled from 'styled-components';
import cn from 'classnames';
import Field from './Field';
import Checkbox from './Checkbox';
import Label from './Label';
import HelpText from './HelpText';

const Help = styled(HelpText)`
  opacity: 1;
  transform: translateY(-0.5em);
`;

export default class CheckboxGroup extends React.Component {
  constructor(props) {
    super(props);
    this._isControlledComponent = this._isControlledComponent.bind(this);
    this._onCheckboxChange = this._onCheckboxChange.bind(this);
    this.getValue = this.getValue.bind(this);
    this.state = {
      value: this.props.value || this.props.defaultValue || []
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.value !== this.props.value) {
      this.setState({
        value: this.props.value
      });
    }
  }

  _prepareBoxes = (children, maxDepth = 1, depth = 1) => {
    if (depth > maxDepth) {
      return children;
    }

    const checkboxGroup = {
      name: this.props.name,
      buttons: this.props.buttons,
      rounded: this.props.rounded,
      isSwitch: this.props.isSwitch,
      right: this.props.right,
      stacked: this.props.stacked,
      fullwidth: this.props.fullwidth,
      checkedValues: this.state.value,
      onChange: this._onCheckboxChange
    };

    return React.Children.map(children, child => {
      if (!(child && child.$$typeof)) {
        return child;
      } else if (
        child.type === Checkbox ||
        (child.type && child.type.prototype instanceof Checkbox)
      ) {
        return React.cloneElement(child, { checkboxGroup });
      } else {
        return React.cloneElement(
          child,
          {},
          child.props.children
            ? React.Children.map(child.props.children, c =>
                this._prepareBoxes(c, maxDepth, depth + 1)
              )
            : null
        );
      }
    });
  };

  render() {
    const {
      Component,
      buttons,
      stacked,
      rounded,
      fullWidth,
      fullwidth,
      isSwitch,
      right,
      label,
      help,
      name,
      value,
      onChange,
      required,
      children,
      checkboxDepth = 1,
      ...rest
    } = this.props;
    return (
      <Field {...rest}>
        {label && (
          <Label className={cn('nab-label', buttons ? '' : 'mb-0')}>
            {label}
            {required && <sup>*</sup>}
          </Label>
        )}
        <div className={cn(fullWidth || fullwidth ? 'is-flex' : '')}>
          {this._prepareBoxes(children, checkboxDepth)}
        </div>
        {help && <Help>{help}</Help>}
      </Field>
    );
  }

  getValue() {
    return this.state.value;
  }

  _isControlledComponent() {
    return Boolean(this.props.value);
  }

  _onCheckboxChange(checkboxValue, event) {
    let newValue;
    if (event.target.checked) {
      newValue = this.state.value && this.state.value.concat(checkboxValue);
    } else {
      newValue =
        this.state.value && this.state.value.filter(v => v !== checkboxValue);
    }

    if (this._isControlledComponent()) {
      this.setState({ value: this.props.value });
    } else {
      this.setState({ value: newValue });
    }

    if (typeof this.props.onChange === 'function') {
      this.props.onChange(newValue, event, this.props.name);
    }
  }
}
