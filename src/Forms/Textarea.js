import styled from 'styled-components';

const Textarea = styled.textarea`
  appearance: none;
  border: none;
  outline: none;
  display: block;
  font-size: 1rem;
  line-height: 1.5;
  padding: 0.625em;
  position: relative;
  background-color: transparent;
  color: #363636;
  max-width: 100%;
  width: 100%;
`;

export default Textarea;
