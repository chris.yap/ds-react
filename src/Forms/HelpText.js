import styled from 'styled-components';
import theme from '../Theme';

const Help = styled.div`
  display: block;
  font-size: ${props => props.theme.fontSizes[5]}px;
  width: 100%;
  letter-spacing: 0.025rem;
  color: ${props =>
    props.error
      ? props.theme.colors.dangers[4]
      : props.theme.colors.bluegreys[4]};
  margin-top: 0.25rem;
  transition: ${props => props.theme.transitionDelays.medium};
  position: relative;
  opacity: ${props => (props.focus || props.error ? '1' : '0')};
  transform: ${props =>
    props.focus || props.error ? 'translateY(0)' : 'translateY(-105%)'};
  z-index: 1;
`;

export default Help;

Help.defaultProps = {
  theme
};
