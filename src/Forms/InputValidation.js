import styled from 'styled-components';

const InputValidation = styled.div`
  position: relative;
  display: flex;
  height: 100%;
  align-items: center;
  .nab-icon.is-small {
    width: 2.25em;
  }
`;

export default InputValidation;
