import React from 'react';
import nextId from 'react-id-generator';
import DateTime from 'react-datetime';
import styled from 'styled-components';
import { rgba } from 'polished';
import { space, position } from 'styled-system';
import Breakpoints from '../Breakpoints';
import Button from '../Button';
import Div from '../Div';
import Icon from '../Icon';
import Label from './Label';
import InputWrapper from './InputWrapper';
import theme from '../Theme';

const DatePickerWrapper = styled.div`
  margin-bottom: 1rem;
  position: relative;

  .rdt {
    display: flex;
    position: absolute;
    top: 0;
    width: 100%;
    flex-direction: column;

    input {
      height: calc(2.375em - 2px);
      padding-left: 3.25em;
    }
    .rdtPicker {
      position: absolute;
      top: 50px;
      right: ${props => props.isRight && 0};
      margin-bottom: ${props => props.theme.space[4]}px;
      display: none;
      padding: ${props =>
        props.activeBreakpoints.below.sm ? '1em' : '1.25em 1.5em'};
      width: ${props => props.activeBreakpoints.below.sm && '100%'};
      min-width: 320px;
      background-color: white;
      box-shadow: 0 3.75px 6.25px 0 rgba(76, 98, 108, 0.1),
        0 2.5px 12.5px 0 rgba(76, 98, 108, 0.1);
      border-radius: ${props => props.theme.radii[2]}px;
      z-index: 1;
      &:before {
        content: '';
        position: absolute;
        top: -8px;
        left: ${props =>
          !props.isRight &&
          (props.activeBreakpoints.below.sm ? '1em' : '1.5em')};
        right: ${props =>
          props.isRight &&
          (props.activeBreakpoints.below.sm ? '1em' : '1.5em')};
        width: 0;
        height: 0;
        border-style: solid;
        border-width: 0 8px 8px 8px;
        border-color: ${props =>
          `transparent transparent ${props.theme.colors.primary} transparent`};
      }
      .rdtSwitch,
      .rdtPrev,
      .rdtNext {
        cursor: pointer;
        vertical-align: middle;
      }
      .rdtYears {
        .rdtSwitch {
          pointer-events: none;
        }
      }
      .rdtDays {
        table {
          thead {
            tr {
              &:last-child {
                th {
                  font-size: ${props => props.theme.fontSizes[2]};
                  font-weight: 400;
                  font-size: 0.75em;
                  padding: 0.5em 0;
                }
              }
            }
          }
        }
      }
      .rdtMonths,
      .rdtYears,
      .rdtTime {
        table {
          tbody {
            &:before {
              content: '@';
              display: block;
              line-height: 10px;
              text-indent: -99999px;
            }
          }
        }
      }
      .rdtDays .rdtTimeToggle,
      .rdtTime .rdtSwitch {
        font-weight: 600;
        font-size: 14px;
        padding: 1em 0 0.5em;
        text-align: center;
        cursor: pointer;
        position: relative;
        &:before {
          content: '';
          position: absolute;
          top: 0.5em;
          left: 0;
          width: 100%;
          height: calc(100% - 0.5em);
          border: 1px solid rgb(228, 231, 231);
          border-radius: ${props => props.theme.radii[1]}px;
          z-index: -1;
        }
        &:hover {
          &:before {
            background-color: ${props => props.theme.colors.secondaries[0]};
          }
        }
      }
      .rdtDays,
      .rdtMonths,
      .rdtYears,
      .rdtTime {
        table {
          width: 100%;
          thead {
            tr {
              th {
                text-align: center;
                vertical-align: middle;
                flex: 1;
                padding-bottom: 0.125em;
                &.rdtPrev,
                &.rdtNext {
                  position: relative;
                  height: 38px;
                  width: 40px;
                  cursor: pointer;
                  &:before {
                    content: '';
                    position: absolute;
                    left: 0;
                    top: 0;
                    width: 100%;
                    height: 100%;
                    /* background-color: red; */
                    z-index: -1;
                    border: 1px solid rgb(228, 231, 231);
                    border-radius: ${props => props.theme.radii[1]}px;
                  }
                  &:hover {
                    &.rdtPrev,
                    &.rdtNext {
                      &:before {
                        background-color: ${props =>
                          props.theme.colors.secondaries[0]};
                      }
                    }
                  }
                }
              }
            }
          }

          tbody {
            .rdtDay,
            .rdtMonth,
            .rdtYear {
              box-sizing: border-box;
              outline: none;
              cursor: pointer;
              font-size: 14px;
              height: 38px;
              width: 40px;
              font-weight: 600;
              text-align: center;
              vertical-align: middle;
              color: ${props => props.theme.colors.secondary};
              border-width: 1px;
              border-style: solid;
              border-color: rgb(228, 231, 231);
              border-image: initial;
              background: white;
              position: relative;
              &:hover:not(.rdtDisabled) {
                background-color: ${props => props.theme.colors.secondaries[0]};
              }
              &.rdtOld {
                font-weight: 300;
                color: ${props => rgba(props.theme.colors.blacks[3], 1)};
                /* cursor: not-allowed;
                pointer-events: none; */
              }
              &.rdtNew {
                font-weight: 300;
                color: ${props => rgba(props.theme.colors.secondaries[4], 1)};
              }
              &.rdtDisabled {
                cursor: not-allowed;
                /* pointer-events: none; */
                opacity: 0.5;
                &:before {
                  content: '';
                  position: absolute;
                  top: 0px;
                  left: 0px;
                  width: calc(100% - 0px);
                  height: calc(100% - 0px);
                  background: linear-gradient(
                    to top left,
                    rgba(0, 0, 0, 0) 0%,
                    rgba(0, 0, 0, 0) calc(50% - 0.8px),
                    rgba(0, 0, 0, 0.2) 50%,
                    rgba(0, 0, 0, 0) calc(50% + 0.8px),
                    rgba(0, 0, 0, 0) 100%
                  );
                }
              }
              &.rdtActive {
                color: white;
                border-style: double;
                background-color: ${props => props.theme.colors.secondary};
                border-color: ${props => props.theme.colors.secondaries[6]};
                pointer-events: none;
              }
            }
            .rdtMonth,
            .rdtYear {
              width: 60px;
            }
          }
        }
      }
    }
    &.rdtOpen {
      .rdtPicker {
        display: block;
      }
    }
    .rdtCounters {
      display: flex;
      align-items: center;
      justify-content: space-evenly;
      .rdtCounterSeparator {
        width: 20px;
        text-align: center;
      }
      .rdtCounter {
        width: 50px;
        font-size: 1.5em;
        text-align: center;
        .rdtCount {
          margin: 0.5em 0;
        }
        .rdtBtn {
          display: flex;
          margin: 0 auto;
          font-size: 8px;
          color: ${props => props.theme.colors.secondary};
          position: relative;
          height: 38px;
          width: 40px;
          align-items: center;
          justify-content: center;
          cursor: pointer;
          &:before {
            content: '';
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            z-index: -1;
            border: 1px solid rgb(228, 231, 231);
            border-radius: ${props => props.theme.radii[1]}px;
          }
          &:hover {
            &:before {
              background-color: ${props => props.theme.colors.secondaries[0]};
            }
          }
        }
      }
    }
  }
  ${position}
  ${space}
`;

const DatePicker = ({
  activeBreakpoints,
  dateFormat,
  id,
  inputProps,
  isClearable,
  label,
  onChange,
  onDatesChange,
  onFocusChange,
  placeholder,
  required,
  timeFormat,
  ...props
}) => {
  const htmlId = nextId();
  return (
    <DatePickerWrapper activeBreakpoints={activeBreakpoints} {...props}>
      {label && (
        <Label htmlFor={id || htmlId}>
          {label}
          {required && <sup className="pos-r has-top2">*</sup>}
        </Label>
      )}
      <InputWrapper>
        <Div
          height="2.375em"
          width="2.5em"
          borderRight="1px solid"
          borderColor="secondaries.1"
          display="flex"
          alignItems="center">
          <Icon small>{timeFormat && !dateFormat ? 'clock' : 'date'}</Icon>
        </Div>
        <DateTime
          closeOnSelect
          dateFormat={dateFormat}
          timeFormat={timeFormat}
          onChange={onChange}
          onDatesChange={onDatesChange}
          onFocusChange={onFocusChange}
          inputProps={{
            ...inputProps,
            id: id || htmlId,
            readOnly: true
          }}
          {...props}
        />
        {isClearable && (
          <Div
            position="absolute"
            right=".25em"
            display="flex"
            alignItems="center">
            <Button flat round small onClick={() => onChange('')}>
              <Icon>close</Icon>
            </Button>
          </Div>
        )}
      </InputWrapper>
    </DatePickerWrapper>
  );
};

export default Breakpoints(DatePicker);

// DatePickerWrapper.defaultProps = {};

DatePicker.defaultProps = {
  dateFormat: 'DD/MM/YYYY',
  timeFormat: false,
  theme
};
