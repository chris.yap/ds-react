import styled from 'styled-components';
// import { composed } from '../Theme/Composed';
import { color, border, shadow, space, layout } from 'styled-system';
import theme from '../Theme';
import InputValidation from './InputValidation';

const InputWrapper = styled.div`
  appearance: none;
  align-items: center;
  border-radius: ${props => (props.underlined ? '0' : props.theme.radii[2])}px;
  display: inline-flex;
  font-size: ${props =>
    props.small
      ? '.75rem'
      : props.medium
      ? '1.25rem'
      : props.large
      ? '1.5rem'
      : '1rem'};
  height: ${props => (props.multiline ? 'auto' : '2.375em')};
  justify-content: flex-start;
  line-height: 1.5;
  padding: 0;
  /* padding: ${props => (props.multiline ? '0' : '0 calc(0.625em - 1px)')}; */
  padding-left: ${props => props.prefix && 'calc(0.625em - 1px)'};
  padding-right: ${props => props.suffix && 'calc(0.625em - 1px)'};
  position: relative;
  vertical-align: top;
  background-color: ${props =>
    props.dark ? props.theme.colors.secondary : '#fff'};
  border: 1px solid;
  border-color: ${props =>
    props.focus && props.dark
      ? props.theme.colors.bluegreys[1]
      : props.dark
      ? props.theme.colors.bluegreys[0]
      : props.focus
      ? props.theme.colors.bluegreys[3]
      : props.theme.colors.bluegreys[1]};
  border-width: ${props => props.underlined && '0 0 2px 0'};
  box-shadow: ${props =>
    !props.underlined && props.focus
      ? `inset 0 1px 2px transparent, 0 0 0.25rem 0.125em ${props.theme.colors.bluegreys[1]}`
      : 'none'};
  color: ${props => (props.dark ? 'white' : '#363636')};
  max-width: 100%;
  width: 100%;
  transition: ${props => props.theme.transitionDelays.medium};
  z-index: 2;

  ${space};
  ${color};
  ${border};
  ${shadow};

  &:focus-within {
    box-shadow: ${props =>
      `inset 0 1px 2px transparent, 0 0 0.25rem 0.125em ${props.theme.colors.bluegreys[1]}`};
    border-color: ${props => props.theme.colors.bluegreys[3]};
  }

  input {
    appearance: none;
    border: none;
    outline: none;
    align-items: center;
    display: inline-flex;
    font-size: inherit;
    justify-content: flex-start;
    text-align: ${props => props.centered && 'center'};
    line-height: 1.5;
    padding: ${props => (props.centered ? '0' : '0 calc(0.625em - 1px)')};
    position: relative;
    background-color: transparent;
    vertical-align: top;
    color: ${props => (props.dark ? 'white' : '#363636')};
    max-width: 100%;
    width: 100%;
    transition: 0.3s ease-out;
    &::placeholder {
      color: ${props =>
        props.dark
          ? 'rgba(255,255,255,0.6)'
          : props.error && props.theme.colors.dangers[2]};
    }
    
    &:-webkit-autofill,
    &:-webkit-autofill:hover,
    &:-webkit-autofill:focus,
    &:-webkit-autofill:active {
      -webkit-transition: "color 9999s ease-out, transparent 9999s ease-out";
      -webkit-transition-delay: 9999s;
    }
  }

  .nab-icon.is-small {
    color: ${props => props.theme.colors.bluegrey};
    width: 1.5em;
    justify-content: flex-end;
    &.is-boxed {
      justify-content: center;
      height: 100%;
      width: 2.375em;
      border-right: 1px solid;
      border-color: ${props =>
        props.success
          ? props.theme.colors.successes[2]
          : props.error
          ? props.theme.colors.dangers[2]
          : props.theme.colors.bluegreys[1]};
    }
  }
  ${InputValidation} {
    > .nab-icon.is-small {
      justify-content: center;
    }
    & + .nab-icon.is-small {
      justify-content: flex-start;
      &.is-boxed {
        justify-content: center;
        border-right: 0px solid;
        border-left: 1px solid;
        border-color: ${props =>
          props.success
            ? props.theme.colors.successes[2]
            : props.error
            ? props.theme.colors.dangers[2]
            : props.theme.colors.bluegreys[1]};
      }
    }
  }
`;

export default InputWrapper;

InputWrapper.defaultProps = {
  theme
};
