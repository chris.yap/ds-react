import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const OverflowWrapper = styled.div`
  overflow: ${props => (props.overflow ? 'auto' : 'visible')};
`;

export class DetectOverflow extends Component {
  static propTypes = {
    onOverflowChange: PropTypes.func,
    children: PropTypes.node,
    style: PropTypes.object,
    className: PropTypes.string
  };

  static defaultProps = {
    style: {}
  };

  constructor(props) {
    super(props);
    this.isOverflowed = false;
    this.domElement = null;
    this.setDOMElement = this.setDOMElement.bind(this);
    this.checkOverflow = this.checkOverflow.bind(this);
  }

  componentDidMount() {
    this.checkOverflow();
  }

  componentDidUpdate() {
    this.checkOverflow();
  }

  setDOMElement(domElement) {
    this.domElement = domElement;
  }

  checkOverflow() {
    const isOverflowed =
      this.domElement.scrollWidth > this.domElement.clientWidth ||
      this.domElement.scrollHeight > this.domElement.clientHeight;

    if (isOverflowed !== this.isOverflowed) {
      this.isOverflowed = isOverflowed;
      if (this.props.onOverflowChange) {
        this.props.onOverflowChange(isOverflowed);
      }
    }
  }

  render() {
    const { style, className, overflow, children } = this.props;
    return (
      <OverflowWrapper
        ref={this.setDOMElement}
        style={{ ...style, position: 'relative' }}
        overflow={overflow ? 1 : 0}
        className={className}>
        {children}
        <ResizeDetector onResize={this.checkOverflow} />
      </OverflowWrapper>
    );
  }
}

const OBJECT_STYLE = {
  display: 'block',
  position: 'absolute',
  top: 0,
  left: 0,
  height: '100%',
  width: '100%',
  overflow: 'hidden',
  pointerEvents: 'none',
  zIndex: -1
};

export class ResizeDetector extends Component {
  static propTypes = {
    onResize: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      isMounted: false
    };
    this.setDOMElement = this.setDOMElement.bind(this);
    this.handleLoad = this.handleLoad.bind(this);
  }

  componentDidMount() {
    this.setState({
      isMounted: true
    });
  }

  componentWillUnmount() {
    if (this.domElement.contentDocument) {
      this.domElement.contentDocument.defaultView.removeEventListener(
        'resize',
        this.props.onResize
      );
    }
  }

  setDOMElement(domElement) {
    this.domElement = domElement;
  }

  handleLoad() {
    this.domElement.contentDocument.defaultView.addEventListener(
      'resize',
      this.props.onResize
    );
  }

  render() {
    return (
      <object
        style={OBJECT_STYLE}
        type="text/html"
        data={this.state.isMounted ? 'about:blank' : null}
        ref={this.setDOMElement}
        onLoad={this.handleLoad}
      />
    );
  }
}
