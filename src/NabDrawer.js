import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import { CSSTransition, Transition } from 'react-transition-group';
import Breakpoints from './Breakpoints/';
import Icon from './Icon/';

class Drawer extends React.Component {
  componentDidMount() {
    this.handleResize();
    // setTimeout(() => {
    if (this.props.variant !== 'temporary' && this.props.isOpened === true) {
      if (this.props.right) {
        document.body.style.paddingRight = this.checkWidth(this.props.width);
      } else {
        document.body.style.paddingLeft = this.checkWidth(this.props.width);
      }
    }
    // }, 100);
  }

  componentDidUpdate(prevProps) {
    if (this.props.isOpened !== prevProps.isOpened) {
      if (this.props.variant !== 'temporary') {
        if (this.props.isOpened === true) {
          if (this.props.right) {
            document.body.style.paddingRight = this.checkWidth(
              this.props.width
            );
          } else {
            document.body.style.paddingLeft = this.checkWidth(this.props.width);
          }
        } else {
          if (this.props.right) {
            document.body.style.paddingRight = '0';
          } else {
            document.body.style.paddingLeft = '0';
          }
        }
      }
    }
  }
  handleResize = () => {
    // this.setState({ windowWidth: window.innerWidth });
    // if (window.innerWidth < 769) {
    if (this.props.activeBreakpoints.below.md) {
      this.setState({ variant: 'temporary' });
    } else {
      this.setState({ variant: this.props.variant });
    }
  };
  checkWidth(value) {
    var newWidth = null;
    if (value) {
      newWidth = value.toString();
      newWidth.indexOf('%') !== -1
        ? (newWidth = value)
        : newWidth.indexOf('px') !== -1
        ? (newWidth = value)
        : (newWidth = value + 'px');
    }
    return newWidth;
  }
  render() {
    const {
      activeBreakpoints,
      children,
      className,
      contentClassName,
      elevation,
      hamburger,
      hamburgerIcon,
      isOpened,
      logo,
      onClose,
      overlay,
      right,
      style,
      toggleDrawer,
      variant,
      width,
      ...props
    } = this.props;
    const left = !right ? true : false;
    const widthSize = {
      width: this.checkWidth(width),
      transition: '500ms ease',
      transform:
        right && isOpened
          ? 'translateX(0)'
          : right && !isOpened
          ? 'translateX(100%)'
          : left && isOpened
          ? 'translateX(0)'
          : 'translateX(-100%)'
    };
    const burgerStyle = {
      color: variant === 'temporary' && isOpened ? '#ffffff' : '#4c626c'
    };
    return (
      <React.Fragment>
        <CSSTransition
          in={!!(variant === 'temporary' && isOpened && overlay)}
          timeout={500}
          classNames="fade"
          mountOnEnter>
          <div className={cn('nab-drawer-overlay')} onClick={toggleDrawer} />
        </CSSTransition>
        {/* <CSSTransition
          in={isOpened}
          timeout={500}
          classNames={right ? 'slide-from-right' : 'slide-from-left'}
        > */}
        <aside
          className={cn(
            'nab-drawer',
            elevation ? 'has-elevation-' + elevation : '',
            right ? 'is-right' : 'is-left',
            className
          )}
          style={{ ...style, ...widthSize }}
          {...props}>
          {hamburger && variant !== 'permanent' && (
            <a
              role="button"
              aria-label="menu"
              aria-expanded="false"
              className="nab-burger"
              onClick={toggleDrawer}
              style={{
                left:
                  isOpened && left && activeBreakpoints.below.sm
                    ? 'calc(100vw - 68px)'
                    : isOpened && left
                    ? width - 68 + 'px'
                    : left && activeBreakpoints.below.sm
                    ? '100vw'
                    : left
                    ? width + 'px'
                    : null,
                right:
                  isOpened && right
                    ? width - 68 + 'px'
                    : right
                    ? width + 'px'
                    : null,
                top: 0
              }}>
              {!hamburgerIcon ? (
                <React.Fragment>
                  <span aria-hidden="true" />
                  <span aria-hidden="true" />
                  <span aria-hidden="true" />
                </React.Fragment>
              ) : (
                <Icon>{hamburgerIcon}</Icon>
              )}
            </a>
          )}
          <div className={cn('nab-drawer-content', contentClassName)}>
            {children}
          </div>
        </aside>
        {/* </CSSTransition> */}
      </React.Fragment>
    );
  }
}

export default Breakpoints(Drawer);

Drawer.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  elevation: PropTypes.number,
  isOpened: PropTypes.bool,
  left: PropTypes.bool,
  logo: PropTypes.bool,
  onClose: PropTypes.func,
  overlay: PropTypes.bool,
  right: PropTypes.bool,
  variant: PropTypes.oneOf(['permanent', 'persistent', 'temporary']),
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
};

Drawer.defaultProps = {
  elevation: 16,
  isOpened: false,
  overlay: true,
  variant: 'temporary',
  width: 320
};
