import React from 'react';
import cn from 'classnames';

const Level = ({ mobile, className, ...props }) => {
	return <div className={cn('nab-level', mobile ? 'is-mobile' : '', className)} {...props} />;
};

export default Level;
