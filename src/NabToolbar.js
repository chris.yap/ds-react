import React from 'react'
import Container from './Container'
import Breakpoints from './Breakpoints/'
import cn from 'classnames'
import nextId from 'react-id-generator'

class Toolbar extends React.Component {
  constructor(props) {
    super()
    this.state = {
      expand: false
    }
  }
  toggleNav = event => {
    this.props.onClick()
    this.setState({ expand: !this.state.expand })
  };
  render() {
    const {
      active,
      burger,
      brand,
      className,
      dark,
      fixed,
      fixedBottom,
      fixedbottom,
      fixedTop,
      fixedtop,
      fluid,
      href,
      image,
      level,
      narrow,
      noburger,
      ver,
      activeBreakpoints,
      ...props
    } = this.props
    const htmlId = nextId()
    if (active) {
      this.setState({ expand: true })
    }
    let brandTag
    let branding
    let updateImage
    let Tag = href ? 'a' : 'div'
    if (image && typeof image === 'string') {
      updateImage = <img src={image} alt={brand} className='mr-3' />
    } else {
      updateImage = image
    }
    if (updateImage || brand) {
      brandTag = (
        <Tag className='nab-toolbar-item' href={href}>
          {updateImage}
          {brand && <React.Fragment>{brand}</React.Fragment>}
        </Tag>
      )
    }
    branding = (
      <div className='nab-toolbar-brand'>
        {burger === 'left' && (
          <React.Fragment>
            {!noburger && (
              <a
                role='button'
                className={cn(
                  'nab-toolbar-burger burger ml-2',
                  this.state.expand ? 'is-active' : ''
                )}
                aria-label='menu'
                aria-expanded='false'
                data-target={id}
                onClick={() => this.toggleNav()}
              >
                <span aria-hidden='true' />
                <span aria-hidden='true' />
                <span aria-hidden='true' />
              </a>
            )}
            {brandTag}
          </React.Fragment>
        )}
        {!burger && (
          <React.Fragment>
            {brandTag}
            {!noburger && (
              <a
                role='button'
                className={cn(
                  'nab-toolbar-burger burger',
                  this.state.expand ? 'is-active' : ''
                )}
                aria-label='menu'
                aria-expanded='false'
                data-target={htmlId}
                onClick={() => this.toggleNav()}
              >
                <span aria-hidden='true' />
                <span aria-hidden='true' />
                <span aria-hidden='true' />
              </a>
            )}
          </React.Fragment>
        )}
      </div>
    )
    let children
    if (fluid) {
      children = (
        <React.Fragment>
          {branding}
          {props.children && (
            <div
              id={htmlId}
              className={cn(
                'nab-toolbar-menu slide-y',
                this.state.expand ? 'is-active' : ''
              )}
            >
              {props.children}
            </div>
          )}
        </React.Fragment>
      )
    } else {
      children = (
        <Container
          display={
            (activeBreakpoints.above.md || activeBreakpoints.when.md) && 'flex'
          }
        >
          {branding}
          {props.children && (
            <div
              id={htmlId}
              className={cn(
                'nab-toolbar-menu slide-y',
                this.state.expand ? 'is-active' : ''
              )}
            >
              {props.children}
            </div>
          )}
          {
            // this.props.floatingNav &&
            // <FloatingNav logout={this.props.logout} />
          }
        </Container>
      )
    }
    return (
      <nav
        className={cn(
          'nab-toolbar',
          level ? 'is-level-' + level : '',
          ver ? 'is-ver-' + ver : '',
          burger ? 'has-burger-' + burger : '',
          dark ? 'is-dark' : '',
          narrow ? 'is-narrow' : '',
          fixed ? 'is-fixed' : '',
          fixedTop || fixedtop ? 'is-fixed-top' : '',
          fixedBottom || fixedbottom ? 'is-fixed-bottom' : '',
          className
        )}
        {...props}
      >
        {children}
      </nav>
    )
  }
}
export default Breakpoints(Toolbar)
