import React from 'react';
import { NavLink } from 'react-router-dom';
import Collapse from './Collapse/';
import cn from 'classnames';
import Button from './Button';
import Columns from './NabColumns';
import Column from './NabColumn';
import Container from './Container/';
import Divider from './Divider';
import Footer from './NabFooter';
import Icon from './Icon/';
import List from './List/List';
import ListItem from './List/ListItem';

export default class MobileNav extends React.Component {
  constructor(props) {
    super();
    this.state = {
      menu: false,
      open: null
    };
  }
  toggleMenu() {
    this.setState({ menu: !this.state.menu });
    if (this.state.menu === false) {
      this.setState({ open: null });
    }
  }
  toggleSubMenu(index) {
    if (this.state.open === index) {
      this.setState({ open: null });
    } else {
      this.setState({ open: index });
    }
  }
  render() {
    return (
      <nav className={cn('nab-mobile-nav', this.props.className)}>
        <Container fluid>
          <Button onClick={() => this.toggleMenu()}>
            <Icon>menu</Icon>
            <span>Menu</span>
          </Button>
          {NavItems.map((item, i) => {
            return (
              <NavLink
                key={i}
                to={item.to}
                className="nab-button"
                activeClassName="is-active">
                <Icon>{item.icon}</Icon>
                <span>{item.label}</span>
              </NavLink>
            );
          })}
          <Button>
            <Icon>search</Icon>
            <span>Search</span>
          </Button>
        </Container>

        <Collapse isOpened={this.state.menu === true}>
          <div className="nab-mobile-more-menu">
            {/* <Container fluid>
              <Button flat className="my-2" onClick={ () => this.toggleMenu() }>
                <Icon danger>times</Icon>
                <span>Close</span>
              </Button>
            </Container> */}
            <Divider />
            <List className="mb-5">
              <ListItem onClick={() => this.toggleMenu()}>
                <strong className="has-text-black">Close</strong>
                <Icon danger>times</Icon>
              </ListItem>
              {MoreItems.map((mm, m) => {
                if (mm.items) {
                  return (
                    <div key={m}>
                      <ListItem href={mm.to}>
                        {mm.label}
                        <div
                          className="action has-text-size4 has-text-danger"
                          onClick={event => {
                            event.stopPropagation();
                            event.preventDefault();
                            this.toggleSubMenu(m);
                          }}>
                          {this.state.open === m && <span>-</span>}
                          {this.state.open !== m && <span>+</span>}
                        </div>
                      </ListItem>
                      {mm.items && (
                        <Collapse isOpened={this.state.open === m}>
                          <List>
                            {mm.items.map((mmm, mi) => {
                              return <ListItem key={mi}>{mmm.label}</ListItem>;
                            })}
                          </List>
                        </Collapse>
                      )}
                    </div>
                  );
                } else {
                  return <ListItem key={m}>{mm.label}</ListItem>;
                }
              })}
            </List>

            <Container fluid className="px-5 pt-3">
              <Columns mobile multiline>
                <Column className="is-12-mobile is-6-tablet py-0">
                  <Button block primary className="mb-2">
                    Trade
                  </Button>
                </Column>
                <Column className="is-12-mobile is-6-tablet py-0">
                  <Button block secondary className="mb-2">
                    Cash transfer
                  </Button>
                </Column>
              </Columns>
            </Container>

            <Footer fixed className="pa-0 has-bg-white">
              <Divider />
              <div className="pa-5">
                <Button flat block primary>
                  <Icon>logout</Icon>
                  <span>Log out</span>
                </Button>
              </div>
            </Footer>
          </div>
        </Collapse>
      </nav>
    );
  }
}

const NavItems = [
  { label: 'Portfolio', icon: 'briefcase', to: '/portfolio' },
  { label: 'Watchlists', icon: 'watchlist', to: '/watchlists' },
  { label: 'Alerts', icon: 'bell-outline', to: '/alerts' }
];

const MoreItems = [
  {
    label: 'Favourites',
    to: '',
    items: [
      { label: 'Cash transfer' },
      { label: "Andre's portfolio" },
      { label: 'My rainy day play watchlist' }
    ]
  },
  {
    label: 'Portfolio',
    to: '/portfolio',
    items: [{ label: 'Orders' }, { label: 'Statement & reports' }]
  },
  {
    label: 'Markets & insights',
    to: '/markets-insights',
    items: [
      { label: 'Markets' },
      { label: 'Tools' },
      { label: 'News & announcements' },
      { label: 'Research & reports' }
    ]
  },
  {
    label: 'Investment options',
    to: '',
    items: [{ label: 'Types' }, { label: 'Tools' }, { label: 'Products' }]
  },
  {
    label: 'Support',
    to: '',
    items: [
      { label: 'Contact' },
      { label: 'Forms' },
      { label: 'F.A.Q.' },
      { label: "How to's" }
    ]
  },
  {
    label: 'Settings',
    to: '',
    items: [{ label: 'Profile' }, { label: 'Password' }]
  }
];
