import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Transition } from 'react-transition-group';

const offset = '-4px';

const TooltipWrapper = styled.div`
  display: inline-flex;
  overflow: auto;
  position: relative;
  overflow: visible;
  text-transform: none !important;
`;

const TooltipMessage = styled.div`
  background-color: ${props => (props.white ? 'white' : 'black')};
  color: ${props => (props.white ? '#4c626c' : 'white')};
  text-shadow: 0 -1px 0 rgba(black, 0.5);
  padding: ${props => (props.noPaddings ? '0' : '4px 8px')};
  border-radius: 3px;
  font-size: 0.9rem;
  line-height: 1;
  text-align: ${props => (props.textLeft ? 'left' : 'center')};
  word-break: normal;
  position: absolute;
  overflow: hidden;

  -webkit-box-shadow: 0px 0px 3px 0px rgba(0, 0, 0, 0.5);
  -moz-box-shadow: 0px 0px 3px 0px rgba(0, 0, 0, 0.5);
  box-shadow: 0px 0px 3px 0px rgba(0, 0, 0, 0.5);

  min-width: ${props => props.minWidth && props.minWidth + 'px'};
  max-width: ${props => props.maxWidth && props.maxWidth + 'px'};

  top: ${props =>
    props.left || props.right
      ? '50%'
      : props.right
      ? null
      : props.top && offset};
  left: ${props =>
    props.bottom
      ? '50%'
      : props.left
      ? offset
      : props.right
      ? null
      : props.top && '50%'};
  right: ${props => props.right && offset};
  bottom: ${props => props.bottom && offset};
  transform: ${props =>
    props.left
      ? 'translate(-100%, -50%)'
      : props.right
      ? 'translate(100%, -50%)'
      : props.bottom
      ? 'translate(-50%, 100%)'
      : 'translate(-50%, -100%)'};
  transform-origin: ${props =>
    props.left
      ? '100% 50%'
      : props.right
      ? '0% 50%'
      : props.bottom
      ? '50% 100%'
      : '50% 0'};

  transition: 300ms;
    display: ${({ state }) => (state === 'exited' ? 'none' : 'block')};
    opacity: ${({ state }) =>
    state === 'entering'
      ? 0
      : state === 'entered'
      ? 1
      : state === 'exiting'
      ? 0
      : state === 'exited' && 0};
`;

class Tooltip extends React.Component {
  state = {
    showTip: false
  };
  render = () => {
    const {
      bottom,
      left,
      right,
      top,
      minWidth,
      maxWidth,
      children,
      tip,
      className,
      white,
      noPaddings,
      textLeft,
      ...props
    } = this.props;
    const newTop = bottom || left || right ? false : top;
    const { showTip } = this.state;
    return (
      <React.Fragment>
        <TooltipWrapper
          onMouseEnter={() => this.setState({ showTip: true })}
          onMouseLeave={() => this.setState({ showTip: false })}
          {...props}
        >
          {children}

          <Transition in={showTip} timeout={300}>
            {state => (
              <TooltipMessage
                top={newTop}
                left={left}
                right={right}
                bottom={bottom}
                minWidth={minWidth}
                maxWidth={maxWidth}
                state={state}
                white={white}
                noPaddings={noPaddings}
                textLeft={textLeft}
                className={className}
                {...props}
              >
                {tip}
              </TooltipMessage>
            )}
          </Transition>
        </TooltipWrapper>
      </React.Fragment>
    );
  };
}

export default Tooltip;

Tooltip.propTypes = {
  tip: PropTypes.string,
  top: PropTypes.bool,
  left: PropTypes.bool,
  bottom: PropTypes.bool,
  right: PropTypes.bool,
  center: PropTypes.bool,
  middle: PropTypes.bool
};

Tooltip.defaultProps = {
  tip: 'Tooltip goes here',
  top: true
};
