import React from 'react';
import cn from 'classnames';

const ToolbarNav = ({ left, right, width, className, ...props }) => {
  let size;
  if (width) {
    size = {
      width: width + 'px'
    };
  }
  return (
    <div
      style={size}
      className={cn(
        left ? 'nab-toolbar-start' : '',
        right ? 'nab-toolbar-end' : '',
        className
      )}
      {...props}
    />
  );
};

export default ToolbarNav;
