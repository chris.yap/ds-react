import React from "react";
import PropTypes from "prop-types";
import throttle from "lodash/throttle";
import cn from "classnames";

class ResponsiveTable extends React.Component {
  state = {
    containerClasses: ""
  };
  // table = React.createRef();
  // scrollableArea = React.createRef();
  // scrollableContainer = React.createRef();

  // componentDidMount() {
  //   this.init();
  // }

  // init() {
  //   this.throttledRefresh = throttle(this.refreshClassName, 10);
  //   window.addEventListener("resize", this.throttledRefresh);
  //   this.scrollableArea.current.addEventListener(
  //     "scroll",
  //     this.throttledRefresh
  //   );

  //   this.refreshClassName();
  // }

  // componentWillUnmount() {
  //   this.scrollableArea.current.removeEventListener(
  //     "scroll",
  //     this.throttledRefresh
  //   );
  //   window.removeEventListener("resize", this.throttledRefresh);
  // }

  // refreshClassName = () => {
  //   this.setState({
  //     containerClasses: this.getClassName()
  //   });
  // };

  // getClassName = () => {
  //   let containerClasses = "";

  //   const innerWidth = this.table.current.getBoundingClientRect().width;
  //   const outerWidth = this.scrollableArea.current.getBoundingClientRect()
  //     .width;

  //   if (outerWidth >= innerWidth) {
  //     containerClasses += "disabled ";
  //   }

  //   const thisScrollLeft = this.scrollableArea.current.scrollLeft;
  //   const thisScrollEnd =
  //     thisScrollLeft + this.scrollableArea.current.clientWidth;

  //   if (thisScrollLeft !== 0) {
  //     const scrollWidth = this.scrollableArea.current.scrollWidth;
  //     if (thisScrollEnd >= scrollWidth) {
  //       containerClasses += "end ";
  //     }
  //   } else {
  //     containerClasses += "start ";
  //   }

  //   return containerClasses;
  // };

  render() {
    const {
      id,
      striped,
      hoverable,
      narrow,
      bordered,
      className,
      ...props
    } = this.props;
    return (
      // <div
      //   className={`scrollable-container sticky-first has-options nab-table-wrapper ${
      //     this.state.containerClasses
      //   } ${className}`}
      //   ref={this.scrollableContainer}
      // >
      <div className={cn("nab-table-wrapper", className)}>
        {/* <div className="scrollable" ref={this.scrollableArea}> */}
        <table
          id={id}
          className={cn(
            "nab-table",
            striped ? "is-striped" : "",
            bordered ? "is-bordered" : "",
            narrow ? "is-narrow" : "",
            hoverable ? "is-hoverable" : ""
          )}
        >
          {props.children}
        </table>
        {/* </div> */}
      </div>
    );
  }
}

ResponsiveTable.propTypes = {
  children: PropTypes.node,
  id: PropTypes.string
};

export default ResponsiveTable;
