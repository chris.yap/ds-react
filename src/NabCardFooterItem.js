import React from 'react'
import cn from 'classnames'

const CardFooterItem = ({ className, ...props }) => {
  return (
    <div 
      className={cn(
        'nab-card-footer-item',
        className
      )}
      { ...props }
    />
  )
}

export default CardFooterItem