import React from 'react';
import {
  Hero,
  Columns,
  Column,
  Container,
  Subtitle,
  Title
} from 'shaper-react';

const PageHeader = ({ category, title, ...props }) => {
  return (
    <Hero>
      <Container>
        <Columns mobile>
          <Column mobile={12} desktop={10} desktopOffset={1}>
            <p className="font-nabscript has-text-size4 mt-3 mb-0 has-text-primary">
              {category}
            </p>
            <Title className="mt-0 font-nabimpact">{title}</Title>
            <Subtitle>{props.children}</Subtitle>
          </Column>
        </Columns>
      </Container>
    </Hero>
  );
};

export default PageHeader;
