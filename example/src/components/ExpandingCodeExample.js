import React, { Component } from 'react';
// import ReactDOM from 'react-dom'
import { Button, Card, CardHeader, CardContent } from 'shaper-react';
import renderHTML from 'react-render-html';
import Highlight from 'react-highlight';
import nextId from 'react-id-generator';
import cn from 'classnames';
import './ExpandingCodeExample.css';

export default class ExpandingCodeExample extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: null,
      showCode: null,
      codeHeight: 0,
      htmlHeight: 0,
      reactHeight: 0,
      background: false,
      html: false,
      react: false,
      example: ''
    };

    if (props.background) this.state.background = 'has-bg-' + props.background;
    if (props.example) this.state.example = props.example;
    if (props.html) this.state.html = props.html;
    if (props.react) this.state.react = props.react;
    if (!props.children && !props.example)
      this.state.example = 'Example goes here';
  }
  componentDidMount() {
    const id = nextId();
    const htmlHeight = this.htmlElement.clientHeight;
    const reactHeight = this.reactElement.clientHeight;
    this.setState({ htmlHeight, reactHeight, id });
  }
  componentDidUpdate = prevProps => {
    if (prevProps.background !== this.props.background)
      this.setState({ background: 'has-bg-' + this.props.background });
    if (prevProps.example !== this.props.example)
      this.setState({ example: this.props.example });
    if (prevProps.html !== this.props.html)
      this.setState({ html: this.props.html });
    if (prevProps.react !== this.props.react)
      this.setState({ react: this.props.react });
  };
  toggleCode = event => {
    if (event === this.state.showCode) {
      this.setState({ showCode: null });
      this.setState({ codeHeight: 0 });
    } else if (event === 'react') {
      this.setState({ showCode: 'react' });
      this.setState({ codeHeight: this.state.reactHeight });
    } else if (event === 'html') {
      this.setState({ showCode: 'html' });
      this.setState({ codeHeight: this.state.htmlHeight });
    }
  };
  render() {
    return (
      <Card className={`example mb-3`}>
        <CardHeader>
          <h4 className={`nab-card-header-title my-2`}>{this.props.title}</h4>
          <Button
            small
            secondary
            inverted
            className="is-pulled-right mr-1"
            disabled={this.state.react ? false : true}
            onClick={() => this.toggleCode('react')}>
            ReactJS
          </Button>
          {/* <Button small secondary inverted className="is-pulled-right mr-1" disabled={this.state.html ? false : true}
            onClick={() => this.toggleCode('html')}>HTML</Button> */}
        </CardHeader>

        <div
          className={`code`}
          style={{ height: `${this.state.codeHeight}px` }}>
          <div
            className={`nab-card-content pa-0 nab-code`}
            style={
              this.state.showCode !== 'html' ? { opacity: 0 } : { zIndex: 1 }
            }
            ref={elem => (this.htmlElement = elem)}
            height={`${this.state.htmlHeight}px`}>
            <Highlight className="xml">{this.state.html}</Highlight>
          </div>
          <div
            className={`nab-card-content pa-0 nab-code`}
            style={
              this.state.showCode !== 'react' ? { opacity: 0 } : { zIndex: 1 }
            }
            ref={elem => (this.reactElement = elem)}
            height={`${this.state.reactHeight}px`}>
            <Highlight className="xml">{this.state.react}</Highlight>
          </div>
        </div>
        <CardContent
          className={cn(
            this.props.padding ? 'pa-' + this.props.padding : '',
            this.state.background ? this.state.background : ''
          )}>
          {this.props.children && this.props.children}
          {this.state.example && renderHTML(this.state.example)}
        </CardContent>
      </Card>
    );
  }
}
