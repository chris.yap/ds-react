import React from 'react';
import Menu from './Menu.js';

const Navs = () => {
  return <Menu items={items} />;
};

const items = [
  {
    header: 'Core documentation'
  },
  {
    icon: 'directions_run',
    title: 'Getting started',
    page: '/'
    // items: [
    // 	// { title: "Designers", page: "/getting-started/designers" },
    // 	{ title: 'Developers', page: '/getting-started/developers' },
    // 	// { title: 'FAQ', page: '/getting-started/faq', disabled: true }
    // ],
  },
  {
    icon: 'card_giftcard',
    title: "What's new",
    page: '/whats-new'
    // disabled: true
  },
  {
    icon: 'grid_on',
    title: 'Guidelines',
    page: '/guidelines',
    disabled: true
  },
  {
    icon: 'color_lens',
    title: 'Style',
    page: '/style',
    items: [
      { title: 'Colours', page: '/style/colours' },
      { title: 'Icons', page: '/style/icons' },
      { title: 'Theming', page: '/style/theme', new: true },
      { title: 'Typography', page: '/style/typography' }
    ]
  },
  {
    icon: 'view_quilt',
    title: 'Layout',
    page: '/layout',
    items: [
      { title: 'Containers', page: '/layout/containers' },
      { title: 'Heroes', page: '/layout/heroes', updated: true },
      { title: 'Sections', page: '/layout/sections', updated: true },
      { title: 'Footer', page: '/layout/footer', updated: true },
      // { title: 'Grid', page: '/layout/grid' },
      { title: 'Columns', page: '/layout/columns' },
      { title: 'Tiles', page: '/layout/tiles' },
      { title: 'Elevation', page: '/layout/elevation' },
      { title: 'Spacing', page: '/layout/spacing' }
    ]
  },
  {
    icon: 'apps',
    title: 'Elements',
    page: '/elements',
    items: [
      { title: 'Boxes', page: '/elements/boxes' },
      { title: 'Buttons', page: '/elements/buttons', updated: true },
      { title: 'Dividers', page: '/elements/dividers' },
      { title: 'Icons', page: '/elements/icons' },
      { title: 'Overlays', page: '/elements/overlay', new: true },
      {
        title: 'Progress',
        page: '/elements/progress',
        updated: true
      },
      { title: 'Tables', page: '/elements/tables' },
      { title: 'Tags', page: '/elements/tags' },
      { title: 'Titles', page: '/elements/titles' }
    ]
  },
  {
    icon: 'text_format',
    title: 'Forms',
    page: '/forms',
    items: [
      { title: 'Checkboxes', page: '/forms/checkboxes' },
      { title: 'Date/Time pickers', page: '/forms/datepickers', new: true },
      // { title: 'Files', page: '/forms/files' },
      { title: 'Radios', page: '/forms/radios' },
      { title: 'Selects', page: '/forms/selects' },
      // { title: 'Textarea', page: '/forms/textarea', disabled: true },
      { title: 'Textfields', page: '/forms/text-fields', updated: true }
    ]
  },
  {
    icon: 'power',
    title: 'Components',
    page: '/components',
    items: [
      { title: 'Alerts', page: '/components/alerts' },
      { title: 'Breadcrumbs', page: '/components/breadcrumbs' },
      { title: 'Cards', page: '/components/cards' },
      { title: 'Carousels', page: '/components/carousels' },
      { title: 'Collapse', page: '/components/collapse' },
      { title: 'Drawers', page: '/components/drawers' },
      { title: 'Dropdowns', page: '/components/dropdowns' },
      { title: 'Messages', page: '/components/messages', new: true },
      { title: 'Lists', page: '/components/lists' },
      { title: 'Modals', page: '/components/modals' },
      { title: 'Pagination', page: '/components/paginations' },
      { title: 'Tabs', page: '/components/tabs' },
      { title: 'Toolbars', page: '/components/toolbars' },
      { title: 'Tooltips', page: '/components/tooltips' }
    ]
  },
  {
    icon: 'create',
    title: 'Modifiers',
    page: '/modifiers',
    items: [
      { title: 'Syntax', page: '/modifiers/syntax', disabled: true },
      { title: 'Modifiers', page: '/modifiers' },
      { title: 'Responsive', page: '/modifiers/responsive' },
      { title: 'Typography', page: '/modifiers/typography' }
    ]
  },
  {
    icon: 'build',
    title: 'Utilities',
    page: '/utilities',
    items: [
      { title: 'Breakpoints', page: '/utilities/breakpoints' },
      { title: 'Browser', page: '/utilities/browser' }
    ]
  },
  {
    header: 'Additional resources'
  },
  {
    icon: 'timeline',
    title: 'Data visualisation',
    page: '/data-visualisation',
    disabled: true
  },
  {
    icon: 'cloud_download',
    title: 'Downloads',
    page: '/downloads',
    disabled: true
  }
];

export default Navs;
