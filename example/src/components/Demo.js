import React from 'react';
import Highlight from 'react-highlight';
import {
  Button,
  Collapse,
  // Divider,
  Dropdown,
  Icon,
  Level,
  LevelItem,
  List,
  ListItem
} from 'shaper-react';

const Demo = ({ children, examples, ...props }) => {
  const [index, setIndex] = React.useState(0);
  const [showCode, updateShowCode] = React.useState(false);
  return (
    <>
      <Level mobile>
        <LevelItem left>
          <Dropdown
            zIndex="90"
            disabled={examples.length <= 1}
            activator={
              <Button disabled={examples.length <= 1}>
                {examples.map((eg, e) => e === index && eg.title)}
                <Icon className="ml-1">arrow-down</Icon>
              </Button>
            }>
            <List>
              {examples.map((eg, e) => (
                <ListItem
                  key={e}
                  className={'has-pointer has-text-nowrap'}
                  onClick={() => {
                    setIndex(e);
                    updateShowCode(false);
                    props.updateIndex(e);
                  }}>
                  {eg.title}
                </ListItem>
              ))}
            </List>
          </Dropdown>
        </LevelItem>
        <LevelItem right>
          <Button
            inverted
            className={showCode && 'is-active'}
            disabled={!examples[index] || !examples[index].react}
            onClick={() => updateShowCode(showCode ? false : true)}>
            Code
          </Button>
        </LevelItem>
      </Level>

      {/* <Divider /> */}

      <style>
        {`pre {
            margin: 0;
          }`}
      </style>
      <Collapse isOpened={showCode}>
        <Highlight className="xml">
          {examples[index] && examples[index].react}
        </Highlight>
      </Collapse>

      <div className="mt-4 mb-10">{children}</div>
    </>
  );
};

export default Demo;

Demo.defaultProps = {
  examples: []
};
