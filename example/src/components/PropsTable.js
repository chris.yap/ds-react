import React from 'react';
import { PropsHeaders, PropsTableRow } from '../constants/PropsTables';
import { Table, Tab, Tabs, Title } from 'shaper-react';

const PropsTable = ({ data, ...props }) => {
  return (
    <>
      <Title size={4} className="font-nabimpact">
        React Props
      </Title>

      {data.length > 1 ? (
        <Tabs>
          {data.map((d, i) => (
            <Tab key={i} label={d.title}>
              <Table
                hasStripes
                data={data[i].items}
                headers={PropsHeaders}
                tableRow={PropsTableRow}
                stickyHeader
                stickyColumn
                defaultSortCol="name"
              />
            </Tab>
          ))}
        </Tabs>
      ) : (
        <Table
          hasStripes
          data={data[0].items}
          headers={PropsHeaders}
          tableRow={PropsTableRow}
          stickyHeader
          stickyColumn
          defaultSortCol="name"
        />
      )}
    </>
  );
};

export default PropsTable;
