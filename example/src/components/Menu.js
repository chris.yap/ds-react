import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Collapse, Divider, Tag } from 'shaper-react';

export default class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openNav: null
    };
  }
  toggleNav = index => {
    if (this.state.openNav === index) {
      this.setState({ openNav: null });
    } else {
      this.setState({ openNav: index });
    }
  };
  render() {
    const { openNav } = this.state;
    return (
      <div className={`nab-menu`}>
        <ul className={`nab-menu-list`}>
          {this.props.items.map((item, index) => {
            if (item.header) {
              return (
                <li key={index}>
                  <h3
                    className={`has-text-size2 has-text-white font-nabimpact pl-4 pr-3 py-3 mb-0`}>
                    {item.header}
                  </h3>
                </li>
              );
            } else {
              if (item.items) {
                return (
                  <li key={index}>
                    {openNav === index && <Divider dark />}
                    <div
                      className={`nab-menu-list-tile has-pointer has-text-white ${
                        openNav !== index ? 'is-collapsed' : ''
                      }`}
                      onClick={() => this.toggleNav(index)}>
                      <span className={`has-prepend-icon`}>
                        <i className={`material-icons`}>{item.icon}</i>
                      </span>
                      <span className={`nab-menu-list-tile-content`}>
                        {item.title}
                      </span>
                      <span className={`has-append-icon`}>
                        <i className={`material-icons`}>keyboard_arrow_down</i>
                      </span>
                    </div>
                    <ul>
                      <Collapse isOpened={openNav === index}>
                        {item.items.map((subitem, sindex) => {
                          return (
                            <li key={sindex}>
                              <NavLink
                                to={subitem.page}
                                className="pl-10 has-text-white"
                                activeClassName="is-active"
                                disabled={subitem.disabled}>
                                <span className="pl-3">
                                  {subitem.title}{' '}
                                  {subitem.new ? (
                                    <Tag
                                      small
                                      success
                                      position="relative"
                                      top="-1px">
                                      new
                                    </Tag>
                                  ) : (
                                    subitem.updated && (
                                      <Tag
                                        small
                                        info
                                        position="relative"
                                        top="-1px">
                                        updated
                                      </Tag>
                                    )
                                  )}
                                </span>
                              </NavLink>
                            </li>
                          );
                        })}
                        <Divider dark />
                      </Collapse>
                    </ul>
                  </li>
                );
              } else {
                return (
                  <li key={index}>
                    <NavLink
                      exact
                      to={item.page}
                      className={`nab-menu-list-tile has-text-white`}
                      activeClassName="is-active"
                      disabled={item.disabled}>
                      <span className={`has-prepend-icon`}>
                        <i className={`material-icons`}>{item.icon}</i>
                      </span>
                      <span className={`nab-menu-list-tile-content`}>
                        {item.title}
                      </span>
                    </NavLink>
                  </li>
                );
              }
            }
          })}
        </ul>
      </div>
    );
  }
}
