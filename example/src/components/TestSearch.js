import React from "react";
import { colourOptions } from "../constants/data";
import { Select } from "shaper-react";

export default class TestSearch extends React.Component {
  state = {
    value: "",
    loading: false
  };
  filterColors = inputValue => {
    if (inputValue) {
      let results = colourOptions.filter(i =>
        i.label.toLowerCase().includes(inputValue.toLowerCase())
      );
      this.setState({
        options: results ? results : [],
        value: inputValue,
        loading: false
      });
    }
  };
  handleInputChange = (newValue, action) => {
    if (newValue) {
      this.setState({
        loading: true
      });
    }
    const value = ("" + newValue).replace(/\W/g, "");
    if (
      action.action === "menu-close" ||
      action.action === "input-blur" ||
      action.action === "set-value"
    ) {
      this.setState({
        value: this.state.value
      });
    } else {
      this.setState({
        value
      });
    }
    this.getOptionsAsync(value);
  };
  getOptionsAsync = newInput => {
    if (newInput) {
      setTimeout(() => {
        this.filterColors(newInput);
      }, 1000);
    }
  };
  render = () => {
    const { ...props } = this.props;
    const { loading, value, options } = this.state;
    return (
      <Select
        isLoading={loading}
        options={options || []}
        label={value}
        inputValue={value}
        onInputChange={this.handleInputChange}
        {...props}
      />
    );
  };
}
