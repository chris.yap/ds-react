const PropsRadioGroup = [
  {
    name: 'buttons',
    def: 'undefined',
    type: 'Boolean',
    desc: 'Make radios into buttons'
  },
  {
    name: 'className',
    def: 'undefined',
    type: 'String',
    desc: 'Add additional classes if needed'
  },
  {
    name: 'fullwidth / fullWidth',
    def: 'undefined',
    type: 'Boolean',
    desc: 'Make radios full width'
  },
  {
    name: 'label',
    def: 'undefined',
    type: 'String',
    desc: 'Label text'
  },
  {
    name: 'name',
    def: 'undefined',
    type: 'String',
    desc: 'name of radio fields'
  },
  {
    name: 'onChange',
    def: 'undefined',
    type: 'Function',
    desc: 'onChange function'
  },
  {
    name: 'rounded',
    def: 'undefined',
    type: 'Boolean',
    desc: 'Have rounded buttons (need buttons prop)'
  },
  {
    name: 'selectedValue',
    def: 'undefined',
    type: 'String/Number/Boolean',
    desc: 'Set default value'
  }
];

const PropsRadio = [
  {
    name: 'button',
    def: 'undefined',
    type: 'Boolean',
    desc: 'Make radio a button'
  },
  {
    name: 'id',
    def: 'auto-generated',
    type: 'string',
    desc: 'Ability to add personalised ID'
  },
  {
    name: 'className',
    def: 'undefined',
    type: 'String',
    desc: 'Add additional classes if needed'
  },
  {
    name: 'label',
    def: 'undefined',
    type: 'String',
    desc: 'Label of radio button'
  },
  {
    name: `primary <br />
        secondary <br />
        success <br />
        info <br />
        warning <br />
        danger`,
    def: 'undefined',
    type: 'Boolean',
    desc: 'Different styling for button (button prop needed)'
  },
  {
    name: 'value',
    def: 'undefined',
    type: 'String',
    desc: 'Value of radio button'
  },
  {
    name: `small <br />
        large`,
    def: 'undefined',
    type: 'Boolean',
    desc: 'Size of radio button'
  }
  // {
  //   name: '',
  //   def: '',
  //   type: '',
  //   desc: '',
  // },
];

export { PropsRadio, PropsRadioGroup };
