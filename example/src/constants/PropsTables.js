import React from 'react';
import { TableRow as Row, TableCell as Cell } from 'shaper-react';
import ReactHtmlParser from 'react-html-parser';

const PropsHeaders = [
  {
    value: 'name',
    name: 'Name'
  },
  {
    value: 'def',
    name: 'Default'
  },
  {
    value: 'type',
    name: 'Type'
  },
  {
    value: 'desc',
    name: 'Description'
  }
];

const PropsTableRow = ({ row }) => (
  <Row>
    <Cell>
      <strong>{ReactHtmlParser(row.name)}</strong>
    </Cell>
    <Cell>{ReactHtmlParser(row.def)}</Cell>
    <Cell>{ReactHtmlParser(row.type)}</Cell>
    <Cell>{ReactHtmlParser(row.desc)}</Cell>
  </Row>
  // <tr>
  // 	<td>
  // 		<strong>{row.name}</strong>
  // 	</td>
  // 	<td>{row.def}</td>
  // 	<td>{row.type}</td>
  // 	<td>{row.desc}</td>
  // </tr>
);

export { PropsHeaders, PropsTableRow };
