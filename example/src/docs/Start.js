import React, { Component } from 'react'
import { Column ,Columns, Container, Divider, Hero, Section, Tag } from 'shaper-react'

export default class Start extends Component {
  render () {
    return (
      <div id="start">
        <Hero medium>
          <Container>
            <Columns mobile>
              <Column className="is-12-mobile is-10-desktop is-offset-1-desktop">
                
                <h1 className="nab-title font-nabimpact">Shaper</h1>
                <p className="nab-subtitle">
                  <Tag>Built with React</Tag> 
                  The Shaper is a design system is an integral part of our products core. It creates focus, clarity and confidence and in turn will create consistency across the product and will speed up the turn around of product development.
                </p>

              </Column>
            </Columns>
          </Container>
        </Hero>

        <Divider className="my-0" />

        <Section medium>
          <Container>
            <Columns mobile>
              <Column className="is-12-mobile is-10-desktop is-offset-1-desktop">
              
                <Columns>
                  <Column>
                    <h3 className={`nab-title is-5 font-nabimpact mb-2`}>Brand</h3>
                    <p>The Brand Identity of a product is made up from key elements that create the visual identity. Colours, typography and iconography are core to any platform.</p>
                  </Column>
                  <Column>
                    <h3 className={`nab-title is-5 font-nabimpact mb-2`}>Elements</h3>
                    <p>The Elements are made up of the smallest reusable parts of the system. These elements are constantly recycled across all areas of the system. (Buttons, inputs)</p>
                  </Column>
                  <Column>
                    <h3 className={`nab-title is-5 font-nabimpact mb-2`}>Components</h3>
                    <p>A Component is a collection of elements, that are commonly used alongside each other to identify a common pattern within a flow. (Alerts, tables, cards, etc)</p>
                  </Column>
                </Columns>

              </Column>
            </Columns>
          </Container>
        </Section>
      </div>
    )
  }
}
