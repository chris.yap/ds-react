import React from 'react';
import PageHeader from '../components/PageHeader';
import {
  Alert,
  Column,
  Columns,
  Container,
  Divider,
  Section,
  Tag,
  Title
} from 'shaper-react';

const PageWhatsNew = () => {
  return (
    <div id="start">
      <PageHeader category="what's new" title="Updates" />

      <Divider />

      <Section>
        <Container>
          <Columns mobile>
            <Column mobile={12} desktop={10} desktopOffset={1}>
              <Alert info inverted value={true} className="mb-6">
                We are in the process of converting all components to{' '}
                <strong>styled-components</strong> +{' '}
                <strong>styled-system</strong>.
              </Alert>

              {/* <Divider my={5} /> */}

              {News.map(n => (
                <>
                  <p className="mb-2">
                    <Tag small>{n.ver}</Tag>
                  </p>
                  <Title size="4" className="mb-0">
                    {n.title}
                  </Title>
                  <p
                    className={`has-text-black text--lighten-2 ${
                      n.desc ? 'mb-1' : 'mb-4'
                    }`}>
                    <small>{n.date}</small>
                  </p>
                  {n.desc && <p dangerouslySetInnerHTML={{ __html: n.desc }} />}
                  <Divider mt={2} mb={5} />
                </>
              ))}
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};

export default PageWhatsNew;

const News = [
  {
    ver: '1.0.189',
    title: 'Button, Checkbox and Tabs are now styled-components',
    date: '16 October 2019',
    desc: `Read <a href="#/elements/buttons" class="has-text-weight-bold">Buttons</a>, <a href="#/forms/checkboxes" class="has-text-weight-bold">Checkboxes</a> and <a href="#/components/tabs" class="has-text-weight-bold">Tabs</a>
  	documentation to find out more.`
  },
  {
    ver: '1.0.152',
    title: 'New Overlay styled-component added',
    date: '4 September 2019',
    desc: `Read <a href="#/elements/overlay" class="has-text-weight-bold">Overlay</a>
  	documentation to find out more.`
  },
  {
    ver: '1.0.151',
    title: 'Modal is now styled-component',
    date: '3 September 2019',
    desc: `Read <a href="#/components/modals" class="has-text-weight-bold">Modal</a>
  	documentation to find out more.`
  },
  {
    ver: '1.0.147',
    title: 'Tooltip & Breadcrumb are now styled-components',
    date: '22 August 2019',
    desc: `Read <a href="#/components/tooltips" class="has-text-weight-bold">Tooltip</a> and 
		<a href="#/components/breadcrumbs" class="has-text-weight-bold">Breadcrumb</a> 
		documentation to find out more.`
  },
  {
    ver: '1.0.143',
    title: 'Collapse component updated',
    date: '21 August 2019',
    desc: ``
  },
  {
    ver: '1.0.142',
    title: 'Hero is now styled-component',
    date: '21 August 2019',
    desc: `Read <a href="#/layout/heroes" class="has-text-weight-bold">Hero</a> 
		documentation to find out more.`
  },
  {
    ver: '1.0.135',
    title: 'Footer is now styled-component',
    date: '16 August 2019',
    desc: `Read <a href="#/layout/footer" class="has-text-weight-bold">Footer</a> 
		documentation to find out more.`
  }
];
