import React from 'react';
import PageHeader from '../../components/PageHeader';
import Demo from '../../components/Demo';
import PropsTable from '../../components/PropsTable';
import {
  Column,
  Columns,
  Container,
  Divider,
  MultiField,
  Section,
  Select,
  Tag,
  Textfield,
  Title
} from 'shaper-react';

const PageTextfields = () => {
  const [index, updateIndex] = React.useState(0);
  return (
    <div id="start">
      <PageHeader category="forms" title="Textfields">
        Used for collecting user provided information. <br />
        <Tag small>styled-component</Tag>
      </PageHeader>

      <Divider className="my-0" />

      <Section>
        <Container>
          <Columns mobile>
            <Column className="is-12-mobile is-10-desktop is-offset-1-desktop">
              {/* <Textfield type="date" /> */}
              <Demo examples={Eg} updateIndex={updateIndex}>
                {index === 0 ? (
                  <React.Fragment>
                    <Textfield
                      id="id-email-address"
                      label="Email address"
                      placeholder="your.name@email.com"
                      hint="Please insert your email address."
                      zIndex="20"
                      required
                    />
                    <Textfield
                      type="password"
                      label="Password"
                      placeholder="••••••••"
                      hint="Your password must be at least 6 characters as well as contain at least one uppercase, one lowercase, and one number."
                      required
                    />
                    <Textfield
                      type="number"
                      label="Quantity"
                      hint="Quantity has to be between 2 and 10"
                      min="2"
                      max="10"
                      required
                    />
                    <Textfield
                      label="Dark theme"
                      placeholder="Please enter your name"
                      hint="Please enter your name"
                      dark
                    />
                    <div className="has-bg-blue-grey pa-3">
                      <Textfield
                        label="Dark background"
                        whiteLabel
                        placeholder="Please enter your name"
                        required
                        autocomplete="off"
                      />
                    </div>
                  </React.Fragment>
                ) : index === 1 ? (
                  <React.Fragment>
                    <Textfield
                      prependIcon="search"
                      label="Search"
                      placeholder="Search site"
                      hint="Please enter your search terms."
                    />
                    <Textfield
                      prependIcon="date"
                      label="Date"
                      type="date"
                      placeholder="DD/MM/YYYY"
                      hint="Please enter date."
                    />
                    <Textfield
                      appendIcon="nabtrade"
                      label="Label"
                      placeholder="Placeholder goes here"
                      hint="Hint goes here."
                    />
                    <Title size="6">Boxed</Title>
                    <Textfield
                      prependBoxedIcon="date"
                      label="Label"
                      placeholder="Placeholder goes here"
                      hint="Hint goes here."
                    />
                    <Textfield
                      appendBoxedIcon="nabtrade"
                      label="Label"
                      placeholder="Placeholder goes here"
                      hint="Hint goes here."
                    />
                  </React.Fragment>
                ) : index === 2 ? (
                  <React.Fragment>
                    <Textfield
                      prefix="$"
                      label="Amount"
                      placeholder="Enter price"
                      hint="Please enter the purchase price."
                    />
                    <Textfield
                      suffix="@email.com"
                      label="Email address"
                      placeholder="Enter email address"
                      hint="Please enter your email address."
                    />
                  </React.Fragment>
                ) : index === 3 ? (
                  <React.Fragment>
                    <Textfield
                      label="Default"
                      placeholder="Placeholder goes here"
                      hint="Hint goes here."
                    />
                    <Textfield
                      success
                      label="Success"
                      placeholder="Placeholder goes here"
                      value="Successful text entry"
                      hint="Hint goes here."
                    />
                    <Textfield
                      success
                      prependIcon="male"
                      label="Success"
                      placeholder="Placeholder goes here"
                      value="Successful text entry"
                      hint="Hint goes here."
                    />
                    <Textfield
                      success
                      appendIcon="nabtrade"
                      label="Success"
                      placeholder="Placeholder goes here"
                      value="Successful text entry"
                      hint="Hint goes here."
                    />
                    <Textfield
                      success
                      prependBoxedIcon="male"
                      label="Success"
                      placeholder="Placeholder goes here"
                      value="Successful text entry"
                      hint="Hint goes here."
                    />
                    <Textfield
                      success
                      appendBoxedIcon="nabtrade"
                      label="Success"
                      placeholder="Placeholder goes here"
                      value="Successful text entry"
                      hint="Hint goes here."
                    />

                    <Textfield
                      id="id-textfield"
                      error
                      label="Error"
                      placeholder="Placeholder goes here"
                      value="Invalid entry"
                      hint="Hint goes here."
                      errorMessage="Error message goes here"
                    />
                    <Textfield
                      error
                      prependIcon="male"
                      label="Error"
                      placeholder="Placeholder goes here"
                      hint="Hint goes here."
                      errorMessage="Error message goes here"
                    />
                    <Textfield
                      error
                      appendIcon="nabtrade"
                      label="Error"
                      placeholder="Placeholder goes here"
                      value="Invalid entry"
                      hint="Hint goes here."
                      errorMessage="Error message goes here"
                    />
                    <Textfield
                      error
                      prependBoxedIcon="male"
                      label="Error"
                      placeholder="Placeholder goes here"
                      value="Invalid entry"
                      hint="Hint goes here."
                      errorMessage="Error message goes here"
                    />
                    <Textfield
                      error
                      appendBoxedIcon="nabtrade"
                      label="Error"
                      placeholder="Placeholder goes here"
                      value="Invalid entry"
                      hint="Hint goes here."
                      errorMessage="Error message goes here"
                    />
                  </React.Fragment>
                ) : index === 4 ? (
                  <React.Fragment>
                    <MultiField
                      label="Label"
                      leftClassName="is-flex-1"
                      leftSlot={
                        <Select
                          isSearchable={false}
                          options={options}
                          defaultValue={options[0]}
                        />
                      }
                      rightClassName="is-flex-3"
                      rightSlot={<Textfield />}
                    />
                    <MultiField
                      label="Label"
                      leftClassName="is-flex-3"
                      rightSlot={
                        <Select
                          isSearchable={false}
                          options={options}
                          defaultValue={options[0]}
                        />
                      }
                      rightClassName="is-flex-1"
                      leftSlot={<Textfield />}
                    />
                  </React.Fragment>
                ) : index === 5 ? (
                  <Textfield
                    type="date"
                    label="Date"
                    hint="Please enter your preferred date"
                    required
                  />
                ) : index === 6 ? (
                  <Textfield
                    mask="(99) 9999-9999"
                    label="Phone number "
                    placeholder="eg. (02) 9302-9391"
                    hint="Please enter your contact number."
                  />
                ) : index === 7 ? (
                  <>
                    <Textfield small label="Small" placeholder="Small input" />
                    <Textfield label="Default" placeholder="Default input" />
                    <Textfield
                      medium
                      label="Medium"
                      placeholder="Medium input"
                    />
                    <Textfield large label="Large" placeholder="Large input" />
                  </>
                ) : (
                  <Textfield
                    multiline
                    label="Comments"
                    placeholder="Enter comments here"
                    hint="Please provide your feedback."
                  />
                )}
              </Demo>
              <PropsTable data={rows} />
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};
export default PageTextfields;

const rows = [
  {
    items: [
      {
        name: 'label',
        def: 'undefined',
        type: 'String',
        desc: 'Sets input label'
      },
      {
        name: 'type',
        def: 'text',
        type: 'String',
        desc: 'Sets input default type'
      },
      {
        name: 'hint',
        def: 'undefined',
        type: 'String',
        desc: 'Add help/hint text'
      },
      {
        name: 'required',
        def: 'false',
        type: 'Boolean',
        desc: 'Add asterisk to label'
      },
      {
        name: 'mask',
        def: 'undefined',
        type: 'String',
        desc: 'Mask string'
      },
      {
        name: 'onChange',
        def: 'undefined',
        type: 'Function',
        desc: 'Function call when value change (event.target.value)'
      },
      {
        name: 'placeholder',
        def: 'undefined',
        type: 'String',
        desc: 'Sets the input’s placeholder text'
      },
      {
        name: 'maxLength',
        def: 'undefined',
        type: 'String / Number',
        desc: 'Sets maximum characters allowed'
      },
      {
        name: 'multiline',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Turn input into textarea'
      },
      {
        name: 'success, \n error',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Put the input into different state'
      },
      {
        name: 'errorMessage',
        def: 'undefined',
        type: 'String',
        desc: 'Replaces hint text (if any) when error occurs'
      },
      {
        name: 'id',
        def: 'auto-generated',
        type: 'String',
        desc: 'Ability to add personalised ID'
      },
      {
        name: 'value',
        def: 'undefined',
        type: 'String',
        desc: 'Set default value'
      },
      {
        name: 'prependIcon, <br />appendIcon',
        def: 'undefined',
        type: 'String',
        desc: 'Put an icon at the end or front of the textfield'
      },
      {
        name: 'prependBoxedIcon, <br />appendBoxedIcon',
        def: 'undefined',
        type: 'String',
        desc: 'Put an boxed icon at the end or front of the textfield'
      },
      {
        name: 'prefix, <br />suffix',
        def: 'undefined',
        type: 'String',
        desc: 'Add additional content at the end or front of the textfield'
      },
      {
        name: 'rows',
        def: '5',
        type: 'Number',
        desc: 'Number of rows for use with multiline prop'
      },
      {
        name: 'dark',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Dark theme with inverted color scheme'
      },
      {
        name: 'small, <br />medium, <br />large',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Sizes'
      },
      {
        name: 'centered',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Centered align input content'
      }
    ]
  }
];

const Eg = [
  {
    title: '# Usage',
    react: `
<Textfield
  label="Email address"
  placeholder="your.name@email.com"
  hint="Please insert your email address."
  required
  maxLength="3"
/>
<Textfield
  type="password"
  label="Password"
  placeholder="••••••••"
  hint="Your password must be at least 6 characters ..."
  required
/>
<Textfield
  type="number"
  label="Quantity"
  hint="Quantity has to be between 2 and 10"
  min="2"
  max="10"
  required
/>
<Textfield
  label='Dark theme'
  placeholder='Please enter your name'
  dark
/>
    `
  },
  {
    title: '# Prepending / Appending icon',
    react: `
<Textfield
  prependIcon="search"
  label="Search"
  placeholder="Search site"
  hint="Please enter your search terms."
/>
<Textfield
  prependIcon="date"
  label="Date"
  type="date"
  placeholder="DD/MM/YYYY"
  hint="Please enter date."
/>
<Textfield
  appendIcon="nabtrade"
  label="Label"
  placeholder="Placeholder goes here"
  hint="Hint goes here."
/>
<Title size="6">Boxed</Title>
<Textfield
  prependBoxedIcon="date"
  label="Label"
  placeholder="Placeholder goes here"
  hint="Hint goes here."
/>
<Textfield
  appendBoxedIcon="nabtrade"
  label="Label"
  placeholder="Placeholder goes here"
  hint="Hint goes here."
/>
    `
  },
  {
    title: '# Prefix / Suffix',
    react: `
<Textfield
  prefix="$"
  label="Amount"
  placeholder="Enter price"
  hint="Please enter the purchase price."
/>
<Textfield
  suffix="@email.com"
  label="Email address"
  placeholder="Enter email address"
  hint="Please enter your email address."
/>
    `
  },
  {
    title: '# States',
    react: `
<Textfield
  label="Default"
  placeholder="Placeholder goes here"
  hint="Hint goes here."
/>
<Textfield
  success
  label="Success"
  placeholder="Placeholder goes here"
  value="Successful text entry"
  hint="Hint goes here."
/>
<Textfield
  success
  prependIcon="male"
  label="Success"
  placeholder="Placeholder goes here"
  value="Successful text entry"
  hint="Hint goes here."
/>
<Textfield
  success
  appendIcon="nabtrade"
  label="Success"
  placeholder="Placeholder goes here"
  value="Successful text entry"
  hint="Hint goes here."
/>
<Textfield
  success
  prependBoxedIcon="male"
  label="Success"
  placeholder="Placeholder goes here"
  value="Successful text entry"
  hint="Hint goes here."
/>
<Textfield
  success
  appendBoxedIcon="nabtrade"
  label="Success"
  placeholder="Placeholder goes here"
  value="Successful text entry"
  hint="Hint goes here."
/>

<Textfield
  error
  label="Error"
  placeholder="Placeholder goes here"
  value="Invalid entry"
  hint="Hint goes here."
  errorMessage="Error message goes here"
/>
<Textfield
  error
  prependIcon="male"
  label="Error"
  placeholder="Placeholder goes here"
  hint="Hint goes here."
  errorMessage="Error message goes here"
/>
<Textfield
  error
  appendIcon="nabtrade"
  label="Error"
  placeholder="Placeholder goes here"
  value="Invalid entry"
  hint="Hint goes here."
  errorMessage="Error message goes here"
/>
<Textfield
  error
  prependBoxedIcon="male"
  label="Error"
  placeholder="Placeholder goes here"
  value="Invalid entry"
  hint="Hint goes here."
  errorMessage="Error message goes here"
/>
<Textfield
  error
  appendBoxedIcon="nabtrade"
  label="Error"
  placeholder="Placeholder goes here"
  value="Invalid entry"
  hint="Hint goes here."
  errorMessage="Error message goes here"
/>
    `
  },
  {
    title: '# Combination with select field',
    react: `
<MultiField
  label="Label"
  leftClassName="is-flex-1"
  leftSlot={
    <Select
      isSearchable={false}
      options={options}
      defaultValue={options[0]}
    />
  }
  rightClassName="is-flex-3"
  rightSlot={<Textfield />}
/>
<MultiField
  label="Label"
  leftClassName="is-flex-3"
  rightSlot={
    <Select
      isSearchable={false}
      options={options}
      defaultValue={options[0]}
    />
  }
  rightClassName="is-flex-1"
  leftSlot={<Textfield />}
/>
		`
  },
  {
    title: '# Date',
    react: `
<Textfield
  type="date"
  label="Date"
  hint="Please enter your preferred date"
  required
/>
    `
  },
  {
    title: '# Mask',
    react: `
<Textfield
  mask="(99) 9999-9999"
  label="Phone number "
  placeholder="eg. (02) 9302-9391"
  hint="Please enter your contact number."
/>
    `
  },
  { title: '# Sizes', react: `` },
  {
    title: '# Multiline',
    react: `
<Textfield
  multiline
  label="Comments"
  placeholder="Enter comments here"
  hint="Please provide your feedback."
/>
    `
  }
];

const options = [
  { value: 'foo', label: 'Foo' },
  { value: 'bar', label: 'Bar' },
  { value: 'Fizz', label: 'Fizz' },
  { value: 'Buzz', label: 'Buzz' }
];
