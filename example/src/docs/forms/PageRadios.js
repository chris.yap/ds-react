import React, { Component } from 'react'
import PageHeader from '../../components/PageHeader'
import Demo from '../../components/Demo'
import {
  Column,
  Columns,
  Container,
  Divider,
  Radio,
  RadioGroup,
  Section,
  Tab,
  Tabs,
  Table,
  Title
} from 'shaper-react'
import { PropsHeaders, PropsTableRow } from '../../constants/PropsTables'
import { PropsRadio, PropsRadioGroup } from '../../constants/FormsProps'

export default class PageRadios extends Component {
  constructor(props) {
    super(props)
    this.state = { index: 0, selectedValue: 'apple' }
    this.handleChange = this.handleChange.bind(this)
    this.handleStatusChange = this.handleStatusChange.bind(this)
  }

  handleChange(value) {
    if (value) {
      this.setState({ selectedValue: value })
    }
  }
  handleStatusChange(value) {
    if (value) {
      this.setState({ selectedStatusValue: value })
    }
  }
  updateIndex = val => {
    this.setState({ index: val })
  };
  render() {
    const { index } = this.state
    return (
      <div id='start'>
        <PageHeader category='forms' title='Radios'>
          A graphical control element that allows the user to choose only one of
          a predefined set of mutually exclusive options.
        </PageHeader>

        <Divider className='my-0' />

        <Section>
          <Container>
            <Columns mobile>
              <Column className='is-12-mobile is-10-desktop is-offset-1-desktop'>
                <Demo examples={Eg} updateIndex={this.updateIndex}>
                  {index === 0 ? (
                    <RadioGroup
                      name='fruit0'
                      label='Which is your favourite fruit?'
                      selectedValue={this.state.selectedValue}
                      onChange={this.handleChange}
                    >
                      <Radio value='apple' label='Apple' />
                      <Radio value='orange' label='Orange' />
                      <Radio value='watermelon' label='Watermelon' />
                      <Radio value='durian' label='Durian' disabled />
                    </RadioGroup>
                  ) : index === 1 ? (
                    <React.Fragment>
                      <RadioGroup
                        buttons
                        name='fruit1'
                        label='Which is your favourite fruit?'
                        selectedValue={this.state.selectedValue}
                        onChange={this.handleChange}
                      >
                        <Radio button value='apple' label='Apple' />
                        <Radio button value='orange' label='Orange' />
                        <Radio button value='watermelon' label='Watermelon' />
                        <Radio button value='durian' label='Durian' disabled />
                      </RadioGroup>
                      <RadioGroup
                        buttons
                        name='fruit2'
                        label='Status'
                        selectedValue={this.state.selectedStatusValue}
                        onChange={this.handleStatusChange}
                      >
                        <Radio button value='default' label='Default' />
                        <Radio button success value='success' label='Success' />
                        <Radio
                          button
                          info
                          value='information'
                          label='Information'
                        />
                        <Radio button warning value='warning' label='Warning' />
                        <Radio button danger value='danger' label='Danger' />
                      </RadioGroup>
                    </React.Fragment>
                  ) : index === 2 ? (
                    <RadioGroup
                      buttons
                      rounded
                      name='fruit3'
                      label='Which is your favourite fruit?'
                      selectedValue={this.state.selectedValue}
                      onChange={this.handleChange}
                    >
                      <Radio button value='apple' label='Apple' />
                      <Radio button value='orange' label='Orange' />
                      <Radio button value='watermelon' label='Watermelon' />
                      <Radio button value='durian' label='Durian' disabled />
                    </RadioGroup>
                  ) : index === 2 ? (
                    <RadioGroup
                      buttons
                      rounded
                      fullWidth
                      name='fruit3'
                      label='Which is your favourite fruit?'
                      selectedValue={this.state.selectedValue}
                      onChange={this.handleChange}
                    >
                      <Radio button value='apple' label='Apple' />
                      <Radio button value='orange' label='Orange' />
                      <Radio button value='watermelon' label='Watermelon' />
                      <Radio button value='durian' label='Durian' disabled />
                    </RadioGroup>
                  ) : (
                    <React.Fragment>
                      <RadioGroup
                        buttons
                        name='fruit2'
                        label='Small'
                        selectedValue={this.state.selectedValue}
                        onChange={this.handleChange}
                      >
                        <Radio button small value='apple' label='Apple' />
                        <Radio button small value='orange' label='Orange' />
                        <Radio
                          button
                          small
                          value='watermelon'
                          label='Watermelon'
                        />
                        <Radio
                          button
                          small
                          value='durian'
                          label='Durian'
                          disabled
                        />
                      </RadioGroup>
                      <RadioGroup
                        buttons
                        name='fruit1'
                        label='Default'
                        selectedValue={this.state.selectedValue}
                        onChange={this.handleChange}
                      >
                        <Radio button value='apple' label='Apple' />
                        <Radio button value='orange' label='Orange' />
                        <Radio button value='watermelon' label='Watermelon' />
                        <Radio button value='durian' label='Durian' disabled />
                      </RadioGroup>

                      <RadioGroup
                        buttons
                        name='fruit3'
                        label='Large'
                        selectedValue={this.state.selectedValue}
                        onChange={this.handleChange}
                      >
                        <Radio button large value='apple' label='Apple' />
                        <Radio button large value='orange' label='Orange' />
                        <Radio
                          button
                          large
                          value='watermelon'
                          label='Watermelon'
                        />
                        <Radio
                          button
                          large
                          value='durian'
                          label='Durian'
                          disabled
                        />
                      </RadioGroup>
                    </React.Fragment>
                  )}
                </Demo>
                {/* <Divider my={4} /> */}
                <Title size='4' className='font-nabimpact'>
                  React props
                </Title>
                <Tabs>
                  <Tab label='RadioGroup'>
                    <Table
                      striped
                      className='mt-0'
                      headers={PropsHeaders}
                      data={PropsRadioGroup}
                      tableRow={PropsTableRow}
                    />
                  </Tab>
                  <Tab label='Radio'>
                    <Table
                      striped
                      className='mt-0'
                      headers={PropsHeaders}
                      data={PropsRadio}
                      tableRow={PropsTableRow}
                    />
                  </Tab>
                </Tabs>
              </Column>
            </Columns>
          </Container>
        </Section>
      </div>
    )
  }
}

const Eg = [
  {
    title: '# Usage',
    react: `
<RadioGroup
	name="fruit0"
	label="Which is your favourite fruit?"
	selectedValue={this.state.selectedValue}
	onChange={this.handleChange}
>
	<Radio value="apple" label="Apple" />
	<Radio value="orange" label="Orange" />
	<Radio value="watermelon" label="Watermelon" />
	<Radio value="durian" label="Durian" disabled />
</RadioGroup>
    `
  },
  {
    title: '# Buttons',
    react: `
<RadioGroup
	buttons
	name="fruit1"
	label="Which is your favourite fruit?"
	selectedValue={this.state.selectedValue}
	onChange={this.handleChange}
>
	<Radio button value="apple" label="Apple" />
	<Radio button value="orange" label="Orange" />
	<Radio button value="watermelon" label="Watermelon" />
	<Radio button value="durian" label="Durian" disabled />
</RadioGroup>
<RadioGroup
	buttons
	name="fruit2"
	label="Status"
	selectedValue={this.state.selectedStatusValue}
	onChange={this.handleStatusChange}
>
	<Radio button value="default" label="Default" />
	<Radio button success value="success" label="Success" />
	<Radio
		button
		info
		value="information"
		label="Information"
	/>
	<Radio button warning value="warning" label="Warning" />
	<Radio button danger value="danger" label="Danger" />
</RadioGroup>
    `
  },
  {
    title: '# Rounded',
    react: `
<RadioGroup
	buttons
	rounded
	name="fruit3"
	label="Which is your favourite fruit?"
	selectedValue={this.state.selectedValue}
	onChange={this.handleChange}
>
	<Radio button value="apple" label="Apple" />
	<Radio button value="orange" label="Orange" />
	<Radio button value="watermelon" label="Watermelon" />
	<Radio button value="durian" label="Durian" disabled />
</RadioGroup>
    `
  },
  {
    title: '# Full width',
    react: `
<RadioGroup
	buttons
	rounded
	fullWidth
	name="fruit3"
	label="Which is your favourite fruit?"
	selectedValue={this.state.selectedValue}
	onChange={this.handleChange}
>
	<Radio button value="apple" label="Apple" />
	<Radio button value="orange" label="Orange" />
	<Radio button value="watermelon" label="Watermelon" />
	<Radio button value="durian" label="Durian" disabled />
</RadioGroup>
    `
  },
  {
    title: '# Sizes',
    react: `
    
    <RadioGroup
    buttons
    name='fruit2'
    label='Small'
    selectedValue={this.state.selectedValue}
    onChange={this.handleChange}
  >
    <Radio button small value='apple' label='Apple' />
    <Radio button small value='orange' label='Orange' />
    <Radio
      button
      small
      value='watermelon'
      label='Watermelon'
    />
    <Radio
      button
      small
      value='durian'
      label='Durian'
      disabled
    />
  </RadioGroup>
  <RadioGroup
    buttons
    name='fruit1'
    label='Default'
    selectedValue={this.state.selectedValue}
    onChange={this.handleChange}
  >
    <Radio button value='apple' label='Apple' />
    <Radio button value='orange' label='Orange' />
    <Radio button value='watermelon' label='Watermelon' />
    <Radio button value='durian' label='Durian' disabled />
  </RadioGroup>

  <RadioGroup
    buttons
    name='fruit3'
    label='Large'
    selectedValue={this.state.selectedValue}
    onChange={this.handleChange}
  >
    <Radio button large value='apple' label='Apple' />
    <Radio button large value='orange' label='Orange' />
    <Radio
      button
      large
      value='watermelon'
      label='Watermelon'
    />
    <Radio
      button
      large
      value='durian'
      label='Durian'
      disabled
    />
  </RadioGroup>
    `
  }
]
