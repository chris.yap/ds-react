import React, { Component } from 'react';
import PageHeader from '../../components/PageHeader';
import Demo from '../../components/Demo';
import PropsTable from '../../components/PropsTable';
import {
  Button,
  Column,
  Columns,
  Container,
  Divider,
  MultiField,
  Section,
  Select,
  Textfield
} from 'shaper-react';

import { names } from '../../constants/Names';

export default class PageSelects extends Component {
  constructor(props) {
    super(props);
    this.state = { index: 0, inputValue: '', value: '' };
  }
  handleInputChange(newValue, event) {
    if (event.action !== 'input-blur' && event.action !== 'menu-close') {
      const inputValue = newValue.replace(/\W/g, '');
      if (inputValue) {
        this.setState({ inputValue: inputValue });
      }
    }
  }
  filterNames = inputValue => {
    if (inputValue) {
      return names.filter(i =>
        i.label.toLowerCase().includes(inputValue.toLowerCase())
      );
    }
  };
  loadOptions = (inputValue, callback) => {
    setTimeout(() => {
      callback(this.filterNames(inputValue));
    }, 1000);
  };
  handleChange = (option, event) => {
    console.log(`Result: `, event, option);
    this.setState({ selectedOption: option, value: option });
  };
  updateIndex = val => {
    this.setState({ index: val });
  };
  render() {
    const { index } = this.state;
    return (
      <div id="start">
        <PageHeader category="forms" title="Selects">
          Select fields components are used for collecting user provided
          information from a list of options.
        </PageHeader>

        <Divider />

        <Section>
          <Container>
            <Columns mobile>
              <Column className="is-12-mobile is-10-desktop is-offset-1-desktop">
                {`${this.state.value && this.state.value.label}`}
                <Demo examples={Eg} updateIndex={this.updateIndex}>
                  {index === 0 ? (
                    <Select
                      label="Label"
                      zIndex="20"
                      isClearable
                      options={options}
                      placeholder="Select an option"
                      name="color"
                      onChange={this.handleChange}
                      value={this.state.value}
                    />
                  ) : index === 1 ? (
                    <Select
                      isMulti
                      label="Label"
                      options={options}
                      placeholder="Select an option"
                    />
                  ) : index === 2 ? (
                    <>
                      <Select
                        label="Label"
                        isClearable
                        options={options}
                        placeholder="Select an option"
                        className="mb-2"
                        onInputChange={this.handleInputChange}
                        onChange={this.handleChange}
                        innerRef={ref => (this.select = ref)}
                      />

                      <Button
                        onClick={e => this.select.focus()}
                        className="ml-0">
                        Focus
                      </Button>
                      <Button onClick={e => this.select.blur()}>Blur</Button>
                    </>
                  ) : index === 3 ? (
                    <Select
                      hasIcons
                      isClearable
                      label="Label"
                      options={options2}
                      onChange={(o, a) => console.log(o, a)}
                      placeholder="Select an option"
                    />
                  ) : index === 4 ? (
                    <Select
                      isLoading
                      label="Label"
                      options={options}
                      placeholder="Select an option"
                    />
                  ) : index === 5 ? (
                    <Select
                      async
                      isClearable
                      label="Async"
                      cacheOption={false}
                      onBlur={this.handleInputBlur}
                      loadOptions={this.loadOptions}
                      placeholder="Search ..."
                      onChange={this.handleChange}
                      onInputChange={this.handleInputChange.bind(this)}
                      // inputValue={this.state.value}
                    />
                  ) : (
                    <>
                      <MultiField
                        label="Label"
                        leftClassName="is-flex-1"
                        leftSlot={
                          <Select
                            isSearchable={false}
                            options={options}
                            defaultValue={options[0]}
                          />
                        }
                        rightClassName="is-flex-3"
                        rightSlot={<Textfield />}
                      />
                      <MultiField
                        label="Label"
                        leftClassName="is-flex-3"
                        rightSlot={
                          <Select
                            isSearchable={false}
                            options={options}
                            defaultValue={options[0]}
                          />
                        }
                        rightClassName="is-flex-1"
                        leftSlot={<Textfield />}
                      />
                    </>
                  )}
                </Demo>

                <PropsTable data={props} />
              </Column>
            </Columns>
          </Container>
        </Section>
      </div>
    );
  }
}

const Eg = [
  {
    title: '# Usage',
    react: `
<Select
  label="Label"
  isClearable
  options={options}
  placeholder="Select an option"
/>
const options = [
  { value: 'foo', label: 'Foo' },
  { value: 'bar', label: 'Bar' },
  { value: 'Fizz', label: 'Fizz' },
  { value: 'Buzz', label: 'Buzz' },
];
    `
  },
  {
    title: '# Multiple',
    react: `
<Select
  isMulti
  label="Label"
  options={options}
  placeholder="Select an option"
/>
    `
  },
  {
    title: '# Focus/Blur',
    react: `
<Select
  label="Label"
  isClearable
  options={options}
  placeholder="Select an option"
  className="mb-2"
  onInputChange={this.handleInputChange}
  onChange={this.handleChange}
  innerRef={ref => (this.select = ref)}
/>

<Button
  onClick={e => this.select.focus()}
  className="ml-0"
>
  Focus
</Button>
<Button onClick={e => this.select.blur()}>Blur</Button>
    `
  },
  {
    title: '# Icons',
    react: `
<Select
  hasIcons
  isClearable
  label="Label"
  options={options2}
  onChange={(o, a) => console.log(o, a)}
  placeholder="Select an option"
/>
const options2 = [
	{ icon: 'briefcase', value: 'foo', label: 'Foo' },
	{ icon: 'feedback', value: 'bar', label: 'Bar' },
	{ icon: 'male-outline', value: 'Fizz', label: 'Fizz' },
	{ icon: 'cog', value: 'Buzz', label: 'Buzz' },
];
    `
  },
  {
    title: '# Loading',
    react: `
<Select
  isLoading
  label="Label"
  options={options}
  placeholder="Select an option"
/>
    `
  },
  {
    title: '# ASync',
    react: `
import { names } from '../../constants/Names';

export default class YourComponent extends Component {
	state = { inputValue: '' };
	handleInputChange = (newValue: string) => {
		const inputValue = newValue.replace(/\\W/g, '');
		this.setState({ inputValue });
		return inputValue;
	};
	filterNames = (inputValue: string) => {
		if (inputValue) {
			return names.filter(i => i.label.toLowerCase().includes(inputValue.toLowerCase()));
		}
	};
	loadOptions = (inputValue, callback) => {
		setTimeout(() => {
			callback(this.filterNames(inputValue));
		}, 1000);
	};
	render() {
		return (
			<Select
        async
        isClearable
        label="Async"
        cacheOption={false}
        onBlur={this.handleInputBlur}
        loadOptions={this.loadOptions}
        placeholder="Search ..."
        onChange={this.handleChange}
        onInputChange={this.handleInputChange.bind(this)}
      />
		)
	}
}
    `
  },
  {
    title: '# Combination with text field',
    react: `
<MultiField
  label="Label"
  leftClassName="is-flex-1"
  leftSlot={
    <Select
      isSearchable={false}
      options={options}
      defaultValue={options[0]}
    />
  }
  rightClassName="is-flex-3"
  rightSlot={<Textfield />}
/>
<MultiField
  label="Label"
  leftClassName="is-flex-3"
  rightSlot={
    <Select
      isSearchable={false}
      options={options}
      defaultValue={options[0]}
    />
  }
  rightClassName="is-flex-1"
  leftSlot={<Textfield />}
/>
		`
  }
];

const props = [
  {
    items: [
      {
        name: 'autoFocus',
        def: 'undefined',
        type: 'boolean',
        desc: 'Focus the control when it is mounted'
      },
      {
        name: 'async',
        def: 'undefined',
        type: 'boolean',
        desc: 'Setup Async select '
      },
      {
        name: 'defaultMenuIsOpen',
        def: 'undefined',
        type: 'boolean',
        desc: 'Set select menu to open on load'
      },
      {
        name: 'defaultValue',
        def: 'undefined',
        type: 'Object, Array&#60;Object&#62;',
        desc: 'Default selected item/s'
      },
      {
        name: 'error',
        def: 'undefined',
        type: 'boolean',
        desc: 'Does the select has error'
      },
      {
        name: 'errorMessage',
        def: 'undefined',
        type: 'string',
        desc: 'Error message if has error'
      },
      {
        name: 'getOptionLabel',
        def: 'undefined',
        type: 'string',
        desc: 'Override default option styling'
      },
      {
        name: 'getOptionValue',
        def: 'undefined',
        type: 'string',
        desc: 'Override default option value'
      },
      {
        name: 'hasIcons',
        def: 'undefined',
        type: 'boolean',
        desc: 'Has icons in data'
      },
      { name: 'hint', def: 'undefined', type: 'string', desc: 'Help text' },
      {
        name: 'height',
        def: 'undefined',
        type: 'number/string',
        desc: 'Override height of select component'
      },
      {
        name: 'id',
        def: 'auto-generated',
        type: 'string',
        desc: 'Ability to add personalised ID'
      },
      {
        name: 'isDisabled',
        def: 'false',
        type: 'boolean',
        desc: 'Is the select disabled'
      },
      {
        name: 'isLoading',
        def: 'false',
        type: 'boolean',
        desc: 'Is the select in a state of loading (async)'
      },
      {
        name: 'isMulti',
        def: 'false',
        type: 'boolean',
        desc: 'Support multiple selected options'
      },
      {
        name: 'isSearchable',
        def: 'true',
        type: 'boolean',
        desc: 'Whether to enable search functionality'
      },
      {
        name: 'label',
        def: 'undefined',
        type: 'string',
        desc: "Select's label"
      },
      {
        name: 'name',
        def: 'undefined',
        type: 'string',
        desc:
          'Name of the HTML Input (optional - without this, no input will be rendered)'
      },
      {
        name: 'onChange',
        def: `(option, action) => undefined`,
        type: 'function',
        desc: 'Handle change events on the select'
      },
      {
        name: 'options',
        def: 'undefined',
        type: 'Array',
        desc: 'Array of options that populate the select menu'
      },
      {
        name: 'placeholder',
        def: 'Select ...',
        type: 'string',
        desc: 'Placeholder text for the select value'
      },
      {
        name: 'required',
        def: 'undefined',
        type: 'boolean',
        desc: 'Display asterisk at the label'
      },
      // { name: 'success', def: '', type: '', desc: '' },
      {
        name: 'value',
        def: 'undefined',
        type: 'union',
        desc: 'The value of the select; reflected by the selected option'
      }
    ]
  }
];

const options = [
  { value: 'foo', label: 'Foo' },
  { value: 'bar', label: 'Bar' },
  { value: 'Fizz', label: 'Fizz' },
  { value: 'Buzz', label: 'Buzz' }
];

const options2 = [
  { icon: 'briefcase', value: 'foo', label: 'Foo' },
  { icon: 'feedback', value: 'bar', label: 'Bar' },
  { icon: 'male-outline', value: 'Fizz', label: 'Fizz' },
  { icon: 'cog', value: 'Buzz', label: 'Buzz' }
];
