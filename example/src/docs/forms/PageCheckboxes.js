import React from 'react';
import PageHeader from '../../components/PageHeader';
import Demo from '../../components/Demo';
import {
  Checkbox,
  CheckboxGroup,
  Column,
  Columns,
  Container,
  Divider,
  Section,
  Tab,
  Tabs,
  Table,
  Title
} from 'shaper-react';
import { PropsHeaders, PropsTableRow } from '../../constants/PropsTables';

const Page = () => {
  const [index, updateIndex] = React.useState(0);
  const [fruits, updateFruits] = React.useState(['apple', 'watermelon']);

  React.useEffect(() => {
    setTimeout(() => {
      updateFruits(['apple', 'orange']);
    }, 5000);
  }, []);

  return (
    <div id="start">
      <PageHeader category="forms" title="Checkboxes">
        A GUI widget that permits the user to make a binary choice, i.e. a
        choice between one of two possible mutually exclusive options.
      </PageHeader>

      <Divider className="my-0" />

      <Section>
        <Container>
          <Columns mobile>
            <Column className="is-12-mobile is-10-desktop is-offset-1-desktop">
              <Demo examples={Eg} updateIndex={updateIndex}>
                {index === 0 ? (
                  <React.Fragment>
                    <CheckboxGroup
                      name="fruits"
                      required
                      label="Terms and conditions"
                      value={fruits}
                      onChange={updateFruits}
                      className="mb-8">
                      <Checkbox
                        value="apple"
                        label="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac augue sodales augue mattis sagittis in eu ligula. Sed ullamcorper, tellus id bibendum aliquet, nisl nibh ornare leo, at aliquam turpis libero vitae nisl. Cras ultricies neque nec consectetur euismod."
                      />
                    </CheckboxGroup>
                    <CheckboxGroup
                      checkboxDepth={2}
                      name="fruits"
                      label="Which is/are your favourite fruit/s?"
                      value={fruits}
                      onChange={updateFruits}
                      help="Please select one or more options">
                      <Checkbox value="apple" label="Apple" />
                      <Checkbox value="orange" label="Orange" />
                      <Checkbox value="watermelon" label="Watermelon" />
                      <Checkbox value="durian" label="Durian" disabled />
                    </CheckboxGroup>
                    <CheckboxGroup
                      stacked
                      checkboxDepth={2}
                      name="fruits"
                      label="Which is/are your favourite fruit/s?"
                      value={fruits}
                      onChange={updateFruits}
                      help="Please select one or more options">
                      <Checkbox value="apple" label="Apple" />
                      <Checkbox value="orange" label="Orange" />
                      <Checkbox value="watermelon" label="Watermelon" />
                      <Checkbox value="durian" label="Durian" disabled />
                    </CheckboxGroup>
                  </React.Fragment>
                ) : index === 1 ? (
                  <React.Fragment>
                    <CheckboxGroup
                      isSwitch
                      name="fruits"
                      value={fruits}
                      onChange={updateFruits}>
                      <Checkbox value="apple" label="Do you like apples?" />
                    </CheckboxGroup>

                    <CheckboxGroup
                      isSwitch
                      right
                      name="fruits"
                      value={fruits}
                      onChange={updateFruits}>
                      <Checkbox value="apple" label="Do you like apples?" />
                    </CheckboxGroup>

                    <CheckboxGroup
                      isSwitch
                      fullwidth
                      right
                      name="fruits"
                      value={fruits}
                      onChange={updateFruits}>
                      <Checkbox value="apple" label="Do you like apples?" />
                    </CheckboxGroup>

                    <CheckboxGroup
                      isSwitch
                      right
                      name="fruits"
                      value={fruits}
                      onChange={updateFruits}>
                      <Checkbox
                        value="apple"
                        label="Do you like apples? (Disabled)"
                        disabled
                      />
                    </CheckboxGroup>

                    <Divider className="mb-8" />

                    <CheckboxGroup
                      isSwitch
                      right
                      name="fruits"
                      value={fruits}
                      onChange={updateFruits}>
                      <Checkbox
                        success
                        value="apple"
                        label="Do you like apples?"
                      />
                    </CheckboxGroup>

                    <CheckboxGroup
                      isSwitch
                      right
                      name="fruits"
                      value={fruits}
                      onChange={updateFruits}>
                      <Checkbox
                        info
                        value="apple"
                        label="Do you like apples?"
                      />
                    </CheckboxGroup>

                    <CheckboxGroup
                      isSwitch
                      right
                      name="fruits"
                      value={fruits}
                      onChange={updateFruits}>
                      <Checkbox
                        warning
                        value="apple"
                        label="Do you like apples?"
                      />
                    </CheckboxGroup>

                    <CheckboxGroup
                      isSwitch
                      right
                      name="fruits"
                      value={fruits}
                      onChange={updateFruits}>
                      <Checkbox
                        danger
                        value="apple"
                        label="Do you like apples?"
                      />
                    </CheckboxGroup>
                  </React.Fragment>
                ) : (
                  <React.Fragment>
                    <CheckboxGroup
                      buttons
                      name="fruits"
                      label="Which is/are your favourite fruit/s?"
                      value={fruits}
                      onChange={updateFruits}
                      className="mb-8">
                      <Checkbox value="apple" label="Apple" />
                      <Checkbox value="orange" label="Orange" />
                      <Checkbox value="watermelon" label="Watermelon" />
                      <Checkbox value="durian" label="Durian" disabled />
                    </CheckboxGroup>

                    <CheckboxGroup
                      buttons
                      rounded
                      name="fruits"
                      label="Which is/are your favourite fruit/s?"
                      value={fruits}
                      onChange={updateFruits}>
                      <Checkbox value="apple" label="Apple" />
                      <Checkbox value="orange" label="Orange" />
                      <Checkbox value="watermelon" label="Watermelon" />
                      <Checkbox value="durian" label="Durian" disabled />
                    </CheckboxGroup>
                  </React.Fragment>
                )}
              </Demo>

              {/* <Divider marginY={4} /> */}

              <Title size="4" className="font-nabimpact">
                React props
              </Title>
              <Tabs>
                <Tab label="CheckboxGroup">
                  <Table
                    striped
                    headers={PropsHeaders}
                    data={CheckboxGroupData}
                    tableRow={PropsTableRow}
                    defaultSortCol="name"
                  />
                </Tab>
                <Tab label="Checkbox">
                  <Table
                    striped
                    headers={PropsHeaders}
                    data={CheckboxData}
                    tableRow={PropsTableRow}
                    defaultSortCol="name"
                  />
                </Tab>
              </Tabs>
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};

export default Page;

const CheckboxGroupData = [
  {
    name: 'name',
    def: 'undefined',
    type: 'String',
    desc: 'Input name for the checkbox'
  },
  {
    name: 'stacked',
    def: 'undefined',
    type: 'Boolean',
    desc: 'Stacked checkboxes'
  },
  {
    name: 'value',
    def: 'undefined',
    type: 'Array',
    desc: 'Selected values'
  },
  {
    name: 'label',
    def: 'undefined',
    type: 'String',
    desc: 'Label for the Checkbox Group'
  },
  {
    name: 'onChange',
    def: 'undefined',
    type: 'Function',
    desc: 'onChange function to be called'
  },
  {
    name: 'buttons',
    def: 'undefined',
    type: 'Boolean',
    desc: 'Turn checkboxes into buttons'
  },
  {
    name: 'rounded',
    def: 'undefined',
    type: 'Boolean',
    desc: 'Make checkboxes buttons rounded. Works with buttons prop'
  },
  {
    name: 'className',
    def: 'undefined',
    type: 'String',
    desc: 'Add additonal class name/s'
  },
  {
    name: 'isSwitch',
    def: 'undefined',
    type: 'Boolean',
    desc: 'Turn checkbox into switch'
  },
  {
    name: 'right',
    def: 'undefined',
    type: 'Boolean',
    desc: 'Align switch right. Work with isSwitch'
  },
  {
    name: 'fullwidth',
    def: 'undefined',
    type: 'Boolean',
    desc: 'Make checkbox spread fullwidth.'
  }
];

const CheckboxData = [
  {
    name: 'label',
    def: 'undefined',
    type: 'String',
    desc: 'Label for checkbox. Work with buttons and switches'
  },
  {
    name: 'id',
    def: 'auto-generated',
    type: 'string',
    desc: 'Ability to add personalised ID'
  },
  {
    name: 'success, info, warning, danger',
    def: 'undefined',
    type: 'Boolean',
    desc: 'Colour styling for checkbox'
  },
  {
    name: 'disabled',
    def: 'undefined',
    type: 'Boolean',
    desc: 'Make checkbox disabled'
  }
];

const Eg = [
  {
    title: '# Usage',
    react: `
<CheckboxGroup
  name="fruits"
  required
  label="Terms and conditions"
  value={this.state.fruits}
  onChange={this.fruitsChanged}
  className="mb-8"
>
  <Checkbox
    value="apple"
    label="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac augue sodales augue mattis sagittis in eu ligula. Sed ullamcorper, tellus id bibendum aliquet, nisl nibh ornare leo, at aliquam turpis libero vitae nisl. Cras ultricies neque nec consectetur euismod."
  />
</CheckboxGroup>
<CheckboxGroup
  checkboxDepth={2}
  name="fruits"
  label="Which is/are your favourite fruit/s?"
  value={this.state.fruits}
  onChange={this.fruitsChanged}
  help="Please select one or more options"
>
  <Checkbox value="apple" label="Apple" />
  <Checkbox value="orange" label="Orange" />
  <Checkbox value="watermelon" label="Watermelon" />
  <Checkbox value="durian" label="Durian" disabled />
</CheckboxGroup>
    `
  },
  {
    title: '# Switches',
    react: `
<CheckboxGroup
  isSwitch
  name="fruits"
  value={this.state.fruits}
  onChange={this.fruitsChanged}
>
  <Checkbox value="apple" label="Do you like apples?" />
</CheckboxGroup>

<CheckboxGroup
  isSwitch
  right
  name="fruits"
  value={this.state.fruits}
  onChange={this.fruitsChanged}
>
  <Checkbox value="apple" label="Do you like apples?" />
</CheckboxGroup>

<CheckboxGroup
  isSwitch
  fullwidth
  right
  name="fruits"
  value={this.state.fruits}
  onChange={this.fruitsChanged}
>
  <Checkbox value="apple" label="Do you like apples?" />
</CheckboxGroup>

<CheckboxGroup
  isSwitch
  right
  name="fruits"
  value={this.state.fruits}
  onChange={this.fruitsChanged}
>
  <Checkbox
    value="apple"
    label="Do you like apples? (Disabled)"
    disabled
  />
</CheckboxGroup>

<Divider className="mb-8" />

<CheckboxGroup
  isSwitch
  right
  name="fruits"
  value={this.state.fruits}
  onChange={this.fruitsChanged}
>
  <Checkbox
    success
    value="apple"
    label="Do you like apples?"
  />
</CheckboxGroup>

<CheckboxGroup
  isSwitch
  right
  name="fruits"
  value={this.state.fruits}
  onChange={this.fruitsChanged}
>
  <Checkbox
    info
    value="apple"
    label="Do you like apples?"
  />
</CheckboxGroup>

<CheckboxGroup
  isSwitch
  right
  name="fruits"
  value={this.state.fruits}
  onChange={this.fruitsChanged}
>
  <Checkbox
    warning
    value="apple"
    label="Do you like apples?"
  />
</CheckboxGroup>

<CheckboxGroup
  isSwitch
  right
  name="fruits"
  value={this.state.fruits}
  onChange={this.fruitsChanged}
>
  <Checkbox
    danger
    value="apple"
    label="Do you like apples?"
  />
</CheckboxGroup>
    `
  },
  {
    title: '# Buttons',
    react: `
<CheckboxGroup
  buttons
  name="fruits"
  label="Which is/are your favourite fruit/s?"
  value={this.state.fruits}
  onChange={this.fruitsChanged}
  className="mb-8"
>
  <Checkbox value="apple" label="Apple" />
  <Checkbox value="orange" label="Orange" />
  <Checkbox value="watermelon" label="Watermelon" />
  <Checkbox value="durian" label="Durian" disabled />
</CheckboxGroup>

<CheckboxGroup
  buttons
  rounded
  name="fruits"
  label="Which is/are your favourite fruit/s?"
  value={this.state.fruits}
  onChange={this.fruitsChanged}
>
  <Checkbox value="apple" label="Apple" />
  <Checkbox value="orange" label="Orange" />
  <Checkbox value="watermelon" label="Watermelon" />
  <Checkbox value="durian" label="Durian" disabled />
</CheckboxGroup>
    `
  }
];
