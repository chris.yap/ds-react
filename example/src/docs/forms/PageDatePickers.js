import React from 'react';
import PageHeader from '../../components/PageHeader';
import Demo from '../../components/Demo';
import PropsTable from '../../components/PropsTable';
// import moment from 'moment';
import {
  // Alert,
  Column,
  Columns,
  Container,
  DatePicker,
  Divider,
  Section
  // Textfield
} from 'shaper-react';

const Page = () => {
  const [index, updateIndex] = React.useState(0);
  const [date, updateDate] = React.useState();

  let valid = current => {
    return current.day() !== 0 && current.day() !== 6;
  };
  return (
    <div id="start">
      <PageHeader category="forms" title="Date/Time pickers">
        {/* A simple responsive footer which can include anything. */}
      </PageHeader>

      <Divider />

      <Section>
        <Container>
          <Columns mobile>
            <Column mobile={12} desktop={10} desktopOffset={1}>
              <Demo examples={Eg} updateIndex={updateIndex}>
                {date && <p>Selected date: {`${date}`}</p>}
                {index === 0 ? (
                  <>
                    <DatePicker
                      zIndex={10}
                      isClearable
                      id="datepicker-only"
                      required
                      label="Date picker"
                      defaultValue={date}
                      value={date}
                      onChange={date => updateDate(date)}
                      inputProps={{
                        placeholder: 'Insert date'
                      }}
                    />
                    <DatePicker
                      zIndex={9}
                      required
                      label="Time picker"
                      defaultValue={date}
                      dateFormat={false}
                      timeFormat={true}
                      onChange={date => updateDate(date)}
                      timeConstraints={{ minutes: { step: 15 } }}
                      inputProps={{
                        placeholder: 'Insert time'
                      }}
                    />
                    <DatePicker
                      zIndex={8}
                      required
                      label="Date and time picker"
                      defaultValue={date}
                      timeFormat={true}
                      onChange={date => updateDate(date)}
                      timeConstraints={{ minutes: { step: 15 } }}
                      inputProps={{
                        placeholder: 'Insert date and time'
                      }}
                    />
                  </>
                ) : index === 1 ? (
                  <DatePicker
                    zIndex={8}
                    required
                    isValidDate={valid}
                    label="Date picker (excluding weekends)"
                    defaultValue={date}
                    timeFormat={true}
                    onChange={date => updateDate(date)}
                    timeConstraints={{ minutes: { step: 15 } }}
                    inputProps={{
                      placeholder: 'Insert date and time',
                      id: 'weekday'
                    }}
                  />
                ) : (
                  <>
                    <DatePicker
                      zIndex={10}
                      id="left"
                      required
                      dateFormat="DD/MM/YYYY"
                      timeFormat={false}
                      label="Date picker"
                      onChange={date => updateDate(date)}
                      inputProps={{
                        placeholder: 'Insert date'
                      }}
                    />
                    <DatePicker
                      isRight
                      id="right"
                      required
                      dateFormat="DD/MM/YYYY"
                      timeFormat={false}
                      label="Date picker"
                      onChange={date => updateDate(date)}
                      inputProps={{
                        placeholder: 'Insert date'
                      }}
                    />
                  </>
                )}
              </Demo>
              <PropsTable data={Data} />
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};

export default Page;

const Data = [
  {
    items: [
      {
        name: 'value',
        def: 'new Date()',
        type: 'Date',
        desc: (
          <p className="mb-0">
            Represents the selected date by the component, in order to use it as
            a{' '}
            <a href="https://facebook.github.io/react/docs/forms.html#controlled-components">
              controlled component
            </a>
            . This prop is parsed by Moment.js, so it is possible to use a date
            <code>string</code> or a <code>moment</code> object.
          </p>
        )
      },
      {
        name: 'defaultValue',
        def: 'new Date()',
        type: 'Date',
        desc: (
          <p className="mb-0">
            Represents the selected date by the component, in order to use it as
            a{' '}
            <a href="https://facebook.github.io/react/docs/forms.html#controlled-components">
              controlled component
            </a>
            . This prop is parsed by Moment.js, so it is possible to use a date
            <code>string</code> or a <code>moment</code> object.
          </p>
        )
      },
      {
        name: 'viewDate',
        def: 'new Date()',
        type: 'Date',
        desc: (
          <p className="mb-0">
            Represents the month which is viewed on opening the calendar when
            there is no selected date. This prop is parsed by Moment.js, so it
            is possible to use a date
            <code>string</code> or a <code>moment</code> object.
          </p>
        )
      },
      {
        name: 'dateFormat',
        def: 'true',
        type: 'Boolean or String	',
        desc: (
          <p className="mb-0">
            Defines the format for the date. It accepts any{' '}
            <a href="http://momentjs.com/docs/#/displaying/format/">
              Moment.js date format
            </a>{' '}
            (not in localized format). If true the date will be displayed using
            the defaults for the current locale. If false the datepicker is
            disabled and the component can be used as timepicker, see{' '}
            <a href="https://github.com/YouCanBookMe/react-datetime#specify-available-units">
              available units docs
            </a>
            .
          </p>
        )
      },
      {
        name: 'timeFormat',
        def: 'false',
        type: 'Boolean or String	',
        desc: (
          <p className="mb-0">
            Defines the format for the time. It accepts any{' '}
            <a href="http://momentjs.com/docs/#/displaying/format/">
              Moment.js date format
            </a>{' '}
            (not in localized format). If true the date will be displayed using
            the defaults for the current locale. If false the timepicker is
            disabled and the component can be used as datepicker, see{' '}
            <a href="https://github.com/YouCanBookMe/react-datetime#specify-available-units">
              available units docs
            </a>
            .
          </p>
        )
      },
      {
        name: 'onChange',
        def: 'null',
        type: 'Function',
        desc: (
          <p className="mb-0">
            Callback trigger when the date changes. The callback receives the
            selected <code>moment</code> object as only parameter, if the date
            in the input is valid. If the date in the input is not valid, the
            callback receives the value of the input (a string).
          </p>
        )
      },
      {
        name: 'inputProps',
        def: 'undefined',
        type: 'Object',
        desc: (
          <p className="mb-0">
            Defines additional attributes for the input element of the
            component. For example: <code>onClick</code>,{' '}
            <code>placeholder</code>, <code>disabled</code>,{' '}
            <code>required</code>,<code>name</code> and <code>className</code>{' '}
            (className sets the class attribute for the input element).{' '}
          </p>
        )
      },
      {
        name: 'isValidDate',
        def: '() => true',
        type: 'Function',
        desc: (
          <p className="mb-0">
            Define the dates that can be selected. The function receives
            <code>(currentDate, selectedDate)</code> and shall return a{' '}
            <code>true</code> or <code>false</code> whether the{' '}
            <code>currentDate</code> is valid or not.
          </p>
        )
      },
      {
        name: 'closeOnSelect',
        def: 'false',
        type: 'Boolean',
        desc: (
          <p className="mb-0">
            When <code>true</code>, once the day has been selected, the
            datepicker will be automatically closed.
          </p>
        )
      },
      {
        name: 'timeConstraints',
        def: 'null',
        type: 'Object',
        desc: (
          <p className="mb-0">
            Add some constraints to the timepicker. It accepts an object with
            the format{' '}
            <code>
              &#123; hours: &#123; min: 9, max: 15, step: 2 &#125;&#125;
            </code>
            , this example means the hours can't be lower than <code>9</code>{' '}
            and higher than
            <code>15</code>, and it will change adding or subtracting{' '}
            <code>2</code> hours everytime the buttons are clicked. The
            constraints can be added to the <code>hours</code>,
            <code>minutes</code>, <code>seconds</code> and{' '}
            <code>milliseconds</code>.
          </p>
        )
      },
      {
        name: 'isRight',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Align picker to the right'
      },
      {
        name: 'isClearable',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Ability to clear field with clear button'
      }
    ]
  }
];

const Eg = [
  {
    title: '# Usage',
    react: `
<DatePicker
  zIndex={10}
  isClearable
  id="date"
  required
  label="Date picker"
  defaultValue={date}
  onChange={date => updateDate(date)}
  inputProps={{
    placeholder: 'Insert date'
  }}
/>
<DatePicker
  zIndex={9}
  id="time"
  required
  label="Time picker"
  defaultValue={date}
  dateFormat={false}
  timeFormat={true}
  onChange={date => updateDate(date)}
  timeConstraints={{ minutes: { step: 15 } }}
  inputProps={{
    placeholder: 'Insert time'
  }}
/>
<DatePicker
  zIndex={8}
  id="datetime"
  required
  label="Date and time picker"
  defaultValue={date}
  timeFormat={true}
  onChange={date => updateDate(date)}
  timeConstraints={{ minutes: { step: 15 } }}
  inputProps={{
    placeholder: 'Insert date and time'
  }}
/>
    `
  },
  {
    title: '# Selectable Dates',
    react: `
let valid = current => {
  return current.day() !== 0 && current.day() !== 6;
};

<DatePicker
  required
  id="weekday"
  isValidDate={valid}
  label="Date picker (excluding weekends)"
  defaultValue={date}
  timeFormat={true}
  onChange={date => updateDate(date)}
  timeConstraints={{ minutes: { step: 15 } }}
  inputProps={{
    placeholder: 'Insert date and time'
  }}
/>
    `
  },
  {
    title: '# Alignment',
    react: ``
  }
];
