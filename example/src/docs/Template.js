import React from 'react';
import PageHeader from '../../components/PageHeader';
import Demo from '../../components/Demo';
import {
  Column,
  Columns,
  Container,
  Divider,
  Footer,
  Section
} from 'shaper-react';

const Page = () => {
  const [index, updateIndex] = React.useState(0);
  return (
    <div id="start">
      <PageHeader category="layout" title="Footer">
        A simple responsive footer which can include anything.
      </PageHeader>

      <Divider />

      <Section>
        <Container>
          <Columns mobile>
            <Column mobile={12} desktop={10} desktopOffset={1}>
              <Demo examples={Eg} updateIndex={updateIndex}>
                {index === 0 && Example}
              </Demo>
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};

export default Page;

const Eg = [
  {
    title: '# Usage',
    react: ``,
    html: ``
  }
];
