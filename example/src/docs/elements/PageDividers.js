import React from 'react';
import Demo from '../../components/Demo';
import PropsTable from '../../components/PropsTable';
import PageHeader from '../../components/PageHeader';
import {
  Column,
  Columns,
  Container,
  Divider,
  Tag,
  Section
} from 'shaper-react';

const PageDivider = () => {
  const [index, updateIndex] = React.useState(0);
  return (
    <div id="start">
      <PageHeader category="elements" title="Dividers">
        Component used for separating content <br />
        <Tag small>styled-component</Tag>
      </PageHeader>

      <Divider />

      <Section>
        <Container>
          <Columns mobile>
            <Column mobile={12} desktop={10} desktopOffset={1}>
              <Demo examples={Eg} updateIndex={updateIndex}>
                {index === 0 ? (
                  <Divider />
                ) : index === 1 ? (
                  <Divider vertical className="mx-auto" />
                ) : (
                  <Divider dotted />
                )}
              </Demo>
              <PropsTable data={Data} />
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};

export default PageDivider;

const Data = [
  {
    items: [
      {
        name: 'dark',
        def: 'undefined',
        type: 'Boolean',
        desc: 'When used on dark background'
      },
      {
        name: 'dotted',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Set dotted divider'
      },
      {
        name: 'vertical',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Set vertical divider'
      }
    ]
  }
];

const Eg = [
  {
    title: '# Usage',
    react: `
<Divider />
    `
  },
  {
    title: '# Vertical',
    react: `
<Divider vertical />
    `
  },
  {
    title: '# Dotted',
    react: `
<Divider dotted />
    `
  }
];
