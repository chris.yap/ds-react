import React from 'react';
import PageHeader from '../../components/PageHeader';
import Demo from '../../components/Demo';
import PropsTable from '../../components/PropsTable';
import {
  Box,
  Button,
  Column,
  Columns,
  Container,
  Divider,
  Icon,
  Section,
  Tag
} from 'shaper-react';

const PageButtons = ({ props }) => {
  const [index, updateIndex] = React.useState(0);
  const [load1, updateLoad1] = React.useState(false);
  const [load2, updateLoad2] = React.useState(false);
  const [load3, updateLoad3] = React.useState(false);
  const [load4, updateLoad4] = React.useState(false);
  const [load5, updateLoad5] = React.useState(false);
  const [load6, updateLoad6] = React.useState(false);
  const [load7, updateLoad7] = React.useState(false);
  return (
    <div id="start">
      <PageHeader category="elements" title="Buttons">
        The classic button, in different colors, sizes, and states. <br />
        <Tag small>styled-components</Tag>
      </PageHeader>

      <Divider />

      <Section>
        <Container>
          <Columns mobile>
            <Column mobile={12} desktop={10} desktopOffset={1}>
              <Demo examples={Eg} updateIndex={updateIndex}>
                {index === 0 ? (
                  <>
                    <p>
                      <Button>Default</Button>
                      <Button white>White</Button>
                      <Button primary>Primary</Button>
                      <Button secondary>Secondary</Button>
                    </p>
                    <p>
                      <Button success>Success</Button>
                      <Button info>Info</Button>
                      <Button warning>Warning</Button>
                      <Button danger>Danger</Button>
                    </p>
                  </>
                ) : index === 1 ? (
                  <p>
                    <Button>
                      <Icon>help-circle</Icon>
                      <span>Help</span>
                    </Button>
                    <Button primary>
                      <Icon>order</Icon>
                      <span>Order</span>
                    </Button>
                    <Button success>
                      <Icon>checked-circle</Icon>
                      <span>Success</span>
                    </Button>
                    <Button info>
                      <Icon>info-circle</Icon>
                      <span>Info</span>
                    </Button>
                    <Button warning>
                      <Icon>warning-triangle</Icon>
                      <span>Warning</span>
                    </Button>
                    <Button danger>
                      <span>Delete</span>
                      <Icon>close</Icon>
                    </Button>
                  </p>
                ) : index === 2 ? (
                  <p>
                    <Button disabled>Default</Button>
                    <Button disabled white>
                      White
                    </Button>
                    <Button disabled primary>
                      Primary
                    </Button>
                    <Button disabled secondary>
                      Secondary
                    </Button>
                    <Button disabled success>
                      Success
                    </Button>
                    <Button disabled info>
                      Info
                    </Button>
                    <Button disabled warning>
                      Warning
                    </Button>
                    <Button disabled danger>
                      Danger
                    </Button>
                  </p>
                ) : index === 3 ? (
                  <div>
                    <p>
                      Round buttons contain an icon, a number or a couple of
                      letters.
                    </p>
                    <Button round>
                      <Icon>quicklinks</Icon>
                    </Button>
                    <Button round primary>
                      <Icon>cog</Icon>
                    </Button>
                    <Button round success>
                      3
                    </Button>
                    <Button round info>
                      JS
                    </Button>
                    <Button round warning>
                      <Icon>warning-triangle</Icon>
                    </Button>
                    <Button round danger>
                      <Icon>lock</Icon>
                    </Button>
                  </div>
                ) : index === 4 ? (
                  <div>
                    <p>
                      Square buttons, like round buttons, also contain an icon
                      or a couple of number or letters.
                    </p>
                    <Button square>
                      <Icon>help-circle</Icon>
                    </Button>
                    <Button square primary>
                      <Icon>nabtrade</Icon>
                    </Button>
                    <Button square success>
                      3
                    </Button>
                    <Button square info>
                      JS
                    </Button>
                    <Button square warning>
                      <Icon>warning-triangle</Icon>
                    </Button>
                    <Button square danger>
                      <Icon>trash</Icon>
                    </Button>
                  </div>
                ) : index === 5 ? (
                  <>
                    <p>
                      <Button small>Small button</Button>
                      <Button small square>
                        <Icon>pencil</Icon>
                      </Button>
                      <Button round small>
                        <Icon>plus</Icon>
                      </Button>
                    </p>
                    <p>
                      <Button primary>Default button</Button>
                      <Button primary square>
                        <Icon>help-circle</Icon>
                      </Button>
                      <Button round danger>
                        <Icon>bin</Icon>
                      </Button>
                    </p>
                    <p>
                      <Button medium warning>
                        Medium button
                      </Button>
                      <Button medium warning square>
                        <Icon>checked-circle</Icon>
                      </Button>
                      <Button round warning medium>
                        <Icon>warning-triangle</Icon>
                      </Button>
                    </p>
                    <p>
                      <Button large success>
                        Large button
                      </Button>
                      <Button large success square>
                        <Icon>bin</Icon>
                      </Button>
                      <Button round success large>
                        <Icon>search</Icon>
                      </Button>
                    </p>
                  </>
                ) : index === 6 ? (
                  <>
                    <p>
                      Flat buttons have no box shadow and no background. Only on
                      hover is the container for the button shown. Should be
                      used with an icon.
                    </p>
                    <p>
                      <Button flat small info>
                        <Icon right>pdf</Icon>
                        <span>Download PDF</span>
                      </Button>
                      <Button flat small round>
                        <Icon>more-vertical</Icon>
                      </Button>
                    </p>
                    <p>
                      <Button flat>
                        <Icon left>cog</Icon>
                        <span>Settings</span>
                      </Button>
                      <Button flat round>
                        <Icon>cog</Icon>
                      </Button>
                    </p>
                    <p>
                      <Button flat medium>
                        <Icon>warning-triangle</Icon>
                        <span>Important information</span>
                      </Button>
                      <Button flat round medium danger>
                        <Icon>bin</Icon>
                      </Button>
                    </p>
                    <p>
                      <Button flat success large>
                        <span>Sign up now</span>
                        <Icon>arrow-right</Icon>
                      </Button>
                      <Button flat round large info>
                        <Icon medium>search</Icon>
                      </Button>
                    </p>
                  </>
                ) : index === 7 ? (
                  <Box
                    boxShadow="none"
                    border="none"
                    bg="secondary"
                    display="flex"
                    justifyContent="center">
                    <Button inverted>Default</Button>
                    <Button inverted primary>
                      Primary
                    </Button>
                    <Button inverted secondary>
                      Secondary
                    </Button>
                    <Button inverted success>
                      Success
                    </Button>
                    <Button inverted info>
                      Info
                    </Button>
                    <Button inverted warning>
                      Warning
                    </Button>
                    <Button inverted danger>
                      Danger
                    </Button>
                  </Box>
                ) : index === 8 ? (
                  <>
                    <Button loading={load1} onClick={() => updateLoad1(true)}>
                      Default
                    </Button>
                    <Button
                      loading={load2}
                      onClick={() => updateLoad2(true)}
                      primary>
                      Primary
                    </Button>
                    <Button
                      loading={load3}
                      onClick={() => updateLoad3(true)}
                      secondary>
                      Secondary
                    </Button>
                    <Button
                      loading={load4}
                      onClick={() => updateLoad4(true)}
                      success>
                      Success
                    </Button>
                    <Button
                      loading={load5}
                      onClick={() => updateLoad5(true)}
                      info>
                      Info
                    </Button>
                    <Button
                      loading={load6}
                      onClick={() => updateLoad6(true)}
                      warning>
                      Warning
                    </Button>
                    <Button
                      loading={load7}
                      onClick={() => updateLoad7(true)}
                      danger>
                      Danger
                    </Button>
                  </>
                ) : (
                  <>
                    <p>
                      <Button mega>
                        <Icon large className="ml-0">
                          lock
                        </Icon>
                        <div>
                          <p className="nab-button-desc">
                            Unlock your potential
                          </p>
                          <p className="nab-button-cta">Do it now</p>
                        </div>
                      </Button>
                    </p>
                    <p>
                      <Button mega secondary>
                        <Icon large className="ml-0">
                          lock
                        </Icon>
                        <div>
                          <p className="nab-button-desc">
                            Unlock your potential
                          </p>
                          <p className="nab-button-cta">Do it now</p>
                        </div>
                      </Button>
                    </p>
                    <p>
                      <Button mega primary className="has-elevation-24">
                        <Icon large className="ml-0">
                          lock
                        </Icon>
                        <div>
                          <p className="nab-button-desc">
                            Unlock your potential
                          </p>
                          <p className="nab-button-cta">Do it now</p>
                        </div>
                      </Button>
                    </p>
                  </>
                )}
              </Demo>
              <PropsTable data={Data} />
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};

export default PageButtons;

const Data = [
  {
    items: [
      {
        name: `white, <br />
            primary, <br />
            secondary, <br />
            success, <br />
            info, <br />
            warning, <br />
            danger`,
        def: 'undefined',
        type: 'Boolean',
        desc: 'Button style'
      },
      {
        name: 'round',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Make round button'
      },
      {
        name: 'square',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Make square button'
      },
      {
        name: 'href',
        def: 'undefined',
        type: 'Any',
        desc: 'Convert button to an a tag'
      },
      {
        name: `small, <br />
            medium, <br />
            large`,
        def: 'undefined',
        type: 'Boolean',
        desc: 'Button size'
      },
      {
        name: 'flat',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Button with transparent background'
      },
      {
        name: 'inverted',
        def: 'undefined',
        type: 'Boolean',
        desc: 'For dark background use'
      },
      {
        name: 'loading',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Load spinner upon clicking'
      },
      {
        name: 'noAction',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Remove pointer events'
      },
      {
        name: 'block',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Full-width button'
      },
      {
        name: 'mega',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Prop for making mega button'
      }
    ]
  }
];

const Eg = [
  {
    title: '# Usage',
    react: `
<Button>Default</Button>
<Button white>White</Button>
<Button primary>Primary</Button>
<Button secondary>Secondary</Button>
<Button success>Success</Button>
<Button info>Info</Button>
<Button warning>Warning</Button>
<Button danger>Danger</Button>
    `
  },
  {
    title: '# Buttons with icon',
    react: `
<Button>
  <Icon>help-circle</Icon>
  <span>Help</span>
</Button>
<Button primary>
  <Icon>order</Icon>
  <span>Order</span>
</Button>
<Button success>
  <Icon>checked-circle</Icon>
  <span>Success</span>
</Button>
<Button info>
  <Icon>info-circle</Icon>
  <span>Info</span>
</Button>
<Button warning>
  <Icon>warning-triangle</Icon>
  <span>Warning</span>
</Button>
<Button danger>
  <span>Delete</span>
  <Icon>close</Icon>
</Button>
    `
  },
  {
    title: '# Disabled',
    react: `
<Button disabled>
  Default
</Button>
<Button disabled white>
  White
</Button>
<Button disabled primary>
  Primary
</Button>
<Button disabled secondary>
  Secondary
</Button>
<Button disabled success>
  Success
</Button>
<Button disabled info>
  Info
</Button>
<Button disabled warning>
  Warning
</Button>
<Button disabled danger>
  Danger
</Button>
    `
  },
  {
    title: '# Round buttons',
    react: `
<Button round>
  <Icon>quicklinks</Icon>
</Button>
<Button round primary>
  <Icon>settings</Icon>
</Button>
<Button round success>
  3
</Button>
<Button round info>
  JS
</Button>
<Button round warning>
  <Icon>warning-triangle</Icon>
</Button>
<Button round danger>
  <Icon>lock</Icon>
</Button>
    `
  },
  {
    title: '# Square buttons',
    react: `
<Button square>
  <Icon>help-circle</Icon>
</Button>
<Button square primary>
  <Icon>nabtrade</Icon>
</Button>
<Button square success>
  3
</Button>
<Button square info>
  JS
</Button>
<Button square warning>
  <Icon>warning-triangle</Icon>
</Button>
<Button square danger>
  <Icon>trash</Icon>
</Button>
    `
  },
  {
    title: '# Sizes',
    react: `
<Button small>Small button</Button>
<Button small square>
  <Icon>pencil</Icon>
</Button>
<Button round small>
  <Icon>plus</Icon>
</Button>

<Button primary>Default button</Button>
<Button primary square>
  <Icon>help-circle</Icon>
</Button>
<Button round danger>
  <Icon>bin</Icon>
</Button>

<Button medium warning>
  Medium button
</Button>
<Button medium warning square>
  <Icon>checked-circle</Icon>
</Button>
<Button round warning medium>
  <Icon>warning-triangle</Icon>
</Button>

<Button large success>
  Large button
</Button>
<Button large success square>
  <Icon>bin</Icon>
</Button>
<Button round success large>
  <Icon>search</Icon>
</Button>
    `
  },
  {
    title: '# Flat',
    react: `
<Button flat small info>
  <Icon right>pdf</Icon>
  <span>Download PDF</span>
</Button>
<Button flat small round>
  <Icon>more-vertical</Icon>
</Button>

<Button flat>
  <Icon left>cog</Icon>
  <span>Settings</span>
</Button>
<Button flat round>
  <Icon>cog</Icon>
</Button>

<Button flat medium>
  <Icon>warning-triangle</Icon>
  <span>Important information</span>
</Button>
<Button flat round medium danger>
  <Icon>bin</Icon>
</Button>

<Button flat success large>
  <span>Sign up now</span>
  <Icon>arrow-right</Icon>
</Button>
<Button flat round large info>
  <Icon medium>search</Icon>
</Button>
    `
  },
  {
    title: '# Inverted',
    react: `
<Button inverted>Default</Button>
<Button inverted primary>
  Primary
</Button>
<Button inverted secondary>
  Secondary
</Button>
<Button inverted success>
  Success
</Button>
<Button inverted info>
  Info
</Button>
<Button inverted warning>
  Warning
</Button>
<Button inverted danger>
  Danger
</Button>
    `
  },
  {
    title: '# Loading',
    react: `
<Button loading>Default</Button>
<Button loading primary>
  Primary
</Button>
<Button loading secondary>
  Secondary
</Button>
<Button loading success>
  Success
</Button>
<Button loading info>
  Info
</Button>
<Button loading warning>
  Warning
</Button>
<Button loading danger>
  Danger
</Button>
`
  },
  {
    title: '# Mega button',
    react: `
<Button mega>
  <Icon large className="ml-0">
    lock
  </Icon>
  <div>
    <p className="nab-button-desc">
      Unlock your potential
    </p>
    <p className="nab-button-cta">Do it now</p>
  </div>
</Button>

<Button mega secondary>
  <Icon large className="ml-0">
    lock
  </Icon>
  <div>
    <p className="nab-button-desc">
      Unlock your potential
    </p>
    <p className="nab-button-cta">Do it now</p>
  </div>
</Button>

<Button mega primary className="has-elevation-24">
  <Icon large className="ml-0">
    lock
  </Icon>
  <div>
    <p className="nab-button-desc">
      Unlock your potential
    </p>
    <p className="nab-button-cta">Do it now</p>
  </div>
</Button>
`
  }
];
