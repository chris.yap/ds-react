import React from 'react';
import PageHeader from '../../components/PageHeader';
import Demo from '../../components/Demo';
import PropsTable from '../../components/PropsTable';
import {
  Box,
  Button,
  CheckboxGroup,
  Checkbox,
  Column,
  Columns,
  Container,
  Divider,
  Overlay,
  Section,
  Tag,
  Title
} from 'shaper-react';

const Page = () => {
  const [index, updateIndex] = React.useState(0);
  const [dark, updateDark] = React.useState(false);
  const [absolute, updateAbsolute] = React.useState(false);
  const [overlay, updateOverlay] = React.useState(null);
  const [timer, updateTimer] = React.useState(5);
  const [spinner, updateSpinner] = React.useState(false);
  const [checkBoxes, setCheckBoxes] = React.useState([]);
  let countDown = time => {
    let counter = time;
    let startTimer = setInterval(() => {
      counter--;
      updateTimer(counter);
      if (counter === 0) {
        updateOverlay(false);
        clearInterval(startTimer);
      }
    }, 1000);
  };

  return (
    <div id="start">
      <PageHeader category="element" title="Overlay">
        Overlay to place over content. Usually use when loading, etc. <br />
        <Tag small>styled-component</Tag>
      </PageHeader>

      <Divider />

      <Section>
        <Container>
          <Columns mobile>
            <Column mobile={12} desktop={10} desktopOffset={1}>
              <Demo examples={Eg} updateIndex={updateIndex}>
                {index === 0 ? (
                  <>
                    <Button
                      medium
                      className="ml-0"
                      onClick={() => {
                        updateOverlay(true);
                        updateDark(false);
                        updateSpinner(false);
                        updateAbsolute(false);
                        countDown(5);
                      }}>
                      Overlay
                    </Button>
                    <Button
                      medium
                      className="ml-0"
                      secondary
                      inverted
                      onClick={() => {
                        updateOverlay(true);
                        updateDark(true);
                        updateSpinner(false);
                        updateAbsolute(false);
                        countDown(5);
                      }}>
                      Dark overlay
                    </Button>
                    <CheckboxGroup
                      className="mt-5"
                      value={checkBoxes}
                      onChange={setCheckBoxes}>
                      <Checkbox label="isTransparent" value="isTransparent" />
                      <Checkbox label="isClear" value="isClear" />
                    </CheckboxGroup>
                  </>
                ) : index === 1 ? (
                  <>
                    <Button
                      medium
                      className="ml-0"
                      onClick={() => {
                        updateOverlay(true);
                        updateDark(false);
                        updateSpinner(true);
                        updateAbsolute(false);
                        countDown(5);
                      }}>
                      Overlay w/ spinner
                    </Button>
                    <Button
                      medium
                      className="ml-0"
                      secondary
                      inverted
                      onClick={() => {
                        updateOverlay(true);
                        updateDark(true);
                        updateSpinner(true);
                        updateAbsolute(false);
                        countDown(5);
                      }}>
                      Dark overlay w/ spinner
                    </Button>
                  </>
                ) : (
                  <>
                    <Button
                      medium
                      className="ml-0"
                      onClick={() => {
                        updateOverlay(true);
                        updateDark(false);
                        updateSpinner(true);
                        updateAbsolute(true);
                        countDown(5);
                      }}>
                      Absolute overlay
                    </Button>
                    <Button
                      medium
                      className="ml-0"
                      secondary
                      inverted
                      onClick={() => {
                        updateOverlay(true);
                        updateDark(true);
                        updateSpinner(true);
                        updateAbsolute(true);
                        countDown(5);
                      }}>
                      Absolute dark overlay
                    </Button>
                  </>
                )}
                <Overlay
                  absolute={absolute}
                  isDark={dark}
                  isClear={checkBoxes.includes('isClear')}
                  isTransparent={checkBoxes.includes('isTransparent')}
                  spinner={spinner}
                  isOpened={overlay}>
                  {timer > 0 && (
                    <Box
                      style={{
                        position: 'absolute',
                        bottom: '2rem',
                        opacity: '.8'
                      }}>
                      <p className="mb-2 has-text-centered">
                        <small>
                          <strong>For demo purpose only</strong>
                        </small>
                        <br /> overlay will close in
                      </p>
                      <Title size="4" className="mb-0 has-text-centered">
                        {`${timer}`} {timer > 1 ? 'seconds' : 'second'}
                      </Title>
                    </Box>
                  )}
                </Overlay>
              </Demo>
              <PropsTable data={Data} />
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};

export default Page;

const Data = [
  {
    items: [
      {
        name: 'absolute',
        def: 'undefined',
        type: 'Boolean',
        desc:
          'Set overlay to absolute position so it will be "housed" in the parent container'
      },
      {
        name: 'isDark',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Set dark theme'
      },
      {
        name: 'isOpened',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Set overlay visibility'
      },
      {
        name: 'spinner',
        def: 'undefined',
        type: 'Any',
        desc:
          'Add a spinner into the overlay. "True" to use default or add your own'
      },
      {
        name: 'isTransparent',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Remove background from overlay'
      },
      {
        name: 'isClear',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Remove blur backdrop from overlay'
      }
    ]
  }
];

const Eg = [
  {
    title: '# Usage',
    react: `
<Overlay isOpened={state} dark={dark} />
    `
  },
  {
    title: '# Spinner',
    react: `
<Overlay isOpened={state} dark={dark} spinner />    
    `
  },
  {
    title: '# Absolute position',
    react: `
<Overlay isOpened={state} dark={dark} spinner absolute />
    `
  }
];
