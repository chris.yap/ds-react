import React from 'react';
import PageHeader from '../../components/PageHeader';
import Demo from '../../components/Demo';
import PropsTable from '../../components/PropsTable';
import {
  Column,
  Columns,
  Container,
  Divider,
  Section,
  Tag
} from 'shaper-react';

const PageTags = () => {
  const [index, updateIndex] = React.useState(0);
  return (
    <div id="start">
      <PageHeader category="elements" title="Tags / Badge">
        It is used to convey small pieces of information. <br />
        <Tag small>styled-component</Tag>
      </PageHeader>

      <Divider />

      <Section>
        <Container>
          <Columns mobile>
            <Column mobile={12} desktop={10} desktopOffset={1}>
              <Demo examples={Examples} updateIndex={updateIndex}>
                {index === 0 ? (
                  <>
                    <p>
                      <Tag>Default</Tag>
                      <Tag ml={1} white>
                        White
                      </Tag>
                      <Tag ml={1} black>
                        Black
                      </Tag>
                      <Tag ml={1} primary>
                        Primary
                      </Tag>
                      <Tag ml={1} secondary>
                        Secondary
                      </Tag>
                      <Tag ml={1} success>
                        Success
                      </Tag>
                      <Tag ml={1} info>
                        Info
                      </Tag>
                      <Tag ml={1} warning>
                        Warning
                      </Tag>
                      <Tag ml={1} danger>
                        Danger
                      </Tag>
                    </p>
                    <p>
                      <Tag>0</Tag>
                      <Tag ml={1} white>
                        34
                      </Tag>
                      <Tag ml={1} black>
                        83
                      </Tag>
                      <Tag ml={1} primary>
                        87
                      </Tag>
                      <Tag ml={1} secondary>
                        59
                      </Tag>
                      <Tag ml={1} success>
                        99+
                      </Tag>
                      <Tag ml={1} info>
                        3
                      </Tag>
                      <Tag ml={1} warning>
                        4
                      </Tag>
                      <Tag ml={1} danger>
                        1
                      </Tag>
                    </p>
                  </>
                ) : index === 1 ? (
                  <>
                    <Tag onClose={() => alert('close')}>Default</Tag>
                    <Tag ml={1} onClose={() => alert('close')} white>
                      White
                    </Tag>
                    <Tag ml={1} onClose={() => alert('close')} black>
                      Black
                    </Tag>
                    <Tag ml={1} onClose={() => alert('close')} primary>
                      Primary
                    </Tag>
                    <Tag ml={1} onClose={() => alert('close')} secondary>
                      Secondary
                    </Tag>
                    <Tag ml={1} onClose={() => alert('close')} success>
                      Success
                    </Tag>
                    <Tag ml={1} onClose={() => alert('close')} info>
                      Info
                    </Tag>
                    <Tag ml={1} onClose={() => alert('close')} warning>
                      Warning
                    </Tag>
                    <Tag ml={1} onClose={() => alert('close')} danger>
                      Danger
                    </Tag>
                  </>
                ) : (
                  <>
                    <p>
                      <Tag small>Small</Tag>
                      <Tag ml={1} small onClose={() => alert('close')}>
                        Small
                      </Tag>
                    </p>
                    <p>
                      <Tag secondary>Default</Tag>
                      <Tag ml={1} secondary onClose={() => alert('close')}>
                        Default
                      </Tag>
                    </p>
                    <p>
                      <Tag success medium>
                        Medium
                      </Tag>
                      <Tag ml={1} success medium onClose={() => alert('close')}>
                        Medium
                      </Tag>
                    </p>
                    <p>
                      <Tag danger large>
                        Large
                      </Tag>
                      <Tag ml={1} danger large onClose={() => alert('close')}>
                        Large
                      </Tag>
                    </p>
                  </>
                )}
              </Demo>

              <PropsTable data={Data} />
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};

export default PageTags;

const Data = [
  {
    items: [
      {
        name: `white, <br />
            black, <br />
            primary, <br />
            secondary, <br />
            success, <br />
            info, <br />
            warning, <br />
            danger`,
        default: 'undefined',
        type: 'Boolean',
        desc: 'Tag style'
      },
      {
        name: 'onClose',
        default: 'undefined',
        type: 'Function',
        desc: 'Close function and adds a close button'
      },
      {
        name: `small, <br />
            medium, <br />
            large`,
        default: 'undefined',
        type: 'Boolean',
        desc: 'Tag size'
      }
    ]
  }
];

const Examples = [
  {
    title: '# Usage',
    react: `
<Tag>Default</Tag>
<Tag white>White</Tag>
<Tag black>Black</Tag>
<Tag primary>Primary</Tag>
<Tag secondary>Secondary</Tag>
<Tag success>Success</Tag>
<Tag info>Info</Tag>
<Tag warning>Warning</Tag>
<Tag danger>Danger</Tag>

<Tag>0</Tag>
<Tag white>34</Tag>
<Tag black>83</Tag>
<Tag primary>87</Tag>
<Tag secondary>59</Tag>
<Tag success>99+</Tag>
<Tag info>3</Tag>
<Tag warning>4</Tag>
<Tag danger>1</Tag>
    `
  },
  {
    title: '# With a onClose button',
    react: `
<Tag onClose={() => function}>Default</Tag>
<Tag onClose={() => function} white>White</Tag>
<Tag onClose={() => function} black>Black</Tag>
<Tag onClose={() => function} primary>Primary</Tag>
<Tag onClose={() => function} secondary>Secondary</Tag>
<Tag onClose={() => function} success>Success</Tag>
<Tag onClose={() => function} info>Info</Tag>
<Tag onClose={() => function} warning>Warning</Tag>
<Tag onClose={() => function} danger>Danger</Tag>
    `
  },
  {
    title: '# Sizes',
    react: `
<Tag small>Small</Tag>
<Tag secondary>Default</Tag>
<Tag medium success>Medium</Tag>
<Tag large danger>Large</Tag>

<Tag onClose={() => function} small>Small</Tag>
<Tag onClose={() => function} secondary>Default</Tag>
<Tag onClose={() => function} medium success>Medium</Tag>
<Tag onClose={() => function} large danger>Large</Tag>
    `
  }
];
