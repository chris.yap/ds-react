import React from 'react';
import PageHeader from '../../components/PageHeader';
import Demo from '../../components/Demo';
import {
  Box,
  Column,
  Columns,
  Container,
  Divider,
  Tag,
  Section
} from 'shaper-react';

const PageBox = () => {
  return (
    <div id="start">
      <PageHeader category="elements" title="Boxes">
        A simple white box with padding to contain other elements <br />
        <Tag small>styled-component</Tag>
      </PageHeader>

      <Divider />

      <Section>
        <Container>
          <Columns mobile>
            <Column mobile={12} desktop={10} desktopOffset={1}>
              <Demo examples={Eg}>
                <Box>Content goes here.</Box>
              </Demo>

              {/* <PropsTable /> */}
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};

export default PageBox;

const Eg = [
  {
    title: '# Usage',
    react: `
<Box>Content goes here.</Box>
		`,
    html: `
<div class="nab-box">Content goes here.</div>
		`
  }
];
