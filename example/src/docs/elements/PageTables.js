import React from 'react';
import faker from 'faker';
import PageHeader from '../../components/PageHeader';
import { TableRow as Row, TableCell as Cell } from 'shaper-react';
import {
  Column,
  Columns,
  Container,
  Divider,
  Section,
  Table,
  Tag
} from 'shaper-react';
import Demo from '../../components/Demo';
import PropsTable from '../../components/PropsTable';

const Page = () => {
  const [index, setIndex] = React.useState(0);
  // const [sort] = React.useState(null);
  // const [asc, updateAsc] = React.useState(true);
  const [headers] = React.useState([
    { name: 'First name', value: 'firstName', sort: true },
    { name: 'Last name', value: 'lastName', sort: true },
    { name: 'Email', value: 'email', sort: true },
    { name: 'Acc #', value: 'account', sort: 'number' },
    { name: 'Funds ($)', value: 'funds', align: 'right' }
  ]);
  const [data, updateData] = React.useState([]);
  // const [currentPage, updateCurrentPage] = React.useState(1);
  // const [totalPages, updateTotalPages] = React.useState(1);
  // const [dataPerPage] = React.useState(10);
  // this.state = {
  //   sort: null,
  //   asc: true,
  //   headers: ,
  //   pageData: [],
  //   data: [],
  //   currentPage: 1,
  //   totalPages: 1,
  //   dataPerPage: 10
  // };

  // let getPageData = (page, data, dataPerPage) => {
  //   let tempData = [];
  //   const maxPages = Math.round(data.length / dataPerPage);
  //   for (let i = page; i <= maxPages; i++) {
  //     tempData.push(data[i]);
  //   }
  //   // updateCurrentPage(page);
  //   // updateTotalPages(maxPages);
  //   updateData(tempData);
  // };

  let generateData = () => {
    let tempArray = [];
    let row = {};
    for (let i = 0; i <= 50; i++) {
      row = {
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        email: faker.internet.email(),
        account: faker.finance.account(),
        funds: faker.finance.amount()
      };
      tempArray.push(row);
    }
    updateData(tempArray);
  };

  // let outsideSort = (field, data, asc) => {
  //   // const { currentPage, dataPerPage } = this.state;
  //   const tempData = data;
  //   // setTimeout(() => {
  //   if (asc) {
  //     tempData &&
  //       tempData.sort((a, b) => {
  //         return a[field] < b[field] ? -1 : b[field] < a[field] ? 1 : 0;
  //       });
  //   } else {
  //     tempData &&
  //       tempData.sort((a, b) => {
  //         return b[field] < a[field] ? -1 : a[field] < b[field] ? 1 : 0;
  //       });
  //   }
  //   updateData(tempData);
  //   updateSort(field);
  //   updateSort(field === sort ? !asc : true);
  //   getPageData(currentPage, data, dataPerPage);
  //   // }, 100);
  // };

  // let sortMyTable = (field, ascending) => {
  //   // console.log(field, asc);
  //   const { data } = this.state;
  //   outsideSort(field, data, ascending);
  // };

  React.useEffect(() => {
    generateData();
    // setTimeout(() => {
    //   getPageData(currentPage, data, dataPerPage);
    // }, 100);
  }, []);

  return (
    <div id="start">
      <PageHeader category="elements" title="Tables">
        Data table. <br />
        <Tag small>styled-component</Tag>
      </PageHeader>

      <Divider />

      <Section>
        <Container>
          <Columns mobile>
            <Column className="is-12-mobile is-10-desktop is-offset-1-desktop">
              <Demo examples={Eg} updateIndex={setIndex}>
                {index === 0 ? (
                  <Table
                    stickyHeader
                    stickyFooter
                    stickyColumn
                    // headerProps={{ warning: true }}
                    // footerProps={{ danger: true }}
                    headers={headers}
                    footers={headers}
                    data={data}
                    tableRow={TableRow}
                  />
                ) : index === 1 ? (
                  <Table
                    hasBorders
                    stickyHeader
                    stickyColumn
                    headers={headers}
                    data={data}
                    tableRow={TableRow}
                    defaultSortCol="firstName"
                  />
                ) : index === 2 ? (
                  <Table
                    hasStripes
                    stickyHeader
                    stickyColumn
                    headers={headers}
                    data={data}
                    tableRow={TableRow}
                    defaultSortCol="firstName"
                  />
                ) : index === 3 ? (
                  <Table
                    isNarrow
                    stickyHeader
                    stickyColumn
                    headers={headers}
                    data={data}
                    tableRow={TableRow}
                    defaultSortCol="firstName"
                  />
                ) : index === 4 ? (
                  <Table
                    isHoverable
                    stickyHeader
                    stickyColumn
                    headers={headers}
                    data={data}
                    tableRow={TableRow}
                    defaultSortCol="firstName"
                  />
                ) : (
                  <Table
                    stickyHeader
                    stickyColumn
                    hasBorders
                    isHoverable
                    isNarrow
                    hasStripes
                    headers={headers}
                    data={data}
                    tableRow={TableRow}
                    defaultSortCol="firstName"
                  />
                )}
              </Demo>
              <PropsTable data={Data} />
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};

export default Page;

const TableRow = ({ row }) => {
  return (
    <Row>
      <Cell>{row.firstName}</Cell>
      <Cell>{row.lastName}</Cell>
      <Cell>{row.email}</Cell>
      <Cell>{row.account}</Cell>
      <Cell right>{row.funds}</Cell>
    </Row>
  );
};

const Data = [
  {
    title: 'Table',
    items: [
      {
        name: 'hasBorders',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Add borders to table'
      },
      {
        name: 'className',
        def: 'undefined',
        type: 'String',
        desc: 'Options to add additional classes to component'
      },
      {
        name: 'data',
        def: 'undefined',
        type: 'Array',
        desc: 'Data to populate table'
      },
      {
        name: 'defaultSortCol',
        def: 'undefined',
        type: 'String',
        desc: 'Set default sort based on column name'
      },
      {
        name: 'headers',
        def: 'undefined',
        type: 'Array',
        desc: 'Table headers'
      },
      {
        name: 'stickyHeader',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Make header sticky'
      },
      {
        name: 'stickyFooter',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Make footer sticky'
      },
      {
        name: 'stickyColumn',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Make 1st column sticky'
      },
      {
        name: 'isHoverable',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Add hover effect to table'
      },
      {
        name: 'isNarrow',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Condensed table'
      },
      {
        name: 'hasStripes',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Add alternate stripes styling to table'
      },
      {
        name: 'tableRow',
        def: 'undefined',
        type: 'Function',
        desc: 'Table row setup'
      },
      {
        name: 'hasNoScroll',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Remove scrolling from table'
      },
      {
        name: 'headerProps',
        def: 'undefined',
        type: 'Object',
        desc: 'Props for TableHeader'
      },
      {
        name: 'footerProps',
        def: 'undefined',
        type: 'Object',
        desc: 'Props for TableFooter'
      }
    ]
  },
  {
    title: 'TableCell',
    items: [
      {
        name: 'hasNoWrap',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Remove text wrap'
      },
      {
        name: 'hasEllipsis',
        def: 'undefined',
        type: 'Boolean',
        desc:
          'Truncate text with ellipsis. Note: Need to include maxWidth prop (eg. maxWidth="100px")'
      },
      {
        name: 'contentProps',
        def: 'undefined',
        type: 'Object',
        desc: ''
      },
      {
        name: 'right',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Right aligned content'
      }
    ]
  }
];

const Eg = [
  {
    title: '# Usage',
    react: `
<Table
  stickyHeader
  stickyFooter
  stickyColumn
  headers={headers}
  footers={headers}
  data={data}
  tableRow={TableRow}
/>

const TableRow = ({ row }) => {
  return (
    <Row>
      <Cell>{row.firstName}</Cell>
      <Cell>{row.lastName}</Cell>
      <Cell>{row.email}</Cell>
      <Cell>{row.account}</Cell>
      <Cell right>{row.funds}</Cell>
    </Row>
  );
};

const headers = [
  { name: 'First name', value: 'firstName', sort: true },
  { name: 'Last name', value: 'lastName', sort: true },
  { name: 'Email', value: 'email', sort: true },
  { name: 'Acc #', value: 'account', sort: 'number' },
  { name: 'Funds ($)', value: 'funds', align: 'right' }
]);
    `
  },
  {
    title: '# Borders',
    react: `
<Table
  hasBorders
  stickyHeader
  stickyColumn
  headers={headers}
  data={data}
  tableRow={TableRow}
  defaultSortCol="firstName"
/>

// See first example to create tableRow
    `
  },
  {
    title: '# Stripes',
    react: `
<Table
  hasStripes
  stickyHeader
  stickyColumn
  headers={headers}
  data={data}
  tableRow={TableRow}
  defaultSortCol="firstName"
/>

// See first example to create tableRow
    `
  },
  {
    title: '# Narrow',
    react: `
<Table
  isNarrow
  stickyHeader
  stickyColumn
  headers={headers}
  data={data}
  tableRow={TableRow}
  defaultSortCol="firstName"
/>

// See first example to create tableRow
    `
  },
  {
    title: '# Has hover effect',
    react: `
<Table
  isHoverable
  stickyHeader
  stickyColumn
  headers={headers}
  data={data}
  tableRow={TableRow}
  defaultSortCol="firstName"
/>

// See first example to create tableRow
    `
  },
  {
    title: '# Combining all',
    react: `
<Table
  stickyHeader
  stickyColumn
  hasBorders
  isHoverable
  isNarrow
  hasStripes
  headers={headers}
  data={data}
  tableRow={TableRow}
  defaultSortCol="firstName"
/>

// See first example to create tableRow
    `
  }
];
