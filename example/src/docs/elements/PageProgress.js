import React from 'react';
import PageHeader from '../../components/PageHeader';
import Demo from '../../components/Demo';
import PropsTable from '../../components/PropsTable';
import {
  Column,
  Columns,
  Container,
  Divider,
  Progress,
  Tag,
  Section
} from 'shaper-react';

const PageProgress = () => {
  const [progress, updateProgress] = React.useState(0);
  const [index, updateIndex] = React.useState(0);

  React.useEffect(() => {
    timerUp();
  });
  let timerUp = i => {
    setTimeout(() => {
      updateProgress(80);
    }, 3000);
  };
  return (
    <div id="start">
      <PageHeader category="elements" title="Progress">
        Native HTML progress bars <br />
        <Tag small>styled-component</Tag>
      </PageHeader>

      <Divider />

      <Section>
        <Container>
          <Columns mobile>
            <Column mobile={12} desktop={10} desktopOffset={1}>
              <Demo examples={Eg} updateIndex={updateIndex}>
                {index === 0 ? (
                  <Progress value={progress} />
                ) : index === 1 ? (
                  <>
                    <Progress value={progress} secondary />
                    <Progress value={progress} info />
                    <Progress value={progress} success />
                    <Progress value={progress} warning />
                    <Progress value={progress} danger />
                  </>
                ) : index === 2 ? (
                  <>
                    <Progress value={progress} small />
                    <Progress value={progress} />
                    <Progress value={progress} medium />
                    <Progress value={progress} large />
                  </>
                ) : index === 3 ? (
                  <>
                    <Progress value={progress} square success small />
                    <Progress value={progress} square info />
                    <Progress value={progress} square warning medium />
                    <Progress value={progress} square danger large />
                  </>
                ) : index === 4 ? (
                  <>
                    <Progress value={progress} transparent success small />
                    <Progress value={progress} transparent info />
                    <Progress value={progress} transparent warning medium />
                    <Progress value={progress} transparent danger large />
                  </>
                ) : (
                  <>
                    <p>
                      You can display an <strong>indeterminate</strong> progress
                      bar by <strong>not having any value</strong> on the
                      progress bar. It is used to show that some progress is
                      going on, but the actual duration is not yet determined.
                    </p>
                    <Progress success small />
                    <Progress info />
                    <Progress />
                    <Progress warning medium />
                    <Progress danger large />
                  </>
                )}
              </Demo>
              <PropsTable data={Data} />
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};

export default PageProgress;

const Data = [
  {
    items: [
      {
        name: 'value',
        def: 'undefined',
        type: 'Number',
        desc: 'The percentage value for current progress'
      },
      {
        name: 'max',
        def: '100',
        type: 'Number',
        desc: 'Maximum value on progress bar'
      },
      {
        name: `small, <br />
            medium, <br />
            large`,
        def: 'undefined',
        type: 'Boolean',
        desc: 'Progress size'
      },
      {
        name: 'transparent',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Remove background in progress bar'
      },
      {
        name: 'right',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Right aligned'
      },
      {
        name: `primary, <br />
            secondary, <br />
            info, <br />
            success, <br />
            warning, <br />
            danger`,
        def: 'undefined',
        type: 'Boolean',
        desc: 'Colours'
      },
      {
        name: 'square',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Remove rounded corners'
      }
    ]
  }
];

const Eg = [
  {
    title: '# Usage',
    react: `
<Progress value={progress} />
		`,
    html: ``
  },
  {
    title: '# Contextual',
    react: `
<Progress value={progress} secondary />
<Progress value={progress} info />
<Progress value={progress} success />
<Progress value={progress} warning />
<Progress value={progress} danger />
		`,
    html: ``
  },
  {
    title: '# Sizes',
    react: `
<Progress value={progress} small />
<Progress value={progress} />
<Progress value={progress} medium />
<Progress value={progress} large />
		`,
    html: ``
  },
  {
    title: '# Square',
    react: `
<Progress value={progress} square success small />
<Progress value={progress} square info />
<Progress value={progress} square warning medium />
<Progress value={progress} square danger large />
		`,
    html: ``
  },
  {
    title: '# No background',
    react: `
<Progress value={progress} transparent success small />
<Progress value={progress} transparent info />
<Progress value={progress} transparent warning medium />
<Progress value={progress} transparent danger large />
		`,
    html: ``
  },
  {
    title: '# Indeterminate',
    react: `
<Progress success small />
<Progress info />
<Progress />
<Progress warning medium />
<Progress danger large />
		`,
    html: ``
  }
];
