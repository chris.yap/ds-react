import React, { Component } from 'react'
import PageHeader from '../../components/PageHeader'
import { 
  Column, Columns, Container, Divider, Section, Title 
  // Subtitle, Hero, 
} from 'shaper-react'
import ExpandingCodeExample from '../../components/ExpandingCodeExample'

export default class PageTitles extends Component {
  constructor (props) {
    super()
    this.state = {
      showCode: false
    }
  }
  toggleCode () {
    this.setState({
      showCode: !this.state.showCode
    })
  }
  render() {
    return (
      <div id="start">
        <PageHeader category="Elements" title="Title and Subtitle">
          Simple <strong>headings</strong> to add depth to your page.
        </PageHeader>

        <Divider />

        <Section>
          <Container>
            <Columns mobile>
              <Column mobile={12} desktop={10} desktopOffset={1} className="nab-content">

                {/* <Title size="1">This is a title</Title>
                <Subtitle size="3">This is a subtitle</Subtitle> */}

                <p>There are 2 types of heading:</p>

                <Columns>
                  <Column size={6}>
                    <p><strong>HTML</strong></p>
                    <ul>
                      <li><code>nab-title</code></li>
                      <li><code>nab-subtitle</code></li>
                    </ul>
                  </Column>
                  <Column size={6}>
                    <p><strong>ReactJS</strong></p>
                    <ul>
                      <li><code>Title</code></li>
                      <li><code>Subtitle</code></li>
                    </ul>
                  </Column>
                </Columns>

                {
                  Examples.map((eg, e) => {
                    return (
                      <ExpandingCodeExample key={e} title={eg.title} example={eg.example} html={eg.html} react={eg.react} />
                    )
                  })
                }

                <Divider className="my-10" />

                <Title size={4} className="font-nabimpact">React Props</Title>

                <table className="nab-table is-striped">
                  <thead>
                    <tr>
                      <th className="has-text-weight-semibold px-3 has-text-size-1">Name</th>
                      <th className="has-text-weight-semibold px-3 has-text-size-1">Default</th>
                      <th className="has-text-weight-semibold px-3 has-text-size-1">Options</th>
                      <th className="has-text-weight-semibold px-3 has-text-size-1">Type</th>
                      <th className="has-text-weight-semibold px-3 has-text-size-1">Description</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td className="has-text-weight-bold">size</td>
                      <td>-</td>
                      <td>1 - 6</td>
                      <td>Number</td>
                      <td>Title size</td>
                    </tr>
                  </tbody>
                </table>

              </Column>
            </Columns>
          </Container>
        </Section>
      </div>
    )
  }
}

const Examples = [
  {
    title: '# Usage',
    example: `
<h1 class="nab-title">Title</h1>
<h2 class="nab-subtitle">Subtitle</h2>
    `,
    html: `
<h1 class="nab-title">Title</h1>
<h2 class="nab-subtitle">Subtitle</h2>
    `,
    react: `
import { Title, Subtitle } from '@shaper/react'

const MyComponent = (props) => (
  <Title>Title</Title>
  <Subtitle>Subtitle</Subtitle>
)
    `
  },
  {
    title: 'Sizes',
    example: `
<p>There are <strong>6 sizes</strong> available:</p>
<div class="nab-columns">
  <div class="nab-column">
    <h1 class="nab-title is-1">Title 1</h1>
    <h2 class="nab-title is-2">Title 2</h2>
    <h3 class="nab-title is-3">Title 3</h3>
    <h4 class="nab-title is-4">Title 4</h4>
    <h5 class="nab-title is-5">Title 5</h5>
    <h6 class="nab-title is-6">Title 6</h6>
  </div>
  <div class="nab-column">
    <h1 class="nab-subtitle is-1">Subtitle 1</h1>
    <h2 class="nab-subtitle is-2">Subtitle 2</h2>
    <h3 class="nab-subtitle is-3">Subtitle 3</h3>
    <h4 class="nab-subtitle is-4">Subtitle 4</h4>
    <h5 class="nab-subtitle is-5">Subtitle 5</h5>
    <h6 class="nab-subtitle is-6">Subtitle 6</h6>
  </div>
</div>
    `,
    html: `
<h1 class="nab-title is-1">Title 1</h1>
<h2 class="nab-title is-2">Title 2</h2>
<h3 class="nab-title is-3">Title 3</h3>
<h4 class="nab-title is-4">Title 4</h4>
<h5 class="nab-title is-5">Title 5</h5>
<h6 class="nab-title is-6">Title 6</h6>

<h1 class="nab-subtitle is-1">Subtitle 1</h1>
<h2 class="nab-subtitle is-2">Subtitle 2</h2>
<h3 class="nab-subtitle is-3">Subtitle 3</h3>
<h4 class="nab-subtitle is-4">Subtitle 4</h4>
<h5 class="nab-subtitle is-5">Subtitle 5</h5>
<h6 class="nab-subtitle is-6">Subtitle 6</h6>
    `,
    react: `
const MyComponent = (props) => (
  <Title size="1">Title</Title>
  <Title size="2">Title</Title>
  <Title size="3">Title</Title>
  <Title size="4">Title</Title>
  <Title size="5">Title</Title>
  <Title size="6">Title</Title>
  
  <Subtitle size="1">Subtitle</Subtitle>
  <Subtitle size="2">Subtitle</Subtitle>
  <Subtitle size="3">Subtitle</Subtitle>
  <Subtitle size="4">Subtitle</Subtitle>
  <Subtitle size="5">Subtitle</Subtitle>
  <Subtitle size="6">Subtitle</Subtitle>
)
    `
  },
  {
    title: 'Combining title and subtitle',
    example: `
<p>When you <strong>combine</strong> a title and a subtitle, they move closer together.</p>
<p>As a rule of thumb, it is recommended to use a size difference of <strong>two</strong>. So if you use a <code>nab-title is-1</code>/<code>Title 1</code>, combine it with a <code>nab-subtitle is-3</code>/<code>Subtitle 3</code>.</p>

<h1 class="nab-title is-1">Title</h1>
<h2 class="nab-subtitle is-3">Subtitle</h2>

<h1 class="nab-title is-2 mt-5">Title</h1>
<h2 class="nab-subtitle is-4">Subtitle</h2>

<h1 class="nab-title is-3 mt-5">Title</h1>
<h2 class="nab-subtitle is-5">Subtitle</h2>
    `,
    html: `
<h1 class="nab-title is-1">Title</h1>
<h2 class="nab-subtitle is-3">Subtitle</h2>

<h1 class="nab-title is-2">Title</h1>
<h2 class="nab-subtitle is-4">Subtitle</h2>

<h1 class="nab-title is-3">Title</h1>
<h2 class="nab-subtitle is-5">Subtitle</h2>
    `,
    react: `
const MyComponent = (props) => (
  <Title size="1">Title</Title>
  <Subtitle size="3">Subtitle</Subtitle>

  <Title size="2">Title</Title>
  <Subtitle size="4">Subtitle</Subtitle>

  <Title size="3">Title</Title>
  <Subtitle size="5">Subtitle</Subtitle>
)
    `
  }
]
