import React, { Component } from 'react'
import PageHeader from '../../components/PageHeader'
import { 
  // Icon,
  Column, Columns, Container, Divider, Section, Title } from 'shaper-react'
import ExpandingCodeExample from '../../components/ExpandingCodeExample'

export default class PageIcon extends Component {
  render() {
    return (
      <div id="start">
        <PageHeader category="Elements" title="Icons">
          Compatible with most icon libraries.
        </PageHeader>

        <Divider />

        <Section>
          <Container>
            <Columns mobile>
              <Column mobile={12} desktop={10} desktopOffset={1}>

                {/* <p>
                  <Icon small>nabtrade</Icon>
                  <Icon>nabtrade</Icon>
                  <Icon medium>nabtrade</Icon>
                  <Icon large>nabtrade</Icon>
                </p> */}

                <p>The <code>nab-icon</code>/<code>Icon</code> element is a container for any type of icon font. Because the icons can take a few seconds to load, and because you want control over the space the icons will take, you can use the icon class as a reliable square container that will prevent the page to "jump" on page load.</p>

                {/* <Icon number={3}>alert</Icon> */}

                <Title size={3} className="font-nabimpact mt-10">Examples</Title>

                {
                  Examples.map((eg, e) => {
                    return (
                      <ExpandingCodeExample key={e} title={eg.title} example={eg.example} html={eg.html} react={eg.react} />
                    )
                  })
                }

                <Divider className="my-10" />

                <Title size={4} className="font-nabimpact">React Props</Title>

                <table className="nab-table is-striped">
                  <thead>
                    <tr>
                      <th className="has-text-weight-semibold px-3 has-text-size-1">Name</th>
                      <th className="has-text-weight-semibold px-3 has-text-size-1">Default</th>
                      <th className="has-text-weight-semibold px-3 has-text-size-1">Type</th>
                      <th className="has-text-weight-semibold px-3 has-text-size-1">Description</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td className="has-text-weight-bold">
                        white, <br/>
                        primary, <br/>
                        secondary, <br/>
                        success, <br/>
                        info, <br/>
                        warning, <br/>
                        danger
                      </td>
                      <td>false</td>
                      <td>Boolean</td>
                      <td>Icon colours</td>
                    </tr>
                    <tr>
                      <td className="has-text-weight-bold">
                        small, <br />
                        medium, <br />
                        large
                      </td>
                      <td>false</td>
                      <td>Boolean</td>
                      <td>Icon size</td>
                    </tr>
                    <tr>
                      <td className="has-text-weight-bold">
                        number
                      </td>
                      <td>null</td>
                      <td>Number</td>
                      <td>Adding number badge to icon</td>
                    </tr>
                  </tbody>
                </table>

              </Column>
            </Columns>
          </Container>
        </Section>
      </div>
    )
  }
}

const Examples = [
  {
    title: '# Usage',
    example: `
<p>By default, the icon container will take up exactly 1.5rem x 1.5rem. The icon itself is sized accordingly to the icon library you're using.</p>

<div class="has-text-centered">
  <span class="nab-icon has-bg-tangerine lighten-4">
    <i class="icon-nabtrade"></i>
  </span>
</div>
    `,
    html: `
<span class="nab-icon">
  <i class="icon-nabtrade"></i>
</span>
    `,
    react: `
import { Icon } from '@shaper/react'

const MyComponent = (props) => (
  <Icon>nabtrade</Icon>
)
    `
  },
  {
    title: 'Colours',
    example: `
<div class="has-text-centered">
  <span class="nab-icon is-medium has-text-info">
    <i class="icon-info-circle-outline"></i>
  </span>
  <span class="nab-icon is-medium has-text-success">
    <i class="icon-checked-square"></i>
  </span>
  <span class="nab-icon is-medium has-text-warning">
    <i class="icon-bell"></i>
  </span>
  <span class="nab-icon is-medium has-text-danger">
    <i class="icon-close-circle"></i>
  </span>
</div>
`,
    html: `
<span class="nab-icon has-text-info">
  <i class="icon-info"></i>
</span>
<span class="nab-icon has-text-success">
  <i class="icon-checkbox-checked"></i>
</span>
<span class="nab-icon has-text-warning">
  <i class="icon-alerts-on"></i>
</span>
<span class="nab-icon has-text-danger">
  <i class="icon-clear"></i>
</span>
    `,
    react: `
const MyComponent = (props) => (
  <Icon color="info">info</Icon>
  <Icon color="success">checkbox-checked</Icon>
  <Icon color="warning">alerts-on</Icon>
  <Icon color="danger">clear</Icon>
)
    `
  },
  {
    title: 'Sizes',
    example: `
<p>
  The <code>.nab-icon</code>/<code>Icon</code> container comes in <strong>4 sizes</strong>. 
  It should always be slightly bigger than the icon it contains.
</p>
<div class="has-text-centered">
  <div class="mb-5">
    <p>Small</p>
    <span class="nab-icon is-small has-text-primary">
      <i class="icon-nabtrade"></i>
    </span>
  </div>
  <div class="mb-5">
    <p>Default</p>
    <span class="nab-icon has-text-primary">
      <i class="icon-nabtrade"></i>
    </span>
  </div>
  <div class="mb-5">
    <p>Medium</p>
    <span class="nab-icon is-medium has-text-primary">
      <i class="icon-nabtrade"></i>
    </span>
  </div>
  <div class="mb-5">
    <p>Large</p>
    <span class="nab-icon is-large has-text-primary">
      <i class="icon-nabtrade"></i>
    </span>
  </div>
</div>
    `,
    html: `
<span class="nab-icon is-small">
  <i class="icon-nabtrade"></i>
</span>
<span class="nab-icon">
  <i class="icon-nabtrade"></i>
</span>
<span class="nab-icon is-medium">
  <i class="icon-nabtrade"></i>
</span>
<span class="nab-icon is-large">
  <i class="icon-nabtrade"></i>
</span>
    `,
    react: `
const MyComponent = (props) => (
  <Icon small>nabtrade</Icon>
  <Icon>nabtrade</Icon>
  <Icon medium>nabtrade</Icon>
  <Icon large>nabtrade</Icon>
)
    `
  },
  {
    title: 'Icons with badge numbers',
    example: `
<div class="has-text-centered">
  <span class="nab-icon mx-3">
    <i class="icon-alert"><span>3</span></i>
  </span>
  <span class="nab-icon mx-3">
    <i class="icon-globe"><span>12</span></i>
  </span>
  <span class="nab-icon mx-3">
    <i class="icon-briefcase"><span>8</span></i>
  </span>
</div>
    `,
    html: `
<div class="has-text-centered">
  <span class="nab-icon mx-3">
    <i class="icon-alert"><span>3</span></i>
  </span>
  <span class="nab-icon mx-3">
    <i class="icon-globe"><span>12</span></i>
  </span>
  <span class="nab-icon mx-3">
    <i class="icon-briefcase"><span>8</span></i>
  </span>
</div>
    `,
    react: `
const MyComponent = (props) => (
  <Icon number={3}>alert</Icon>
  <Icon number={12}>globe</Icon>
  <Icon number={8}>briefcase</Icon>
)
    `
  }
]