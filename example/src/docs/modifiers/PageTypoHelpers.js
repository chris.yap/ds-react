import React from 'react';
import PageHeader from '../../components/PageHeader';
import {
  Box,
  Column,
  Columns,
  Container,
  Divider,
  Section,
  Table,
  TableRow as Row,
  TableCell as Cell,
  Title
} from 'shaper-react';
import Highlight from 'react-highlight';

const PageTypoHelpers = () => {
  return (
    <div id="start">
      <PageHeader category="Modifiers" title="Typography Helpers">
        Helpers that can change the size and color of the text for one or
        multiple viewport width.
      </PageHeader>

      <Divider />

      <Section>
        <Container>
          <Columns mobile>
            <Column mobile={12} desktop={10} desktopOffset={1}>
              <h3 className="has-text-size4 font-nabimpact mt-0">Sizes</h3>
              <p>
                There are <strong>23 sizes</strong> to choose from:
              </p>

              <Table
                striped
                hasNoScroll
                headers={SizeHeaders}
                data={SizeData}
                tableRow={SizeRow}
              />

              <Divider className="my-10" />

              <h3 className="has-text-size4 font-nabimpact">Responsive size</h3>
              <p>
                You can choose a <strong>specific</strong> size for{' '}
                <em>each</em> viewport width. You simply need to append the{' '}
                <strong>viewport width</strong> to the size modifier.
              </p>
              <p>
                For example, here are the modifiers for <code>$size-1</code>
              </p>

              <Table
                striped
                headers={ResHeaders}
                data={ResData}
                tableRow={ResRow}
              />

              <p>
                You can use the same logic for each of the{' '}
                <strong>7 sizes</strong>.
              </p>

              <Divider className="my-10" />

              <h3 className="has-text-size4 font-nabimpact">Colours</h3>
              <p>You can set any element to one of the colours below:</p>

              <table className="nab-table is-bordered">
                <thead>
                  <tr>
                    <th>Class</th>
                    <th>Colour</th>
                    <th>Example</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <code>has-text-white</code>
                    </td>
                    <td>#FFFFFF</td>
                    <td className="has-bg-black">
                      <strong className="has-text-white">Text colour</strong>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <code>has-text-black</code>
                    </td>
                    <td className="has-bg-black has-text-white">#000000</td>
                    <td>
                      <strong className="has-text-black">Text colour</strong>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <code>has-text-primary</code>
                    </td>
                    <td className="has-bg-primary has-text-white">#c20000</td>
                    <td>
                      <strong className="has-text-primary">Text colour</strong>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <code>has-text-secondary</code>
                    </td>
                    <td className="has-bg-secondary has-text-white">#4c626c</td>
                    <td>
                      <strong className="has-text-secondary">
                        Text colour
                      </strong>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <code>has-text-info</code>
                    </td>
                    <td className="has-bg-info has-text-white">#005aa3</td>
                    <td>
                      <strong className="has-text-info">Text colour</strong>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <code>has-text-success</code>
                    </td>
                    <td className="has-bg-success has-text-white">#a4b123</td>
                    <td>
                      <strong className="has-text-success">Text colour</strong>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <code>has-text-warning</code>
                    </td>
                    <td className="has-bg-warning has-text-white">#e9600e</td>
                    <td>
                      <strong className="has-text-warning">Text colour</strong>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <code>has-text-danger</code>
                    </td>
                    <td className="has-bg-danger has-text-white">#ce3030</td>
                    <td>
                      <strong className="has-text-danger">Text colour</strong>
                    </td>
                  </tr>
                </tbody>
              </table>

              <table className="nab-table is-bordered">
                <tbody>
                  <tr>
                    <td>
                      <code>has-text-red</code>
                    </td>
                    <td className="has-bg-red has-text-white">#c20000</td>
                    <td>
                      <strong className="has-text-red">Text colour</strong>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <code>has-text-blue-grey</code>
                    </td>
                    <td className="has-bg-blue-grey has-text-white">#4c626c</td>
                    <td>
                      <strong className="has-text-blue-grey">
                        Text colour
                      </strong>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <code>has-text-sea</code>
                    </td>
                    <td className="has-bg-sea has-text-white">#152773</td>
                    <td>
                      <strong className="has-text-sea">Text colour</strong>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <code>has-text-blueberry</code>
                    </td>
                    <td className="has-bg-blueberry has-text-white">#572381</td>
                    <td>
                      <strong className="has-text-blueberry">
                        Text colour
                      </strong>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <code>has-text-sky</code>
                    </td>
                    <td className="has-bg-sky has-text-white">#005aa3</td>
                    <td>
                      <strong className="has-text-sky">Text colour</strong>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <code>has-text-lime</code>
                    </td>
                    <td className="has-bg-lime has-text-white">#a4b123</td>
                    <td>
                      <strong className="has-text-lime">Text colour</strong>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <code>has-text-tangerine</code>
                    </td>
                    <td className="has-bg-tangerine has-text-white">#e9600e</td>
                    <td>
                      <strong className="has-text-tangerine">
                        Text colour
                      </strong>
                    </td>
                  </tr>
                </tbody>
              </table>

              <h3 className="has-text-size5 font-nabimpact">Shades</h3>
              <p>
                There are <strong>5 lighter</strong> shades and{' '}
                <strong>5 darker</strong> shades for each colour (except black -
                only lighter shades and none for white).
              </p>

              <Title size={6}>Usage</Title>

              <Highlight className="xml">
                &lt;p className="has-text-info text--lighten-4"&gt; Text colour
                &lt;/p&gt; <br />
                &lt;p className="has-text-tangerine text--darken-5"&gt; Text
                colour &lt;/p&gt;
              </Highlight>

              <Divider className="my-10" />

              <h3 className="has-text-size4 font-nabimpact">Alignment</h3>
              <p>
                Align the text with the use of one of{' '}
                <strong>4 alignment helpers</strong>:
              </p>

              <table className="nab-table is-bordered">
                <thead>
                  <tr>
                    <th>Class</th>
                    <th>Alignment</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <code>has-text-centered</code>
                    </td>
                    <td>
                      Makes the text <strong>centered</strong>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <code>has-text-justified</code>
                    </td>
                    <td>
                      Makes the text <strong>justified</strong>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <code>has-text-left</code>
                    </td>
                    <td>
                      Makes the text aligned to the <strong>left</strong>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <code>has-text-right</code>
                    </td>
                    <td>
                      Makes the text aligned to the <strong>right</strong>
                    </td>
                  </tr>
                </tbody>
              </table>

              <Divider className="my-10" />

              <h3 className="has-text-size4 font-nabimpact">Text weight</h3>
              <p>
                Transform the text weight with the use of one of{' '}
                <strong>4 weight helpers</strong>:
              </p>

              <table className="nab-table is-bordered">
                <thead>
                  <tr>
                    <th>Class</th>
                    <th>Weight</th>
                    <th>Example</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <code>has-text-weight-light</code>
                    </td>
                    <td>
                      <strong>Light</strong>
                    </td>
                    <td className="has-text-weight-light">Light</td>
                  </tr>
                  <tr>
                    <td>
                      <code>has-text-weight-normal</code>
                    </td>
                    <td>
                      <strong>Normal</strong>
                    </td>
                    <td className="has-text-weight-normal">Normal</td>
                  </tr>
                  <tr>
                    <td>
                      <code>has-text-weight-semibold</code>
                    </td>
                    <td>
                      <strong>Semi-bold</strong>
                    </td>
                    <td className="has-text-weight-semibold">Semi bold</td>
                  </tr>
                  <tr>
                    <td>
                      <code>has-text-weight-bold</code>
                    </td>
                    <td>
                      <strong>Bold</strong>
                    </td>
                    <td className="has-text-weight-bold">Bold</td>
                  </tr>
                </tbody>
              </table>

              <Divider className="my-10" />

              <h3 className="has-text-size4 font-nabimpact">Line height</h3>
              <p>
                Transform the text line height with the use of one of{' '}
                <strong>X line height helpers</strong>:
              </p>

              <table className="nab-table is-bordered">
                <thead>
                  <tr>
                    <th>Class</th>
                    <th>Line height</th>
                    <th>Example</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <code>has-line-height-100</code>
                    </td>
                    <td>
                      <strong>100%</strong>
                    </td>
                    <td
                      className="has-line-height-100"
                      style={{ whiteSpace: 'normal' }}
                    >
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Proin pretium tincidunt urna. Maecenas ultrices semper ex,
                      a congue lorem. Praesent fermentum tortor eget mauris
                      cursus, quis aliquet orci blandit.
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <code>has-line-height-125</code>
                    </td>
                    <td>
                      <strong>125%</strong>
                    </td>
                    <td
                      className="has-line-height-125"
                      style={{ whiteSpace: 'normal' }}
                    >
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Proin pretium tincidunt urna. Maecenas ultrices semper ex,
                      a congue lorem. Praesent fermentum tortor eget mauris
                      cursus, quis aliquet orci blandit.
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <code>has-line-height-150</code>
                    </td>
                    <td>
                      <strong>150%</strong>
                    </td>
                    <td
                      className="has-line-height-150"
                      style={{ whiteSpace: 'normal' }}
                    >
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Proin pretium tincidunt urna. Maecenas ultrices semper ex,
                      a congue lorem. Praesent fermentum tortor eget mauris
                      cursus, quis aliquet orci blandit.
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <code>has-line-height-200</code>
                    </td>
                    <td>
                      <strong>200%</strong>
                    </td>
                    <td
                      className="has-line-height-200"
                      style={{ whiteSpace: 'normal' }}
                    >
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Proin pretium tincidunt urna. Maecenas ultrices semper ex,
                      a congue lorem. Praesent fermentum tortor eget mauris
                      cursus, quis aliquet orci blandit.
                    </td>
                  </tr>
                </tbody>
              </table>
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};

export default PageTypoHelpers;

const ResData = [
  {
    class: 'has-text-size0-mobile',
    xs: (
      <Box bg="secondary" color="white" py={2}>
        1rem
      </Box>
    ),
    sm: 'N.A.',
    md: 'N.A.',
    lg: 'N.A.',
    xl: 'N.A.'
  },
  {
    class: 'has-text-size0-tablet',
    xs: 'N.A.',
    sm: (
      <Box bg="secondary" color="white" py={2}>
        1rem
      </Box>
    ),
    md: 'N.A.',
    lg: 'N.A.',
    xl: 'N.A.'
  },
  {
    class: 'has-text-size0-touch',
    xs: (
      <Box bg="secondary" color="white" py={2}>
        1rem
      </Box>
    ),
    sm: (
      <Box bg="secondary" color="white" py={2}>
        1rem
      </Box>
    ),
    md: 'N.A.',
    lg: 'N.A.',
    xl: 'N.A.'
  },
  {
    class: 'has-text-size0-desktop',
    xs: 'N.A.',
    sm: 'N.A.',
    md: (
      <Box bg="secondary" color="white" py={2}>
        1rem
      </Box>
    ),
    lg: (
      <Box bg="secondary" color="white" py={2}>
        1rem
      </Box>
    ),
    xl: (
      <Box bg="secondary" color="white" py={2}>
        1rem
      </Box>
    )
  },
  {
    class: 'has-text-size0-widescreen',
    xs: 'N.A.',
    sm: 'N.A.',
    md: 'N.A.',
    lg: (
      <Box bg="secondary" color="white" py={2}>
        1rem
      </Box>
    ),
    xl: (
      <Box bg="secondary" color="white" py={2}>
        1rem
      </Box>
    )
  },
  {
    class: 'has-text-size0-fullhd',
    xs: 'N.A.',
    sm: 'N.A.',
    md: 'N.A.',
    lg: 'N.A.',
    xl: (
      <Box bg="secondary" color="white" py={2}>
        1rem
      </Box>
    )
  }
];

const ResRow = ({ row }) => (
  <Row>
    <Cell className="has-text-nowrap">{row.class}</Cell>
    <Cell className="has-text-centered">{row.xs}</Cell>
    <Cell className="has-text-centered">{row.sm}</Cell>
    <Cell className="has-text-centered">{row.md}</Cell>
    <Cell className="has-text-centered">{row.lg}</Cell>
    <Cell className="has-text-centered">{row.xl}</Cell>
  </Row>
);

const ResHeaders = [
  { name: 'Class' },
  {
    name: (
      <span>
        Mobile <br />
        Up to <code>768px</code>
      </span>
    )
  },
  {
    name: (
      <span>
        Tablet <br />
        Between<code>769px</code> <br />
        and <code>1024px</code>
      </span>
    )
  },
  {
    name: (
      <span>
        Desktop <br />
        Between<code>1025px</code> <br />
        and <code>1215px</code>
      </span>
    )
  },
  {
    name: (
      <span>
        Widescreen <br />
        Between<code>1216px</code> <br />
        and <code>1407px</code>
      </span>
    )
  },
  {
    name: (
      <span>
        FullHD <br />
        Between<code>1408px</code> <br />
        and above
      </span>
    )
  }
];

const SizeData = [
  { class: 'has-text-size16', em: '6.583', px: '105' },
  { class: 'has-text-size15', em: '5.852', px: '94' },
  { class: 'has-text-size14', em: '5.202', px: '83' },
  { class: 'has-text-size13', em: '4.624', px: '74' },
  { class: 'has-text-size12', em: '4.11', px: '66' },
  { class: 'has-text-size11', em: '3.653', px: '58' },
  { class: 'has-text-size10', em: '3.247', px: '52' },
  { class: 'has-text-size9', em: '2.887', px: '46' },
  { class: 'has-text-size8', em: '2.566', px: '41' },
  { class: 'has-text-size7', em: '2.281', px: '36' },
  { class: 'has-text-size6', em: '2.027', px: '32' },
  { class: 'has-text-size5', em: '1.802', px: '29' },
  { class: 'has-text-size4', em: '1.602', px: '26' },
  { class: 'has-text-size3', em: '1.424', px: '23' },
  { class: 'has-text-size2', em: '1.266', px: '20' },
  { class: 'has-text-size1', em: '1.125', px: '18' },
  { class: 'has-text-size0', em: '1', px: '16' },
  { class: 'has-text-size-1', em: '0.889', px: '14' },
  { class: 'has-text-size-2', em: '0.79', px: '13' },
  { class: 'has-text-size-3', em: '0.702', px: '11' },
  { class: 'has-text-size-4', em: '0.624', px: '10' },
  { class: 'has-text-size-5', em: '0.555', px: '9' },
  { class: 'has-text-size-6', em: '0.493', px: '8' }
];

const SizeRow = ({ row }) => (
  <Row>
    <Cell>{row.class}</Cell>
    <Cell>{row.em}em</Cell>
    <Cell>{row.px}px</Cell>
  </Row>
);

const SizeHeaders = [
  { name: 'Class' },
  { name: 'Size in em' },
  { name: 'Size in px' }
];
