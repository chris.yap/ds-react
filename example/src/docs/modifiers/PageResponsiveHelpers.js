import React, { Component } from 'react'
import PageHeader from '../../components/PageHeader'
import { 
  Column, Columns, Container, Content, Divider, Section, Title 
  // Subtitle, Hero, 
} from 'shaper-react'

export default class Template extends Component {
  render() {
    return (
      <div id="start">

        <PageHeader category="Modifiers" title="Responsive Helpers">
          <strong>Show/hide content</strong> depending on the width of the viewport
        </PageHeader>

        <Divider className="my-0" />

        <Section>
          <Container>
            <Columns mobile>
              <Column className="is-12-mobile is-10-desktop is-offset-1-desktop">

                <Content>
                  <p>To change the <code>display</code> style, add one of the following classes:</p>
                  <ul>
                    <li><code>is-block</code></li>
                    <li><code>is-flex</code></li>
                    <li><code>is-inline</code></li>
                    <li><code>is-inline-block</code></li>
                    <li><code>is-inline-flex</code></li>
                  </ul>
                </Content>

                <Divider className="my-10" />

                <Title size="3" className="font-nabimpact">Example</Title>

                <p>For example, here's how the <code>is-flex</code> helper works:</p>
                <table className="nab-table is-border">
                  <thead>
                    <tr>
                      <th>Class</th>
                      <th>Mobile<br />
                      Up to <code>768px</code></th>
                      <th>Tablet<br />
                      Between <code>769px</code> and <code>1023px</code></th>
                      <th>Desktop<br />
                      Between <code>1024px</code> and <code>1215px</code></th>
                      <th>Widescreen<br />
                      Between <code>1216px</code> and <code>1407px</code></th>
                      <th>FullHD<br />
                      <code>1408px</code> and above</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td className="is-narrow">
                        <code>is-flex-mobile</code>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active pa-3 mb-0">flex</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                    </tr>
                    <tr>
                      <td className="is-narrow">
                        <code>is-flex-tablet-only</code>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active pa-3 mb-0">flex</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                    </tr>
                    <tr>
                      <td className="is-narrow">
                        <code>is-flex-desktop-only</code>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active pa-3 mb-0">flex</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                    </tr>
                    <tr>
                      <td className="is-narrow">
                        <code>is-flex-widescreen-only</code>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active pa-3 mb-0">flex</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                    </tr>
                    <tr>
                      <th colSpan="6">
                        <p className="mt-5 mb-0">Classes to display <strong>up to</strong> or <strong>from</strong> a specific breakpoint</p>
                      </th>
                    </tr>
                    <tr>
                      <td className="is-narrow">
                        <code>is-flex-touch</code>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active pa-3 mb-0">flex</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active pa-3 mb-0">flex</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                    </tr>
                    <tr>
                      <td className="is-narrow">
                        <code>is-flex-tablet</code>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active pa-3 mb-0">flex</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active pa-3 mb-0">flex</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active pa-3 mb-0">flex</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active pa-3 mb-0">flex</p>
                      </td>
                    </tr>
                    <tr>
                      <td className="is-narrow">
                        <code>is-flex-desktop</code>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active pa-3 mb-0">flex</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active pa-3 mb-0">flex</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active pa-3 mb-0">flex</p>
                      </td>
                    </tr>
                    <tr>
                      <td className="is-narrow">
                        <code>is-flex-widescreen</code>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active pa-3 mb-0">flex</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active pa-3 mb-0">flex</p>
                      </td>
                    </tr>
                    <tr>
                      <td className="is-narrow">
                        <code>is-flex-fullhd</code>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active is-inverted pa-3 mb-0">unchanged</p>
                      </td>
                      <td className="is-narrow">
                        <p className="nab-alert is-active pa-3 mb-0">flex</p>
                      </td>
                    </tr>
                  </tbody>
                </table>

              </Column>
            </Columns>
          </Container>
        </Section>
      </div>
    )
  }
}
