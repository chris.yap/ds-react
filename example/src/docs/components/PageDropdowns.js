import React from 'react';
import Demo from '../../components/Demo';
import PageHeader from '../../components/PageHeader';
import PropsTable from '../../components/PropsTable';
import {
  Button,
  Column,
  Columns,
  Container,
  Div,
  Divider,
  Dropdown,
  Icon,
  List,
  ListItem,
  Section,
  Tag
} from 'shaper-react';

const Page = () => {
  const [index, updateIndex] = React.useState(0);
  return (
    <div id="start">
      <PageHeader category="components" title="Dropdowns">
        An interactive dropdown menu for discoverable content <br />
        <Tag small>styled-component</Tag>
      </PageHeader>

      <Divider />

      <Section>
        <Container>
          <Columns mobile>
            <Column mobile={12} desktop={10} desktopOffset={1}>
              <Demo examples={Examples} updateIndex={updateIndex}>
                {index === 0 ? (
                  <Dropdown
                    activator={
                      <Button>
                        <span>Dropdown</span>
                        <Icon>arrow-down</Icon>
                      </Button>
                    }
                  >
                    <List>
                      <ListItem href="#">Item 1</ListItem>
                      <ListItem href="#">Item 2</ListItem>
                      <ListItem href="#">Item 3</ListItem>
                    </List>
                  </Dropdown>
                ) : index === 1 ? (
                  <>
                    <p>
                      You can have any content in the dropdown or the trigger
                    </p>
                    <Dropdown
                      activator={
                        <Button>
                          <span>Toggle content dropdown</span>
                          <Icon>arrow-down</Icon>
                        </Button>
                      }
                    >
                      <Div p={3}>
                        You can insert <strong>any type of content</strong>{' '}
                        within the dropdown menu.
                      </Div>
                      <Divider />
                      <Div p={3}>
                        Just add a <code>div</code> and add your content.
                      </Div>
                      <Divider />
                      <a className="is-block pa-3" href="/#">
                        This is a link
                      </a>
                    </Dropdown>
                  </>
                ) : index === 2 ? (
                  <>
                    <p>
                      There are 2 types of trigger types for the dropdown.{' '}
                      <br />
                      Apart from the default toggle type, there is the other
                      hover type.
                    </p>
                    <Dropdown
                      activator={
                        <Button mr={2}>
                          <span>Toggle me</span>
                          <Icon>arrow-down</Icon>
                        </Button>
                      }
                    >
                      <List>
                        <ListItem href="#">Introduction</ListItem>
                        <ListItem href="#">Getting started</ListItem>
                        <ListItem href="#">How to</ListItem>
                        <ListItem href="#">Products</ListItem>
                        <ListItem href="#">Help</ListItem>
                        <ListItem href="#">More</ListItem>
                      </List>
                    </Dropdown>

                    <Dropdown
                      hover
                      activator={
                        <Button mr={2}>
                          <span>Hover me</span>
                          <Icon>arrow-down</Icon>
                        </Button>
                      }
                    >
                      <Div p={3}>
                        You can insert <strong>any type of content</strong>{' '}
                        within the dropdown menu.
                      </Div>
                    </Dropdown>
                  </>
                ) : index === 3 ? (
                  <>
                    <p>You can align the dropdown to the right if needed.</p>
                    <Dropdown
                      activator={
                        <Button mr={2}>
                          <span>Left aligned</span>
                          <Icon>arrow-down</Icon>
                        </Button>
                      }
                    >
                      <Div p={3}>
                        This dropdown is <strong>left-aligned</strong> by
                        default.
                      </Div>
                    </Dropdown>
                    <Dropdown
                      right
                      activator={
                        <Button>
                          <span>Right aligned</span>
                          <Icon>arrow-down</Icon>
                        </Button>
                      }
                    >
                      <Div p={3}>
                        Add the <code>is-right</code> class to the html or{' '}
                        <code>right</code> prop if you are using the react
                        component.
                      </Div>
                      <Divider />
                    </Dropdown>
                  </>
                ) : index === 4 ? (
                  <>
                    <p>
                      To have a dropdown menu that appears above the dropdown
                      button, simply add the <code>up</code> prop to the react
                      component.
                    </p>
                    <Dropdown
                      up
                      activator={
                        <Button>
                          <span>Dropup button</span>
                          <Icon>arrow-up</Icon>
                        </Button>
                      }
                    >
                      <Div p={3}>
                        To have a dropdown menu that appears above the dropdown
                        button, simply add the <code>is-up</code> modifier to
                        the HTML or add <code>dropup</code> prop to the react
                        component.
                      </Div>
                    </Dropdown>
                  </>
                ) : (
                  <Dropdown
                    className="is-block"
                    fullWidth
                    activator={
                      <Button className="is-block is-flex" fullWidth>
                        <span className="is-flex-1 has-text-left">
                          Dropdown
                        </span>
                        <Icon>arrow-down</Icon>
                      </Button>
                    }
                  >
                    <List>
                      <ListItem href="#">Item 1</ListItem>
                      <ListItem href="#">Item 2</ListItem>
                      <ListItem href="#">Item 3</ListItem>
                    </List>
                  </Dropdown>
                )}
              </Demo>
              <PropsTable data={PropsData} />
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};

export default Page;

const PropsData = [
  {
    items: [
      {
        name: 'hover',
        type: 'Boolean',
        default: 'false',
        desc: 'Make dropdown expand on hover instead.'
      },
      {
        name: 'right',
        type: 'Boolean',
        default: 'false',
        desc: 'Align dropdown to the right of the trigger.'
      },
      {
        name: 'up',
        type: 'Boolean',
        default: 'false',
        desc: 'Make dropdown appear on top instead.'
      }
    ]
  }
];
const Examples = [
  {
    title: '# Usage',
    react: `
<Dropdown activator={
  <Button>
    <span>Dropdown</span>
    <Icon>arrow-down</Icon>
  </Button>
}>
  <List>
    <ListItem href="#">Item 1</ListItem>
    <ListItem href="#">Item 2</ListItem>
    <ListItem href="#">Item 3</ListItem>
  </List>
</Dropdown>
    `
  },
  {
    title: '# Content',
    react: `
<Dropdown
  activator={
    <Button>
      <span>Toggle content dropdown</span>
      <Icon>arrow-down</Icon>
    </Button>
  }>
  <Div p={3}>
    You can insert <strong>any type of content</strong>{' '}
    within the dropdown menu.
  </Div>
  <Divider />
  <Div p={3}>
    Just add a <code>div</code> and add your content.
  </Div>
  <Divider />
  <a className="is-block pa-3" href="/#">
    This is a link
  </a>
</Dropdown>
    `
  },
  {
    title: '# Hover',
    react: `
<Dropdown
  hover
  activator={
    <Button mr={2}>
      <span>Hover me</span>
      <Icon>arrow-down</Icon>
    </Button>
  }>
  <Div p={3}>
    You can insert <strong>any type of content</strong>{' '}
    within the dropdown menu.
  </Div>
</Dropdown>
    `
  },
  {
    title: '# Right aligned',
    react: `
<Dropdown
  right
  activator={
    <Button>
      <span>Right aligned</span>
      <Icon>arrow-down</Icon>
    </Button>
  }>
  <Div p={3}>
    Add the <code>is-right</code> class to the html or{' '}
    <code>right</code> prop if you are using the react
    component.
  </Div>
  <Divider />
</Dropdown>
    `
  },
  {
    title: '# Dropup',
    react: `
<Dropdown
  up
  activator={
    <Button>
      <span>Dropup button</span>
      <Icon>arrow-up</Icon>
    </Button>
  }>
  <Div p={3}>
    To have a dropdown menu that appears above the dropdown
    button, simply add the <code>is-up</code> modifier to
    the HTML or add <code>dropup</code> prop to the react
    component.
  </Div>
</Dropdown>
    `
  },
  {
    title: '# Fullwidth dropdown list',
    react: `
    <Dropdown
      className='is-block'
      fullWidth
      activator={
        <Button className='is-block is-flex' fullWidth>
          <span className='is-flex-1 has-text-left'>
            Dropdown
          </span>
          <Icon>arrow-down</Icon>
        </Button>
      }
    >
      <List>
        <ListItem href='#'>Item 1</ListItem>
        <ListItem href='#'>Item 2</ListItem>
        <ListItem href='#'>Item 3</ListItem>
      </List>
    </Dropdown>
    `
  }
];
