import React from 'react';
import PageHeader from '../../components/PageHeader';
import Demo from '../../components/Demo';
import PropsTable from '../../components/PropsTable';
import {
  Button,
  Column,
  Columns,
  Container,
  Divider,
  Modal,
  Section,
  Tag,
  Title
} from 'shaper-react';

const Page = () => {
  const [index, updateIndex] = React.useState(0);
  const [modal, showModal] = React.useState(0);

  return (
    <React.Fragment>
      <div id="start">
        <PageHeader category="Components" title="Modals">
          A classic <strong>modal</strong> overlay, in which you can include{' '}
          <em>any</em> content you want. <br />
          <Tag small>styled-component</Tag>
        </PageHeader>

        <Divider />

        <Section>
          <Container>
            <Columns mobile>
              <Column className="is-12-mobile is-10-desktop is-offset-1-desktop">
                <Demo examples={Eg} updateIndex={updateIndex}>
                  {index === 0 ? (
                    <>
                      <Button
                        medium
                        onClick={() => showModal(1)}
                        className="mx-0">
                        Launch simple modal
                      </Button>
                      <Modal
                        isOpened={modal === 1}
                        onClose={() => showModal(null)}
                        closeIcon>
                        <Title size="5" className="mb-2">
                          Jane Doe{' '}
                          <small className="has-text-weight-light has-text-size-2">
                            @sydney
                          </small>
                        </Title>
                        <p className="mb-0">
                          Lorem ipsum dolor sit amet consectetur adipisicing
                          elit. Commodi, quidem dignissimos reiciendis eum eius
                          odit in magni amet! Sint tempora exercitationem
                          aliquam earum adipisci accusamus amet saepe sed animi
                          enim.
                        </p>
                      </Modal>
                    </>
                  ) : index === 1 ? (
                    <>
                      <Button
                        medium
                        onClick={() => showModal(2)}
                        className="mx-0">
                        Launch image modal
                      </Button>
                      <Modal
                        isOpened={modal === 2}
                        onClose={() => showModal(null)}
                        backgroundImage="url(/images/testimage.jpg)"
                        backgroundSize="cover"
                        height="400px"
                        closeIcon
                      />
                    </>
                  ) : index === 2 ? (
                    <>
                      <Button
                        medium
                        onClick={() => showModal(3)}
                        className="mx-0">
                        Launch card modal
                      </Button>
                      <Modal
                        header="Modal title"
                        isOpened={modal === 3}
                        onClose={() => showModal(false)}
                        footer={
                          <>
                            <Button
                              success
                              onClick={() => {
                                showModal(false);
                              }}>
                              Save changes
                            </Button>
                            <Button onClick={() => showModal(false)}>
                              Cancel
                            </Button>
                          </>
                        }>
                        <Title size="3">Hello world</Title>
                        <p>
                          Lorem ipsum dolor sit amet consectetur adipisicing
                          elit. Totam unde, aspernatur assumenda mollitia, enim
                          corrupti ut libero molestiae neque cupiditate nam
                          veritatis deserunt aut ab ipsum officiis nisi
                          reiciendis dolor.
                        </p>
                        <Title size="4">Title</Title>
                        <p>
                          Lorem ipsum dolor sit amet consectetur adipisicing
                          elit. Totam unde, aspernatur assumenda mollitia, enim
                          corrupti ut libero molestiae neque cupiditate nam
                          veritatis deserunt aut ab ipsum officiis nisi
                          reiciendis dolor.
                        </p>
                      </Modal>
                    </>
                  ) : index === 3 ? (
                    <>
                      <Button
                        medium
                        onClick={() => showModal(4)}
                        className="mx-0">
                        Launch alert modal
                      </Button>
                      <Modal
                        alert="warning"
                        isOpened={modal === 4}
                        onClose={() => showModal(null)}>
                        <p className="has-text-size1 mb-2">
                          <strong>Warning</strong>
                        </p>
                        <p>
                          Lorem ipsum dolor sit amet consectetur adipisicing
                          elit. Officiis, praesentium. Sunt accusamus
                          perspiciatis minus optio quo, qui nihil vel magnam
                          debitis non quos excepturi assumenda recusandae rem
                          maxime suscipit aspernatur!
                        </p>
                        <Button
                          warning
                          className="mx-0"
                          onClick={() => showModal(null)}>
                          I agree
                        </Button>
                      </Modal>
                    </>
                  ) : (
                    <>
                      <Button
                        medium
                        onClick={() => showModal(5)}
                        className="mx-0">
                        Launch fullscreen modal
                      </Button>
                      <Modal
                        fullscreen
                        isOpened={modal === 5}
                        onClose={() => showModal(null)}
                        header={'Fullscreen modal'}
                        footer={
                          <Button onClick={() => showModal(null)}>Close</Button>
                        }>
                        <Title size="4">Hello world</Title>
                        <p>
                          Lorem ipsum dolor sit amet consectetur adipisicing
                          elit. Impedit minima ex, corporis sint nobis
                          dignissimos aspernatur architecto illum accusantium
                          beatae deleniti nulla maxime ab voluptas cupiditate
                          quae magnam quod ipsa!
                        </p>
                      </Modal>
                    </>
                  )}
                </Demo>

                <PropsTable data={PropsData} />
              </Column>
            </Columns>
          </Container>
        </Section>
      </div>
    </React.Fragment>
  );
};

export default Page;

const PropsData = [
  {
    items: [
      {
        name: 'header',
        def: 'null',
        type: 'String',
        desc: 'Set header of modal'
      },
      {
        name: 'footer',
        def: 'null',
        type: 'String',
        desc: 'Content in footer'
      },
      {
        name: 'isOpened',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Controls visibility of modal'
      },
      {
        name: 'onClose',
        def: 'undefined',
        type: 'function',
        desc: 'Link to close modal function'
      },
      {
        name: 'onClosed',
        def: 'undefined',
        type: 'function',
        desc: 'Callback fired after the "onClose" function has completed.'
      },
      {
        name: 'fullscreen',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Set modal to fullscreen'
      },
      {
        name: 'closeIcon',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Enable close button on overlay'
      },
      {
        name: 'alert',
        def: 'undefined',
        type: 'String',
        desc: (
          <span>
            Convert modal into an alert component. <br />4 types - "
            <strong>success</strong>", "<strong>info</strong>", "
            <strong>warning</strong>", "<strong>danger</strong>"
          </span>
        )
      },
      {
        name: 'modalWidth',
        def: 'undefined',
        type: 'Number or String',
        desc: 'Set Modal width in percentage'
      },
      {
        name: 'bodyClassName',
        def: 'undefined',
        type: 'String',
        desc: 'Class name/s for the modal body'
      }
    ]
  }
];

const Eg = [
  {
    title: '# Usage',
    react: `
<Modal
  isOpened={state}
  onClose={closeFunc}
  closeIcon
>
  Content goes here...
</Modal>
    `
  },
  {
    title: '# Image',
    react: `
<Modal
  isOpened={state}
  onClose={closeFunc}
  backgroundImage="url(/images/testimage.jpg)"
  backgroundSize="cover"
  height="400px"
  closeIcon
/>
		`
  },
  {
    title: '# Modal card',
    react: `
<Modal
  isOpened={state}
  header="Header title"
  onClose={closeFunc}
  footer={Footer content}
>
  Body content goes here...
</Modal>
		`
  },
  {
    title: '# Alert',
    react: `
<Modal
  alert="warning"
  isOpened={state}
  onClose={closeFunc}
>
  Content...
</Modal>
		`
  },
  {
    title: '# Fullscreen',
    react: `
<Modal
  fullscreen
  isOpened={state}
  onClose={closeFunc}
  header={'Fullscreen modal'}
  footer={
    <Button onClick={() => showModal(null)}>Close</Button>
  }
>
  <Title size="4">Hello world</Title>
  <p>
    Lorem ipsum dolor sit amet consectetur adipisicing
    elit. Impedit minima ex, corporis sint nobis
    dignissimos aspernatur architecto illum accusantium
    beatae deleniti nulla maxime ab voluptas cupiditate
    quae magnam quod ipsa!
  </p>
</Modal>
		`
  }
];
