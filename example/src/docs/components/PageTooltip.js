import React from 'react';
import PageHeader from '../../components/PageHeader';
import Demo from '../../components/Demo';
import PropsTable from '../../components/PropsTable';
import {
  Button,
  Column,
  Columns,
  Container,
  Div,
  Divider,
  Section,
  Tag,
  Tooltip
} from 'shaper-react';

const TooltipView = props => (
  <React.Fragment>
    <Div p={4} bg="secondaries.0">
      <p className="has-text-black has-text-size1 mb-0 has-text-weight-semibold">
        {/* <img
          alt='nabtrade'
          src='/images/nabtrade-logo.png'
          height='20'
          width='89'
          className='mr-2 is-pulled-left'
        /> */}
        Header
      </p>
    </Div>
    <Divider />
    <Div p={4} className="has-text-size0">
      <p>Lorem ipsum dolor, sit amet consectetur elit.</p>
      <p className="mb-0">
        Iusto excepturi ullam et itaque{' '}
        <a href="/#" className="has-text-primary">
          officiis rem velit distinctio
        </a>{' '}
        quisquam enim atque.
      </p>
    </Div>
  </React.Fragment>
);

const PageTooltip = () => {
  const [index, updateIndex] = React.useState(0);
  return (
    <div id="start">
      <PageHeader category="components" title="Tooltip">
        A useful component for conveying information when a user hovers over an
        element. <br />
        <Tag small>styled-component</Tag>
      </PageHeader>

      <Divider />

      <Section>
        <Container>
          <Columns mobile>
            <Column className="is-12-mobile is-10-desktop is-offset-1-desktop">
              <Demo examples={Eg} updateIndex={updateIndex}>
                {index === 0 ? (
                  <Tooltip tip="Tooltip content here" zIndex={100}>
                    <Button secondary className="mx-0">
                      Show tooltip here
                    </Button>
                  </Tooltip>
                ) : index === 1 ? (
                  <div className="has-text-centered">
                    <Tooltip tip="Tooltip on left" left>
                      <Button secondary>Tooltip on left</Button>
                    </Tooltip>
                    <Tooltip tip="Tooltip on top" top>
                      <Button secondary>Tooltip on top (Default)</Button>
                    </Tooltip>
                    <Tooltip tip="Tooltip on bottom" bottom>
                      <Button secondary>Tooltip on bottom</Button>
                    </Tooltip>
                    <Tooltip tip="Tooltip on right" right>
                      <Button secondary>Tooltip on right</Button>
                    </Tooltip>
                  </div>
                ) : (
                  <Tooltip
                    white
                    textLeft
                    noPaddings
                    tip={<TooltipView />}
                    minWidth="300">
                    <Button secondary>Styled tooltip</Button>
                  </Tooltip>
                )}
              </Demo>

              <PropsTable data={Data} />
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};
export default PageTooltip;

const Data = [
  {
    items: [
      {
        name: 'tip',
        def: 'undefined',
        type: 'String',
        desc: 'Tooltip content'
      },
      {
        name: 'top',
        def: 'true',
        type: 'Boolean',
        desc: 'Top placement'
      },
      {
        name: `left, <br /> bottom, <br /> right`,
        def: 'undefined',
        type: 'Boolean',
        desc: 'Other placement'
      },
      {
        name: 'minWidth',
        def: 'undefined',
        type: 'Number',
        desc: 'Set minimum width of tooltip message'
      },
      {
        name: 'maxWidth',
        def: 'undefined',
        type: 'Number',
        desc: 'Set maximum width of tooltip message'
      }
    ]
  }
];

const Eg = [
  {
    title: '# Usage',
    react: `
<Tooltip tip="Tooltip content here">
  <Button secondary>
    Show tooltip here
  </Button>
</Tooltip>
    `
  },
  {
    title: '# Placements',
    react: `
<Tooltip tip="Tooltip on left" left>
  <Button secondary>Tooltip on left</Button>
</Tooltip>
<Tooltip tip="Tooltip on top">
  <Button secondary>Tooltip on top (Default)</Button>
</Tooltip>
<Tooltip tip="Tooltip on bottom" bottom>
  <Button secondary>Tooltip on bottom</Button>
</Tooltip>
<Tooltip tip="Tooltip on right" right>
  <Button secondary>Tooltip on right</Button>
</Tooltip>
    `
  },
  {
    title: '# Styled',
    react: `

  <Tooltip
    white
    textLeft
    noPaddings
    tip={<TooltipView />}
    minWidth='300'
  >
    <Button secondary>Styled tooltip</Button>
  </Tooltip>

  const TooltipView = props => (
    <React.Fragment>
    ....
    </React.Fragment>
  )
    `
  }
];
