import React from 'react';
import PageHeader from '../../components/PageHeader';
import Demo from '../../components/Demo';
import PropsTable from '../../components/PropsTable';
import {
  Column,
  Columns,
  Container,
  Divider,
  Section,
  Tab,
  Tabs,
  Tag,
  Title
} from 'shaper-react';

const Page = ({ props }) => {
  const [index, updateIndex] = React.useState(0);
  // let handleOnClick = event => {
  //   alert(event);
  // };
  return (
    <div id="start">
      <PageHeader category="components" title="Tabs">
        Simple responsive horizontal navigation <strong>tabs</strong>, with
        different styles <br />
        <Tag small>styled-component</Tag>
      </PageHeader>

      <Divider />

      <Section>
        <Container>
          <Columns mobile>
            <Column className="is-12-mobile is-10-desktop is-offset-1-desktop">
              <Demo examples={Eg} updateIndex={updateIndex}>
                {index === 0 ? (
                  <Tabs selectedTab={2}>
                    <Tab label="Link">Content 1</Tab>
                    <Tab label="Link">
                      Lorem ipsum dolor sit amet consectetur adipisicing elit.
                      Incidunt distinctio ducimus tempora laborum expedita
                      asperiores molestiae quaerat illo pariatur perferendis!
                      Assumenda repellendus, at voluptas corrupti labore amet!
                      Optio, non est.
                    </Tab>
                    <Tab label="Active">Content 3</Tab>
                    <Tab label="Disabled" disabled>
                      Content 4
                    </Tab>
                  </Tabs>
                ) : index === 1 ? (
                  <>
                    <Title size={5}>Center aligned</Title>
                    <Tabs centered className="mb-10">
                      <Tab label="Active" />
                      <Tab label="Link" />
                      <Tab label="Link" />
                      <Tab label="Disabled" disabled />
                    </Tabs>
                    <Title size={5}>Right aligned</Title>
                    <Tabs right className="mb-10">
                      <Tab label="Active" />
                      <Tab label="Link" />
                      <Tab label="Link" />
                      <Tab label="Disabled" disabled />
                    </Tabs>
                    <Title size={5}>Full width</Title>
                    <Tabs fullwidth>
                      <Tab label="Active" />
                      <Tab label="Link" />
                      <Tab label="Link" />
                      <Tab label="Disabled" disabled />
                    </Tabs>
                  </>
                ) : index === 2 ? (
                  <Tabs>
                    <Tab icon="checked-circle-outline" label="Active" />
                    <Tab icon="close-circle-outline" label="Link" />
                    <Tab icon="close-circle-outline" label="Link" />
                    <Tab
                      icon="close-circle-outline"
                      label="Disabled"
                      disabled
                    />
                  </Tabs>
                ) : index === 3 ? (
                  <>
                    <Tabs small className="mb-5">
                      <Tab label="Small" />
                      <Tab label="Link" />
                      <Tab label="Link" />
                      <Tab label="Disabled" disabled />
                    </Tabs>
                    <Tabs className="mb-5">
                      <Tab label="Default" />
                      <Tab label="Link" />
                      <Tab label="Link" />
                      <Tab label="Disabled" disabled />
                    </Tabs>
                    <Tabs medium className="mb-5">
                      <Tab label="Medium" />
                      <Tab label="Link" />
                      <Tab label="Link" />
                      <Tab label="Disabled" disabled />
                    </Tabs>
                    <Tabs large>
                      <Tab label="Large" />
                      <Tab label="Link" />
                      <Tab label="Link" />
                      <Tab label="Disabled" disabled />
                    </Tabs>
                  </>
                ) : index === 4 ? (
                  <Tabs boxed>
                    <Tab icon="checked-circle-outline" label="Active" active />
                    <Tab icon="close-circle-outline" label="Link" />
                    <Tab icon="close-circle-outline" label="Link" />
                    <Tab
                      icon="close-circle-outline"
                      label="Disabled"
                      disabled
                    />
                  </Tabs>
                ) : index === 5 ? (
                  <Tabs toggle>
                    <Tab icon="checked-circle-outline" label="Active" active />
                    <Tab icon="close-circle-outline" label="Link" />
                    <Tab icon="close-circle-outline" label="Link" />
                    <Tab
                      icon="close-circle-outline"
                      label="Disabled"
                      disabled
                    />
                  </Tabs>
                ) : (
                  <Tabs toggle rounded>
                    <Tab label="Active" active />
                    <Tab label="Link" />
                    <Tab label="Link" />
                    <Tab label="Disabled" disabled />
                  </Tabs>
                )}
              </Demo>

              <PropsTable data={Data} />
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};

export default Page;

const Data = [
  {
    title: 'Tabs',
    items: [
      {
        name: `left,<br /> centered,<br /> right,<br /> fullwidth`,
        def: 'undefined',
        type: 'Boolean',
        desc: 'Tabs alignment'
      },
      {
        name: `small,<br /> medium,<br /> large`,
        def: 'undefined',
        type: 'Boolean',
        desc: 'Set tab size'
      },
      {
        name: `boxed,<br /> toggle,<br /> rounded`,
        def: 'undefined',
        type: 'Boolean',
        desc: 'Set tab style'
      },
      {
        name: 'selectedTab',
        def: '0',
        type: 'Number',
        desc: 'Set active tab'
      },
      {
        name: 'className',
        def: 'undefined',
        type: 'string',
        desc: 'Add custom class/es'
      }
    ]
  },
  {
    title: 'Tab',
    items: [
      {
        name: 'className',
        def: 'undefined',
        type: 'string',
        desc: 'Add custom class/es'
      },
      {
        name: 'icon',
        def: 'undefined',
        type: 'String',
        desc: 'Give tab an icon'
      },
      {
        name: 'tabClick',
        def: 'undefined',
        type: 'Function',
        desc: 'Function call for tab click'
      }
    ]
  }
];

const Eg = [
  {
    title: '# Usage',
    react: `
<Tabs selectedTab={2}>
  <Tab label="Link">Tab content</Tab>
  <Tab label="Link">Tab content</Tab>
  <Tab label="Active">Tab content</Tab>
  <Tab label="Disabled" disabled>Tab content</Tab>
</Tabs>
    `
  },
  {
    title: '# Alignment',
    react: `
<Tabs centered>
  <Tab label="Active" />
  <Tab label="Link" />
  <Tab label="Link" />
  <Tab label="Disabled" disabled />
</Tabs>
<Tabs right>
  <Tab label="Active" />
  <Tab label="Link" />
  <Tab label="Link" />
  <Tab label="Disabled" disabled />
</Tabs>
<Tabs fullwidth>
  <Tab label="Active" />
  <Tab label="Link" />
  <Tab label="Link" />
  <Tab label="Disabled" disabled />
</Tabs>
    `
  },
  {
    title: '# Icons',
    react: `
<Tabs>
  <Tab icon="checked-circle-outline" label="Active" />
  <Tab icon="close-circle-outline" label="Link" />
  <Tab icon="close-circle-outline" label="Link" />
  <Tab icon="close-circle-outline" label="Disabled" disabled />
</Tabs>
    `
  },
  {
    title: '# Sizes',
    react: `
<Tabs small>
  <Tab label="Link 1">Content 1 </Tab>
  <Tab label="Link 2">Content 2 </Tab>
  <Tab label="Link 3">Content 3 </Tab>
  <Tab label="Link 4" disabled>Content 4 </Tab>
</Tabs>
<Tabs>
  <Tab label="Link 1">Content 1 </Tab>
  <Tab label="Link 2">Content 2 </Tab>
  <Tab label="Link 3">Content 3 </Tab>
  <Tab label="Link 4" disabled>Content 4 </Tab>
</Tabs>
<Tabs medium>
  <Tab label="Link 1">Content 1 </Tab>
  <Tab label="Link 2">Content 2 </Tab>
  <Tab label="Link 3">Content 3 </Tab>
  <Tab label="Link 4" disabled>Content 4 </Tab>
</Tabs>
<Tabs large>
  <Tab label="Link 1">Content 1 </Tab>
  <Tab label="Link 2">Content 2 </Tab>
  <Tab label="Link 3">Content 3 </Tab>
  <Tab label="Link 4" disabled>Content 4 </Tab>
</Tabs>
    `
  },
  {
    title: '# Boxed',
    react: `
<Tabs boxed>
  <Tab icon="checked-circle-outline" label="Active" active />
  <Tab icon="close-circle-outline" label="Link" />
  <Tab icon="close-circle-outline" label="Link" />
  <Tab
    icon="close-circle-outline"
    label="Disabled"
    disabled
  />
</Tabs>
    `
  },
  {
    title: '# Toggle',
    react: `
<Tabs toggle>
  <Tab label="Link 1">Content 1 </Tab>
  <Tab label="Link 2">Content 2 </Tab>
  <Tab label="Link 3">Content 3 </Tab>
  <Tab label="Link 4" disabled>Content 4 </Tab>
</Tabs>
    `
  },
  {
    title: '# Toggle rounded',
    react: `
<Tabs toggle rounded>
  <Tab label="Active" active />
  <Tab label="Link" />
  <Tab label="Link" />
  <Tab label="Disabled" disabled />
</Tabs>
    `
  }
];
