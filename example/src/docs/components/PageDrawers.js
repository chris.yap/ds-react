import React, { Component } from 'react';
import {
  Button,
  Column,
  Columns,
  Container,
  Divider,
  Section,
  Table,
  Title
} from 'shaper-react';
import PageHeader from '../../components/PageHeader';
import { PropsHeaders, PropsTableRow } from '../../constants/PropsTables';
import ExpandingCodeExample from '../../components/ExpandingCodeExample';

export default class PageDrawers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      drawer: false
    };
  }
  render() {
    const { toggleDrawer } = this.props;
    return (
      <div id="start">
        <PageHeader category="Components" title="Drawers">
          A Drawer component is what your users utilize to navigate through the
          application.
        </PageHeader>

        <Divider className="my-0" />

        <Section>
          <Container>
            <Columns mobile>
              <Column className="is-12-mobile is-10-desktop is-offset-1-desktop">
                <Title size="3" className="font-nabimpact">
                  Examples
                </Title>

                <ExpandingCodeExample title="# Usage">
                  <div className="has-text-centered">
                    <Button onClick={toggleDrawer}>Toggle drawer</Button>
                  </div>
                </ExpandingCodeExample>

                <Table
                  striped
                  headers={PropsHeaders}
                  tableRow={PropsTableRow}
                  defaultSortCol="name"
                />
              </Column>
            </Columns>
          </Container>
        </Section>
      </div>
    );
  }
}
