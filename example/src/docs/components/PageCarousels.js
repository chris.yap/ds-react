import React from 'react';
import PageHeader from '../../components/PageHeader';
import cn from 'classnames';
import Demo from '../../components/Demo';
import PropsTable from '../../components/PropsTable';
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Carousel,
  Column,
  Columns,
  Container,
  Divider,
  Icon,
  Section,
  Subtitle,
  Title
} from 'shaper-react';

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.play = this.play.bind(this);
    this.pause = this.pause.bind(this);

    this.state = {
      playing: true,
      index: 0,
      items: 5,
      slidesToShow: 2,
      spacing: 20,
      autoplay: true,
      centerMode: false,
      infinite: false,
      arrows: false,
      pause: false,
      variableWidth: false
    };
  }

  play() {
    this.slider.slickPlay();
    this.setState({ playing: true });
  }
  pause() {
    this.slider.slickPause();
    this.setState({ playing: false });
  }

  refreshSlider = (c, v, i) => {
    setTimeout(() => {
      this.setState({
        centerMode: c ? false : true,
        variableWidth: v ? false : true,
        infinite: i ? false : true
      });
    }, 100);
    setTimeout(() => {
      this.setState({
        centerMode: c,
        variableWidth: v,
        infinite: i
      });
    }, 5000);
  };
  render = () => {
    const {
      index,
      items,
      spacing,
      slidesToShow,
      arrows,
      centerMode,
      infinite,
      variableWidth,
      autoplay,
      pause,
      playing
    } = this.state;
    let listing = [];
    for (let i = 0; i < items; i++) {
      listing.push(
        <Box key={i} border="1px solid #ddd" textAlign="center">{`${i +
          1}`}</Box>
      );
    }
    return (
      <div id="start">
        <PageHeader category="Components" title="Carousels">
          A component is used to display large numbers of visual content on a
          rotating timer.
        </PageHeader>

        <Divider />

        <Section>
          <Container>
            <Columns mobile>
              <Column className="is-12-mobile is-10-desktop is-offset-1-desktop">
                <Demo
                  examples={Eg}
                  updateIndex={e => this.setState({ index: e })}>
                  {index === 0 ? (
                    <>
                      <Carousel
                        pauseOnFocus={pause}
                        arrows={arrows}
                        autoplay={autoplay}
                        autoplaySpeed={1000}
                        dots
                        fade={false}
                        cellSpacing={spacing}
                        slidesToShow={slidesToShow}
                        infinite={infinite}
                        speed={3000}
                        edgeFriction={0}
                        centerMode={centerMode}
                        variableWidth={variableWidth}>
                        {listing}
                      </Carousel>
                      <div className="my-4 has-text-centered">
                        <Button
                          small
                          className="ml-0"
                          disabled
                          secondary={!autoplay}
                          inverted={!autoplay}
                          onClick={() =>
                            this.setState({ autoplay: autoplay ? false : true })
                          }>
                          <Icon style={{ opacity: autoplay ? '1' : '.2' }}>
                            {autoplay ? 'checked-square' : 'square'}
                          </Icon>
                          <span>autoplay</span>
                        </Button>
                        <Button
                          small
                          className="ml-0"
                          secondary={!centerMode}
                          inverted={!centerMode}
                          onClick={() =>
                            this.setState({
                              centerMode: centerMode ? false : true
                            })
                          }>
                          <Icon style={{ opacity: centerMode ? '1' : '.2' }}>
                            {centerMode ? 'checked-square' : 'square'}
                          </Icon>
                          <span>centerMode</span>
                        </Button>
                        <Button
                          small
                          className="ml-0"
                          secondary={!infinite}
                          inverted={!infinite}
                          onClick={() =>
                            this.setState({ infinite: infinite ? false : true })
                          }>
                          <Icon style={{ opacity: infinite ? '1' : '.2' }}>
                            {infinite ? 'checked-square' : 'square'}
                          </Icon>
                          <span>infinite</span>
                        </Button>
                        <Button
                          small
                          className="ml-0"
                          secondary={!arrows}
                          inverted={!arrows}
                          onClick={() =>
                            this.setState({ arrows: arrows ? false : true })
                          }>
                          <Icon style={{ opacity: arrows ? '1' : '.2' }}>
                            {arrows ? 'checked-square' : 'square'}
                          </Icon>
                          <span>arrows</span>
                        </Button>
                        <Button
                          small
                          className="ml-0"
                          secondary={!pause}
                          inverted={!pause}
                          onClick={() =>
                            this.setState({ pause: pause ? false : true })
                          }>
                          <Icon style={{ opacity: pause ? '1' : '.2' }}>
                            {pause ? 'checked-square' : 'square'}
                          </Icon>
                          <span>pauseOnHover</span>
                        </Button>
                        <Button
                          small
                          className="ml-0"
                          secondary={!variableWidth}
                          inverted={!variableWidth}
                          onClick={() =>
                            this.setState({
                              variableWidth: variableWidth ? false : true
                            })
                          }>
                          <Icon style={{ opacity: variableWidth ? '1' : '.2' }}>
                            {variableWidth ? 'checked-square' : 'square'}
                          </Icon>
                          <span>variableWidth</span>
                        </Button>
                      </div>
                    </>
                  ) : index === 1 ? (
                    <Carousel
                      autoplay={true}
                      autoplaySpeed={1500}
                      centerMode={false}
                      centerPadding={0}
                      infinite={true}
                      fade={false}
                      cellSpacing={30}
                      speed={1000}
                      slidesToShow={3}>
                      {Currencies.map(money => {
                        return (
                          <Card key={money.name} outline>
                            <CardHeader className="px-4">
                              <div>
                                <Title size="5" className="mb-5">
                                  {money.name}
                                </Title>
                                <Subtitle
                                  size="7"
                                  className="has-text-secondary text--lighten-2">
                                  {money.currency}
                                </Subtitle>
                              </div>
                            </CardHeader>
                            <CardContent className="has-bg-blue-grey lighten-5 px-4">
                              <Columns>
                                <Column>
                                  <Subtitle size="7" className="mb-4">
                                    Exchange rate
                                  </Subtitle>
                                  <Title size="5" className="mb-0">
                                    {money.rates}
                                  </Title>
                                </Column>
                                <Column narrow className="has-text-right">
                                  <Subtitle size="7" className="mb-4">
                                    Price change
                                  </Subtitle>
                                  <Title size="5" className="mb-0">
                                    <span
                                      className={cn(
                                        money.change > 0
                                          ? 'has-text-success'
                                          : '',
                                        money.change < 0
                                          ? 'has-text-danger'
                                          : ''
                                      )}>
                                      <Icon small>
                                        {money.change > 0
                                          ? 'caret-up'
                                          : 'caret-down'}
                                      </Icon>
                                      {money.change}
                                      <sup
                                        style={{
                                          position: 'relative',
                                          top: '4px'
                                        }}>
                                        %
                                      </sup>
                                    </span>
                                  </Title>
                                </Column>
                              </Columns>
                            </CardContent>
                          </Card>
                        );
                      })}
                    </Carousel>
                  ) : (
                    <>
                      <Carousel
                        innerRef={slider => (this.slider = slider)}
                        arrows={false}
                        autoplay={true}
                        autoplaySpeed={1000}
                        dots
                        fade={false}
                        cellSpacing={20}
                        slidesToShow={2}
                        infinite={true}
                        speed={3000}
                        edgeFriction={0}>
                        {listing}
                      </Carousel>
                      <div className="has-text-centered">
                        <Button onClick={this.play} disabled={playing}>
                          Play
                        </Button>
                        <Button onClick={this.pause} disabled={!playing}>
                          Pause
                        </Button>
                      </div>
                    </>
                  )}
                </Demo>

                <PropsTable data={Data} />
              </Column>
            </Columns>
          </Container>
        </Section>
      </div>
    );
  };
}
export default Page;

const Data = [
  {
    items: [
      {
        name: 'arrows',
        def: 'true',
        type: 'Boolean',
        desc: "Carousel's navigation"
      },
      {
        name: 'autoplay',
        def: 'false',
        type: 'Boolean',
        desc: 'Set carousel to rotate'
      },
      {
        name: 'autoplaySpeed',
        def: '3000',
        type: 'Int',
        desc: 'Delay between each auto scroll (in milliseconds)'
      },
      {
        name: 'cellSpacing',
        def: '20',
        type: 'Number',
        desc: 'Spacing between slides'
      },
      {
        name: 'centerMode',
        def: 'false',
        type: 'Boolean',
        desc: 'Center current slide'
      },
      {
        name: 'dots',
        def: 'false',
        type: 'Boolean',
        desc: 'Slide navigation dots'
      },
      {
        name: 'fade',
        def: 'false',
        type: 'Boolean',
        desc: 'Fading transition (only applicable to 1 slide visible at a time)'
      },
      {
        name: 'infinite',
        def: 'true',
        type: 'Boolean',
        desc: 'Infinitely wrap around contents'
      },
      {
        name: 'initialSlide',
        def: '0',
        type: 'Integer',
        desc: 'Index of first slide'
      },
      {
        name: 'innerRef',
        def: 'undefined',
        type: 'Any',
        desc: 'Ref to Carousel'
      },
      {
        name: 'pauseOnFocus',
        def: 'fas',
        type: 'Boolean',
        desc: 'Prevents autoplay while focused on slides'
      },
      {
        name: 'pauseOnHover',
        def: 'true',
        type: 'Boolean',
        desc: 'Prevents autoplay while hovering on track'
      },
      {
        name: 'responsive',
        def: 'null',
        type: 'Array',
        desc: 'Customize config in objects based on breakpoints'
      },
      {
        name: 'slidesToScroll',
        def: '1',
        type: 'Int',
        desc: 'How many slides to scroll at once'
      },
      {
        name: 'slidesToShow',
        def: '1',
        type: 'Int',
        desc: 'How many slides to show in one frame'
      },
      {
        name: 'speed',
        def: '500',
        type: 'Int',
        desc: 'Animation speed in milliseconds'
      },
      {
        name: 'variableWidth',
        def: 'false',
        type: 'Boolean',
        desc: 'Use slide existing width based on content'
      }
    ]
  }
];

const Eg = [
  {
    title: '# Usage',
    react: `
<Carousel
  autoplay={true}
  autoplaySpeed={1500}
  centerMode={false}
  dots
  cellSpacing={20}
  infinite={true}
  speed={3000}
  slidesToShow={2}
>
  {Array.from(Array(10), (e, i) => {
    return (
      <Box>
        {i + 1}
      </Box>
    );
  })}
</Carousel>
    `
  },
  {
    title: '# Example',
    react: `
<Carousel
  autoplay={false}
  autoplaySpeed={1500}
  centerMode={false}
  centerPadding={0}
  infinite={true}
  fade={false}
  cellSpacing={30}
  speed={1000}
  slidesToShow={3}
>
  {Currencies.map(money => {
    return (
      <Card key={money.name} outline>
        <CardHeader className="px-4">
          <div>
            <Title size="5" className="mb-5">
              {money.name}
            </Title>
            <Subtitle
              size="7"
              className="has-text-secondary text--lighten-2"
            >
              {money.currency}
            </Subtitle>
          </div>
        </CardHeader>
        <CardContent className="has-bg-blue-grey lighten-5 px-4">
          <Columns>
            <Column>
              <Subtitle size="7" className="mb-4">
                Exchange rate
              </Subtitle>
              <Title size="5" className="mb-0">
                {money.rates}
              </Title>
            </Column>
            <Column narrow className="has-text-right">
              <Subtitle size="7" className="mb-4">
                Price change
              </Subtitle>
              <Title size="5" className="mb-0">
                <span
                  className={cn(
                    money.change > 0
                      ? 'has-text-success'
                      : '',
                    money.change < 0 ? 'has-text-danger' : ''
                  )}
                >
                  <Icon small>
                    {money.change > 0
                      ? 'caret-up'
                      : 'caret-down'}
                  </Icon>
                  {money.change}
                  <sup
                    style={{
                      position: 'relative',
                      top: '4px'
                    }}
                  >
                    %
                  </sup>
                </span>
              </Title>
            </Column>
          </Columns>
        </CardContent>
      </Card>
    );
  })}
</Carousel>
    `
  },
  {
    title: '# Play / Pause',
    react: `
...

play() {
  this.slider.slickPlay();
  this.setState({ playing: true });
}
pause() {
  this.slider.slickPause();
  this.setState({ playing: false });
}

...
    
<Carousel
  innerRef={slider => (this.slider = slider)}
  {...settings}
> ... </Carousel>
<div className="has-text-centered">
  <Button onClick={this.play} disabled={playing}>
    Play
  </Button>
  <Button onClick={this.pause} disabled={!playing}>
    Pause
  </Button>
</div>
    `
  }
];

const Currencies = [
  {
    name: 'AUD/USD',
    currency: 'United States Dollar',
    rates: '0.6828',
    change: '0.15'
  },
  {
    name: 'AUD/EUR',
    currency: 'Euro',
    rates: '0.5907',
    change: '-0.24'
  },
  {
    name: 'AUD/GBP',
    currency: 'Great Britain Pound',
    rates: '0.5338',
    change: '0.34'
  },
  {
    name: 'AUD/JPN',
    currency: 'Japanese Yen',
    rates: '73.9748',
    change: '-0.13'
  },
  {
    name: 'AUD/CNY',
    currency: 'Chinese Renmingbi',
    rates: '4.4585',
    change: '0.23'
  },
  {
    name: 'AUD/SGP',
    currency: 'Singapore Dollar',
    rates: '0.9051'
  }
];
