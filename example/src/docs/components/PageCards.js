import React, { Component } from 'react';
import PageHeader from '../../components/PageHeader';
import {
	// Card, CardImage, CardContent, Hero, Subtitle, Title
	Card,
	CardContent,
	Column,
	Columns,
	Container,
	Divider,
	Section,
} from 'shaper-react';
import ExpandingCodeExample from '../../components/ExpandingCodeExample';

export default class Template extends Component {
	render() {
		return (
			<div id="start">
				<PageHeader category="Components" title="Cards">
					An all-around flexible and composable component.
				</PageHeader>

				<Divider />

				<Section>
					<Container>
						<Columns mobile>
							<Column className="is-12-mobile is-10-desktop is-offset-1-desktop nab-content">
								<Card hasLink>
									{/* <CardImage image="/images/testimage.jpg" /> */}
									<CardContent>123</CardContent>
								</Card>

								<p>
									The card component can be used for anything from a panel to a static image. The card component has
									numerous elements to make markup as easy as possible.
								</p>

								<p>The card component comprises several elements that you can mix and match:</p>

								<ul>
									<li>
										<code>.nab-card</code>: the main container
									</li>
									<ul>
										<li>
											<code>.nab-card-header</code>: a horizontal bar with a shadow
										</li>
										<ul>
											<li>
												<code>.nab-card-header-title</code>: a left-aligned bold text
											</li>
											<li>
												<code>.nab-card-header-icon</code>: a placeholder for an icon
											</li>
										</ul>
										<li>
											<code>.nab-card-image</code>: a fullwidth container for a responsive image
										</li>
										<li>
											<code>.nab-card-content</code>: a multi-purpose container for any other element
										</li>
										<li>
											<code>.nab-card-footer</code>: a horizontal list of controls
										</li>
										<ul>
											<li>
												<code>.nab-card-footer-item</code>: a repeatable list item
											</li>
										</ul>
									</ul>
								</ul>

								{Examples.map((eg, e) => {
									return (
										<ExpandingCodeExample
											key={e}
											title={eg.title}
											example={eg.example}
											html={eg.html}
											react={eg.react}
										/>
									);
								})}
							</Column>
						</Columns>
					</Container>
				</Section>
			</div>
		);
	}
}

const Examples = [
	{
		title: '# Usage',
		example: `
<div class="nab-card mb-5">
  <div class="nab-card-content">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
  </div>
</div>

<div class="nab-card has-link">
  <a class="nab-card-header" aria-hidden="true">
    <p class="nab-card-header-title mb-0">
      China's Fosun buys majority stake in French fashion house
    </p>
    <div class="nab-card-header-icon">
      <span class="nab-icon">
        <i class="icon-right"></i>
      </span>
    </div>
  </a>
  <div class="nab-card-content">
    <div class="nab-content is-small">
      <strong>Thomson Reuters</strong> | 22/02/2018 8:19pm AEDT
    </div>
  </div>
</div>
    `,
		html: `
<div class="nab-card mb-5">
  <div class="nab-card-content">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
  </div>
</div>

<div class="nab-card has-link">
  <a class="nab-card-header" aria-hidden="true">
    <p class="nab-card-header-title mb-0">
      China's Fosun buys majority stake in French fashion house
    </p>
    <div class="nab-card-header-icon">
      <span class="nab-icon">
        <i class="icon-right"></i>
      </span>
    </div>
  </a>
  <div class="nab-card-content">
    <div class="nab-content is-small">
      <strong>Thomson Reuters</strong> | 22/02/2018 8:19pm AEDT
    </div>
  </div>
</div>
    `,
		react: `
import React from 'react'
import { Card, CardHeader, CardHeaderTitle, CardHeaderIcon, CardContent, Content, Icon } from '@shaper/react'

const MyComponent = (props) => (
  <Card>
    <CardContent>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
      sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    </CardContent>
  </Card>

  <Card href="http://google.com">
    <CardHeader>
      <CardHeaderTitle>China's Fosun buys majority stake in French fashion house</CardHeaderTitle>
      <CardHeaderIcon>
        <Icon>right</Icon>
      </CardHeaderIcon>
    </CardHeader>
    <CardContent>
      <Content small>
        <strong>Thomson Reuters</strong> | 22/02/2018 8:19pm AEDT
      </Content>
    </CardContent>
  </Card>
)
    `,
	},
	{
		title: 'Grid in card',
		example: `
<div class="nab-card">
  <div class="nab-columns my-0">
    <div class="nab-column">
      <div class="nab-card-content nab-content">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
        <ul>
          <li>Allows investors to understand all the information before making a decision</li>
          <li>Investors can look at all contributing factors to assess a company for current and future value and growth</li>
        </ul>
      </div>
    </div>
    <div class="nab-column">
      <div class="nab-card-content nab-content">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
        <ul>
          <li>Allows investors to understand all the information before making a decision</li>
          <li>Investors can look at all contributing factors to assess a company for current and future value and growth</li>
          <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
        </ul>
      </div>
    </div>
  </div>
</div>
    `,
		html: `
<div class="nab-card">
  <div class="nab-columns my-0">
    <div class="nab-column">
      <div class="nab-card-content nab-content">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
        <ul>
          <li>Allows investors to understand all the information before making a decision</li>
          <li>Investors can look at all contributing factors to assess a company for current and future value and growth</li>
        </ul>
      </div>
    </div>
    <div class="nab-column">
      <div class="nab-card-content nab-content">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
        <ul>
          <li>Allows investors to understand all the information before making a decision</li>
          <li>Investors can look at all contributing factors to assess a company for current and future value and growth</li>
          <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
        </ul>
      </div>
    </div>
  </div>
</div>
    `,
		react: `
const MyComponent = (props) => (
  <Card>
    <Columns>
      <Column>
        <CardContent>
          <Content>
            ...
          </Content>
        </CardContent>
      </Column>
      <Column>
        <CardContent>
          <Content>
            ...
          </Content>
        </CardContent>
      </Column>
    </Columns>
  </Card>
)
    `,
	},
];
