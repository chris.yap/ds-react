import React from 'react';
import PageHeader from '../../components/PageHeader';
import Demo from '../../components/Demo';
import PropsTable from '../../components/PropsTable';
import {
  Button,
  Column,
  Columns,
  Container,
  Divider,
  Message,
  MessageHeader,
  MessageBody,
  Section
} from 'shaper-react';

const Page = () => {
  const [isOpened, setIsOpened] = React.useState(true);
  const [index, updateIndex] = React.useState(0);
  return (
    <div id="start">
      <PageHeader category="components" title="Messages">
        Colored message blocks, to emphasize part of your page.
      </PageHeader>

      <Divider />

      <Section>
        <Container>
          <Columns mobile>
            <Column mobile={12} desktop={10} desktopOffset={1}>
              <Demo examples={Eg} updateIndex={updateIndex}>
                <Button
                  mx={0}
                  mb={4}
                  secondary={!isOpened}
                  onClick={() => setIsOpened(isOpened ? false : true)}>
                  {isOpened ? 'Hide' : 'Show'} message{index > 0 && 's'}
                </Button>
                {index === 0 ? (
                  <Message
                    isOpened={isOpened}
                    onClose={() => setIsOpened(isOpened ? false : true)}>
                    <MessageHeader>Hello World</MessageHeader>
                    <MessageBody>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Pellentesque risus mi, tempus quis placerat ut, porta nec
                      nulla. Vestibulum rhoncus ac ex sit amet fringilla. Nullam
                      gravida purus diam, et dictum felis venenatis efficitur.
                      Aenean ac eleifend lacus, in mollis lectus.{' '}
                      <a href="./">Donec sodales</a>, arcu et sollicitudin
                      porttitor, tortor urna tempor ligula, id porttitor mi
                      magna a neque. Donec dui urna, vehicula et sem eget,
                      facilisis sodales sem.
                    </MessageBody>
                  </Message>
                ) : index === 1 ? (
                  <>
                    <Message isOpened={isOpened} mb={4}>
                      <MessageHeader>Default</MessageHeader>
                      <MessageBody>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.{' '}
                        <strong>Pellentesque</strong> risus mi.{' '}
                        <a href="./">Donec sodales</a>, arcu et sollicitudin.
                        Donec dui urna, vehicula et sem eget, facilisis sodales
                        sem.
                      </MessageBody>
                    </Message>
                    <Message success isOpened={isOpened} mb={4}>
                      <MessageHeader icon="checked-circle">
                        Success
                      </MessageHeader>
                      <MessageBody>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.{' '}
                        <strong>Pellentesque</strong> risus mi.{' '}
                        <a href="./">Donec sodales</a>, arcu et sollicitudin.
                        Donec dui urna, vehicula et sem eget, facilisis sodales
                        sem.
                      </MessageBody>
                    </Message>
                    <Message info isOpened={isOpened} mb={4}>
                      <MessageHeader icon="info-circle">
                        Information
                      </MessageHeader>
                      <MessageBody>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.{' '}
                        <strong>Pellentesque</strong> risus mi.{' '}
                        <a href="./">Donec sodales</a>, arcu et sollicitudin.
                        Donec dui urna, vehicula et sem eget, facilisis sodales
                        sem.
                      </MessageBody>
                    </Message>
                    <Message warning isOpened={isOpened} mb={4}>
                      <MessageHeader icon="exclamation-triangle">
                        Warning
                      </MessageHeader>
                      <MessageBody>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.{' '}
                        <strong>Pellentesque</strong> risus mi.{' '}
                        <a href="./">Donec sodales</a>, arcu et sollicitudin.
                        Donec dui urna, vehicula et sem eget, facilisis sodales
                        sem.
                      </MessageBody>
                    </Message>
                    <Message danger isOpened={isOpened} mb={4}>
                      <MessageHeader icon="exclamation-triangle">
                        Danger
                      </MessageHeader>
                      <MessageBody>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.{' '}
                        <strong>Pellentesque</strong> risus mi.{' '}
                        <a href="./">Donec sodales</a>, arcu et sollicitudin.
                        Donec dui urna, vehicula et sem eget, facilisis sodales
                        sem.
                      </MessageBody>
                    </Message>
                  </>
                ) : (
                  <>
                    <Message isOpened={isOpened} mb={4}>
                      <MessageBody>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.{' '}
                        <strong>Pellentesque</strong> risus mi.{' '}
                        <a href="./">Donec sodales</a>, arcu et sollicitudin.
                        Donec dui urna, vehicula et sem eget, facilisis sodales
                        sem.
                      </MessageBody>
                    </Message>
                    <Message success isOpened={isOpened} mb={4}>
                      <MessageBody>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.{' '}
                        <strong>Pellentesque</strong> risus mi.{' '}
                        <a href="./">Donec sodales</a>, arcu et sollicitudin.
                        Donec dui urna, vehicula et sem eget, facilisis sodales
                        sem.
                      </MessageBody>
                    </Message>
                    <Message info isOpened={isOpened} mb={4}>
                      <MessageBody>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.{' '}
                        <strong>Pellentesque</strong> risus mi.{' '}
                        <a href="./">Donec sodales</a>, arcu et sollicitudin.
                        Donec dui urna, vehicula et sem eget, facilisis sodales
                        sem.
                      </MessageBody>
                    </Message>
                    <Message warning isOpened={isOpened} mb={4}>
                      <MessageBody>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.{' '}
                        <strong>Pellentesque</strong> risus mi.{' '}
                        <a href="./">Donec sodales</a>, arcu et sollicitudin.
                        Donec dui urna, vehicula et sem eget, facilisis sodales
                        sem.
                      </MessageBody>
                    </Message>
                    <Message danger isOpened={isOpened} mb={4}>
                      <MessageBody>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.{' '}
                        <strong>Pellentesque</strong> risus mi.{' '}
                        <a href="./">Donec sodales</a>, arcu et sollicitudin.
                        Donec dui urna, vehicula et sem eget, facilisis sodales
                        sem.
                      </MessageBody>
                    </Message>
                  </>
                )}
              </Demo>
              <PropsTable data={Data} />
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};

export default Page;

const Data = [
  {
    title: 'Message',
    items: [
      {
        name: 'isOpened',
        def: 'false',
        type: 'Boolean',
        desc: 'Controls the visibility of the message'
      },
      {
        name: 'onClose',
        def: 'undefined',
        type: 'Function',
        desc:
          'Close function to the message. If not defined, no close button will be shown.'
      },
      {
        name: `success, <br />
            info, <br />
            warning, <br />
            danger`,
        def: 'undefined',
        type: 'Boolean',
        desc: 'Message styling'
      }
      // { name: '', def: '', type: '', desc: '' },
      // { name: '', def: '', type: '', desc: '' }
    ]
  },
  {
    title: 'MessageHeader',
    items: [
      {
        name: 'icon',
        def: 'undefined',
        type: 'String',
        desc: 'Add icon to header'
      }
    ]
  }
];

const Eg = [
  {
    title: '# Usage',
    react: `
<Message isOpened={isOpened}>
  <MessageHeader>Hello World</MessageHeader>
  <MessageBody>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    Pellentesque risus mi, tempus quis placerat ut, porta nec
    nulla. Vestibulum rhoncus ac ex sit amet fringilla. Nullam
    gravida purus diam, et dictum felis venenatis efficitur.
    Aenean ac eleifend lacus, in mollis lectus.{' '}
    <a href="./">Donec sodales</a>, arcu et sollicitudin
    porttitor, tortor urna tempor ligula, id porttitor mi
    magna a neque. Donec dui urna, vehicula et sem eget,
    facilisis sodales sem.
  </MessageBody>
</Message>
`
  },
  {
    title: '# Colours',
    react: `
<Message isOpened={isOpened} mb={4}>
  <MessageHeader>Default</MessageHeader>
  <MessageBody>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.{' '}
    <strong>Pellentesque</strong> risus mi.{' '}
    <a href="./">Donec sodales</a>, arcu et sollicitudin.
    Donec dui urna, vehicula et sem eget, facilisis sodales
    sem.
  </MessageBody>
</Message>
<Message success isOpened={isOpened} mb={4}>
  <MessageHeader icon="checked-circle">
    Success
  </MessageHeader>
  <MessageBody>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.{' '}
    <strong>Pellentesque</strong> risus mi.{' '}
    <a href="./">Donec sodales</a>, arcu et sollicitudin.
    Donec dui urna, vehicula et sem eget, facilisis sodales
    sem.
  </MessageBody>
</Message>
<Message info isOpened={isOpened} mb={4}>
  <MessageHeader icon="info-circle">
    Information
  </MessageHeader>
  <MessageBody>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.{' '}
    <strong>Pellentesque</strong> risus mi.{' '}
    <a href="./">Donec sodales</a>, arcu et sollicitudin.
    Donec dui urna, vehicula et sem eget, facilisis sodales
    sem.
  </MessageBody>
</Message>
<Message warning isOpened={isOpened} mb={4}>
  <MessageHeader icon="exclamation-triangle">
    Warning
  </MessageHeader>
  <MessageBody>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.{' '}
    <strong>Pellentesque</strong> risus mi.{' '}
    <a href="./">Donec sodales</a>, arcu et sollicitudin.
    Donec dui urna, vehicula et sem eget, facilisis sodales
    sem.
  </MessageBody>
</Message>
<Message danger isOpened={isOpened} mb={4}>
  <MessageHeader icon="exclamation-triangle">
    Danger
  </MessageHeader>
  <MessageBody>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.{' '}
    <strong>Pellentesque</strong> risus mi.{' '}
    <a href="./">Donec sodales</a>, arcu et sollicitudin.
    Donec dui urna, vehicula et sem eget, facilisis sodales
    sem.
  </MessageBody>
</Message>
    `
  },
  {
    title: '# Body only',
    react: `
<Message isOpened={isOpened} mb={4}>
  <MessageBody>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.{' '}
    <strong>Pellentesque</strong> risus mi.{' '}
    <a href="./">Donec sodales</a>, arcu et sollicitudin.
    Donec dui urna, vehicula et sem eget, facilisis sodales
    sem.
  </MessageBody>
</Message>
<Message success isOpened={isOpened} mb={4}>
  <MessageBody>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.{' '}
    <strong>Pellentesque</strong> risus mi.{' '}
    <a href="./">Donec sodales</a>, arcu et sollicitudin.
    Donec dui urna, vehicula et sem eget, facilisis sodales
    sem.
  </MessageBody>
</Message>
<Message info isOpened={isOpened} mb={4}>
  <MessageBody>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.{' '}
    <strong>Pellentesque</strong> risus mi.{' '}
    <a href="./">Donec sodales</a>, arcu et sollicitudin.
    Donec dui urna, vehicula et sem eget, facilisis sodales
    sem.
  </MessageBody>
</Message>
<Message warning isOpened={isOpened} mb={4}>
  <MessageBody>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.{' '}
    <strong>Pellentesque</strong> risus mi.{' '}
    <a href="./">Donec sodales</a>, arcu et sollicitudin.
    Donec dui urna, vehicula et sem eget, facilisis sodales
    sem.
  </MessageBody>
</Message>
<Message danger isOpened={isOpened} mb={4}>
  <MessageBody>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.{' '}
    <strong>Pellentesque</strong> risus mi.{' '}
    <a href="./">Donec sodales</a>, arcu et sollicitudin.
    Donec dui urna, vehicula et sem eget, facilisis sodales
    sem.
  </MessageBody>
</Message>
    `
  }
];
