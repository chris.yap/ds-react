import React from 'react';
import PageHeader from '../../components/PageHeader';
import Demo from '../../components/Demo';
import PropsTable from '../../components/PropsTable';
import {
  Button,
  Collapse,
  Column,
  Columns,
  Container,
  Divider,
  Icon,
  Section,
  Tag
} from 'shaper-react';

const PageIcon = () => {
  const [isOpened, updateIsOpened] = React.useState(false);
  return (
    <div id="start">
      <PageHeader category="components" title="Collapse">
        Expandible/Collapsible panel <br />
        <Tag small>styled-component</Tag>
      </PageHeader>

      <Divider />

      <Section>
        <Container>
          <Columns mobile>
            <Column mobile={12} desktop={10} desktopOffset={1}>
              <Demo examples={Eg}>
                <Button
                  secondary={!isOpened}
                  className="ma-0"
                  onClick={() => updateIsOpened(isOpened ? false : true)}>
                  <span>{isOpened ? 'Hide info' : 'Show more info'}</span>
                  <Icon>{isOpened ? 'arrow-up' : 'arrow-down'}</Icon>
                </Button>
                <Collapse isOpened={isOpened} mt={isOpened ? 4 : 0}>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Fusce pharetra consectetur turpis, eu dignissim eros posuere
                    vitae. Maecenas neque nisl, rutrum vel elit feugiat,
                    consequat interdum erat. Maecenas lobortis gravida enim, a
                    mattis est auctor eu. Ut sollicitudin erat ipsum, faucibus
                    commodo elit porttitor ac.{' '}
                  </p>
                </Collapse>
              </Demo>
              <PropsTable data={Data} />
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};
export default PageIcon;

const Data = [
  {
    items: [
      {
        name: 'isOpened',
        def: 'false',
        type: 'Boolean',
        desc: 'Controls component visibility'
      },
      {
        name: 'duration',
        def: '300',
        type: 'Number',
        desc: 'Animation duration (in ms)'
      }
    ]
  }
];

const Eg = [
  {
    title: '# Usage',
    react: `
<Button
  secondary={!isOpened}
  className="ma-0"
  onClick={() => updateIsOpened(isOpened ? false : true)}
>
  <span>{isOpened ? 'Hide info' : 'Show more info'}</span>
  <Icon>{isOpened ? 'arrow-up' : 'arrow-down'}</Icon>
</Button>

<Collapse isOpened={isOpened} mt={isOpened ? 4 : 0}>
  <p>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    Fusce pharetra consectetur turpis, eu dignissim eros posuere
    vitae. Maecenas neque nisl, rutrum vel elit feugiat,
    consequat interdum erat. Maecenas lobortis gravida enim, a
    mattis est auctor eu. Ut sollicitudin erat ipsum, faucibus
    commodo elit porttitor ac.{' '}
  </p>
</Collapse>
    `
  }
];
