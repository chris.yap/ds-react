import React from 'react';
import PageHeader from '../../components/PageHeader';
import Demo from '../../components/Demo';
import PropsTable from '../../components/PropsTable';
import {
  Alert,
  Button,
  Checkbox,
  CheckboxGroup,
  Column,
  Columns,
  Container,
  Divider,
  Icon,
  Radio,
  RadioGroup,
  Section,
  Tag,
  Title
} from 'shaper-react';

const Page = () => {
  const [index, updateIndex] = React.useState(0);
  const [visible, updateVisible] = React.useState(false);
  const [context, updateContext] = React.useState('Default');
  const [inverted, updateInverted] = React.useState(false);
  const [closeButton, updateCloseButton] = React.useState(false);
  // const [errorMessage, updateErrorMessage] = React.useState(0);

  let toggleAlert = () => {
    updateVisible(visible ? false : true);
    // updateErrorMessage(generateNumber(1, 1000));
  };
  // let generateNumber = (min, max) => {
  //   const rndNum = Math.floor(Math.random() * (max - min + 1) + min);
  //   return rndNum;
  // };
  return (
    <div id="start">
      <PageHeader category="components" title="Alerts">
        Bold blocks, to alert/notify your users of something. <br />
        <Tag small>styled-components</Tag>
      </PageHeader>

      <Divider />
      <Section bg="blacks.0">
        <Container>
          <Columns mobile>
            <Column className="is-12-mobile is-10-desktop is-offset-1-desktop">
              <Title size="4">Playground</Title>
              <Alert
                id="demo"
                moreInfo="Loremipsumdolorsitamet,consecteturadipisicingelit.Cumque,magnimolestiaeearumsaepearchitectosuscipitrepudiandaeoptio#Loremipsumdolorsitamet,consecteturadipisicingelit.Cumque,magnimolestiaeearumsaepearchitectosuscipitrepudiandaeoptio."
                inverted={inverted}
                onClose={closeButton ? () => toggleAlert() : false}
                isOpened={visible}
                success={context === 'Success'}
                info={context === 'Info'}
                warning={context === 'Warning'}
                danger={context === 'Danger'}>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Cumque, magni molestiae earum saepe architecto suscipit
                  repudiandae optio. <a href="/#">Quidem doloremque</a>
                </p>
              </Alert>
              <Columns>
                <Column narrow>
                  <RadioGroup
                    buttons
                    name="context"
                    onChange={e => updateContext(e)}
                    selectedValue={context}>
                    <Radio button label="Default" value="Default"></Radio>
                    <Radio button label="Success" value="Success"></Radio>
                    <Radio button label="Info" value="Info"></Radio>
                    <Radio button label="Warning" value="Warning"></Radio>
                    <Radio button label="Danger" value="Danger"></Radio>
                  </RadioGroup>
                </Column>
                <Column narrow>
                  <CheckboxGroup
                    buttons
                    name="inverted"
                    onChange={() => updateInverted(inverted ? false : true)}
                    selectedValue={inverted}>
                    <Checkbox button label="Inverted" value={true} />
                  </CheckboxGroup>
                </Column>
                <Column narrow>
                  <CheckboxGroup
                    buttons
                    name="close"
                    onChange={() =>
                      updateCloseButton(closeButton ? false : true)
                    }
                    selectedValue={closeButton}>
                    <Checkbox button label="Close button" value={true} />
                  </CheckboxGroup>
                </Column>
                <Column className="has-text-right">
                  <Button
                    success={!visible}
                    danger={visible}
                    className="ma-0"
                    onClick={() => toggleAlert()}>
                    <span>{!visible ? 'Trigger' : 'Close'} alert</span>
                    <Icon>{!visible ? 'arrow-right' : 'close'}</Icon>
                  </Button>
                </Column>
              </Columns>
            </Column>
          </Columns>
        </Container>
      </Section>
      <Divider />

      <Section>
        <Container>
          <Columns mobile>
            <Column className="is-12-mobile is-10-desktop is-offset-1-desktop">
              <Demo examples={Eg} updateIndex={updateIndex}>
                {index === 0 ? (
                  <>
                    <p>Fly-in Alert style</p>
                    {Eg1.map((item, i) => {
                      let visible = true;
                      return (
                        <Alert
                          key={i}
                          demo
                          onClose={item.dismissible}
                          info={item.info}
                          success={item.success}
                          warning={item.warning}
                          danger={item.danger}
                          isOpened={visible === true}>
                          <strong>{item.msg}</strong>
                        </Alert>
                      );
                    })}
                  </>
                ) : index === 1 ? (
                  <>
                    {Eg2.map((item, i) => {
                      let visible = true;
                      return (
                        <Alert
                          demo
                          key={i}
                          onClose={item.dismissible}
                          icon={item.icon}
                          info={item.info}
                          success={item.success}
                          warning={item.warning}
                          danger={item.danger}
                          isOpened={visible === true}>
                          <strong>{item.msg}</strong>
                        </Alert>
                      );
                    })}
                  </>
                ) : index === 2 ? (
                  <>
                    <p>
                      These are usually to display alerts/notifications within
                      the context of where it is used. <br />
                      <em>
                        For example, at the top of a form if the form cannot be
                        submitted.
                      </em>
                    </p>
                    {Eg3.map((item, i) => {
                      let show = true;
                      return (
                        <Alert
                          key={i}
                          onClose={item.dismissible}
                          info={item.info}
                          success={item.success}
                          warning={item.warning}
                          danger={item.danger}
                          inverted={item.inverted}
                          isOpened={show}>
                          <strong>{item.msg}</strong>
                        </Alert>
                      );
                    })}
                  </>
                ) : (
                  <React.Fragment>
                    <Alert
                      inverted
                      moreInfo="Loremipsumdolorsitamet,consecteturadipisicingelit.Cumque,magnimolestiaeearumsaepearchitectosuscipitrepudiandaeoptio#Loremipsumdolorsitamet,consecteturadipisicingelit.Cumque,magnimolestiaeearumsaepearchitectosuscipitrepudiandaeoptio."
                      isOpened={true}>
                      <p>
                        <strong>
                          This is a default alert. <a href="/#">Click here</a>{' '}
                          to URL.
                        </strong>
                      </p>
                    </Alert>
                    <Alert
                      info
                      inverted
                      moreInfo="Loremipsumdolorsitamet,consecteturadipisicingelit.Cumque,magnimolestiaeearumsaepearchitectosuscipitrepudiandaeoptio#Loremipsumdolorsitamet,consecteturadipisicingelit.Cumque,magnimolestiaeearumsaepearchitectosuscipitrepudiandaeoptio."
                      isOpened={true}>
                      <p>
                        <strong>
                          This is an info alert. <a href="/#">Click here</a> to
                          URL.
                        </strong>
                      </p>
                    </Alert>
                  </React.Fragment>
                )}
              </Demo>
              <PropsTable data={Data} />
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};

export default Page;

const Data = [
  {
    items: [
      {
        name: 'isOpened',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Sets visibility'
      },
      {
        name: 'onClose',
        def: 'undefined',
        type: 'Function',
        desc: 'Close function for close button, also make close button visible '
      },
      {
        name: 'icon',
        def: 'undefined',
        type: 'String',
        desc: 'Set custom icon if needed'
      },
      {
        name: 'inverted',
        def: 'undefined',
        type: 'Boolean',
        desc:
          'Use for inline alert messages. If not set, it will be set to "fly-in" by default'
      },
      {
        name: `success, <br />
            info, <br />
            warning, <br />
            danger`,
        def: 'undefined',
        type: 'Boolean',
        desc: 'Set alert styles'
      },
      {
        name: 'moreInfo',
        def: 'undefined',
        type: 'String',
        desc: 'For adding error code (Collapsible)'
      }
    ]
  }
];

const Eg = [
  {
    title: '# Usage',
    react: `
<Alert onClose={Func} isOpened={true}>
  <strong>This is a default alert.</strong>
</Alert>
<Alert info onClose={Func} isOpened={true}>
  <strong>This is an info alert.</strong>
</Alert>
<Alert success onClose={Func} isOpened={true}>
  <strong>This is a success alert.</strong>
</Alert>
<Alert warning onClose={Func} isOpened={true}>
  <strong>This is a warning alert.</strong>
</Alert>
<Alert danger onClose={Func} isOpened={true}>
  <strong>This is a danger alert.</strong>
</Alert>
    `
  },
  {
    title: '# Without icon / close button / Custom icon',
    react: `
<Alert icon onClose={Func} isOpened={true}>
  <strong>This is a default alert without icon.</strong>
</Alert>
<Alert info isOpened={true}>
  <strong>This is an info alert without the close button.</strong>
</Alert>
<Alert icon="bin" danger isOpened={true}>
  <strong>This is an danger alert with custom icon.</strong>
</Alert>
    `
  },
  {
    title: '# Inverted',
    react: `
<Alert inverted onClose={Func} isOpened={true}>
  <strong>This is a default alert. <a href="/#">Click here</a> to URL.</strong>
</Alert>
<Alert info inverted onClose={Func} isOpened={true}>
  <strong>This is an info alert. <a href="/#">Click here</a> to URL.</strong>
</Alert>
<Alert success inverted onClose={Func} isOpened={true}>
  <strong>This is a success alert. <a href="/#">Click here</a> to URL.</strong>
</Alert>
<Alert warning inverted onClose={Func} isOpened={true}>
  <strong>This is a warning alert. <a href="/#">Click here</a> to URL.</strong>
</Alert>
<Alert danger inverted onClose={Func} isOpened={true}>
  <strong>This is a danger alert. <a href="/#">Click here</a> to URL.</strong>
</Alert>
    `
  },
  {
    title: '# More info - Collapsible',
    react: `
<Alert
  inverted
  moreInfo="Loremipsumdolorsitamet,consecteturadipisicingelit.Cumque,magnimolestiaeearumsaepearchitectosuscipitrepudiandaeoptio#Loremipsumdolorsitamet,consecteturadipisicingelit.Cumque,magnimolestiaeearumsaepearchitectosuscipitrepudiandaeoptio."
  isOpened={true}>
  <p>
    <strong>
      This is a default alert. <a href="/#">Click here</a>{' '}
      to URL.
    </strong>
  </p>
</Alert>
<Alert
  info
  inverted
  moreInfo="Loremipsumdolorsitamet,consecteturadipisicingelit.Cumque,magnimolestiaeearumsaepearchitectosuscipitrepudiandaeoptio#Loremipsumdolorsitamet,consecteturadipisicingelit.Cumque,magnimolestiaeearumsaepearchitectosuscipitrepudiandaeoptio."
  isOpened={true}>
  <p>
    <strong>
      This is an info alert. <a href="/#">Click here</a> to
      URL.
    </strong>
  </p>
</Alert>
`
  }
];

const Eg1 = [
  { msg: 'This is a default alert', dismissible: function() {} },
  { msg: 'This is an info alert', dismissible: function() {}, info: true },
  { msg: 'This is a success alert', dismissible: function() {}, success: true },
  { msg: 'This is a warning alert', dismissible: function() {}, warning: true },
  { msg: 'This is a danger alert', dismissible: function() {}, danger: true }
];

const Eg2 = [
  {
    msg: 'This is a default alert without icon',
    icon: true,
    dismissible: function() {}
  },
  { msg: 'This is an info alert without the close button', info: true },
  { msg: 'This is a danger alert with custom icon', icon: 'bin', danger: true }
];

const Eg3 = [
  {
    msg: (
      <React.Fragment>
        This is a default alert. <a href="/#">Click here</a> to URL.{' '}
      </React.Fragment>
    ),
    dismissible: function() {},
    inverted: true
  },
  {
    msg: (
      <React.Fragment>
        This is an info alert. <a href="/#">Click here</a> to URL.
      </React.Fragment>
    ),
    dismissible: function() {},
    info: true,
    inverted: true
  },
  {
    msg: (
      <React.Fragment>
        This is a success alert. <a href="/#">Click here</a> to URL.
      </React.Fragment>
    ),
    dismissible: function() {},
    success: true,
    inverted: true
  },
  {
    msg: (
      <React.Fragment>
        This is a warning alert. <a href="/#">Click here</a> to URL.
      </React.Fragment>
    ),
    dismissible: function() {},
    warning: true,
    inverted: true
  },
  {
    msg: (
      <React.Fragment>
        This is a danger alert. <a href="/#">Click here</a> to URL.
      </React.Fragment>
    ),
    dismissible: function() {},
    danger: true,
    inverted: true
  }
];
