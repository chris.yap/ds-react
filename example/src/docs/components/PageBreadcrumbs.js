import React from 'react';
import PageHeader from '../../components/PageHeader';
import Demo from '../../components/Demo';
import PropsTable from '../../components/PropsTable';
import {
  Breadcrumb,
  BreadcrumbItem,
  Column,
  Columns,
  Container,
  Divider,
  Section,
  Tag
} from 'shaper-react';

const Page = () => {
  const [index, updateIndex] = React.useState(0);
  return (
    <div id="start">
      <PageHeader category="components" title="Breadcrumbs">
        A navigational helper for pages. <br />
        <Tag small>styled-component</Tag>
      </PageHeader>

      <Divider />

      <Section>
        <Container>
          <Columns mobile>
            <Column className="is-12-mobile is-10-desktop is-offset-1-desktop">
              <Demo examples={Examples} updateIndex={updateIndex}>
                {index === 0 ? (
                  <Breadcrumb>
                    <BreadcrumbItem href="/">Shaper</BreadcrumbItem>
                    <BreadcrumbItem href="/layout/heroes">
                      Heroes
                    </BreadcrumbItem>
                    <BreadcrumbItem href="/elements/buttons">
                      Buttons
                    </BreadcrumbItem>
                    <BreadcrumbItem href="#" active>
                      Breadcrumb
                    </BreadcrumbItem>
                  </Breadcrumb>
                ) : index === 1 ? (
                  <>
                    <Breadcrumb centered>
                      <BreadcrumbItem href="/">Shaper</BreadcrumbItem>
                      <BreadcrumbItem href="/layout/heroes">
                        Heroes
                      </BreadcrumbItem>
                      <BreadcrumbItem href="/elements/buttons">
                        Buttons
                      </BreadcrumbItem>
                      <BreadcrumbItem href="#" active>
                        Breadcrumb
                      </BreadcrumbItem>
                    </Breadcrumb>
                    <Breadcrumb right>
                      <BreadcrumbItem href="/">Shaper</BreadcrumbItem>
                      <BreadcrumbItem href="/layout/heroes">
                        Heroes
                      </BreadcrumbItem>
                      <BreadcrumbItem href="/elements/buttons">
                        Buttons
                      </BreadcrumbItem>
                      <BreadcrumbItem href="#" active>
                        Breadcrumb
                      </BreadcrumbItem>
                    </Breadcrumb>
                  </>
                ) : (
                  <>
                    <Breadcrumb small>
                      <BreadcrumbItem href="/">Shaper</BreadcrumbItem>
                      <BreadcrumbItem href="/layout/heroes">
                        Heroes
                      </BreadcrumbItem>
                      <BreadcrumbItem href="/elements/buttons">
                        Buttons
                      </BreadcrumbItem>
                      <BreadcrumbItem href="#" active>
                        Breadcrumb
                      </BreadcrumbItem>
                    </Breadcrumb>

                    <Breadcrumb>
                      <BreadcrumbItem href="/">Shaper</BreadcrumbItem>
                      <BreadcrumbItem href="/layout/heroes">
                        Heroes
                      </BreadcrumbItem>
                      <BreadcrumbItem href="/elements/buttons">
                        Buttons
                      </BreadcrumbItem>
                      <BreadcrumbItem href="#" active>
                        Breadcrumb
                      </BreadcrumbItem>
                    </Breadcrumb>

                    <Breadcrumb medium>
                      <BreadcrumbItem href="/">Shaper</BreadcrumbItem>
                      <BreadcrumbItem href="/layout/heroes">
                        Heroes
                      </BreadcrumbItem>
                      <BreadcrumbItem href="/elements/buttons">
                        Buttons
                      </BreadcrumbItem>
                      <BreadcrumbItem href="#" active>
                        Breadcrumb
                      </BreadcrumbItem>
                    </Breadcrumb>

                    <Breadcrumb large>
                      <BreadcrumbItem href="/">Shaper</BreadcrumbItem>
                      <BreadcrumbItem href="/layout/heroes">
                        Heroes
                      </BreadcrumbItem>
                      <BreadcrumbItem href="/elements/buttons">
                        Buttons
                      </BreadcrumbItem>
                      <BreadcrumbItem href="#" active>
                        Breadcrumb
                      </BreadcrumbItem>
                    </Breadcrumb>
                  </>
                )}
              </Demo>
              <PropsTable data={Data} />
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};
export default Page;

const Data = [
  {
    title: 'Breadcrumb',
    items: [
      {
        name: `centered, <br />
            right`,
        def: 'undefined',
        type: 'Boolean',
        desc: 'Breadcrumb alignment'
      },
      {
        name: `small, <br />
            medium, <br />
            large`,
        def: 'undefined',
        type: 'Boolean',
        desc: 'Set breadcrumb size'
      },
      { name: 'divider', def: '>', type: 'string', desc: "Set link's divider" }
    ]
  },
  {
    title: 'BreadcrumbItem',
    items: [
      {
        name: 'active',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Set active link'
      },
      {
        name: 'href',
        def: 'undefined',
        type: 'String',
        desc: 'Link'
      }
    ]
  }
];

const Examples = [
  {
    title: '# Usage',
    react: `
<Breadcrumb>
  <BreadcrumbItem href="/">Shaper</BreadcrumbItem>
  <BreadcrumbItem href="/layout/heroes">Heroes</BreadcrumbItem>
  <BreadcrumbItem href="/elements/buttons">Buttons</BreadcrumbItem>
  <BreadcrumbItem href="#" active>
    Breadcrumb
  </BreadcrumbItem>
</Breadcrumb>
    `
  },
  {
    title: '# Alignment',
    react: `
<Breadcrumb centered>
  <BreadcrumbItem href="#">Shaper</BreadcrumbItem>
  <BreadcrumbItem href="#">Documentation</BreadcrumbItem>
  <BreadcrumbItem href="#">Components</BreadcrumbItem>
  <BreadcrumbItem href="#" active>
    Breadcrumb
  </BreadcrumbItem>
</Breadcrumb>

<Breadcrumb right>
  <BreadcrumbItem href="#">Shaper</BreadcrumbItem>
  <BreadcrumbItem href="#">Documentation</BreadcrumbItem>
  <BreadcrumbItem href="#">Components</BreadcrumbItem>
  <BreadcrumbItem href="#" active>
    Breadcrumb
  </BreadcrumbItem>
</Breadcrumb>
    `
  },
  {
    title: '# Sizes',
    react: `
<Breadcrumb small>
  <BreadcrumbItem href="#">Shaper</BreadcrumbItem>
  <BreadcrumbItem href="#">Documentation</BreadcrumbItem>
  <BreadcrumbItem href="#">Components</BreadcrumbItem>
  <BreadcrumbItem href="#" active>
    Breadcrumb
  </BreadcrumbItem>
</Breadcrumb>

<Breadcrumb>
  <BreadcrumbItem href="#">Shaper</BreadcrumbItem>
  <BreadcrumbItem href="#">Documentation</BreadcrumbItem>
  <BreadcrumbItem href="#">Components</BreadcrumbItem>
  <BreadcrumbItem href="#" active>
    Breadcrumb
  </BreadcrumbItem>
</Breadcrumb>

<Breadcrumb medium>
  <BreadcrumbItem href="#">Shaper</BreadcrumbItem>
  <BreadcrumbItem href="#">Documentation</BreadcrumbItem>
  <BreadcrumbItem href="#">Components</BreadcrumbItem>
  <BreadcrumbItem href="#" active>
    Breadcrumb
  </BreadcrumbItem>
</Breadcrumb>

<Breadcrumb large>
  <BreadcrumbItem href="#">Shaper</BreadcrumbItem>
  <BreadcrumbItem href="#">Documentation</BreadcrumbItem>
  <BreadcrumbItem href="#">Components</BreadcrumbItem>
  <BreadcrumbItem href="#" active>
    Breadcrumb
  </BreadcrumbItem>
</Breadcrumb>
    `
  }
];
