import React from 'react';
import PageHeader from '../../components/PageHeader';
import Demo from '../../components/Demo';
import PropsTable from '../../components/PropsTable';
import {
  Card,
  Column,
  Columns,
  Container,
  Divider,
  List,
  ListItem,
  Section,
  Tag
} from 'shaper-react';

const Page = () => {
  const [index, updateIndex] = React.useState(0);
  return (
    <div id="start">
      <PageHeader category="component" title="Lists">
        A simple list. <br />
        <Tag small>styled-component</Tag>
      </PageHeader>

      <Divider />

      <Section>
        <Container>
          <Columns mobile>
            <Column mobile={12} desktop={10} desktopOffset={1}>
              <Demo examples={Eg} updateIndex={updateIndex}>
                {index === 0 && (
                  <Card>
                    <List>
                      <ListItem>List item</ListItem>
                      <ListItem id="id-123">List item with ID</ListItem>
                      <ListItem href="!#">Link</ListItem>
                      <ListItem
                        href="http://www.google.com"
                        target="_blank"
                        linkType="external">
                        External Link (Google.com)
                      </ListItem>
                      <ListItem href="!#" disabled>
                        Disabled Link
                      </ListItem>
                      <List bg="#293b49">
                        <ListItem href="!#" dark>
                          Dark list
                        </ListItem>
                      </List>
                    </List>
                  </Card>
                )}
              </Demo>
              <PropsTable data={Data} />
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};

export default Page;

const Data = [
  {
    title: 'ListItem',
    items: [
      {
        name: 'href',
        def: 'undefined',
        type: 'String',
        desc:
          'To other pages or set prop "linkType" to "external" if external link required.'
      },
      {
        name: `isDisabled`,
        def: 'undefined',
        type: 'Boolean',
        desc: ''
      },
      { name: 'onClick', def: 'undefined', type: 'function', desc: '' },
      {
        name: 'linkType',
        def: 'undefined',
        type: 'String',
        desc: 'Set external if link is not to other pages within app.'
      },
      {
        name: 'target',
        def: 'undefined',
        type: 'String',
        desc: 'Link target attribute'
      },
      {
        name: 'dark',
        def: 'undefined',
        type: 'Boolean',
        desc: 'Dark theme'
      }
    ]
  },
  {}
  // { title: 'List', items: [] }
];

const Eg = [
  {
    title: '# Usage',
    react: `
<Card>
  <List>
    <ListItem>List item</ListItem>
    <ListItem id="id-123">List item with ID</ListItem>
    <ListItem href="!#">Link</ListItem>
    <ListItem
      href="http://www.google.com"
      target="_blank"
      linkType="external">
      External Link (Google.com)
    </ListItem>
    <ListItem href="!#" disabled>
      Disabled Link
    </ListItem>
  </List>
  <List bg="#293b49">
    <ListItem href="!#" dark>
      Dark list
    </ListItem>
  </List>
</Card>
    `
  }
];
