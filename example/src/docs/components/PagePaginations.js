import React, { Component } from 'react';
import PageHeader from '../../components/PageHeader';
import {
  Column,
  Columns,
  Container,
  Divider,
  Pagination,
  Section,
  Table
} from 'shaper-react';
import ExpandingCodeExample from '../../components/ExpandingCodeExample';
import { PropsHeaders, PropsTableRow } from '../../constants/PropsTables';

export default class PagePaginations extends Component {
  constructor(props) {
    super(props);
    this.state = {
      current: 1,
      total: 22
    };
    this.pageChange = this.pageChange.bind(this);
    this.changeCurrent = this.changeCurrent.bind(this);
    this.changeTotal = this.changeTotal.bind(this);
  }
  pageChange = (current, total) => {
    // console.log(current, total);
    this.setState({
      current: current,
      total: total
    });
  };
  changeCurrent = e => {
    this.setState({
      current: e.target.value
    });
  };
  changeTotal = e => {
    this.setState({
      total: e.target.value
    });
  };
  render() {
    return (
      <div id="start">
        <PageHeader category="Components" title="Paginations">
          A responsive, usable, and flexible pagination.
        </PageHeader>

        <Divider />

        <Section>
          <Container>
            <Columns mobile>
              <Column mobile={12} desktop={10} desktopOffset={1}>
                {/* <Columns>
									<Column mobile>
										<Textfield label="Current page" value={this.state.current} onChange={this.changeCurrent} />
									</Column>
									<Column>
										<Textfield label="Total page" value={this.state.total} onChange={this.changeTotal} />
									</Column>
								</Columns> */}

                <ExpandingCodeExample
                  title={Eg[0].title}
                  react={Eg[0].react}
                  html={Eg[0].html}>
                  <Pagination
                    current={this.state.current}
                    total={this.state.total}
                    onChange={this.pageChange}
                  />
                </ExpandingCodeExample>

                <Table
                  data={Data}
                  striped
                  headers={PropsHeaders}
                  tableRow={PropsTableRow}
                  defaultSortCol="name"
                />

                <ExpandingCodeExample
                  title={Eg[1].title}
                  react={Eg[1].react}
                  html={Eg[1].html}>
                  <Pagination
                    current={this.state.current}
                    total={this.state.total}
                    onChange={this.pageChange}
                    small
                  />
                  <Pagination
                    current={this.state.current}
                    total={this.state.total}
                    onChange={this.pageChange}
                  />
                  <Pagination
                    current={this.state.current}
                    total={this.state.total}
                    onChange={this.pageChange}
                    medium
                  />
                  <Pagination
                    current={this.state.current}
                    total={this.state.total}
                    onChange={this.pageChange}
                    large
                  />
                </ExpandingCodeExample>
              </Column>
            </Columns>
          </Container>
        </Section>
      </div>
    );
  }
}

const Data = [
  {
    name: 'current',
    def: '1',
    type: 'Number',
    desc: 'Current selected page'
  },
  {
    name: 'total',
    def: 'undefined',
    type: 'Number',
    desc: 'Specify the max total visible pagination numbers'
  },
  {
    name: 'small, medium, large',
    def: 'undefined',
    type: 'Boolean',
    desc: 'Size of pagination'
  }
];

const Eg = [
  {
    title: '# Usage',
    react: `
<Pagination current={this.state.current} total={this.state.total} onChange={this.pageChange} />
		`,
    html: ``
  },
  {
    title: '# Sizes',
    react: `
<Pagination current={this.state.current} total={this.state.total} onChange={this.pageChange} small />
<Pagination current={this.state.current} total={this.state.total} onChange={this.pageChange} />
<Pagination current={this.state.current} total={this.state.total} onChange={this.pageChange} medium />
<Pagination current={this.state.current} total={this.state.total} onChange={this.pageChange} large />
									`,
    html: ``
  }
];
