import React, { Component } from 'react';
import PageHeader from '../../components/PageHeader';
import {
	Column,
	Columns,
	Container,
	Divider,
	Section,
	// Toolbar, ToolbarNav, ToolbarNavItem, Subtitle, Title, Hero,
} from 'shaper-react';
import ExpandingCodeExample from '../../components/ExpandingCodeExample';

export default class Template extends Component {
	render() {
		return (
			<div id="start">
				<PageHeader category="Components" title="Toolbars">
					A responsive horizontal <strong>toolbar</strong> that can support images, links, buttons, and dropdowns
				</PageHeader>

				<Divider />

				<Section>
					<Container>
						<Columns mobile>
							<Column className="is-12-mobile is-10-desktop is-offset-1-desktop">
								{/* <Toolbar dark fluid className="has-bg-black"
                  image="/images/nabtrade-logo-rev.png">
                  <ToolbarNav left>
                    <ToolbarNavItem href="link">Link</ToolbarNavItem>
                    <ToolbarNavItem href="link">Link</ToolbarNavItem>
                    <ToolbarNavItem href="link">Link</ToolbarNavItem>
                  </ToolbarNav>
                  <ToolbarNav right>
                    <ToolbarNavItem href="link">Link</ToolbarNavItem>
                    <ToolbarNavItem href="link">Link</ToolbarNavItem>
                    <ToolbarNavItem href="link">Link</ToolbarNavItem>
                  </ToolbarNav>
                </Toolbar> */}

								{Examples.map((eg, e) => {
									return (
										<ExpandingCodeExample
											key={e}
											title={eg.title}
											example={eg.example}
											html={eg.html}
											react={eg.react}
										/>
									);
								})}
							</Column>
						</Columns>
					</Container>
				</Section>
			</div>
		);
	}
}

const Examples = [
	{
		title: '# Usage',
		example: `
<nav class="nab-toolbar has-bg-black lighten-5 mb-3 has-elevation-1" role="navigation" aria-label="main navigation">
  <div class="nab-toolbar-brand">
    <a class="nab-toolbar-item" href="#">
      <strong>Brand</strong>
    </a>
    <a role="button" class="nab-toolbar-burger burger is-absolute" aria-label="menu" aria-expanded="false" data-target="toolbarBasicExample">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>
  <div id="toolbarBasicExample" class="nab-toolbar-menu">
    <div class="nab-toolbar-start">
      <a class="nab-toolbar-item">Link</a>
      <a class="nab-toolbar-item">Link</a>
      <a class="nab-toolbar-item">Link</a>
    </div>
    <div class="nab-toolbar-end">
      <a class="nab-toolbar-item">Link</a>
      <a class="nab-toolbar-item">Link</a>
    </div>
  </div>
</nav>

<nav class="nab-toolbar is-dark has-bg-black" role="navigation" aria-label="main navigation">
  <div class="nab-toolbar-brand">
    <a class="nab-toolbar-item" href="#">
      <strong>Brand</strong>
    </a>
    <a role="button" class="nab-toolbar-burger burger is-absolute" aria-label="menu" aria-expanded="false" data-target="toolbarBasicExample">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>
  <div id="toolbarBasicExample" class="nab-toolbar-menu">
    <div class="nab-toolbar-start">
      <a class="nab-toolbar-item">Link</a>
      <a class="nab-toolbar-item">Link</a>
      <a class="nab-toolbar-item">Link</a>
    </div>
    <div class="nab-toolbar-end">
      <a class="nab-toolbar-item">Link</a>
      <a class="nab-toolbar-item">Link</a>
    </div>
  </div>
</nav>
    `,
		html: `
<nav class="nab-toolbar has-bg-black lighten-5 has-elevation-1" role="navigation" aria-label="main navigation">
  <div class="nab-toolbar-brand">
    <a class="nab-toolbar-item" href="#">
      <img src="/images/nabtrade-logo.png" />
    </a>
    <a role="button" class="nab-toolbar-burger burger is-absolute" aria-label="menu" aria-expanded="false" data-target="toolbarBasicExample">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>
  <div id="toolbarBasicExample" class="nab-toolbar-menu">
    <div class="nab-toolbar-start">
      <a class="nab-toolbar-item">Link</a>
      <a class="nab-toolbar-item">Link</a>
      <a class="nab-toolbar-item">Link</a>
    </div>
    <div class="nab-toolbar-end">
      <a class="nab-toolbar-item">Link</a>
      <a class="nab-toolbar-item">Link</a>
    </div>
  </div>
</nav>   

<nav class="nab-toolbar is-dark has-bg-black" role="navigation" aria-label="main navigation">
  <div class="nab-toolbar-brand">
    <a class="nab-toolbar-item" href="#">
      <img src="/images/nabtrade-logo-rev.png" />
    </a>
    <a role="button" class="nab-toolbar-burger burger is-absolute" aria-label="menu" aria-expanded="false" data-target="toolbarBasicExample">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>
  <div id="toolbarBasicExample" class="nab-toolbar-menu">
    <div class="nab-toolbar-start">
      <a class="nab-toolbar-item">Link</a>
      <a class="nab-toolbar-item">Link</a>
      <a class="nab-toolbar-item">Link</a>
    </div>
    <div class="nab-toolbar-end">
      <a class="nab-toolbar-item">Link</a>
      <a class="nab-toolbar-item">Link</a>
    </div>
  </div>
</nav>
    `,
		react: `
<Toolbar fluid className="has-bg-black lighten-5"
  image="/images/nabtrade-logo.png">
  <ToolbarNav left>
    <ToolbarNavItem href="link">Link</ToolbarNavItem>
    <ToolbarNavItem href="link">Link</ToolbarNavItem>
    <ToolbarNavItem href="link">Link</ToolbarNavItem>
  </ToolbarNav>
  <ToolbarNav right>
    <ToolbarNavItem href="link">Link</ToolbarNavItem>
    <ToolbarNavItem href="link">Link</ToolbarNavItem>
    <ToolbarNavItem href="link">Link</ToolbarNavItem>
  </ToolbarNav>
</Toolbar>

<Toolbar dark fluid className="has-bg-black"
  image="/images/nabtrade-logo-rev.png">
  <ToolbarNav left>
    <ToolbarNavItem href="link">Link</ToolbarNavItem>
    <ToolbarNavItem href="link">Link</ToolbarNavItem>
    <ToolbarNavItem href="link">Link</ToolbarNavItem>
  </ToolbarNav>
  <ToolbarNav right>
    <ToolbarNavItem href="link">Link</ToolbarNavItem>
    <ToolbarNavItem href="link">Link</ToolbarNavItem>
    <ToolbarNavItem href="link">Link</ToolbarNavItem>
  </ToolbarNav>
</Toolbar>
    `,
	},
	{
		title: '# Narrow',
		example: `
<nav class="nab-toolbar has-bg-black lighten-5 is-narrow mb-3 has-elevation-1" role="navigation" aria-label="main navigation">
  <div class="nab-toolbar-brand">
    <a class="nab-toolbar-item" href="#">
      <strong>Brand</strong>
    </a>
    <a role="button" class="nab-toolbar-burger burger is-absolute" aria-label="menu" aria-expanded="false" data-target="toolbarBasicExample">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>
  <div id="toolbarBasicExample" class="nab-toolbar-menu">
    <div class="nab-toolbar-start">
      <a class="nab-toolbar-item">Link</a>
      <a class="nab-toolbar-item">Link</a>
      <a class="nab-toolbar-item">Link</a>
    </div>
    <div class="nab-toolbar-end">
      <a class="nab-toolbar-item">Link</a>
      <a class="nab-toolbar-item">Link</a>
    </div>
  </div>
</nav> 
<nav class="nab-toolbar has-bg-black is-dark is-narrow" role="navigation" aria-label="main navigation">
  <div class="nab-toolbar-brand">
    <a class="nab-toolbar-item" href="#">
      
      <strong>Brand</strong>
    </a>
    <a role="button" class="nab-toolbar-burger burger is-absolute" aria-label="menu" aria-expanded="false" data-target="toolbarBasicExample">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>
  <div id="toolbarBasicExample" class="nab-toolbar-menu">
    <div class="nab-toolbar-start">
      <a class="nab-toolbar-item">Link</a>
      <a class="nab-toolbar-item">Link</a>
      <a class="nab-toolbar-item">Link</a>
    </div>
    <div class="nab-toolbar-end">
      <a class="nab-toolbar-item">Link</a>
      <a class="nab-toolbar-item">Link</a>
    </div>
  </div>
</nav> 
    `,
		html: `
<nav class="nab-toolbar has-bg-black lighten-5 is-narrow mb-3 has-elevation-1" role="navigation" aria-label="main navigation">
  <div class="nab-toolbar-brand">
    <a class="nab-toolbar-item" href="#">
      <img src="/images/nabtrade-logo.png" />
    </a>
    <a role="button" class="nab-toolbar-burger burger is-absolute" aria-label="menu" aria-expanded="false" data-target="toolbarBasicExample">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>
  <div id="toolbarBasicExample" class="nab-toolbar-menu">
    <div class="nab-toolbar-start">
      <a class="nab-toolbar-item">Link</a>
      <a class="nab-toolbar-item">Link</a>
      <a class="nab-toolbar-item">Link</a>
    </div>
    <div class="nab-toolbar-end">
      <a class="nab-toolbar-item">Link</a>
      <a class="nab-toolbar-item">Link</a>
    </div>
  </div>
</nav> 
<nav class="nab-toolbar has-bg-black is-dark is-narrow" role="navigation" aria-label="main navigation">
  <div class="nab-toolbar-brand">
    <a class="nab-toolbar-item" href="#">
      <img src="/images/nabtrade-logo-rev.png" />
    </a>
    <a role="button" class="nab-toolbar-burger burger is-absolute" aria-label="menu" aria-expanded="false" data-target="toolbarBasicExample">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>
  <div id="toolbarBasicExample" class="nab-toolbar-menu">
    <div class="nab-toolbar-start">
      <a class="nab-toolbar-item">Link</a>
      <a class="nab-toolbar-item">Link</a>
      <a class="nab-toolbar-item">Link</a>
    </div>
    <div class="nab-toolbar-end">
      <a class="nab-toolbar-item">Link</a>
      <a class="nab-toolbar-item">Link</a>
    </div>
  </div>
</nav>     
    `,
		react: `
<Toolbar narrow fluid className="has-bg-black lighten-5"
  image="/images/nabtrade-logo.png">
  <ToolbarNav left>
    <ToolbarNavItem href="link">Link</ToolbarNavItem>
    <ToolbarNavItem href="link">Link</ToolbarNavItem>
    <ToolbarNavItem href="link">Link</ToolbarNavItem>
  </ToolbarNav>
  <ToolbarNav right>
    <ToolbarNavItem href="link">Link</ToolbarNavItem>
    <ToolbarNavItem href="link">Link</ToolbarNavItem>
    <ToolbarNavItem href="link">Link</ToolbarNavItem>
  </ToolbarNav>
</Toolbar>

<Toolbar narrow dark fluid className="has-bg-black"
  image="/images/nabtrade-logo-rev.png">
  <ToolbarNav left>
    <ToolbarNavItem href="link">Link</ToolbarNavItem>
    <ToolbarNavItem href="link">Link</ToolbarNavItem>
    <ToolbarNavItem href="link">Link</ToolbarNavItem>
  </ToolbarNav>
  <ToolbarNav right>
    <ToolbarNavItem href="link">Link</ToolbarNavItem>
    <ToolbarNavItem href="link">Link</ToolbarNavItem>
    <ToolbarNavItem href="link">Link</ToolbarNavItem>
  </ToolbarNav>
</Toolbar>
    `,
	},
];
