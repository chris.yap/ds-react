import React from 'react';
import { Button, Container, Section } from 'shaper-react';

const Test = ({ props }) => {
  const [load, setLoad] = React.useState(false);
  return (
    <Section>
      <Container>
        {[...Array(1000)].map((x, i) => (
          <>
            <Button
              key={i}
              mb={2}
              loading={!load}
              onClick={() => setLoad(load ? false : true)}>
              Generated Button {i}
            </Button>
            {/* <button
              key={i}
              // mb={2}
              // loading={load}
              onClick={() => setLoad(load ? false : true)}>
              Generated Button {i}
            </button> */}
            <br />
          </>
        ))}
      </Container>
    </Section>
  );
};

export default Test;
