import React from 'react';
import PageHeader from '../../components/PageHeader';
import PropsTable from '../../components/PropsTable';
import {
  Column,
  Columns,
  Container,
  Divider,
  Section,
  Title
} from 'shaper-react';
import Highlight from 'react-highlight';

const Page = () => {
  // const [index, updateIndex] = React.useState(0);
  return (
    <div id="start">
      <PageHeader category="utilities" title="Breakpoints">
        A simple tool to customise layout or content based on breakpoints.
      </PageHeader>

      <Divider />

      <Section>
        <Container>
          <Columns mobile>
            <Column mobile={12} desktop={10} desktopOffset={1}>
              {/* <Demo examples={Eg} updateIndex={updateIndex}></Demo> */}
              <Title size="4">Usage</Title>
              <Highlight className="mb-4">{code}</Highlight>
              <PropsTable data={Data}></PropsTable>
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};

export default Page;

const Data = [
  {
    items: [
      {
        name: 'above',
        def: '-',
        type: 'Boolean',
        desc: 'True when above specified viewport'
      },
      {
        name: 'below',
        def: '-',
        type: 'Boolean',
        desc: 'True when below specified viewport'
      },
      {
        name: 'when',
        def: '-',
        type: 'Boolean',
        desc: 'True when within specified viewport'
      },
      {
        name: 'xs, sm, md, lg, xl',
        def: '-',
        type: '-',
        desc: 'Viewports'
      }
    ]
  }
];

const code = `import Breakpoints from 'shaper-react';

const Component = ({ activeBreakpoints, ...props }) => {
  return (
    <div {...props}>
      {activeBreakpoints.below.sm ? 'Mobile' : 'Not mobile'}
    </div>
  )
};

export default Breakpoints(Component);
`;

// const Eg = [
//   {
//     title: '# Usage',
//     react: ``,
//     html: ``
//   }
// ];
