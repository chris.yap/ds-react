import React from 'react';
import PageHeader from '../../components/PageHeader';
import PropsTable from '../../components/PropsTable';
import {
  Column,
  Columns,
  Container,
  Divider,
  Section,
  Title
} from 'shaper-react';
import Highlight from 'react-highlight';

const Page = () => {
  // const [index, updateIndex] = React.useState(0);
  return (
    <div id="start">
      <PageHeader category="utilities" title="Browser">
        A simple tool to detect which browser the user is on.
      </PageHeader>

      <Divider />

      <Section>
        <Container>
          <Columns mobile>
            <Column mobile={12} desktop={10} desktopOffset={1}>
              {/* <Demo examples={Eg} updateIndex={updateIndex}></Demo> */}
              <Title size="4">Usage</Title>
              <Highlight className="mb-4">{code}</Highlight>
              <PropsTable data={Data}></PropsTable>
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};

export default Page;

const Data = [
  {
    items: [
      {
        name: `isOpera,<br/> isFirefox,<br/> isSafari,<br/> isIE,<br/> isEdge,<br/> isChrome`,
        def: '-',
        type: '-',
        desc: 'True when detected.'
      }
    ]
  }
];

const code = `import WhichBrowser from 'shaper-react';

const Component = ({ activeBrowser, ...props }) => {
  return (
    <div {...props}>
      {activeBrowser.isSafari ? 'Safari' : 'Not safari'}
    </div>
  )
};

export default WhichBrowser(Component);
`;

// const Eg = [
//   {
//     title: '# Usage',
//     react: ``,
//     html: ``
//   }
// ];
