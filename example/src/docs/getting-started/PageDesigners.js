import React, { Component } from 'react'
import PageHeader from '../../components/PageHeader'
import { 
  Column, Columns, Container, Divider, Section
  // , Subtitle, Title, Hero, 
} from 'shaper-react'

export default class PageDesigners extends Component {
  render() {
    return (
      <div id="start">
        <PageHeader category="Getting started" title="Designers">
          The Shaper is a living, breathing document that contains all of our visual assets (components, iconography, color palettes, grids, etc).
        </PageHeader>

        <Divider className="my-0" />

        <Section>
          <Container>
            <Columns mobile>
              <Column className="is-12-mobile is-10-desktop is-offset-1-desktop">

                <h3 className="has-text-size3 font-nabimpact">Setup</h3>
                <h3 className="has-text-size4 font-nabimpact">Download the kit</h3>
                <p>Go to the <a href="/#">Shaper Design Kit</a> repo and click on the latest version of the Design Kit. Click the “View Raw” link to begin downloading the kit.</p>
                <p>To be continued...</p>

              </Column>
            </Columns>
          </Container>
        </Section>
      </div>
    )
  }
}
