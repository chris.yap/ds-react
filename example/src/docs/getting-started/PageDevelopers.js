import React from 'react';
import PageHeader from '../../components/PageHeader';
import {
  Alert,
  Column,
  Columns,
  Container,
  Divider,
  Message,
  MessageHeader,
  MessageBody,
  Section,
  Title,
  WhichBrowser
} from 'shaper-react';
import Highlight from 'react-highlight';

const Page = ({ activeBrowser, ...props }) => {
  return (
    <div id="start">
      <PageHeader category="getting started" title="Developers">
        The Shaper Library provides front-end developers & engineers a
        collection of reusable HTML and SASS partials to build websites and user
        interfaces. Adopting the library enables developers to use consistent
        markup, styles, and behavior in prototype and production work.
      </PageHeader>

      <Divider />

      <Section>
        <Container>
          <Columns mobile>
            <Column className="is-12-mobile is-10-desktop is-offset-1-desktop">
              <Title size="3" className="font-nabimpact">
                Quickstart with Create React app
              </Title>

              <p>
                <a href="http://github.com/facebookincubator/create-react-app">
                  Create React App
                </a>{' '}
                is a comfortable environment for <strong>learning React</strong>
                , and is the best way to start building{' '}
                <strong>
                  a new{' '}
                  <a href="https://reactjs.org/docs/glossary.html#single-page-application">
                    single-page
                  </a>{' '}
                  application
                </strong>{' '}
                in React.
              </p>

              <p>
                It sets up your development environment so that you can use the
                latest JavaScript features, provides a nice developer
                experience, and optimizes your app for production. You’ll need
                to have Node >= 6 and npm >= 5.2 on your machine. To create a
                project, run:
              </p>

              <Highlight className="">
                $ npx create-react-app my-app <br />
                $ cd my-app <br />
                $ npm start <br />
              </Highlight>

              <Message isOpened={true} info my={4}>
                <MessageHeader>Note</MessageHeader>
                <MessageBody>
                  npx on the first line is not a typo — it’s a{' '}
                  <a href="https://medium.com/@maybekatz/introducing-npx-an-npm-package-runner-55f7d4bd282b">
                    package runner tool that comes with npm 5.2+
                  </a>
                  .
                </MessageBody>
              </Message>

              <p>
                <strong>Create React App</strong> doesn’t handle backend logic
                or databases; it just creates a frontend build pipeline, so you
                can use it with any backend you want. Under the hood, it uses
                Babel and webpack, but you don’t need to know anything about
                them.
              </p>

              <p>
                When you’re ready to deploy to production, running{' '}
                <code>npm run build</code> will create an optimized build of
                your app in the <code>build</code> folder. You can learn more
                about <strong>Create React App</strong>{' '}
                <a href="https://github.com/facebookincubator/create-react-app#create-react-app-">
                  from its README
                </a>{' '}
                and the{' '}
                <a href="https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md#table-of-contents">
                  User Guide
                </a>
                .
              </p>

              <Divider className="my-10" />

              <Title size="3" className="font-nabimpact">
                Import stylesheet
              </Title>

              <Alert info inverted value={true} className="mb-4">
                <p className="mb-1">
                  We are in the process of converting the components to{' '}
                  <a
                    href="https://www.styled-components.com"
                    className="has-text-weight-bold">
                    styled-components
                  </a>
                  .
                </p>
                <p className="mb-0">
                  So in due time, there won't be a need to import the stylesheet
                  hence this step will be unnecessary.
                </p>
              </Alert>

              <Title size="6">Install with NPM</Title>

              <Highlight>$ npm install shaper-css --save</Highlight>

              <Title size="6">Install with Yarn</Title>

              <Highlight>$ yarn add shaper-css</Highlight>

              <Title size="6">Import into your application/stylesheet</Title>

              <Highlight>import 'shaper-css/dist/css/shaper.css'</Highlight>

              <Divider className="my-10" />

              <Title size="3" className="font-nabimpact">
                Install react components
              </Title>
              <Title size="6">Install with NPM</Title>

              <Highlight>$ npm install shaper-react --save</Highlight>
              <Title size="6">Install with Yarn</Title>

              <Highlight>$ yarn add shaper-react</Highlight>

              <Title size="6">
                Import individual components from library when needed
              </Title>

              <Highlight>
                import &#123; Container, Title, ... &#125; from 'shaper-react'
              </Highlight>

              {/* <Divider className="my-10" />
                
                <h3 class="has-text-size3 font-nabimpact">Documentation</h3>
                <p>Run documentation locally. You can easily run the documentation locally by running the following command in terminal or command prompt.</p>
                
                <Highlight className="shell">
$ npm start
                </Highlight>
                
                <p>There're basic requirements to run the documentation locally. This project was bootstrapped with <a href="https://github.com/facebookincubator/create-react-app">Create React App</a>.</p> */}
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};

export default WhichBrowser(Page);
