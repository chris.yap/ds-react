import React, { Component } from 'react';
import PageHeader from '../../components/PageHeader';
import {
  Breakpoints,
  Column,
  Columns,
  Container,
  Divider,
  Section,
  Tab,
  Tabs,
  Title
  // Subtitle, Hero,
} from 'shaper-react';
import ExpandingCodeExample from '../../components/ExpandingCodeExample';

class PageColumns extends Component {
  render() {
    const { activeBreakpoints } = this.props;
    return (
      <div id="start">
        <PageHeader category="Layout" title="Grid system / columns">
          Building responsive columns
        </PageHeader>

        <Divider className="my-0" />

        <Section>
          <Container>
            <Columns mobile>
              <Column mobile={12} desktop={10} desktopOffset={1}>
                <Columns>
                  <Column className="has-text-centered">
                    XS
                    <p className="mb-0">
                      above: <strong>{`${activeBreakpoints.above.xs}`}</strong>
                    </p>
                    <p className="mb-0">
                      when: <strong>{`${activeBreakpoints.when.xs}`}</strong>
                    </p>
                    <p className="mb-0">below: -</p>
                  </Column>
                  <Column className="has-text-centered">
                    SM
                    <p className="mb-0">
                      above: <strong>{`${activeBreakpoints.above.sm}`}</strong>
                    </p>
                    <p className="mb-0">
                      when: <strong>{`${activeBreakpoints.when.sm}`}</strong>
                    </p>
                    <p className="mb-0">
                      below: <strong>{`${activeBreakpoints.below.sm}`}</strong>
                    </p>
                  </Column>
                  <Column className="has-text-centered">
                    MD
                    <p className="mb-0">
                      above: <strong>{`${activeBreakpoints.above.md}`}</strong>
                    </p>
                    <p className="mb-0">
                      when: <strong>{`${activeBreakpoints.when.md}`}</strong>
                    </p>
                    <p className="mb-0">
                      below: <strong>{`${activeBreakpoints.below.md}`}</strong>
                    </p>
                  </Column>
                  <Column className="has-text-centered">
                    LG
                    <p className="mb-0">
                      above: <strong>{`${activeBreakpoints.above.lg}`}</strong>
                    </p>
                    <p className="mb-0">
                      when: <strong>{`${activeBreakpoints.when.lg}`}</strong>
                    </p>
                    <p className="mb-0">
                      below: <strong>{`${activeBreakpoints.below.lg}`}</strong>
                    </p>
                  </Column>
                  <Column className="has-text-centered">
                    XL
                    <p className="mb-0">
                      above: <strong>{`${activeBreakpoints.above.xl}`}</strong>
                    </p>
                    <p className="mb-0">
                      when: <strong>{`${activeBreakpoints.when.xl}`}</strong>
                    </p>
                    <p className="mb-0">
                      below: <strong>{`${activeBreakpoints.below.xl}`}</strong>
                    </p>
                  </Column>
                  <Column className="has-text-centered">
                    XXL
                    <p className="mb-0">above: -</p>
                    <p className="mb-0">
                      when: <strong>{`${activeBreakpoints.when.xxl}`}</strong>
                    </p>
                    <p className="mb-0">
                      below: <strong>{`${activeBreakpoints.below.xxl}`}</strong>
                    </p>
                  </Column>
                </Columns>

                {Examples.map((ex, e) => {
                  return (
                    <ExpandingCodeExample
                      key={e}
                      title={ex.title}
                      html={ex.html}
                      react={ex.react}
                      example={ex.example}
                      background={ex.background}
                    />
                  );
                })}

                <Divider className="my-10" />

                <Title size="4" className="font-nabimpact">
                  React Props
                </Title>

                <Tabs selected={`0`} toggle>
                  <Tab label="Columns">
                    <table className="nab-table is-striped">
                      <thead>
                        <tr>
                          <th className="has-text-weight-semibold px-3 has-text-size-1">
                            Name
                          </th>
                          <th className="has-text-weight-semibold px-3 has-text-size-1">
                            Default
                          </th>
                          <th className="has-text-weight-semibold px-3 has-text-size-1">
                            Type
                          </th>
                          <th className="has-text-weight-semibold px-3 has-text-size-1">
                            Description
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td className="has-text-weight-bold">mobile</td>
                          <td>false</td>
                          <td>Boolean</td>
                          <td>
                            Columns are activated from <strong>tablet</strong>{' '}
                            size onwards.
                          </td>
                        </tr>
                        <tr>
                          <td className="has-text-weight-bold">desktop</td>
                          <td>false</td>
                          <td>Boolean</td>
                          <td>
                            Columns are activated from <strong>desktop</strong>{' '}
                            size onwards.
                          </td>
                        </tr>
                        <tr>
                          <td className="has-text-weight-bold">multiline</td>
                          <td>false</td>
                          <td>Boolean</td>
                          <td>Make overflow columns wrap.</td>
                        </tr>
                        <tr>
                          <td className="has-text-weight-bold">gapless</td>
                          <td>false</td>
                          <td>Boolean</td>
                          <td>Remove gap between columns.</td>
                        </tr>
                        <tr>
                          <td className="has-text-weight-bold">centered</td>
                          <td>false</td>
                          <td>Boolean</td>
                          <td>Align column centered in columns container.</td>
                        </tr>
                      </tbody>
                    </table>
                  </Tab>
                  <Tab label="Column">
                    <table className="nab-table is-striped">
                      <thead>
                        <tr>
                          <th className="has-text-weight-semibold px-3 has-text-size-1">
                            Name
                          </th>
                          <th className="has-text-weight-semibold px-3 has-text-size-1">
                            Default
                          </th>
                          <th className="has-text-weight-semibold px-3 has-text-size-1">
                            Options
                          </th>
                          <th className="has-text-weight-semibold px-3 has-text-size-1">
                            Type
                          </th>
                          <th className="has-text-weight-semibold px-3 has-text-size-1">
                            Description
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td className="has-text-weight-bold">
                            size, <br />
                            mobile, <br />
                            tablet, <br />
                            desktop, <br />
                            widescreen, <br />
                            fullhd
                          </td>
                          <td>-</td>
                          <td>
                            1 - 12, <br />
                            one-quarter <br />
                            one-third <br />
                            half <br />
                            two-thirds <br />
                            three-quarters
                          </td>
                          <td>String</td>
                          <td>Column sizes</td>
                        </tr>
                        <tr>
                          <td className="has-text-weight-bold">
                            sizeOffset, <br />
                            mobileOffset, <br />
                            tabletOffset, <br />
                            desktopOffset, <br />
                            widescreenOffset, <br />
                            fullhdOffset
                          </td>
                          <td>-</td>
                          <td>
                            1 - 12, <br />
                            one-quarter <br />
                            one-third <br />
                            half <br />
                            two-thirds <br />
                            three-quarters
                          </td>
                          <td>String</td>
                          <td>Offset column sizes</td>
                        </tr>
                      </tbody>
                    </table>
                  </Tab>
                </Tabs>
              </Column>
            </Columns>
          </Container>
        </Section>
      </div>
    );
  }
}

export default Breakpoints(PageColumns);

const Examples = [
  {
    title: "Let's start simple",
    example: `
<div class="nab-content">
  <p>To build a grid, just</p>
  <ol>
    <li>Add a <code>nab-columns</code> container </li>
    <li>Add as many <code>nab-column</code> elements as you want</li>
  </ol>
  <p>Each column will have an equal width, no matter the number of columns.</p>
</div>
<div class="nab-columns">
  <div class="nab-column has-text-centered">
    <p class="has-bg-secondary lighten-4 pa-5">First column</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-secondary lighten-4 pa-5">Second column</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-secondary lighten-4 pa-5">Third column</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-secondary lighten-4 pa-5">Fourth column</p>
  </div>
</div>
    `,
    html: `
<div class="nab-columns">
  <div class="nab-column">First column</div>
  <div class="nab-column">Second column</div>
  <div class="nab-column">Third column</div>
  <div class="nab-column">Fourth column</div>
</div>
    `,
    react: `
import { Columns, Column } from '@shaper/react'

const MyComponent = (props) => (
  <Columns>
    <Column>First column</Column>
    <Column>Second column</Column>
    <Column>Third column</Column>
    <Column>Fourth column</Column>
  </Columns>
)
`
  },
  {
    title: 'Column sizes',
    example: `
<div class="nab-content">
  <p>If you want to change the size of a single column, you can use one the following classes:</p>
  <ul>
    <li><code>is-three-quarters</code></li>
    <li><code>is-two-thirds</code></li>
    <li><code>is-half</code></li>
    <li><code>is-one-third</code></li>
    <li><code>is-one-quarter</code></li>
  </ul>
  <p>The other columns will fill up the remaining space automatically.</p>
</div>
<div class="nab-columns">
  <div class="nab-column has-text-centered is-three-quarters">
    <p class="has-bg-secondary lighten-4 pa-5"><code>.is-three-quarters</code></p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">Auto</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">Auto</p>
  </div>
</div>
<div class="nab-columns">
  <div class="nab-column has-text-centered is-two-thirds">
    <p class="has-bg-secondary lighten-4 pa-5"><code>.is-two-thirds</code></p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">Auto</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">Auto</p>
  </div>
</div>
<div class="nab-columns">
  <div class="nab-column has-text-centered is-half">
    <p class="has-bg-secondary lighten-4 pa-5"><code>.is-half</code></p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">Auto</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">Auto</p>
  </div>
</div>
<div class="nab-columns">
  <div class="nab-column has-text-centered is-one-third">
    <p class="has-bg-secondary lighten-4 pa-5"><code>.is-one-third</code></p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">Auto</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">Auto</p>
  </div>
</div>
<div class="nab-columns">
  <div class="nab-column has-text-centered is-one-quarter">
    <p class="has-bg-secondary lighten-4 pa-5"><code>.is-one-quarter</code></p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">Auto</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">Auto</p>
  </div>
</div>
    `,
    html: `
<div class="nab-columns">
  <div class="nab-column is-three-quarters">is-three-quarters</div>
  <div class="nab-column">Auto</div>
  <div class="nab-column">Auto</div>
</div>
<div class="nab-columns">
  <div class="nab-column is-two-thirds">is-two-thirds</div>
  <div class="nab-column">Auto</div>
  <div class="nab-column">Auto</div>
</div>
<div class="nab-columns">
  <div class="nab-column is-half">is-half</div>
  <div class="nab-column">Auto</div>
  <div class="nab-column">Auto</div>
</div>
<div class="nab-columns">
  <div class="nab-column is-one-third">is-one-third</div>
  <div class="nab-column">Auto</div>
  <div class="nab-column">Auto</div>
</div>
<div class="nab-columns">
  <div class="nab-column is-one-quarter">is-one-quarter</div>
  <div class="nab-column">Auto</div>
  <div class="nab-column">Auto</div>
</div>
    `,
    react: `
const MyComponent = (props) => (
  <Columns>
    <Column size="three-quarters">is-three-quarters</Column>
    <Column>Auto</Column>
    <Column>Auto</Column>
  </Columns>
  <Columns>
    <Column size="two-thirds">is-two-thirds</Column>
    <Column>Auto</Column>
    <Column>Auto</Column>
  </Columns>
  <Columns>
    <Column size="half">is-half</Column>
    <Column>Auto</Column>
    <Column>Auto</Column>
  </Columns>
  <Columns>
    <Column size="one-third">is-one-third</Column>
    <Column>Auto</Column>
    <Column>Auto</Column>
  </Columns>
  <Columns>
    <Column size="one-quarter">is-one-quarter</Column>
    <Column>Auto</Column>
    <Column>Auto</Column>
  </Columns>
)
    `
  },
  {
    title: '12 columns',
    example: `
<div class="nab-content">
  <p>As the grid can be divided into <strong>12 columns</strong>, there are size classes for each division:</p>
  <ul>
    <li><code>is-1</code></li>
    <li><code>is-2</code></li>
    <li><code>is-3</code></li>
    <li><code>is-4</code></li>
    <li><code>is-5</code></li>
    <li><code>is-6</code></li>
    <li><code>is-7</code></li>
    <li><code>is-8</code></li>
    <li><code>is-9</code></li>
    <li><code>is-10</code></li>
    <li><code>is-11</code></li>
  </ul>
</div>
<div class="nab-columns">
  <div class="nab-column has-text-centered is-2">
    <p class="has-bg-secondary lighten-4 pa-5"><code>.is-2</code></p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
</div>
<div class="nab-columns">
  <div class="nab-column has-text-centered is-3">
    <p class="has-bg-secondary lighten-4 pa-5"><code>.is-3</code></p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
</div>
<div class="nab-columns">
  <div class="nab-column has-text-centered is-4">
    <p class="has-bg-secondary lighten-4 pa-5"><code>.is-4</code></p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
</div>
<div class="nab-columns">
  <div class="nab-column has-text-centered is-5">
    <p class="has-bg-secondary lighten-4 pa-5"><code>.is-5</code></p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
</div>
<div class="nab-columns">
  <div class="nab-column has-text-centered is-6">
    <p class="has-bg-secondary lighten-4 pa-5"><code>.is-6</code></p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
</div>
<div class="nab-columns">
  <div class="nab-column has-text-centered is-7">
    <p class="has-bg-secondary lighten-4 pa-5"><code>.is-7</code></p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
</div>
<div class="nab-columns">
  <div class="nab-column has-text-centered is-8">
    <p class="has-bg-secondary lighten-4 pa-5"><code>.is-8</code></p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
</div>
<div class="nab-columns">
  <div class="nab-column has-text-centered is-9">
    <p class="has-bg-secondary lighten-4 pa-5"><code>.is-9</code></p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
</div>
<div class="nab-columns">
  <div class="nab-column has-text-centered is-10">
    <p class="has-bg-secondary lighten-4 pa-5"><code>.is-10</code></p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
</div>
<div class="nab-columns">
  <div class="nab-column has-text-centered is-11">
    <p class="has-bg-secondary lighten-4 pa-5"><code>.is-11</code></p>
  </div>
  <div class="nab-column has-text-centered">
    <p class="has-bg-info lighten-4 pa-5">1</p>
  </div>
</div>
    `,
    html: `
<div class="nab-columns">
  <div class="nab-column is-2">.is-2</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
</div>
<div class="nab-columns">
  <div class="nab-column is-3">is-3</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
</div>
<div class="nab-columns">
  <div class="nab-column is-4">is-4</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
</div>
<div class="nab-columns">
  <div class="nab-column is-5">is-5</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
</div>
<div class="nab-columns">
  <div class="nab-column is-6">is-6</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
</div>
<div class="nab-columns">
  <div class="nab-column is-7">is-7</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
</div>
<div class="nab-columns">
  <div class="nab-column is-8">is-8</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
</div>
<div class="nab-columns">
  <div class="nab-column is-9">is-9</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
</div>
<div class="nab-columns">
  <div class="nab-column is-10">is-10</div>
  <div class="nab-column">1</div>
  <div class="nab-column">1</div>
</div>
<div class="nab-columns">
  <div class="nab-column is-11">is-11</div>
  <div class="nab-column">1</div>
</div>
    `,
    react: `
const MyComponent = (props) => (
  <Columns>
    <Column size="2">First column</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
  </Columns>
  <Columns>
    <Column size="3">First column</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
  </Columns>
  <Columns>
    <Column size="4">First column</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
  </Columns>
  <Columns>
    <Column size="5">First column</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
  </Columns>
  <Columns>
    <Column size="6">First column</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
  </Columns>
  <Columns>
    <Column size="7">First column</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
  </Columns>
  <Columns>
    <Column size="8">First column</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
  </Columns>
  <Columns>
    <Column size="9">First column</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
  </Columns>
  <Columns>
    <Column size="10">First column</Column>
    <Column>1</Column>
    <Column>1</Column>
  </Columns>
  <Columns>
    <Column size="11">First column</Column>
    <Column>1</Column>
  </Columns>
)
    `
  },
  {
    title: 'Offsetting columns',
    example: `
<div class="nab-content">
  <p>While you can use empty columns (like <code>&lt;div class="nab-column"&gt;&lt;/div&gt;</code>) to create horizontal space around <code>nab-column</code> elements, you can also use <strong>offset modifiers</strong> like <code>is-offset-x</code>:</p>
</div>
<div class="nab-columns">
  <div class="nab-column has-text-centered is-half is-offset-one-quarter">
    <p class="has-bg-secondary lighten-4 pa-5"><code>.is-half .is-offset-one-quarter</code></p>
  </div>
</div>
<div class="nab-columns">
  <div class="nab-column has-text-centered is-3 is-offset-7">
    <p class="has-bg-secondary lighten-4 pa-5"><code>.is-3 .is-offset-7</code></p>
  </div>
</div>
<div class="nab-columns">
  <div class="nab-column has-text-centered is-5 is-offset-2">
    <p class="has-bg-secondary lighten-4 pa-5"><code>.is-5 .is-offset-2</code></p>
  </div>
</div>
<div class="nab-columns">
  <div class="nab-column has-text-centered is-11 is-offset-1">
    <p class="has-bg-secondary lighten-4 pa-5"><code>.is-11 .is-offset-1</code></p>
  </div>
</div>
    `,
    html: `
<div class="nab-columns">
  <div class="nab-column is-half is-offset-one-quarter">.is-half .is-offset-one-quarter</div>
</div>
<div class="nab-columns">
  <div class="nab-column is-3 is-offset-7">.is-3 .is-offset-7</div>
</div>
<div class="nab-columns">
  <div class="nab-column is-5 is-offset-2">.is-5 .is-offset-2</div>
</div>
<div class="nab-columns">
  <div class="nab-column is-11 is-offset-1">.is-11 .is-offset-1</div>
</div>
    `,
    react: `
const MyComponent = (props) => (
  <Columns>
    <Column size="half" sizeOffset="one-quarter">.is-half .is-offset-one-quarter</Column>
  </Columns>
  <Columns>
    <Column size="3" sizeOffset="7">.is-3 .is-offset-7</Column>
  </Columns>
  <Columns>
    <Column size="5" sizeOffset="2">.is-5 .is-offset-2</Column>
  </Columns>
  <Columns>
    <Column size="11" sizeOffset="1">.is-11 .is-offset-1</Column>
  </Columns>
)
    `
  },
  {
    title: 'Gapless columns',
    example: `
<div class="nab-content">
  <p>If you want to <strong>remove the space between the columns</strong>, add the
    <code>is-gapless</code> modifier on the columns container:</p>
  <div class="nab-columns is-gapless">
    <div class="nab-column has-text-centered">
      <p class="has-bg-secondary lighten-4 pa-5">First column</p>
    </div>
    <div class="nab-column has-text-centered">
      <p class="has-bg-warning lighten-4 pa-5">Second column</p>
    </div>
    <div class="nab-column has-text-centered">
      <p class="has-bg-info lighten-4 pa-5">Third column</p>
    </div>
    <div class="nab-column has-text-centered">
      <p class="has-bg-danger lighten-4 pa-5">Fourth column</p>
    </div>
  </div>

  <p>You can even combine it with <strong><code>is-multiline</code></strong> modifier:</p>

  <div class="nab-columns is-gapless is-multiline">
    <div class="nab-column has-bg-secondary lighten-4 has-text-centered is-one-quarter">
      <code class="my-5">is-one-quarter</code>
    </div>
    <div class="nab-column has-bg-warning lighten-4 has-text-centered is-one-quarter">
      <code class="my-5">is-one-quarter</code>
    </div>
    <div class="nab-column has-bg-info lighten-4 has-text-centered is-one-quarter">
      <code class="my-5">is-one-quarter</code>
    </div>
    <div class="nab-column has-bg-danger lighten-4 has-text-centered is-one-quarter">
      <code class="my-5">is-one-quarter</code>
    </div>
    <div class="nab-column has-bg-danger lighten-4 has-text-centered is-half">
      <code class="my-5">is-half</code>
    </div>
    <div class="nab-column has-bg-warning lighten-4 has-text-centered is-one-quarter">
      <code class="my-5">is-one-quarter</code>
    </div>
    <div class="nab-column has-bg-secondary lighten-4 has-text-centered is-one-quarter">
      <code class="my-5">is-one-quarter</code>
    </div>
    <div class="nab-column has-bg-secondary lighten-4 has-text-centered is-one-quarter">
      <code class="my-5">is-one-quarter</code>
    </div>
    <div class="nab-column has-bg-info lighten-4 has-text-centered">
      <span class="is-block my-5">Auto</span>
    </div>
  </div>
</div>
    `,
    html: `
<div class="nab-columns is-gapless">
  <div class="nab-column">First column</div>
  <div class="nab-column">Second column</div>
  <div class="nab-column">Third column</div>
  <div class="nab-column">Fourth column</div>
</div>
<div class="nab-columns is-gapless is-multiline">
  <div class="nab-column is-one-quarter">is-one-quarter</div>
  <div class="nab-column is-one-quarter">is-one-quarter</div>
  <div class="nab-column is-one-quarter">is-one-quarter</div>
  <div class="nab-column is-one-quarter">is-one-quarter</div>
  <div class="nab-column is-half">is-half</div>
  <div class="nab-column is-one-quarter">is-one-quarter</div>
  <div class="nab-column is-one-quarter">is-one-quarter</div>
  <div class="nab-column is-one-quarter">is-one-quarter</div>
  <div class="nab-column">Auto</div>
</div>
    `,
    react: `
const MyComponent = (props) => (
  <Columns gapless>
    <Column>First column</Column>
    <Column>Second column</Column>
    <Column>Third column</Column>
    <Column>Fourth column</Column>
  </Columns>
  <Columns gapless multiline>
    <Column size="one-quarter">is-one-quarter</Column>
    <Column size="one-quarter">is-one-quarter</Column>
    <Column size="one-quarter">is-one-quarter</Column>
    <Column size="one-quarter">is-one-quarter</Column>
    <Column size="half">is-half</Column>
    <Column size="one-quarter">is-one-quarter</Column>
    <Column size="one-quarter">is-one-quarter</Column>
    <Column size="one-quarter">is-one-quarter</Column>
    <Column>Auto</Column>
  </Columns>
)
`
  },
  {
    title: 'Columns responsiveness',
    example: `
<div class="nab-content">          
  <div class="nab-alert is-inverted is-active">
    <span class="nab-icon mr-2">
      <i class="icon-info"></i>
    </span>
    <div><strong>
      Resize your browser and see when the columns are stacked and when they are horizontally distributed.
    </strong></div>
  </div>

  <h5 class="nab-title is-5 mb-2">Mobile columns</h5>
  <p>By default, columns are only activated from <strong>tablet</strong> onwards. Columns will be stacked on top of each other when displayed on <strong>mobile</strong>. If you want columns to work on mobile, simply just add the <code>is-mobile</code> modifier on the columns container.</p>
  <div class="nab-columns is-mobile">
    <div class="nab-column">
      <div class="nab-card has-bg-sky lighten-4 has-elevation-0">
        <div class="nab-card-content has-text-centered">1</div>
      </div>
    </div>
    <div class="nab-column">
      <div class="nab-card has-bg-sky lighten-4 has-elevation-0">
        <div class="nab-card-content has-text-centered">2</div>
      </div>
    </div>
    <div class="nab-column">
      <div class="nab-card has-bg-sky lighten-4 has-elevation-0">
        <div class="nab-card-content has-text-centered">3</div>
      </div>
    </div>
    <div class="nab-column">
      <div class="nab-card has-bg-sky lighten-4 has-elevation-0">
        <div class="nab-card-content has-text-centered">4</div>
      </div>
    </div>
  </div>

  <h5 class="nab-title is-5 mb-2">Desktop columns</h5>
  <p>If you only want columns on <strong>desktop</strong> upwards, just use the <code>is-desktop</code> modifier on the columns container.</p>
  <div class="nab-columns is-desktop">
    <div class="nab-column">
      <div class="nab-card has-bg-sky lighten-4 has-elevation-0">
        <div class="nab-card-content has-text-centered">1</div>
      </div>
    </div>
    <div class="nab-column">
      <div class="nab-card has-bg-sky lighten-4 has-elevation-0">
        <div class="nab-card-content has-text-centered">2</div>
      </div>
    </div>
    <div class="nab-column">
      <div class="nab-card has-bg-sky lighten-4 has-elevation-0">
        <div class="nab-card-content has-text-centered">3</div>
      </div>
    </div>
    <div class="nab-column">
      <div class="nab-card has-bg-sky lighten-4 has-elevation-0">
        <div class="nab-card-content has-text-centered">4</div>
      </div>
    </div>
  </div>
</div>
    `,
    html: `
<div class="nab-columns is-mobile">
  <div class="nab-column">1</div>
  <div class="nab-column">2</div>
  <div class="nab-column">3</div>
  <div class="nab-column">4</div>
</div>

<div class="nab-columns is-desktop">
  <div class="nab-column">1</div>
  <div class="nab-column">2</div>
  <div class="nab-column">3</div>
  <div class="nab-column">4</div>
</div>
    `,
    react: `
const MyComponent = (props) => (
  <Columns mobile>
    <Column>1</Column>
    <Column>2</Column>
    <Column>3</Column>
    <Column>4</Column>
  </Columns>
  <Columns desktop>
    <Column>1</Column>
    <Column>2</Column>
    <Column>3</Column>
    <Column>4</Column>
  </Columns>
)
`
  },
  {
    title: 'Different column sizes per breakpoint',
    example: `
<div class="nab-content">
  <div class="nab-alert is-inverted is-active">
    <span class="nab-icon">
      <i class="icon-info"></i>
    </span>
    <div><strong>
      Resize your browser and see when the columns are stacked and when they are horizontally distributed.
    </strong></div>
  </div>

  <p>You can define a <strong>column size</strong> for <em>each</em> viewport size: <strong>mobile</strong>, <strong>tablet</strong>, and <strong>desktop</strong>.</p>
  <div class="nab-columns is-multiline is-mobile">
    <div class="nab-column is-three-quarters-mobile is-two-thirds-tablet is-half-desktop is-one-third-widescreen is-one-quarter-fullhd">
      <div class="nab-card has-bg-sky lighten-4 has-elevation-0">
        <div class="nab-card-content">
          <ul class="my-0">
            <li><code>is-three-quarters-mobile</code></li>
            <li><code>is-two-thirds-tablet</code></li>
            <li><code>is-half-desktop</code></li>
            <li><code>is-one-third-widescreen</code></li>
            <li><code>is-one-quarter-fullhd</code></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="nab-column">
      <div class="nab-card has-bg-sky lighten-4 has-elevation-0">
        <div class="nab-card-content has-text-centered">1</div>
      </div>
    </div>
    <div class="nab-column">
      <div class="nab-card has-bg-sky lighten-4 has-elevation-0">
        <div class="nab-card-content has-text-centered">1</div>
      </div>
    </div>
    <div class="nab-column">
      <div class="nab-card has-bg-sky lighten-4 has-elevation-0">
        <div class="nab-card-content has-text-centered">1</div>
      </div>
    </div>
    <div class="nab-column">
      <div class="nab-card has-bg-sky lighten-4 has-elevation-0">
        <div class="nab-card-content has-text-centered">1</div>
      </div>
    </div>
  </div>
</div>
    `,
    html: `
  <div class="nab-columns is-multiline is-mobile">
    <div class="nab-column is-three-quarters-mobile is-two-thirds-tablet is-half-desktop 
      is-one-third-widescreen is-one-quarter-fullhd">
      is-three-quarters-mobile
      is-two-thirds-tablet
      is-half-desktop
      is-one-third-widescreen
      is-one-quarter-fullhd
    </div>
    <div class="nab-column">1</div>
    <div class="nab-column">1</div>
    <div class="nab-column">1</div>
    <div class="nab-column">1</div>
  </div>
    `,
    react: `
const MyComponent = (props) => (
  <Columns>
    <Column 
      mobile="three-quarters" 
      tablet="two-thirds" 
      desktop="half" 
      widescreen="one-third" 
      fullhd="one-quarter"
    >
      is-three-quarters-mobile
      is-two-thirds-tablet
      is-half-desktop
      is-one-third-widescreen
      is-one-quarter-fullhd
    </Column>
    <Column>1</Column>
  </Columns>
)
`
  }
];
