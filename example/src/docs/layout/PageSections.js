import React from 'react';
import PageHeader from '../../components/PageHeader';
import Demo from '../../components/Demo';
import PropsTable from '../../components/PropsTable';
import {
  Column,
  Columns,
  Container,
  Divider,
  Section,
  Tag,
  Title
} from 'shaper-react';

const Page = () => {
  const [index, updateIndex] = React.useState(0);
  return (
    <div id="start">
      <PageHeader category="layout" title="Sections">
        A simple container to divide your page into sections. <br />
        <Tag small>styled-components</Tag>
      </PageHeader>

      <Divider />

      <Section>
        <Container>
          <Columns mobile>
            <Column mobile={12} desktop={10} desktopOffset={1}>
              <Demo examples={Eg} updateIndex={updateIndex}>
                {index === 0 ? (
                  <>
                    <p>
                      Use sections as direct children of <code>app</code>
                    </p>
                    <Section bg="blacks.0">
                      <Container>
                        <Title size="3" className="mb-2">
                          Section title
                        </Title>
                        <p>
                          Lorem ipsum dolor, sit amet consectetur adipisicing
                          elit. Blanditiis fugiat nobis ipsa molestias illum
                          iste dolor corrupti a voluptatem dignissimos maiores
                          assumenda vero earum laudantium, accusantium velit
                          autem voluptatum reiciendis!
                        </p>
                      </Container>
                    </Section>
                  </>
                ) : (
                  <>
                    <Section bg="skies.1" medium>
                      <Container>
                        <Title size="3" className="mb-2">
                          Medium size
                        </Title>
                        <p>
                          Lorem ipsum dolor, sit amet consectetur adipisicing
                          elit. Blanditiis fugiat nobis ipsa molestias illum
                          iste dolor corrupti a voluptatem dignissimos maiores
                          assumenda vero earum laudantium, accusantium velit
                          autem voluptatum reiciendis!
                        </p>
                      </Container>
                    </Section>
                    <Section bg="tangerines.1" large>
                      <Container>
                        <Title size="3" className="mb-2">
                          Large size
                        </Title>
                        <p>
                          Lorem ipsum dolor, sit amet consectetur adipisicing
                          elit. Blanditiis fugiat nobis ipsa molestias illum
                          iste dolor corrupti a voluptatem dignissimos maiores
                          assumenda vero earum laudantium, accusantium velit
                          autem voluptatum reiciendis!
                        </p>
                      </Container>
                    </Section>
                  </>
                )}
              </Demo>

              <PropsTable data={Data} />
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};

export default Page;

const Data = [
  {
    items: [
      {
        name: `Medium, <br />
            Large`,
        def: 'undefined',
        type: 'Boolean',
        desc: 'Section padding sizes'
      }
    ]
  }
];

const Eg = [
  {
    title: '# Usage',
    react: `<Section bg="blacks.0">
  <Container>
    <Title size="3" className="mb-2">
      Section title
    </Title>
    <p>
      Lorem ipsum dolor, sit amet consectetur adipisicing
      elit. Blanditiis fugiat nobis ipsa molestias illum iste
      dolor corrupti a voluptatem dignissimos maiores
      assumenda vero earum laudantium, accusantium velit autem
      voluptatum reiciendis!
    </p>
  </Container>
</Section>
`
  },
  {
    title: '# Sizes',
    react: `
<Section bg="skies.0" medium>
  <Container>
    <Title size="3" className="mb-2">
      Section title
    </Title>
    <p>
      Lorem ipsum dolor, sit amet consectetur adipisicing
      elit. Blanditiis fugiat nobis ipsa molestias illum iste
      dolor corrupti a voluptatem dignissimos maiores
      assumenda vero earum laudantium, accusantium velit autem
      voluptatum reiciendis!
    </p>
  </Container>
</Section>
<Section bg="seas.0" large>
  <Container>
    <Title size="3" className="mb-2">
      Section title
    </Title>
    <p>
      Lorem ipsum dolor, sit amet consectetur adipisicing
      elit. Blanditiis fugiat nobis ipsa molestias illum iste
      dolor corrupti a voluptatem dignissimos maiores
      assumenda vero earum laudantium, accusantium velit autem
      voluptatum reiciendis!
    </p>
  </Container>
</Section>
`
  }
];
