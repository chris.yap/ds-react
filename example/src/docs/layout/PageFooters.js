import React from 'react';
import PageHeader from '../../components/PageHeader';
import Demo from '../../components/Demo';
import {
  Column,
  Columns,
  Container,
  Divider,
  Footer,
  Section,
  Tag
} from 'shaper-react';

const PageFooters = () => {
  const [index, updateIndex] = React.useState(0);
  return (
    <div id="start">
      <PageHeader category="layout" title="Footer">
        A simple responsive footer which can include anything. <br />
        <Tag small>styled-components</Tag>
      </PageHeader>

      <Divider />

      <Section>
        <Container>
          <Columns mobile>
            <Column mobile={12} desktop={10} desktopOffset={1}>
              <Demo examples={Eg} updateIndex={updateIndex}>
                {index === 0 && (
                  <Footer bg="blacks.0" color="secondary">
                    <Container>
                      <p className="has-text-centered">
                        <strong>SHAPER</strong> built by the{' '}
                        <strong>Digital Team</strong>.
                      </p>
                    </Container>
                  </Footer>
                )}
              </Demo>
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};

export default PageFooters;

const Eg = [
  {
    title: '# Usage',
    react: `<Footer bg="blacks.0" color="secondary">
  <Container>
    <p className="has-text-centered">
      <strong>SHAPER</strong> built by{' '}
      <strong>Self Directed Wealth (SDW) Digital</strong>.
    </p>
  </Container>
</Footer>
`
  }
];
