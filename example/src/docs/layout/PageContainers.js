import React from 'react';
import PageHeader from '../../components/PageHeader';
import HighlightJS from 'react-highlight';
import {
  Column,
  Columns,
  Container,
  Divider,
  Section,
  Tag
} from 'shaper-react';
import Demo from '../../components/Demo';

const Page = () => {
  const [index, updateIndex] = React.useState(0);
  return (
    <div id="start">
      <PageHeader category="layout" title="Containers">
        Simple <strong>containers</strong> that holds stuff together <br />
        <Tag small>styled-component</Tag>
      </PageHeader>

      <Divider className="my-0" />

      <Section>
        <Container>
          <Columns mobile>
            <Column mobile={12} desktop={10} desktopOffset={1}>
              <Demo examples={Eg} updateIndex={updateIndex}>
                {index === 0 ? (
                  <div className="nab-content">
                    <p>
                      The <code>Container</code> component can be used in any
                      context, but mostly as a <strong>direct child</strong> of
                      either:
                    </p>

                    <ul>
                      <li>
                        <code>Toolbar</code>
                      </li>
                      <li>
                        <code>Hero</code>
                      </li>
                      <li>
                        <code>Section</code>
                      </li>
                      <li>
                        <code>Footer</code>
                      </li>
                    </ul>

                    <p>
                      The containers <strong>width</strong> for each{' '}
                      <strong>breakpoint</strong> is the result of:{' '}
                      <code>$device - (2 * $gap)</code>. The <code>$gap</code>{' '}
                      variable has a default value of <code>32px</code> but can
                      be modified.
                    </p>

                    <p>This is how the container will behave:</p>

                    <ul>
                      <li>
                        on <code>$desktop</code> it will have a maximum width of{' '}
                        <strong>960px</strong>.
                      </li>
                      <li>
                        on <code>$widescreen</code> it will have a maximum width
                        of <strong>1152px</strong>.
                      </li>
                      <li>
                        on <code>$fullhd</code> it will have a maximum width of{' '}
                        <strong>1344px</strong>.
                      </li>
                    </ul>

                    <p>
                      The values <strong>960</strong>, <strong>1152</strong> and{' '}
                      <strong>1344</strong> have been chosen because they are
                      divisible by both <strong>12</strong> and{' '}
                      <strong>16</strong>.
                    </p>

                    <HighlightJS className="xml">{Eg[0].html}</HighlightJS>
                  </div>
                ) : index === 1 ? (
                  <>
                    <p>
                      If you don't want to have a maximum width but want to keep
                      the 32px margin on the left and right sides, add the{' '}
                      <code>fluid</code> prop.
                    </p>
                    <HighlightJS className="xml">{Eg[1].html}</HighlightJS>
                  </>
                ) : (
                  <>
                    <p>
                      With the two props <code>widescreen</code> and{' '}
                      <code>fullhd</code>, you can have a fullwidth container
                      until those specific breakpoints.
                    </p>
                    <HighlightJS className="xml">{Eg[2].html}</HighlightJS>
                  </>
                )}
              </Demo>
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};

export default Page;

const Eg = [
  {
    title: '# Usage',
    html: `<Container>
  This container is <strong>centered</strong> on desktop.
</Container>
`
  },
  {
    title: '# Fluid',
    html: `
<Container>
  <p>This container is <strong>fluid</strong>: it will have a 
  32px gap on either side, on any viewport size.</p>
</Container>
    `
  },
  {
    title: '# Breakpoints containers',
    html: `
<Container widescreen>
  <p>This container is <strong>fullwidth</strong> until the 
  <code>widescreen</code> breakpoint</p>
</Container>

<Container fullhd>
  <p>This container is <strong>fullwidth</strong> until the 
  <code>fullhd</code> breakpoint</p>
</Container>
    `
  }
];
