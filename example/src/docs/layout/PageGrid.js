import React from 'react';
import PageHeader from '../../components/PageHeader';
import Demo from '../../components/Demo';
import {
  Box,
  Column,
  Columns,
  Container,
  Divider,
  // Grid,
  Radio,
  RadioGroup,
  Section,
  Tag
} from 'shaper-react';

const Page = () => {
  const [index, updateIndex] = React.useState(0);
  const [space, updateSpace] = React.useState('0');
  return (
    <div id="start">
      <PageHeader category="layout" title="Grid">
        Responsive layout grid adapts to screen size and orientation, ensuring
        consistency across layouts. <br />
        <Tag small>styled-system</Tag>
      </PageHeader>

      <Divider />

      <Section>
        <Container>
          <Columns mobile>
            <Column mobile={12} desktop={10} desktopOffset={1}>
              {/* <Grid
                container
                justifyContent="center"
                gridAutoColumns="auto"
                gridGap={space}
                mb={5}
              >
                {[0, 1, 2].map((g, i) => (
                  <Grid key={i} gridColumn={i + 1}>
                    <Box p={6}>{g}</Box>
                  </Grid>
                ))}
              </Grid> */}
              <Box>
                <RadioGroup
                  label="Gap"
                  name="gap"
                  onChange={updateSpace}
                  selectedValue={space}
                >
                  <Radio value="0" label="0" />
                  <Radio value="1" label="1" />
                  <Radio value="2" label="2" />
                  <Radio value="3" label="3" />
                </RadioGroup>
              </Box>
              <Demo examples={Eg} updateIndex={updateIndex}>
                {index === 0 ? null : null}
              </Demo>
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};

export default Page;

const Eg = [
  {
    title: '# Usage',
    react: ``,
    html: ``
  }
];
