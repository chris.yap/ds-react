import React, { Component } from 'react'
import PageHeader from '../../components/PageHeader'
import { 
  Column, Columns, Container, Divider, Section, Title 
  // Subtitle, Hero, 
} from 'shaper-react'
import ExpandingCodeExample from '../../components/ExpandingCodeExample'
import Highlight from 'react-highlight'

export default class PageTiles extends Component {
  render() {
    return (
      <div id="start">
        <PageHeader category="Layout" title="Tiles">
          A single tile element to build 2-dimensional masonry grids
        </PageHeader>

        <Divider className="my-0" />

        <Section>
          <Container>
            <Columns mobile>
              <Column mobile={12} desktop={10} desktopOffset={1}>

                {/* <Tile ancestor>
                  <Tile size="8">123</Tile>
                  <Tile>123</Tile>
                  <Tile>123</Tile>
                </Tile> */}

                <p>To build intricate masonry layouts, just use a single element:</p>

                <Columns>
                  <Column size={6}>
                    <p><strong>HTML</strong></p>
                    <p>the <code>nab-tile</code></p>
                    <Highlight className="xml">
&lt;div class="nab-tile"&gt; <br />
&nbsp;&nbsp;&lt;!-- The magical tile element! --&gt; <br />
&lt;/div&gt;
                    </Highlight>
                  </Column>
                  <Column size={6}>
                    <p><strong>ReactJs</strong></p>
                    <p>the <code>Tile</code></p>
                    <Highlight className="xml">
&lt;Tile&gt; <br />
&nbsp;&nbsp;&lt;!-- The magical tile element! --&gt; <br />
&lt;/Tile&gt;
                    </Highlight>
                  </Column>
                </Columns>

                <Title size="3" className="font-nabimpact mt-10">Examples</Title>

                <ExpandingCodeExample title={Ex1.title} example={Ex1.example} html={Ex1.html} react={Ex1.react} />

                <h3 className="has-text-size4 font-nabimpact mt-10">Modifiers</h3>

                <Columns>
                  <Column size={6}>
                    <p><strong>HTML</strong></p>
                    <div className="nab-content">
                      <p>The <code>nab-tile</code> element has <strong>16 modifiers</strong>:</p>
                      <ul>
                        <li><strong>3 contextual</strong> modifiers</li>
                        <ul>
                          <li><code>is-ancestor</code></li>
                          <li><code>is-parent</code></li>
                          <li><code>is-child</code></li>
                        </ul>
                        <li><strong>1 directional</strong> modifier</li>
                        <ul>
                          <li><code>is-vertical</code></li>
                        </ul>
                        <li><strong>12 horizontal size</strong> modifiers</li>
                        <ul>
                          <li>from <code>is-1</code></li>
                          <li>to <code>is-12</code></li>
                        </ul>
                      </ul>
                    </div>
                  </Column>
                  <Column size={6}>
                    <div className="nab-content">
                      <p><strong>ReactJS</strong></p>
                      <p>The <code>Tile</code> element has <strong>16 modifiers</strong>:</p>
                      <ul>
                        <li><strong>3 contextual</strong> modifiers</li>
                        <ul>
                          <li><code>ancestor</code></li>
                          <li><code>parent</code></li>
                          <li><code>child</code></li>
                        </ul>
                        <li><strong>1 directional</strong> modifier</li>
                        <ul>
                          <li><code>vertical</code></li>
                        </ul>
                        <li><strong>12 horizontal size</strong> modifiers</li>
                        <ul>
                          <li>from <code>size="1"</code></li>
                          <li>to <code>size="12"</code></li>
                        </ul>
                      </ul>
                    </div>
                  </Column>
                </Columns>

                <Divider className="my-10" />

                <h3 className="has-text-size4 font-nabimpact mt-10 mb-1">How it works: Nesting</h3>
                
                <p>Everything is a tile! To create a grid of tiles, you only need to <strong>nest</strong> <code>nab-tile</code>/<code>Tile</code> elements.</p>

                <p>Start with an <strong>ancestor</strong> tile that will wrap all other tiles:</p>

                <Columns className="mb-0">
                  <Column size={6}>
                    <p><strong>HTML</strong></p>
                    <Highlight className="xml">
&lt;div class="nab-tile is-ancestor"&gt; <br/>
&nbsp;&nbsp;&lt;!-- All other tile elements --&gt; <br/>
&lt;/div&gt;
                    </Highlight>
                  </Column>
                  <Column size={6}>
                    <p><strong>ReactJS</strong></p>
                    <Highlight className="xml">
&lt;Tile ancestor&gt; <br/>
&nbsp;&nbsp;&lt;!-- All other tile elements --&gt; <br/>
&lt;/Tile&gt;
                    </Highlight>
                  </Column>
                </Columns>

                <p>Add tile elements that will distribute themselves <strong>horizontally</strong>:</p>

                <Columns className="mb-0">
                  <Column size={6}>
                    <p><strong>HTML</strong></p>
                    <Highlight className="xml">
&lt;div class="nab-tile is-ancestor"&gt; <br/>
&nbsp;&nbsp;&lt;div class="nab-tile"&gt; <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;!-- Add content or other tiles --&gt; <br/>
&nbsp;&nbsp;&lt;/div&gt; <br/>
&nbsp;&nbsp;&lt;div class="nab-tile"&gt; <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;!-- Add content or other tiles --&gt; <br/>
&nbsp;&nbsp;&lt;/div&gt; <br/>
&lt;/div&gt;
                    </Highlight>
                  </Column>
                  <Column size={6}>
                    <p><strong>ReactJS</strong></p>
                    <Highlight className="xml">
&lt;Tile ancestor&gt; <br/>
&nbsp;&nbsp;&lt;Tile&gt; <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;!-- Add content or other tiles --&gt; <br/>
&nbsp;&nbsp;&lt;/Tile&gt; <br/>
&nbsp;&nbsp;&lt;Tile&gt; <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;!-- Add content or other tiles --&gt; <br/>
&nbsp;&nbsp;&lt;/Tile&gt; <br/>
&lt;/Tile&gt;
                    </Highlight>
                  </Column>
                </Columns>

                <p>You can <strong>resize</strong> any tile according to a <strong>12 column</strong> grid.</p>
                <p>For example, class <code>is-4</code> will take up 1/3 of the horizontal space:</p>

                <Columns className="mb-0">
                  <Column size={6}>
                    <p><strong>HTML</strong></p>
                    <Highlight className="xml">
                      &lt;div class="nab-tile is-ancestor"&gt; <br />
                      &nbsp;&nbsp;&lt;div class="nab-tile is-4"&gt; <br />
                      &nbsp;&nbsp;&nbsp;&nbsp;&lt;!-- 1/3 --&gt; <br />
                      &nbsp;&nbsp;&lt;/div&gt; <br />
                      &nbsp;&nbsp;&lt;div class="nab-tile"&gt; <br />
                      &nbsp;&nbsp;&nbsp;&nbsp;&lt;!-- This tile will take the rest: 2/3 --&gt; <br />
                      &nbsp;&nbsp;&lt;/div&gt; <br />
                      &lt;/div&gt;
                    </Highlight>
                  </Column>
                  <Column size={6}>
                    <p><strong>ReactJS</strong></p>
                    <Highlight className="xml">
                      &lt;Tile ancestor&gt; <br />
                      &nbsp;&nbsp;&lt;Tile size="4"&gt; <br />
                      &nbsp;&nbsp;&nbsp;&nbsp;&lt;!-- 1/3 --&gt; <br />
                      &nbsp;&nbsp;&lt;/Tile&gt; <br />
                      &nbsp;&nbsp;&lt;Tile&gt; <br />
                      &nbsp;&nbsp;&nbsp;&nbsp;&lt;!-- This tile will take the rest: 2/3 --&gt; <br />
                      &nbsp;&nbsp;&lt;/Tile&gt; <br />
                      &lt;/Tile&gt;
                    </Highlight>
                  </Column>
                </Columns>

                <p>If you want to stack tiles vertically, add <code>is-vertical</code>/<code>vertical</code> on the parent tile:</p>

                <Columns className="mb-0">
                  <Column size={6}>
                    <p><strong>HTML</strong></p>
                    <Highlight className="xml">
                      &lt;div class="nab-tile is-ancestor"&gt; <br />
                      &nbsp;&nbsp;&lt;div class="nab-tile is-4 is-vertical"&gt; <br />
                      &nbsp;&nbsp;&nbsp;&nbsp;&lt;div class="nab-tile"&gt; <br />
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;!-- Top tile --&gt; <br />
                      &nbsp;&nbsp;&nbsp;&nbsp;&lt;/div&gt; <br />
                      &nbsp;&nbsp;&nbsp;&nbsp;&lt;div class="nab-tile"&gt; <br />
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;!-- Bottom tile --&gt; <br />
                      &nbsp;&nbsp;&nbsp;&nbsp;&lt;/div&gt; <br />
                      &nbsp;&nbsp;&lt;/div&gt; <br />
                      &nbsp;&nbsp;&lt;div class="nab-tile"&gt; <br />
                      &nbsp;&nbsp;&nbsp;&nbsp;&lt;!-- Tile will take up the whole vertical space --&gt; <br />
                      &nbsp;&nbsp;&lt;/div&gt; <br />
                      &lt;/div&gt;
                    </Highlight>
                  </Column>
                  <Column size={6}>
                    <p><strong>ReactJS</strong></p>
                    <Highlight className="xml">
                      &lt;Tile ancestor&gt; <br />
                      &nbsp;&nbsp;&lt;Tile vertical size="4"&gt; <br />
                      &nbsp;&nbsp;&nbsp;&nbsp;&lt;Tile&gt; <br />
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;!-- Top tile --&gt; <br />
                      &nbsp;&nbsp;&nbsp;&nbsp;&lt;/Tile&gt; <br />
                      &nbsp;&nbsp;&nbsp;&nbsp;&lt;Tile&gt; <br />
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;!-- Bottom tile --&gt; <br />
                      &nbsp;&nbsp;&nbsp;&nbsp;&lt;/Tile&gt; <br />
                      &nbsp;&nbsp;&lt;/Tile&gt; <br />
                      &nbsp;&nbsp;&lt;Tile&gt; <br />
                      &nbsp;&nbsp;&nbsp;&nbsp;&lt;!-- Tile will take up the whole vertical space --&gt; <br />
                      &nbsp;&nbsp;&lt;/Tile&gt; <br />
                      &lt;/Tile&gt;
                    </Highlight>
                  </Column>
                </Columns>

                <ExpandingCodeExample title={Ex2.title} example={Ex2.example} html={Ex2.html} react={Ex2.react} />

                <Divider className="my-10" />

                <h3 className="has-text-size4 font-nabimpact mt-10 mb-1">Nesting requirements</h3>
                
                <div className="nab-message is-danger">
                  <div className="nab-message-header">
                    3 levels deep at least...
                  </div>
                  <div className="nab-message-body">
                    You need at least 3 levels of hierarchy:
                    <Highlight className="xml">
                      nab-tile is-ancestor <br />
                      | <br />
                      └───nab-tile is-parent <br />
                      &nbsp;&nbsp;&nbsp; | <br />
                      &nbsp;&nbsp;&nbsp; └───nab-tile is-child <br />
                    </Highlight>
                  </div>
                </div>

                <div className="nab-message is-success">
                  <div className="nab-message-header">
                    ... but more levels if you want
                  </div>
                  <div className="nab-message-body">
                    You can, however, nest tiles deeper than that, and mix it up!
                    <Highlight className="xml">
                      nab-tile is-ancestor <br/>
                      | <br />
                      ├───nab-tile is-vertical is-8 <br />
                      |   | <br />
                      |   ├───nab-tile <br />
                      |   |   | <br />
                      |   |   ├───nab-tile is-parent is-vertical <br />
                      |   |   |   ├───nab-tile is-child <br />
                      |   |   |   └───nab-tile is-child <br />
                      |   |   | <br />
                      |   |   └───nab-tile is-parent <br />
                      |   |       └───nab-tile is-child <br />
                      |   | <br />
                      |   └───nab-tile is-parent <br />
                      |       └───nab-tile is-child <br />
                      | <br />
                      └───nab-tile is-parent <br />
                      &nbsp;&nbsp;  └───nab-tile is-child
                    </Highlight>
                  </div>
                </div>

                <Divider className="my-10" />

                <h3 className="has-text-size3 font-nabimpact mt-10 mb-1">More examples</h3>
                
                {
                  moreEx.map((mEx, mIndex) => {
                    return (
                      <ExpandingCodeExample key={mIndex} title={mEx.title} example={mEx.example} html={mEx.html} react={mEx.react} />
                    )
                  })
                }

                <Divider className="my-10" />

                <Title size={4} className="font-nabimpact">React props</Title>

                <table className="nab-table is-striped">
                  <thead>
                    <tr>
                      <th className="has-text-weight-semibold px-3 has-text-size-1">Name</th>
                      <th className="has-text-weight-semibold px-3 has-text-size-1">Default</th>
                      <th className="has-text-weight-semibold px-3 has-text-size-1">Type</th>
                      <th className="has-text-weight-semibold px-3 has-text-size-1">Description</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td className="has-text-weight-bold">ancestor</td>
                      <td>false</td>
                      <td>Boolean</td>
                      <td>Tile that wraps all other tiles.</td>
                    </tr>
                    <tr>
                      <td className="has-text-weight-bold">parent</td>
                      <td>false</td>
                      <td>Boolean</td>
                      <td>Self explanatory - parent tile</td>
                    </tr>
                    <tr>
                      <td className="has-text-weight-bold">child</td>
                      <td>false</td>
                      <td>Boolean</td>
                      <td>Self explanatory - child tile</td>
                    </tr>
                    <tr>
                      <td className="has-text-weight-bold">vertical</td>
                      <td>false</td>
                      <td>Boolean</td>
                      <td>Stack tiles vertically</td>
                    </tr>
                  </tbody>
                </table>

              </Column>
            </Columns>
          </Container>
        </Section>
      </div>
    )
  }
}

const Ex1 = {
  title: '# Usage',
  example: `
<div class="nab-tile is-ancestor">
  <div class="nab-tile is-vertical is-8">
    <div class="nab-tile">
      <div class="nab-tile is-vertical">
        <div class="nab-tile is-parent is-vertical">
          <article class="nab-tile is-child nab-card is-flat has-bg-blue-grey lighten-4 pa-4">
            <h3 class="nab-title is-3">Vertical...</h3>
            <p class="nab-subtitle">Top tile</p>
          </article>
        </div>
        <div class="nab-tile is-parent">
          <article class="nab-tile is-child nab-card is-flat has-bg-warning pa-4 has-elevation-10">
            <h3 class="nab-title has-text-white is-3">... tile</h3>
            <p class="nab-subtitle has-text-white">Bottom tile</p>
          </article>
        </div>
      </div>
      <div class="nab-tile is-parent is-vertical">
        <article class="nab-tile is-child nab-card is-flat has-bg-info has-text-white pa-4">
          <h3 class="nab-title has-text-white is-3">Middle tile</h3>
          <p class="nab-subtitle has-text-white">Evenly spaced</p>
        </article>
      </div>
    </div>
    <div class="nab-tile is-parent">
      <article class="nab-tile is-child nab-card is-flat has-bg-danger has-text-white pa-4">
        <h3 class="nab-title has-text-white is-3">Wide tile</h3>
        <p class="nab-subtitle has-text-white">Aligned with the right tile</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
      </article>
    </div>
  </div>
  <div class="nab-tile is-parent">
    <article class="nab-tile is-child nab-card is-flat has-bg-success lighten-1 has-text-white pa-4">
      <h3 class="nab-title has-text-white is-3">Tall tile</h3>
      <p class="nab-subtitle has-text-white">With even more content</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in augue id sem imperdiet gravida non sed justo. Ut a semper diam. Morbi ut risus id magna lobortis cursus. Donec vel nunc quis felis posuere dictum. Quisque pellentesque orci non eros imperdiet, sit amet sollicitudin quam posuere.</p>
      <p>Sed accumsan dui rutrum arcu gravida, in faucibus libero suscipit. Sed in elit at nibh luctus accumsan. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. </p>
      <p>Fusce in scelerisque mi, maximus varius sem. Integer efficitur, lacus eu gravida semper, est lacus tempus tortor, ac laoreet lacus nibh quis eros. Ut feugiat aliquam enim. In non dolor vitae enim gravida vehicula. Duis erat sem, euismod eu laoreet at, gravida at tellus. Mauris semper velit neque, id aliquam ipsum iaculis sed. Nam fermentum arcu mi, ac lobortis tellus gravida lobortis. Fusce mollis orci sit amet luctus rutrum.</p>
    </article>
  </div>
</div>
`,
  html: `
<div class="nab-tile is-ancestor">
  <div class="nab-tile is-vertical is-8">
    <div class="nab-tile">
      <div class="nab-tile is-vertical">
        <div class="nab-tile is-parent is-vertical">
          <article class="nab-tile is-child nab-card">Vertical...</article>
        </div>
        <div class="nab-tile is-parent">
          <article class="nab-tile is-child nab-card">... tile</article>
        </div>
      </div>
      <div class="nab-tile is-parent is-vertical">
        <article class="nab-tile is-child nab-card">Middle tile</article>
      </div>
    </div>
    <div class="nab-tile is-parent">
      <article class="nab-tile is-child nab-card">Wide tile</article>
    </div>
  </div>
  <div class="nab-tile is-parent">
    <article class="nab-tile is-child nab-card">Tall tile</article>
  </div>
</div>

`,
  react: `
import { Tile, Card } from '@shaper/react'

const MyComponent = (props) => (
  <Tile ancestor>
    <Tile vertical size="8">
      <Tile>
        <Tile vertical>
          <Tile parent vertical>
            <Tile child>
              <Card>Vertical...</Card>
            </Tile>
          </Tile>
          <Tile parent>
            <Tile child>
              <Card>... tile</Card>
            </Tile>
          </Tile>
        </Tile>
        <Tile parent vertical>
          <Tile child>
            <Card>Middle tile</Card>
          </Tile>
        </Tile>
      </Tile>
      <Tile parent>
        <Tile child>
          <Card>Wide tile</Card>
        </Tile>
      </Tile>
    </Tile>
    <Tile parent>
      <Tile child>
        <Card>Tall tile</Card>
      </Tile>
    </Tile>
  </Tile>
)
  `
}

const Ex2 = {
  title: '# Putting them together',
  example: `
<p>As soon as you want to add <strong>content</strong> to a tile, just:</p>
<ul class="mb-10">
  <li>add <em>any</em> class you want, like <code>nab-card</code>/<code>Card</code></li>
  <li>add the <code>is-child</code>/<code>child</code> modifier on the tile</li>
  <li>add the <code>is-parent</code>/<code>parent</code> modifier on the <em>parent</em> tile</li>
</ul>
<div class="nab-tile is-ancestor">
  <div class="nab-tile is-4 is-vertical is-parent">
    <div class="nab-tile is-child nab-box">
      <p class="nab-title is-4">One</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
    </div>
    <div class="nab-tile is-child nab-box">
      <p class="nab-title is-4">Two</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
    </div>
  </div>
  <div class="nab-tile is-parent">
    <div class="nab-tile is-child nab-box">
      <p class="nab-title is-4">Three</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam semper diam at erat pulvinar, at pulvinar felis blandit. Vestibulum volutpat tellus diam, consequat gravida libero rhoncus ut. Morbi maximus, leo sit amet vehicula eleifend, nunc dui porta orci, quis semper odio felis ut quam.</p>
      <p>Suspendisse varius ligula in molestie lacinia. Maecenas varius eget ligula a sagittis. Pellentesque interdum, nisl nec interdum maximus, augue diam porttitor lorem, et sollicitudin felis neque sit amet erat. Maecenas imperdiet felis nisi, fringilla luctus felis hendrerit sit amet. Aenean vitae gravida diam, finibus dignissim turpis. Sed eget varius ligula, at volutpat tortor.</p>
      <p>Integer sollicitudin, tortor a mattis commodo, velit urna rhoncus erat, vitae congue lectus dolor consequat libero. Donec leo ligula, maximus et pellentesque sed, gravida a metus. Cras ullamcorper a nunc ac porta. Aliquam ut aliquet lacus, quis faucibus libero. Quisque non semper leo.</p>
    </div>
  </div>
</div>
  `,
  html: `
<div class="nab-tile is-ancestor">
  <div class="nab-tile is-4 is-vertical is-parent">
    <div class="nab-tile is-child nab-card">
      One
      ...
    </div>
    <div class="nab-tile is-child nab-card">
      Two
      ...
    </div>
  </div>
  <div class="nab-tile is-parent">
    <div class="nab-tile is-child nab-card">
      Three
      ...
    </div>
  </div>
</div>
  `,
  react: `
<Tile ancestor>
  <Tile parent vertical size="4">
    <Tile child>
      <Card>
        One
        ...
      </Card>
    </Tile>
    <Tile child>
      <Card>
        Two
        ...
      </Card>
    </Tile>
  </Tile>
  <Tile parent>
    <Tile child>
      <Card>
        Three
        ...
      </Card>
    </Tile>
  </Tile>
</Tile>
  `
}

const moreEx = [
  {
    title: '3 columns',
    example: `
<div class="nab-tile is-ancestor">
  <div class="nab-tile is-parent">
    <article class="nab-tile is-child nab-box">
      <p class="nab-title is-4">Hello World</p>
      <p class="nab-subtitle">What is up?</p>
    </article>
  </div>
  <div class="nab-tile is-parent">
    <article class="nab-tile is-child nab-box">
      <p class="nab-title is-4">Foo</p>
      <p class="nab-subtitle">Bar</p>
    </article>
  </div>
  <div class="nab-tile is-parent">
    <article class="nab-tile is-child nab-box">
      <p class="nab-title is-4">Third column</p>
      <p class="nab-subtitle">With some content</p>
      <div class="content">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
      </div>
    </article>
  </div>
</div>
<div class="nab-tile is-ancestor">
  <div class="nab-tile is-vertical is-8">
    <div class="nab-tile">
      <div class="nab-tile is-parent is-vertical">
        <article class="nab-tile is-child nab-box">
          <p class="nab-title is-4">Vertical tiles</p>
          <p class="nab-subtitle">Top nab-box</p>
        </article>
        <article class="nab-tile is-child nab-box">
          <p class="nab-title is-4">Vertical tiles</p>
          <p class="nab-subtitle">Bottom nab-box</p>
        </article>
      </div>
      <div class="nab-tile is-parent">
        <article class="nab-tile is-child nab-box">
          <p class="nab-title is-4">Middle box</p>
          <p class="nab-subtitle">Subtitle</p>
        </article>
      </div>
    </div>
    <div class="nab-tile is-parent">
      <article class="nab-tile is-child nab-box">
        <p class="nab-title is-4">Wide column</p>
        <p class="nab-subtitle">Aligned with the right column</p>
        <div class="content">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
        </div>
      </article>
    </div>
  </div>
  <div class="nab-tile is-parent">
    <article class="nab-tile is-child nab-box">
      <div class="content">
        <p class="nab-title is-4">Tall column</p>
        <p class="nab-subtitle">With even more content</p>
        <div class="content">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam semper diam at erat pulvinar, at pulvinar felis blandit. Vestibulum volutpat tellus diam, consequat gravida libero rhoncus ut. Morbi maximus, leo sit amet vehicula eleifend, nunc dui porta orci, quis semper odio felis ut quam.</p>
          <p>Suspendisse varius ligula in molestie lacinia. Maecenas varius eget ligula a sagittis. Pellentesque interdum, nisl nec interdum maximus, augue diam porttitor lorem, et sollicitudin felis neque sit amet erat. Maecenas imperdiet felis nisi, fringilla luctus felis hendrerit sit amet. Aenean vitae gravida diam, finibus dignissim turpis. Sed eget varius ligula, at volutpat tortor.</p>
          <p>Integer sollicitudin, tortor a mattis commodo, velit urna rhoncus erat, vitae congue lectus dolor consequat libero. Donec leo ligula, maximus et pellentesque sed, gravida a metus. Cras ullamcorper a nunc ac porta. Aliquam ut aliquet lacus, quis faucibus libero. Quisque non semper leo.</p>
        </div>
      </div>
    </article>
  </div>
</div>
<div class="nab-tile is-ancestor">
  <div class="nab-tile is-parent">
    <article class="nab-tile is-child nab-box">
      <p class="nab-title is-4">Side column</p>
      <p class="nab-subtitle">With some content</p>
      <div class="content">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
      </div>
    </article>
  </div>
  <div class="nab-tile is-parent is-8">
    <article class="nab-tile is-child nab-box">
      <p class="nab-title is-4">Main column</p>
      <p class="nab-subtitle">With some content</p>
      <div class="content">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
      </div>
    </article>
  </div>
</div>
    `,
    html: `
<div class="nab-tile is-ancestor">
  <div class="nab-tile is-parent">
    <article class="nab-tile is-child nab-card">
      Hello World
    </article>
  </div>
  <div class="nab-tile is-parent">
    <article class="nab-tile is-child nab-card">
      Foo
    </article>
  </div>
  <div class="nab-tile is-parent">
    <article class="nab-tile is-child nab-card">
      Third column
    </article>
  </div>
</div>
<div class="nab-tile is-ancestor">
  <div class="nab-tile is-vertical is-8">
    <div class="nab-tile">
      <div class="nab-tile is-parent is-vertical">
        <article class="nab-tile is-child nab-card">
          Vertical tiles
        </article>
        <article class="nab-tile is-child nab-card">
          Vertical tiles
        </article>
      </div>
      <div class="nab-tile is-parent">
        <article class="nab-tile is-child nab-card">
          Middle box
        </article>
      </div>
    </div>
    <div class="nab-tile is-parent">
      <article class="nab-tile is-child nab-card">
        Wide column
      </article>
    </div>
  </div>
  <div class="nab-tile is-parent">
    <article class="nab-tile is-child nab-card">
      Tall column
    </article>
  </div>
</div>
<div class="nab-tile is-ancestor">
  <div class="nab-tile is-parent">
    <article class="nab-tile is-child nab-card">
      Side column
    </article>
  </div>
  <div class="nab-tile is-parent is-8">
    <article class="nab-tile is-child nab-card">
      Main column
    </article>
  </div>
</div>
    `,
    react: `
<Tile ancestor>
  <Tile parent>
    <Tile child>
      <Card>
        Hello World
      </Card>
    </Tile>
  </Tile>
  <Tile parent>
    <Tile child>
      <Card>
        Foo
      </Card>
    </Tile>
  </Tile>
  <Tile parent>
    <Tile child>
      <Card>
        Third column
      </Card>
    </Tile>
  </Tile>
</Tile>
<Tile ancestor>
  <Tile vertical size="8">
    <Tile>
      <Tile parent vertical>
        <Tile child>
          <Card>
            Vertical tiles
          </Card>
        </Tile>
        <Tile child>
          <Card>
            Vertical tiles
          </Card>
        </Tile>
      </Tile>
      <Tile parent>
        <Tile child>
          <Card>
            Middle box
          </Card>
        </Tile>
      </Tile>
    </Tile>
    <Tile parent>
      <Tile child>
        <Card>
          Wide column
        </Card>
      </Tile>
    </Tile>
  </Tile>
  <Tile parent>
    <Tile child>
      <Card>
        Tall column
      </Card>
    </Tile>
  </Tile>
</Tile>
<Tile ancestor>
  <Tile parent>
    <Tile child>
      <Card>
        Side column
      </Card>
    </Tile>
  </Tile>
  <Tile parent size="8">
    <Tile child>
      <Card>
        Main column
      </Card>
    </Tile>
  </Tile>
</Tile>
    `
  },
  {
    title: '4 columns',
    example: `
<div class="nab-tile is-ancestor">
  <div class="nab-tile is-parent">
    <article class="nab-tile is-child nab-box">
      <p class="nab-title is-4">One</p>
      <p class="nab-subtitle">Subtitle</p>
    </article>
  </div>
  <div class="nab-tile is-parent">
    <article class="nab-tile is-child nab-box">
      <p class="nab-title is-4">Two</p>
      <p class="nab-subtitle">Subtitle</p>
    </article>
  </div>
  <div class="nab-tile is-parent">
    <article class="nab-tile is-child nab-box">
      <p class="nab-title is-4">Three</p>
      <p class="nab-subtitle">Subtitle</p>
    </article>
  </div>
  <div class="nab-tile is-parent">
    <article class="nab-tile is-child nab-box">
      <p class="nab-title is-4">Four</p>
      <p class="nab-subtitle">Subtitle</p>
    </article>
  </div>
</div>
<div class="nab-tile is-ancestor">
  <div class="nab-tile is-vertical is-9">
    <div class="nab-tile">
      <div class="nab-tile is-parent">
        <article class="nab-tile is-child nab-box">
          <p class="nab-title is-4">Five</p>
          <p class="nab-subtitle">Subtitle</p>
          <div class="content">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam semper diam at erat pulvinar, at pulvinar felis blandit. Vestibulum volutpat tellus diam, consequat gravida libero rhoncus ut. Morbi maximus, leo sit amet vehicula eleifend, nunc dui porta orci, quis semper odio felis ut quam.</p>
          </div>
        </article>
      </div>
      <div class="nab-tile is-8 is-vertical">
        <div class="nab-tile">
          <div class="nab-tile is-parent">
            <article class="nab-tile is-child nab-box">
              <p class="nab-title is-4">Six</p>
              <p class="nab-subtitle">Subtitle</p>
            </article>
          </div>
          <div class="nab-tile is-parent">
            <article class="nab-tile is-child nab-box">
              <p class="nab-title is-4">Seven</p>
              <p class="nab-subtitle">Subtitle</p>
            </article>
          </div>
        </div>
        <div class="nab-tile is-parent">
          <article class="nab-tile is-child nab-box">
            <p class="nab-title is-4">Eight</p>
            <p class="nab-subtitle">Subtitle</p>
          </article>
        </div>
      </div>
    </div>
    <div class="nab-tile">
      <div class="nab-tile is-8 is-parent">
        <article class="nab-tile is-child nab-box">
          <p class="nab-title is-4">Nine</p>
          <p class="nab-subtitle">Subtitle</p>
          <div class="content">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
          </div>
        </article>
      </div>
      <div class="nab-tile is-parent">
        <article class="nab-tile is-child nab-box">
          <p class="nab-title is-4">Ten</p>
          <p class="nab-subtitle">Subtitle</p>
          <div class="content">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
          </div>
        </article>
      </div>
    </div>
  </div>
  <div class="nab-tile is-parent">
    <article class="nab-tile is-child nab-box">
      <div class="content">
        <p class="nab-title is-4">Eleven</p>
        <p class="nab-subtitle">Subtitle</p>
        <div class="content">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam semper diam at erat pulvinar, at pulvinar felis blandit. Vestibulum volutpat tellus diam, consequat gravida libero rhoncus ut. Morbi maximus, leo sit amet vehicula eleifend, nunc dui porta orci, quis semper odio felis ut quam.</p>
          <p>Integer sollicitudin, tortor a mattis commodo, velit urna rhoncus erat, vitae congue lectus dolor consequat libero. Donec leo ligula, maximus et pellentesque sed, gravida a metus. Cras ullamcorper a nunc ac porta. Aliquam ut aliquet lacus, quis faucibus libero. Quisque non semper leo.</p>
        </div>
      </div>
    </article>
  </div>
</div>
<div class="nab-tile is-ancestor">
  <div class="nab-tile is-parent">
    <article class="nab-tile is-child nab-box">
      <p class="nab-title is-4">Twelve</p>
      <p class="nab-subtitle">Subtitle</p>
      <div class="content">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut.</p>
      </div>
    </article>
  </div>
  <div class="nab-tile is-parent is-6">
    <article class="nab-tile is-child nab-box">
      <p class="nab-title is-4">Thirteen</p>
      <p class="nab-subtitle">Subtitle</p>
      <div class="content">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
      </div>
    </article>
  </div>
  <div class="nab-tile is-parent">
    <article class="nab-tile is-child nab-box">
      <p class="nab-title is-4">Fourteen</p>
      <p class="nab-subtitle">Subtitle</p>
      <div class="content">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut.</p>
      </div>
    </article>
  </div>
</div>
    `,
    html: `
<div class="nab-tile is-ancestor">
  <div class="nab-tile is-parent">
    <article class="nab-tile is-child nab-card">
      One
    </article>
  </div>
  <div class="nab-tile is-parent">
    <article class="nab-tile is-child nab-card">
      Two
    </article>
  </div>
  <div class="nab-tile is-parent">
    <article class="nab-tile is-child nab-card">
      Three
    </article>
  </div>
  <div class="nab-tile is-parent">
    <article class="nab-tile is-child nab-card">
      Four
    </article>
  </div>
</div>
<div class="nab-tile is-ancestor">
  <div class="nab-tile is-vertical is-9">
    <div class="nab-tile">
      <div class="nab-tile is-parent">
        <article class="nab-tile is-child nab-card">
          Five
        </article>
      </div>
      <div class="nab-tile is-8 is-vertical">
        <div class="nab-tile">
          <div class="nab-tile is-parent">
            <article class="nab-tile is-child nab-card">
              Six
            </article>
          </div>
          <div class="nab-tile is-parent">
            <article class="nab-tile is-child nab-card">
              Seven
            </article>
          </div>
        </div>
        <div class="nab-tile is-parent">
          <article class="nab-tile is-child nab-card">
            Eight
          </article>
        </div>
      </div>
    </div>
    <div class="nab-tile">
      <div class="nab-tile is-8 is-parent">
        <article class="nab-tile is-child nab-card">
          Nine
        </article>
      </div>
      <div class="nab-tile is-parent">
        <article class="nab-tile is-child nab-card">
          Ten
        </article>
      </div>
    </div>
  </div>
  <div class="nab-tile is-parent">
    <article class="nab-tile is-child nab-card">
      Eleven
    </article>
  </div>
</div>
<div class="nab-tile is-ancestor">
  <div class="nab-tile is-parent">
    <article class="nab-tile is-child nab-card">
      Twelve
    </article>
  </div>
  <div class="nab-tile is-parent is-6">
    <article class="nab-tile is-child nab-card">
      Thirteen
    </article>
  </div>
  <div class="nab-tile is-parent">
    <article class="nab-tile is-child nab-card">
      Fourteen
    </article>
  </div>
</div>
    `,
    react: `
<Tile ancestor>
  <Tile parent>
    <Tile child>
      <Card>
        One
      </Card>
    </Tile>
  </Tile>
  <Tile parent>
    <Tile child>
      <Card>
        Two
      </Card>
    </Tile>
  </Tile>
  <Tile parent>
    <Tile child>
      <Card>
        Three
      </Card>
    </Tile>
  </Tile>
  <Tile parent>
    <Tile child>
      <Card>
        Four
      </Card>
    </Tile>
  </Tile>
</Tile>
<Tile ancestor>
  <Tile vertical size="9">
    <Tile>
      <Tile parent>
        <Tile child>
          <Card>
            Five
          </Card>
        </Tile>
      </Tile>
      <Tile vertical size="8">
        <Tile>
          <Tile parent>
            <Tile child>
              <Card>
                Six
              </Card>
            </Tile>
          </Tile>
          <Tile parent>
            <Tile child>
              <Card>
                Seven
              </Card>
            </Tile>
          </Tile>
        </Tile>
        <Tile parent>
          <Tile child>
            <Card>
              Eight
            </Card>
          </Tile>
        </Tile>
      </Tile>
    </Tile>
    <Tile>
      <Tile parent>
        <Tile child>
          <Card>
            Nine
          </Card>
        </Tile>
      </Tile>
      <Tile parent>
        <Tile child>
          <Card>
            Ten
          </Card>
        </Tile>
      </Tile>
    </Tile>
  </Tile>
  <Tile parent>
    <Tile child>
      <Card>
        Eleven
      </Card>
    </Tile>
  </Tile>
</Tile>
<Tile ancestor>
  <Tile parent>
    <Tile child>
      <Card>
        Twelve
      </Card>
    </Tile>
  </Tile>
  <Tile parent size="6">
    <Tile child>
      <Card>
        Thirteen
      </Card>
    </Tile>
  </Tile>
  <Tile parent>
    <Tile child>
      <Card>
        Fourteen
      </Card>
    </Tile>
  </Tile>
</Tile>
    `
  }
]