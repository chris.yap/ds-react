import React, { Component } from 'react';
import PageHeader from '../../components/PageHeader';
import {
  Alert,
  Box,
  Column,
  Columns,
  Container,
  Divider,
  Section
  // , Subtitle, Title, Hero,
} from 'shaper-react';
import './PageElevation.css';

export default class PageElevation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      elevation: 8,
      elevationClass: 'has-elevation-8'
    };
  }
  changeElevation(event) {
    this.setState({
      elevation: event.target.value,
      elevationClass: 'has-elevation-' + event.target.value
    });
  }
  render() {
    return (
      <div id="start">
        <PageHeader category="layout" title="Elevation">
          The elevation helpers allow you to control relative depth, or
          distance, between two surfaces along the <strong>z-axis</strong>.
        </PageHeader>

        <Divider className="my-0" />

        <Section>
          <Container>
            <Columns mobile>
              <Column className="is-12-mobile is-10-desktop is-offset-1-desktop">
                <p>
                  There is a total of <strong>25</strong> elevation levels. You
                  can set an element's elevation by using the class{' '}
                  <code>has-elevation-[n]</code>, where <code>n</code> is a
                  integer between <strong>0-24</strong> corresponding to the
                  desired elevation.
                </p>

                <Columns>
                  <Column className="is-8-mobile is-6-desktop is-offset-2-mobile is-offset-3-desktop has-text-centered">
                    <input
                      type="range"
                      min="0"
                      max="24"
                      className="slider my-5"
                      value={this.state.elevation}
                      onChange={this.changeElevation.bind(this)}
                    />

                    <Box className={this.state.elevationClass}>
                      <code>
                        &#x3C;Box className=&#x22;has-elevation-
                        {this.state.elevation}&#x22; /&#x3E;
                      </code>
                    </Box>

                    <br />

                    <Alert
                      info
                      inverted
                      value={true}
                      className="mb-4 has-text-left"
                    >
                      For components that has been converted to{' '}
                      <strong>styled-component</strong> component,{' '}
                      <strong>boxShadow</strong> prop could be used instead as
                      shown below.
                    </Alert>

                    <Box boxShadow={this.state.elevation}>
                      <code>
                        &#x3C;Box boxShadow=&#x22;{this.state.elevation}&#x22;
                        /&#x3E;
                      </code>
                    </Box>
                  </Column>
                </Columns>
              </Column>
            </Columns>
          </Container>
        </Section>
      </div>
    );
  }
}
