import React from 'react';
import PageHeader from '../../components/PageHeader';
import Demo from '../../components/Demo';
import PropsTable from '../../components/PropsTable';
import {
  Column,
  Columns,
  Container,
  Divider,
  Hero,
  Section,
  Subtitle,
  Tag,
  Title
} from 'shaper-react';

const PageHero = () => {
  const [index, updateIndex] = React.useState(0);
  return (
    <div id="start">
      <PageHeader category="layout" title="Heroes">
        <strong>Hero banner</strong> to showcase something <br />
        <Tag small>styled-component</Tag>
      </PageHeader>

      <Divider className="my-0" />

      <Section>
        <Container>
          <Columns mobile>
            <Column mobile={12} desktop={10} desktopOffset={1}>
              <Demo examples={Examples1} updateIndex={updateIndex}>
                {index === 0 ? (
                  <Hero light>
                    <Container>
                      <Title>Hero title</Title>
                      <Subtitle>Hero subtitle</Subtitle>
                    </Container>
                  </Hero>
                ) : index === 1 ? (
                  <>
                    <Hero primary>
                      <Container>
                        <Title className="has-text-white">Primary title</Title>
                        <Subtitle className="has-text-white">
                          Primary subtitle
                        </Subtitle>
                      </Container>
                    </Hero>
                    <Hero secondary>
                      <Container>
                        <Title className="has-text-white">
                          Secondary title
                        </Title>
                        <Subtitle className="has-text-white">
                          Secondary subtitle
                        </Subtitle>
                      </Container>
                    </Hero>
                    <Hero success>
                      <Container>
                        <Title className="has-text-white">Success title</Title>
                        <Subtitle className="has-text-white">
                          Success subtitle
                        </Subtitle>
                      </Container>
                    </Hero>
                    <Hero info>
                      <Container>
                        <Title className="has-text-white">Info title</Title>
                        <Subtitle className="has-text-white">
                          Info subtitle
                        </Subtitle>
                      </Container>
                    </Hero>
                    <Hero warning>
                      <Container>
                        <Title className="has-text-white">Warning title</Title>
                        <Subtitle className="has-text-white">
                          Warning subtitle
                        </Subtitle>
                      </Container>
                    </Hero>
                    <Hero danger>
                      <Container>
                        <Title className="has-text-white">Danger title</Title>
                        <Subtitle className="has-text-white">
                          Danger subtitle
                        </Subtitle>
                      </Container>
                    </Hero>
                    <Hero light>
                      <Container>
                        <Title>Light title</Title>
                        <Subtitle>Light subtitle</Subtitle>
                      </Container>
                    </Hero>
                    <Hero dark>
                      <Container>
                        <Title className="has-text-white">Dark title</Title>
                        <Subtitle className="has-text-white">
                          Dark subtitle
                        </Subtitle>
                      </Container>
                    </Hero>
                  </>
                ) : index === 2 ? (
                  <>
                    <Hero gradient primary>
                      <Container>
                        <Title className="has-text-white">Primary title</Title>
                        <Subtitle className="has-text-white">
                          Primary subtitle
                        </Subtitle>
                      </Container>
                    </Hero>
                    <Hero gradient secondary>
                      <Container>
                        <Title className="has-text-white">
                          Secondary title
                        </Title>
                        <Subtitle className="has-text-white">
                          Secondary subtitle
                        </Subtitle>
                      </Container>
                    </Hero>
                    <Hero gradient success>
                      <Container>
                        <Title className="has-text-white">Success title</Title>
                        <Subtitle className="has-text-white">
                          Success subtitle
                        </Subtitle>
                      </Container>
                    </Hero>
                    <Hero gradient info>
                      <Container>
                        <Title className="has-text-white">Info title</Title>
                        <Subtitle className="has-text-white">
                          Info subtitle
                        </Subtitle>
                      </Container>
                    </Hero>
                    <Hero gradient warning>
                      <Container>
                        <Title className="has-text-white">Warning title</Title>
                        <Subtitle className="has-text-white">
                          Warning subtitle
                        </Subtitle>
                      </Container>
                    </Hero>
                    <Hero gradient danger>
                      <Container>
                        <Title className="has-text-white">Danger title</Title>
                        <Subtitle className="has-text-white">
                          Danger subtitle
                        </Subtitle>
                      </Container>
                    </Hero>
                    <Hero gradient light>
                      <Container>
                        <Title>Light title</Title>
                        <Subtitle>Light subtitle</Subtitle>
                      </Container>
                    </Hero>
                    <Hero gradient dark>
                      <Container>
                        <Title className="has-text-white">Dark title</Title>
                        <Subtitle className="has-text-white">
                          Dark subtitle
                        </Subtitle>
                      </Container>
                    </Hero>
                  </>
                ) : index === 3 ? (
                  <Hero
                    secondary
                    gradient
                    backgroundImage="url(/images/hero-home.png)"
                    backgroundSize="cover">
                    <Container>
                      <Title className="has-text-white">Hero title</Title>
                      <Subtitle className="has-text-white">
                        Hero subtitle
                      </Subtitle>
                    </Container>
                  </Hero>
                ) : (
                  <>
                    <Hero light medium>
                      <Container>
                        <Title>Hero title</Title>
                        <Subtitle>Hero subtitle</Subtitle>
                      </Container>
                    </Hero>
                    <Hero dark large>
                      <Container>
                        <Title className="has-text-white">Hero title</Title>
                        <Subtitle className="has-text-white">
                          Hero subtitle
                        </Subtitle>
                      </Container>
                    </Hero>
                    <Hero secondary fullHeight>
                      <Container>
                        <Title className="has-text-white">Hero title</Title>
                        <Subtitle className="has-text-white">
                          Hero subtitle
                        </Subtitle>
                      </Container>
                    </Hero>
                  </>
                )}
              </Demo>

              <PropsTable data={Data} />
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};

export default PageHero;

const Data = [
  {
    items: [
      {
        name: `primary,
            <br /> secondary,
            <br /> success,
            <br /> info,
            <br /> warning,
            <br /> danger,
            <br /> light,
            <br /> dark`,
        default: 'undefined',
        type: 'Boolean',
        desc: 'Color style'
      },
      {
        name: 'gradient',
        default: 'undefined',
        type: 'Boolean',
        desc: 'Set gradient. To be used with color props'
      },
      {
        name: `medium,
            <br /> large,
            <br /> fullheight`,
        default: 'undefined',
        type: 'Boolean',
        desc: 'Set size'
      },
      {
        name: 'backgroundImage',
        default: 'undefined',
        type: 'string',
        desc: 'Path to background image. Eg. "url(path..)"'
      },
      {
        name: 'backgroundSize',
        default: 'undefined',
        type: 'string',
        desc: 'CSS style'
      },
      {
        name: 'backgroundPosition',
        default: 'undefined',
        type: 'string',
        desc: 'CSS style'
      },
      {
        name: 'backgroundRepeat',
        default: 'undefined',
        type: 'string',
        desc: 'CSS style'
      }
    ]
  }
];

const Examples1 = [
  {
    title: '# Usage',
    react: `
<Hero light>
  <Container>
    <Title>Hero title</Title>
    <Subtitle>Hero subtitle</Subtitle>
  </Container>
</Hero>
    `
  },
  {
    title: '# Colours',
    react: `
<Hero primary>
  <Container>
    <Title>Hero title</Title>
    <Subtitle>Hero subtitle</Subtitle>
  </Container>
</Hero>
<Hero secondary>
  <Container>
    <Title>Hero title</Title>
    <Subtitle>Hero subtitle</Subtitle>
  </Container>
</Hero>
<Hero info>
  <Container>
    <Title>Hero title</Title>
    <Subtitle>Hero subtitle</Subtitle>
  </Container>
</Hero>
<Hero warning>
  <Container>
    <Title>Hero title</Title>
    <Subtitle>Hero subtitle</Subtitle>
  </Container>
</Hero>
<Hero danger>
  <Container>
    <Title>Hero title</Title>
    <Subtitle>Hero subtitle</Subtitle>
  </Container>
</Hero>
<Hero light>
  <Container>
    <Title>Hero title</Title>
    <Subtitle>Hero subtitle</Subtitle>
  </Container>
</Hero>
<Hero dark>
  <Container>
    <Title>Hero title</Title>
    <Subtitle>Hero subtitle</Subtitle>
  </Container>
</Hero>
    `
  },
  {
    title: '# Gradients',
    react: `
<Hero gradient primary>
  <Container>
    <Title>Hero title</Title>
    <Subtitle>Hero subtitle</Subtitle>
  </Container>
</Hero>
<Hero gradient secondary>
  <Container>
    <Title>Hero title</Title>
    <Subtitle>Hero subtitle</Subtitle>
  </Container>
</Hero>
<Hero gradient info>
  <Container>
    <Title>Hero title</Title>
    <Subtitle>Hero subtitle</Subtitle>
  </Container>
</Hero>
<Hero gradient warning>
  <Container>
    <Title>Hero title</Title>
    <Subtitle>Hero subtitle</Subtitle>
  </Container>
</Hero>
<Hero gradient danger>
  <Container>
    <Title>Hero title</Title>
    <Subtitle>Hero subtitle</Subtitle>
  </Container>
</Hero>
<Hero gradient light>
  <Container>
    <Title>Hero title</Title>
    <Subtitle>Hero subtitle</Subtitle>
  </Container>
</Hero>
<Hero gradient dark>
  <Container>
    <Title>Hero title</Title>
    <Subtitle>Hero subtitle</Subtitle>
  </Container>
</Hero>
)
    `
  },
  {
    title: '# Background image',
    react: `
<Hero secondary backgroundImage="url(/images/hero-home.png)" backgroundSize="cover">
  <Container>
    <Title>Hero title</Title>
    <Subtitle>Hero subtitle</Subtitle>
  </Container>
</Hero>
    `
  },
  {
    title: '# Sizes',
    react: `
<Hero light medium>
  <Container>
    <Title>Hero title</Title>
    <Subtitle>Hero subtitle</Subtitle>
  </Container>
</Hero>
<Hero dark large>
  <Container>
    <Title>Hero title</Title>
    <Subtitle>Hero subtitle</Subtitle>
  </Container>
</Hero>
<Hero secondary fullHeight>
  <Container>
    <Title>Hero title</Title>
    <Subtitle>Hero subtitle</Subtitle>
  </Container>
</Hero>
    `
  }
];
