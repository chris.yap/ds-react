import React, { Component } from 'react';
import PageHeader from '../../components/PageHeader';
import {
  Alert,
  Column,
  Columns,
  Container,
  Divider,
  Section,
  Tab,
  Tabs,
  Table,
  TableRow as Row,
  TableCell as Cell
} from 'shaper-react';
import Highlight from 'react-highlight';

export default class PageSpacing extends Component {
  render() {
    return (
      <div id="start">
        <PageHeader category="layout" title="Spacing">
          Spacing helpers are useful for modifying the padding and margin of an
          element.
        </PageHeader>

        <Divider className="my-0" />

        <Section>
          <Container>
            <Columns mobile>
              <Column className="is-12-mobile is-10-desktop is-offset-1-desktop">
                <p>
                  <strong>Margin</strong> or <strong>Padding</strong> can be set
                  with either classes or props.
                </p>

                <Alert info inverted value={true} className="mb-4">
                  Components that has been converted to{' '}
                  <strong>styled-components</strong> are able to use props to
                  set padding and margin.
                </Alert>

                <Tabs>
                  <Tab label="Props">
                    <Table
                      striped
                      headers={PropsHeaders}
                      data={Data}
                      tableRow={TRow}
                    />

                    <h3 className="has-text-size4 font-nabimpact mt-10 mb-1">
                      Sizes
                    </h3>
                    <div className="nab-content">
                      <code>[0, 4, 8, 12, 16, 20, 32, 64, 128, 256, 512]</code>
                    </div>
                  </Tab>
                  <Tab label="Classes">
                    <h3 className="has-text-size4 font-nabimpact mb-1">Type</h3>
                    <div className="nab-content">
                      <ul>
                        <li>
                          <code>m</code> - to set margin
                        </li>
                        <li>
                          <code>p</code> - to set padding
                        </li>
                      </ul>
                    </div>

                    <h3 className="has-text-size4 font-nabimpact mt-10 mb-1">
                      Direction
                    </h3>
                    <div className="nab-content">
                      <ul>
                        <li>
                          <code>t</code> - to set top
                        </li>
                        <li>
                          <code>r</code> - to set right
                        </li>
                        <li>
                          <code>b</code> - to set bottom
                        </li>
                        <li>
                          <code>l</code> - to set left
                        </li>
                        <li>
                          <code>x</code> - to set x-axis
                        </li>
                        <li>
                          <code>y</code> - to set y-axis
                        </li>
                        <li>
                          <code>a</code> - to set all axis
                        </li>
                      </ul>
                    </div>

                    <h3 className="has-text-size4 font-nabimpact mt-10 mb-1">
                      Size
                    </h3>
                    <div className="nab-content">
                      <code>[0, 4, 8, 12, 16, 20, 32, 64, 128, 256, 512]</code>
                    </div>

                    <h3 className="has-text-size4 font-nabimpact mt-10 mb-1">
                      Usage
                    </h3>

                    <Highlight className="xml">
                      &lt;Button className="mt-1"&gt;...&lt;/Button&gt; <br />
                      &lt;Select className="pb-3"&gt;...&lt;/Select&gt;
                    </Highlight>
                  </Tab>
                </Tabs>
              </Column>
            </Columns>
          </Container>
        </Section>
      </div>
    );
  }
}

const Data = [
  { props: 'm, margin', desc: 'margin' },
  { props: 'mt, marginTop', desc: 'margin-top' },
  { props: 'mr, marginRight', desc: 'margin-right' },
  { props: 'mb, marginBottom', desc: 'margin-bottom' },
  { props: 'ml, marginLeft', desc: 'margin-left' },
  { props: 'mx', desc: 'margin-left and margin-right' },
  { props: 'my', desc: 'margin-top and margin-bottom' },
  { props: 'p, padding', desc: 'padding' },
  { props: 'pt, paddingTop', desc: 'padding-top' },
  { props: 'pr, paddingRight', desc: 'padding-right' },
  { props: 'pb, paddingBottom', desc: 'padding-bottom' },
  { props: 'pl, paddingLeft', desc: 'padding-left' },
  { props: 'px', desc: 'padding-left and padding-right' },
  { props: 'py', desc: 'padding-top and padding-bottom' }
];

const TRow = ({ row }) => (
  <Row>
    <Cell>{row.props}</Cell>
    <Cell>{row.desc}</Cell>
  </Row>
);

const PropsHeaders = [{ name: 'Props' }, { name: 'Desc' }];
