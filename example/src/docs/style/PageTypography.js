import React, { Component } from 'react'
import PageHeader from '../../components/PageHeader'
import { 
  Column, Columns, Container, Divider, Section
  // Subtitle, Title, Hero, 
} from 'shaper-react'
import './PageTypography.css'
import Highlight from 'react-highlight'

export default class PageTypography extends Component {
  render() {
    return (
      <div id="start">
        <PageHeader category="Style" title="Typography">
          Typography is used to create clear hierarchies, useful organizations, and purposeful alignments that guide users through the product and experience. It is the core structure of any well designed interface.
        </PageHeader>

        <Divider className="my-0" />

        <Section>
          <Container>
            <Columns mobile>
              <Column className="is-12-mobile is-10-desktop is-offset-1-desktop">

                <h3 className="has-text-size3 font-nabimpact">Font</h3>
                <h3 className="has-text-size5">Source Sans Pro
                  <span className="is-pulled-right nab-icon">
                    <a href="https://fonts.google.com/specimen/Source+Sans+Pro" rel="noopener noreferrer" target="_blank">
                      <i className="material-icons">cloud_download</i>
                    </a>
                  </span>
                </h3>

                <Columns>
                  <Column>
                    <Divider className="mt-0 mb-3" />
                    <p><strong>Glyph</strong></p>
                    <h1 className="is-gigantic">Ss</h1>
                  </Column>
                  <Column className="is-8">
                    <Divider className="mt-0 mb-3" />
                    <p><strong>Characters</strong></p>
                    <p className="has-text-size3 characters">
                      {
                        sspChars.map((ssp, sspIndex) => {
                          return (
                            <span className="specimen-characters-char">{ssp}</span>
                          )
                        })
                      }
                    </p>

                    <Divider className="mb-3" />

                    <p><strong>Styles used</strong></p>

                    <p className="style" style={{fontWeight: 300}}>Light</p>
                    <p className="style">Regular</p>
                    <p className="style"><em>Regular Italic</em></p>
                    <p className="style" style={{ fontWeight: 600 }}>Bold</p>

                    <Divider className="my-3" />

                    <p><strong>Example</strong></p>

                    <h1 class="has-text-size3">The spectacle before us was indeed sublime.</h1>
                    <p>Apparently we had reached a great height in the atmosphere, for the sky was a dead black, and the stars had ceased to twinkle. By the same illusion which lifts the horizon of the sea to the level of the spectator on a hillside, the sable cloud beneath was dished out, and the car seemed to float in the middle of an immense dark sphere, whose upper half was strewn with silver. Looking down into the dark gulf below, I could see a ruddy light streaming through a rift in the clouds.</p>

                  </Column>
                </Columns>

                <Divider className="my-10" />

                <h3 className="has-text-size5">Nab Impact
                  <span className="is-pulled-right nab-icon">
                    <a href="/downloads/NabImpact.zip">
                      <i className="material-icons">cloud_download</i>
                    </a>
                  </span>
                </h3>

                <Columns>
                  <Column>
                    <Divider className="mt-0 mb-3" />
                    <p><strong>Glyph</strong></p>
                    <h1 className="is-gigantic font-nabimpact">Ni</h1>
                  </Column>
                  <Column className="is-8">
                    <Divider className="mt-0 mb-3" />
                    <p><strong>Characters</strong></p>
                    <p className="has-text-size3 characters font-nabimpact">
                      {
                        niChars.map((ni, niIndex) => {
                          return (
                            <span className="specimen-characters-char">{ni}</span>
                          )
                        })
                      }
                    </p>
                      
                    <Divider className="mb-3" />

                    <p><strong>Style used</strong></p>
                    <p className="style font-nabimpact">Regular</p>

                    <Divider className="my-3" />

                    <p><strong>Example</strong></p>

                    <h1 className="has-text-size3 font-nabimpact">The spectacle before us was indeed sublime.</h1>
                    <p className="font-nabimpact">Apparently we had reached a great height in the atmosphere, for the sky was a dead black, and the stars had ceased to twinkle. By the same illusion which lifts the horizon of the sea to the level of the spectator on a hillside, the sable cloud beneath was dished out, and the car seemed to float in the middle of an immense dark sphere, whose upper half was strewn with silver. Looking down into the dark gulf below, I could see a ruddy light streaming through a rift in the clouds.</p>

                    <Divider className="mb-3" />

                    <p>To use this font, just add class <code>font-nabimpact</code></p>

                    <Highlight className="xml">
&lt;p class="font-nabimpact"&gt;The spectacle was indeed sublime.&lt;/p&gt;
                    </Highlight>

                  </Column>
                </Columns>

                <Divider className="my-10" />

                <h3 className="has-text-size5">Nab Script
                  <span className="is-pulled-right nab-icon">
                    <a href="/downloads/NabScript.zip">
                      <i className="material-icons">cloud_download</i>
                    </a>
                  </span>
                </h3>

                <Columns>
                  <Column>
                    <Divider className="mt-0 mb-3" />
                    <p><strong>Glyph</strong></p>
                    <h1 className="is-gigantic font-nabscript">Ns</h1>
                  </Column>
                  <Column className="is-8">
                    <Divider className="mt-0 mb-3" />
                    <p><strong>Characters</strong></p>
                    <p className="has-text-size3 characters font-nabscript">
                      {
                        nsChars.map((ns, nsIndex) => {
                          return (
                            <span className="specimen-characters-char">{ns}</span>
                          )
                        })
                      }
                    </p>

                    <Divider className="mb-3" />

                    <p><strong>Style used</strong></p>
                    <p className="style font-nabscript">Regular</p>

                    <Divider className="my-3" />

                    <p><strong>Example</strong></p>

                    <h1 className="has-text-size3 font-nabscript">The spectacle before us was indeed sublime.</h1>
                    <p className="has-text-size5 font-nabscript">Apparently we had reached a great height in the atmosphere, for the sky was a dead black, and the stars had ceased to twinkle. By the same illusion which lifts the horizon of the sea to the level of the spectator on a hillside, the sable cloud beneath was dished out, and the car seemed to float in the middle of an immense dark sphere, whose upper half was strewn with silver. Looking down into the dark gulf below, I could see a ruddy light streaming through a rift in the clouds.</p>

                    <Divider className="mb-3" />

                    <p>To use this font, just add class <code>font-nabscript</code></p>

                    <Highlight className="xml">
                      &lt;p class="font-nabscript"&gt;The spectacle was indeed sublime.&lt;/p&gt;
                    </Highlight>


                  </Column>
                </Columns>

              </Column>
            </Columns>
          </Container>
        </Section>
      </div>
    )
  }
}

const sspChar = "ABCČĆDĐEFGHIJKLMNOPQRSŠTUVWXYZŽabcčćdđefghijklmnopqrsštuvwxyzžАБВГҐДЂЕЁЄЖЗЅИІЇЙЈКЛЉМНЊОПРСТЋУЎФХЦЧЏШЩЪЫЬЭЮЯабвгґдђеёєжзѕиіїйјклљмнњопрстћуўфхцчџшщъыьэюяΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩαβγδεζηθικλμνξοπρστυφχψωάΆέΈέΉίϊΐΊόΌύΰϋΎΫΏĂÂÊÔƠƯăâêôơư1234567890‘?’“!”(%)[#]{@}/&<-+÷×=>®©$€£¥¢:;,.*"
const niChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890‘?’“!”(%)[#]{@}/&<-+÷×=>®©$€£¥¢:;,.*"
const nsChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890‘?’“!”(%)[#]{@}/&<-+÷×=>®©$€£¥¢:;,.*"

const sspChars = sspChar.split('')
const niChars = niChar.split('')
const nsChars = nsChar.split('')