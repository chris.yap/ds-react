import React from 'react';
import PageHeader from '../../components/PageHeader';
import Highlight from 'react-highlight';
import {
  Column,
  Columns,
  Container,
  Divider,
  Section,
  Title
} from 'shaper-react';

const Page = () => {
  return (
    <div id="start">
      <PageHeader category="style" title="Theming">
        Way to theme your application
      </PageHeader>

      <Divider />

      <Section>
        <Container>
          <Columns mobile>
            <Column mobile={12} desktop={10} desktopOffset={1}>
              <p className="mb-6">
                <strong>Shaper</strong> components are built with themeability
                in mind.
              </p>
              <Title size="4">Applying themes</Title>
              <p>
                To apply themes to <strong>Shaper</strong> components, pass a
                theme object as a prop to the App component.
              </p>
              <Highlight className="js mb-4">{Apply}</Highlight>
              <p className="mb-6">
                <strong>Shaper</strong> follows the Theme Specification, which
                allows you to define thematic values in a more portable format.
                This also means that themes created for use with{' '}
                <strong>Shaper</strong> will work in other applications that
                follow the same specification.
              </p>

              <Title size="4">Example</Title>
              <p>
                The following is an example theme, showing some of the design
                constraints that can be defined as scales, including colors,
                typography, layouts, etc.
              </p>
              <Highlight className="js mb-4">{Example}</Highlight>

              <div className="nab-content">
                <Title size="4">Themable objects</Title>
                <ul>
                  <li>breakpoints</li>
                  <li>space</li>
                  <li>font</li>
                  <li>fontSizes</li>
                  <li>fontWeights</li>
                  <li>lineHeights</li>
                  <li>letterSpacings</li>
                  <li>textStyles</li>
                  <li>colors</li>
                  <li>colorStyles</li>
                  <li>radii</li>
                  <li>shadows</li>
                  <li>duration</li>
                  <li>transitionDelays</li>
                </ul>
              </div>
            </Column>
          </Columns>
        </Container>
      </Section>
    </div>
  );
};

export default Page;

const Example = `
// example theme.js
export default {
  breakpoints: ['40em', '52em', '64em'],
  fontSizes: [
    12, 14, 16, 20, 24, 32, 48, 64
  ],
  colors: {
    blue: '#07c',
    lightgray: '#f6f6ff'
  },
  space: [
    0, 4, 8, 16, 32, 64, 128, 256
  ],
  fonts: {
    body: 'system-ui, sans-serif',
    heading: 'inherit',
    monospace: 'Menlo, monospace',
  },
  fontWeights: {
    body: 400,
    heading: 700,
    bold: 700,
  },
  lineHeights: {
    body: 1.5,
    heading: 1.25,
  },
  shadows: {
    small: '0 0 4px rgba(0, 0, 0, .125)',
    large: '0 0 24px rgba(0, 0, 0, .125)'
  }
}

`;

const Apply = `
import React from 'react';
import { App } from 'shaper-react';
// import your theme file
import theme from './theme';

export default props =>
  <App newTheme={theme}>
    {props.children}
  </App>

`;
