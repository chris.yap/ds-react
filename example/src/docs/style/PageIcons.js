import React, { Component } from 'react';
import PageHeader from '../../components/PageHeader';
import IconPaths from '../../constants/shaper-icons';
import {
  Card,
  CardHeader,
  CardContent,
  Column,
  Columns,
  Container,
  Divider,
  Icon,
  Section,
  Tag
} from 'shaper-react';

export default class PageIcons extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterIcons: [],
      type: ''
    };
  }
  componentDidMount = () => {
    this.setState({ filterIcons: IconPaths.icons });
  };
  filterList(event) {
    let filteredResult = [];
    if (event.target.value.length > 0) {
      this.setState({ filterIcons: [] });
      for (let icon of IconPaths.icons) {
        if (icon.properties.name) {
          if (
            icon.properties.name
              .toLowerCase()
              .indexOf(event.target.value.toLowerCase()) >= 0
          ) {
            filteredResult.push(icon);
          }
        }
      }
      this.setState({ filterIcons: filteredResult });
    } else {
      this.setState({ filterIcons: IconPaths.icons });
    }
  }
  render() {
    const { filterIcons } = this.state;
    return (
      <div id="start">
        <PageHeader category="style" title="Icons">
          Shaper comes with its own customised icon font.
        </PageHeader>

        <Divider className="my-0" />

        <Section>
          <Container>
            <Columns mobile>
              <Column className="is-12-mobile is-10-desktop is-offset-1-desktop">
                <Columns>
                  <Column className="is-12-mobile is-8-tablet is-offset-2-tablet is-6-desktop is-offset-3-desktop has-text-centered">
                    <input
                      type="text"
                      className="nab-input mb-5 is-large"
                      placeholder="Search icon"
                      onChange={this.filterList.bind(this)}
                    />
                  </Column>
                </Columns>

                <Columns>
                  <Column className="pt-0">
                    <p className="has-text-centered mb-0">
                      <Tag large>{filterIcons.length} icons</Tag>
                    </p>
                  </Column>
                </Columns>

                <Columns multiline>
                  {filterIcons.length > 0 && (
                    <>
                      {filterIcons
                        .sort((a, b) =>
                          a.icon.tags[0].toLowerCase() <
                          b.icon.tags[0].toLowerCase()
                            ? -1
                            : 1
                        )
                        .map((icon, index) => {
                          return (
                            <Column
                              key={index}
                              className={`is-6-mobile is-3-tablet has-text-centered`}>
                              <Card>
                                <CardHeader className="py-10">
                                  <Icon secondary>
                                    {icon.icon && icon.icon.tags[0]}
                                  </Icon>
                                </CardHeader>
                                <CardContent>
                                  {icon &&
                                    icon.properties &&
                                    icon.properties.name && (
                                      <p className="has-text-size-1 mb-0 is-lowercase">
                                        {icon &&
                                          icon.properties &&
                                          icon.properties.name}
                                      </p>
                                    )}
                                  {!icon.properties.name && (
                                    <p className="has-text-size-1 mb-0 is-lowercase">
                                      {icon.properties.name}
                                    </p>
                                  )}
                                </CardContent>
                              </Card>
                            </Column>
                          );
                        })}
                    </>
                  )}
                </Columns>
              </Column>
            </Columns>
          </Container>
        </Section>
      </div>
    );
  }
}
