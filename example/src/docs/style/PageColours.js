import React, { Component } from 'react'
import PageHeader from '../../components/PageHeader'
import { 
  Card, CardContent, Column, Columns, Container, Divider, Section
  // Subtitle, Title, Hero, 
} from 'shaper-react'
import Highlight from 'react-highlight'

function Steps (shade, props) {
  let cards = []
  for (let i = 0; i < 5; i++) {
    var lighten = 5 - i
    var darken = i + 1
    lighten = shade + '-' + lighten
    darken = shade + '-' + darken
    if (shade === 'lighten') {
      cards.push (
        <Card key={i} flat className={'has-bg-' + props.name + ' ' + lighten + ' has-smartColor'}>
          <CardContent className="pa-4">
            <p className="ma-0">has-bg-{ props.name } { lighten }</p>
          </CardContent>
        </Card>
      )
    } else {
      if (props.name !== 'black') {
        cards.push(
          <Card key={i} flat className={'has-bg-' + props.name + ' ' + darken + ' has-smartColor'}>
            <CardContent className="pa-4">
              <p className="ma-0">has-bg-{props.name} {darken}</p>
            </CardContent>
          </Card>
        )
      }
    }
  }
  return cards
}

export default class PageColours extends Component {
  constructor (props) {
    super (props)
    this.state = {
      level: 5
    }
  }
  render() {
    return (
      <div id="start">
        <PageHeader category="Style" title="Colours">
          Out of the box you get access to all colors in our SHAPE spec. These values can be used within your style sheets, your component files and on actual components via the color class system.
        </PageHeader>

        <Divider className="my-0" />

        <Section>
          <Container>
            <Columns mobile>
              <Column className="is-12-mobile is-10-desktop is-offset-1-desktop">

                <h3 className="has-text-size3 font-nabimpact">Background colours</h3>
                <p>Each color from the spec gets converted to a background variant for styling within your application through a class, eg. <code className="xml">&lt;div className="has-bg-red darken-3"&gt;&lt;/div&gt;</code>. Below is the list of our themed color palette.</p>

                <Divider className="my-10" />

                <h3 className="has-text-size4 font-nabimpact">Primary colours</h3>

                <Columns multiline mobile>
                  {
                    PrimaryColours.map((priCol, pIndex) => {
                      return (
                        <Column key={pIndex} className="is-6-mobile is-6-tablet is-4-desktop">
                          <Card className={'has-bg-' + priCol.name}>
                            <CardContent>
                              <h3 className="nab-title ma-0 is-5 has-text-white">{ priCol.name }</h3>
                              <p className="ma-0 has-text-white">has-bg-{priCol.name}</p>
                            </CardContent>
                          </Card>

                          { Steps('lighten', priCol) }

                          { Steps('darken', priCol) }

                        </Column>
                      )
                    })
                  }
                </Columns>

                <h3 className="has-text-size4 font-nabimpact">Secondary colours</h3>

                <Columns multiline mobile>
                  {
                    SecondaryColours.map((secCol, sIndex) => {
                      return (
                        <Column key={sIndex} className="is-6-mobile is-6-tablet is-4-desktop">
                          <Card className={'has-bg-' + secCol.name}>
                            <CardContent>
                              <h3 className="nab-title ma-0 is-5 has-text-white">{secCol.name}</h3>
                              <p className="ma-0 has-text-white">has-bg-{secCol.name}</p>
                            </CardContent>
                          </Card>

                          {Steps('lighten', secCol)}

                          {Steps('darken', secCol)}

                        </Column>
                      )
                    })
                  }
                </Columns>

                <h3 className="has-text-size4 font-nabimpact">Contextual colours</h3>

                <Columns multiline mobile>
                  {
                    ContextualColours.map((conCol, cIndex) => {
                      return (
                        <Column key={cIndex} className="is-6-mobile is-6-tablet is-4-desktop">
                          <Card className={'has-bg-' + conCol.name}>
                            <CardContent>
                              <h3 className="nab-title ma-0 is-5 has-text-white">{conCol.name}</h3>
                              <p className="ma-0 has-text-white">has-bg-{conCol.name}</p>
                            </CardContent>
                          </Card>

                          {Steps('lighten', conCol)}

                          {Steps('darken', conCol)}

                        </Column>
                      )
                    })
                  }
                </Columns>

                <Divider className="my-10" />

                <h3 className="has-text-size3 font-nabimpact">Text colours</h3>

                <p>To change the colour of text using our colour palette, use similar classes like the following example.</p>

                <Highlight className='html'>
&lt;h3 className="has-text-red"&gt;Red text&lt;/h3&gt;<br />
&lt;p className="has-text-red text--darken-2"&gt;Red text - darken-2 variant&lt;/p&gt;
                </Highlight>

              </Column>
            </Columns>
          </Container>
        </Section>
      </div>
    )
  }
}

const PrimaryColours = [
  { name: 'red' },
  { name: 'blue-grey' },
  { name: 'black' }
]

const SecondaryColours = [
  { name: 'sea' },
  { name: 'blueberry' },
  { name: 'sky' },
  { name: 'lime' },
  { name: 'tangerine' }
]

const ContextualColours = [
  { name: 'success' },
  { name: 'info' },
  { name: 'warning' },
  { name: 'danger' }
]