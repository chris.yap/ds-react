import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import {
  // Start,
  // PageDesigners,
  PageWhatsNew,
  PageAlerts,
  PageBox,
  PageBreadcrumbs,
  PageBreakpoints,
  PageBrowser,
  PageButtons,
  PageCards,
  PageCarousels,
  PageCheckboxes,
  PageColours,
  PageCollapse,
  PageColumns,
  PageContainers,
  PageDatePickers,
  PageDrawers,
  PageDropdowns,
  PageDevelopers,
  PageDividers,
  PageElevation,
  PageFooters,
  // PageGrid,
  PageHelpers,
  PageHeroes,
  PageIcon,
  PageIcons,
  PageLists,
  PageMessages,
  PageModals,
  PageOverlay,
  PagePaginations,
  PageProgress,
  PageResponsiveHelpers,
  PageRadios,
  PageSections,
  PageSelects,
  PageSpacing,
  PageTheme,
  PageTypography,
  PageTiles,
  PageTextfields,
  PageTables,
  PageTabs,
  PageTags,
  PageTitles,
  PageToolbars,
  PageTooltips,
  PageTypoHelpers,
  PageTest
} from './index';

export default class Routes extends Component {
  render() {
    const { toggleDrawer } = this.props;
    return (
      <React.Fragment>
        <Route exact path="/" component={PageDevelopers} />
        <Route path="/whats-new" component={PageWhatsNew} />

        <Route exact path="/style/colours" component={PageColours} />
        <Route exact path="/style/icons" component={PageIcons} />
        <Route exact path="/style/theme" component={PageTheme} />
        <Route exact path="/style/typography" component={PageTypography} />

        <Route exact path="/layout/breakpoints" component={PageBreakpoints} />
        <Route exact path="/layout/containers" component={PageContainers} />
        <Route exact path="/layout/columns" component={PageColumns} />
        <Route exact path="/layout/elevation" component={PageElevation} />
        <Route exact path="/layout/footer" component={PageFooters} />
        {/* <Route exact path="/layout/grid" component={PageGrid} /> */}
        <Route exact path="/layout/heroes" component={PageHeroes} />
        <Route exact path="/layout/sections" component={PageSections} />
        <Route exact path="/layout/spacing" component={PageSpacing} />
        <Route exact path="/layout/tiles" component={PageTiles} />

        <Route exact path="/forms/checkboxes" component={PageCheckboxes} />
        <Route exact path="/forms/datepickers" component={PageDatePickers} />
        <Route exact path="/forms/radios" component={PageRadios} />
        <Route exact path="/forms/selects" component={PageSelects} />
        <Route exact path="/forms/text-fields" component={PageTextfields} />

        <Route exact path="/elements/boxes" component={PageBox} />
        <Route exact path="/elements/buttons" component={PageButtons} />
        <Route exact path="/elements/dividers" component={PageDividers} />
        <Route exact path="/elements/icons" component={PageIcon} />
        <Route exact path="/elements/overlay" component={PageOverlay} />
        <Route exact path="/elements/progress" component={PageProgress} />
        <Route exact path="/elements/tables" component={PageTables} />
        <Route exact path="/elements/tags" component={PageTags} />
        <Route exact path="/elements/titles" component={PageTitles} />

        <Route exact path="/components/alerts" component={PageAlerts} />
        <Route
          exact
          path="/components/breadcrumbs"
          component={PageBreadcrumbs}
        />
        <Route exact path="/components/cards" component={PageCards} />
        <Route exact path="/components/carousels" component={PageCarousels} />
        <Route exact path="/components/collapse" component={PageCollapse} />
        <Route
          exact
          path="/components/drawers"
          component={PageDrawers}
          toggleDrawer={toggleDrawer}
        />
        <Route exact path="/components/dropdowns" component={PageDropdowns} />
        <Route exact path="/components/lists" component={PageLists} />
        <Route exact path="/components/messages" component={PageMessages} />
        <Route exact path="/components/modals" component={PageModals} />
        <Route
          exact
          path="/components/paginations"
          component={PagePaginations}
        />
        <Route exact path="/components/tabs" component={PageTabs} />
        <Route exact path="/components/toolbars" component={PageToolbars} />
        <Route exact path="/components/tooltips" component={PageTooltips} />

        <Route exact path="/modifiers" component={PageHelpers} />
        <Route
          exact
          path="/modifiers/responsive"
          component={PageResponsiveHelpers}
        />
        <Route exact path="/modifiers/typography" component={PageTypoHelpers} />

        <Route
          exact
          path="/utilities/breakpoints"
          component={PageBreakpoints}
        />
        <Route exact path="/utilities/browser" component={PageBrowser} />
        <Route exact path="/test" component={PageTest} />
      </React.Fragment>
    );
  }
}
