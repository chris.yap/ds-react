import React, { Component } from 'react';
import {
  App as Theme,
  Breakpoints,
  Button,
  Div,
  Divider,
  Drawer,
  Icon,
  Title,
  Toolbar
} from 'shaper-react';
import ScrollToTop from 'react-scroll-up';
import Routes from './docs/Routes';
import ShaperLogo from './components/ShaperLogo';
import Navs from './components/Navs';
import { HashRouter } from 'react-router-dom';

import 'shaper-css/dist/css/shaper.css';
import './css/codepen-embed.css';

const newTheme = {
  colors: {
    // primary: 'purple',
    // secondary: '#1F6764',
    // info: 'cyan',
    // success: '#62997A',
    // warning: '#E7C049',
    // danger: '#C94D65',
    // text: 'pink'
  }
};

class App extends Component {
  state = {
    navIsOpen: true,
    drawer: {
      placement: 'left',
      variant: 'persistent'
    }
  };

  componentDidMount() {
    document.body.classList.add('docs');
  }

  toggleNav = event => {
    this.setState({ navIsOpen: !this.state.navIsOpen });
  };

  render() {
    const { activeBreakpoints } = this.props;
    const { drawer, navIsOpen } = this.state;
    // console.log(newTheme);
    return (
      <HashRouter>
        <Theme newTheme={newTheme}>
          <Drawer
            hamburger
            variant={drawer.variant}
            // left={drawer.placement === 'left'}
            right={drawer.placement === 'right'}
            isOpened={navIsOpen}
            className={'has-bg-black'}
            width={activeBreakpoints.below.sm ? '100%' : 320}
            toggleDrawer={() => this.toggleNav()}>
            <Toolbar
              fluid
              className="px-3"
              noburger
              brand={
                <Title
                  size="3"
                  className="is-flex is-align-items-center has-text-white font-nabimpact mb-0">
                  <span className="prepend-icon mr-2">
                    <ShaperLogo dark />
                  </span>
                  Shaper
                </Title>
              }></Toolbar>

            <Divider dark className="my-0" />

            <Navs />
          </Drawer>

          <Routes />

          <ScrollToTop showUnder={160}>
            <Div position="fixed" bottom="0" right="0">
              <Button square large inverted borderRadius="0">
                <Icon material>arrow_upward</Icon>
              </Button>
            </Div>
          </ScrollToTop>
        </Theme>
      </HashRouter>
    );
  }
}

export default Breakpoints(App);
